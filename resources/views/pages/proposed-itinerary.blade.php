@extends( 'layouts.search' )
@section( 'custom-css' )
<style type="text/css">
	.wrapper {
		background-image: url( {{ url( 'images/bg2.jpg' ) }} ) !important;
	}
	.pi-padding{
		padding:60px;
	}
	.display-inline{
		display:inline-block;
	}	
	.pi-data-box-padding{
		padding-right:15px;
	}
	.proposed-city-image {
		height: 250px;
		width: 100%;
	}
	a{
		color:#fff;	
	}
	a:hover{
		color:black;
		text-decoration: none;
	}
	.thumb{
		background-color: transparent!important;
	}
	#send-email:disabled{
		border: 1px solid #E0E0E0;
	  	background-color: #E0E0E0;
	    color: #212121;
	    outline: none;
	}
	#discard:disabled{
		border: 1px solid #E0E0E0;
	  	background-color: #E0E0E0;
	    color: #212121;
	    outline: none;
	}
</style>
@stop

@section('content')
<div class="tabs-content-wrapper2">
	<div class="pi-padding iternary-box">
		<div class="text-right">
			<label class="btn btn-primary full-width no-radius default-border m-r-10" data-toggle="modal" data-target="#emailFriendModal"> EMAIL A FRIEND</label>
			<a href="{{URL('proposed-itinerary-pdf')}}" target="_blank"><label class="btn btn-primary full-width no-radius default-border" > VIEW PDF </label></a>
		</div>
		<div class="row" style="margin-top: 10%">
			<h4 class="bold-txt text-center">PROPOSED ITINERARY</h4>
			<h4 class="blue-txt bold-txt text-center">{{ $search['total_number_of_days'] .' '. $search['currency'] .' '. $search['cost_per_person'] }} (Per Person)</h4>
		</div>
		
		@foreach ( $search['itinerary'] as $key => $leg )
			<?php 
				$last = count($search['itinerary']) - 1;
				$itineraryIndex = $key == $last ? $key : $key + 1;
				$departTimezone = get_timezone_abbreviation($leg['city']['timezone']['name']);
				$arriveTimezone = get_timezone_abbreviation($search['itinerary'][$itineraryIndex]['city']['timezone']['name']);
			?>
			<div class="row" style="margin-top:35px;">
				<div class="col-md-8 pi-data-box-padding">
					
					<h4 class="bold-txt">{{ $leg['city']['country']['name'] }}, {{ $leg['city']['name'] }}</h4>
					<p>
						<?php
							$accommodation_string = '';

							if(empty($leg['hotel'])){
								$accommodation_string = 'Own Arrangement';
							}
							if(!empty($leg['hotel']['room_name'])){

								$accommodation_string = $leg['hotel']['name'].'('.$leg['hotel']['room_name'].')';
							}
							if(empty($leg['hotel']['room_name']) && isset($leg['hotel'])){


								if( !empty( $leg['hotel']['price'] ) ){ //added to fix undefined index price EMAIL of ant april 05, 2017

									$price = array_first($leg['hotel']['price']);
									$room_name = $price['room_type']['name'];
									$accommodation_string = $leg['hotel']['name'].'('.$room_name.')';
								}
							}
						?>
						<div class="row">
							<div class="col-md-3"><strong>Accommodation:</strong></div>
							<div class="col-md-9">{{ $leg['hotel']['name'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-3"><strong>Check-in:</strong></div>
							<div class="col-md-9">{{ date( 'jS, F Y', strtotime( $leg['city']['date_from'] ) ) }}</div>
						</div>
						<div class="row">
							<div class="col-md-3"><strong>Check-out:</strong></div>
							<div class="col-md-9">{{ date( 'jS, F Y', strtotime( $leg['city']['date_to'] ) ) }}</div>
						</div>
						<div class="row">
							<div class="col-md-12"><strong>Description:</strong></div>
							<div class="col-md-12">{!! ( isset($leg['city']['description']) ) ? $leg['city']['description'] : '' !!}</div>
						</div>
					</p>

					<p>		
						@if ( $leg['activities'] )
							@foreach ( $leg['activities'] as $activity )
							
								<div class="row">
									<div class="col-md-2"><strong>Activity:</strong></div>
									<div class="col-md-10">{{ $activity['name'] }} </div>
								</div>
								<div class="row">
									<div class="col-md-2"><strong>Date:</strong></div>
									<div class="col-md-10">{{ date( 'jS, F Y', strtotime( $activity['date_selected'] ) ) }}</div>
								</div>
								<div class="row">
									<div class="col-md-12"><strong>Description:</strong></div>
									<div class="col-md-12">{!! isset($activity['description']) ? $activity['description'] : '' !!}</div>
								</div>
								<br /> <br />

							@endforeach
						@else
							<div class="row">
								<div class="col-md-2"><strong>Activity:</strong></div>
								<div class="col-md-10">None</div>
							</div>
						@endif
					</p>
					
						@if ( $leg['transport'] )
							<?php 
								$the_etd         = new DateTime( $leg['city']['date_to'] );
								$formatted_etd   = $the_etd->format('jS, F Y');
								$next_leg_date_from = isset($search['itinerary'][$key+1]) ? date('jS, F Y', strtotime($search['itinerary'][$key+1]['city']['date_from'] )) : ''; 

								$duration = $leg['transport']['duration'] ? $leg['transport']['duration'] : '' ;
								$departure = date( 'H:i ', strtotime($leg['transport']['etd']) );
										
								$date_to = $leg['city']['date_to'];
								$exact_arrival_date_time = $date_to.' '.date('H:i',strtotime($departure.' '.$duration));
							?>
							@if ( !isset($leg['transport']['provider']) )
								<div class="row">
									<div class="col-md-2"><strong>Transport: </strong></div>
									<div class="col-md-10">{{ $leg['transport']['operator']['name'] . ' (' . $leg['transport']['transporttype']['name'] . ')' }}</div>
								</div>
								<div class="row">
									<div class="col-md-2"><strong>Depart: </strong></div>
									<div class="col-md-10">{{ date( 'H:i' ,strtotime($leg['transport']['etd']) ) . ', '.$formatted_etd.' ( '. $departTimezone .' ) - '.get_city_by_id( $leg['transport']['from_city_id'] )['name'] }}</div>
								</div>
								<div class="row">
									<div class="col-md-2"><strong>Arrive: </strong></div>
									<div class="col-md-10">{{ date('H:i', strtotime($exact_arrival_date_time) ).', '.$next_leg_date_from.' ( '.$arriveTimezone.' ) - ' .get_city_by_id( $leg['transport']['to_city_id'] )['name']  }}</div>
								</div>
							@else
								<?php 
								switch( $leg['transport']['provider'] ): 
									case 'eroam': ?>
										<div class="row">
											<div class="col-md-2"><strong>Transport: </strong></div>
											<div class="col-md-10">{{ $leg['transport']['operator']['name'] . ' (' . $leg['transport']['transporttype']['name'] . ')' }}</div>
										</div>
										<div class="row">
											<div class="col-md-2"><strong>Depart: </strong></div>
											<div class="col-md-10">{{ date( 'H:i',strtotime($leg['transport']['etd']) ). ', '.$formatted_etd.' ( '.$departTimezone .' ) - '.get_city_by_id( $leg['transport']['from_city_id'] )['name'] }}</div>
										</div>
										<div class="row">
											<div class="col-md-2"><strong>Arrive: </strong></div>
											<div class="col-md-10">{{ date('H:i', strtotime($exact_arrival_date_time) ).', '.$next_leg_date_from.' ( '. $arriveTimezone .' ) - ' .get_city_by_id( $leg['transport']['to_city_id'] )['name']  }}</div>
										</div>
								<?php break; ?>
								<?php case 'mystifly': ?>
									<?php
										$departure_time2 = (new DateTime($leg['transport']['etd']))->format('A');
										$arrival_time2   = (new DateTime($leg['transport']['eta']))->format('A');
										
									?>
									<div class="row">
										<div class="col-md-2"><strong>Transport: </strong></div>
										<div class="col-md-10">{{ $leg['transport']['operating_airline'] . ', Flight # ' . $leg['transport']['flight_number'] }}</div>
									</div>
									<div class="row">
										<?php $depart = new DateTime(str_replace('T',' ',$leg['transport']['etd'])); ?>
										<div class="col-md-2"><strong>Depart: </strong></div>
										<div class="col-md-10">{{ $depart->format('H:i').', '.$formatted_etd.' ( '.$departTimezone.' ) - '.$leg['transport']['departure_data'] }}</div>
									</div>
									<div class="row">
										<?php $arrive = new DateTime(str_replace('T',' ',$leg['transport']['eta'])); ?>
										<div class="col-md-2"><strong>Arrive: </strong></div>

										<div class="col-md-10">{{ $arrive->format('H:i').' '.$next_leg_date_from.' ( '.$arriveTimezone.' ) - '.$leg['transport']['arrival_data'] }}</div>
									</div>
								<?php break; ?>
								<?php endswitch; ?>

							@endif
						@elseif ( last( $search['itinerary'] ) != $leg )
							<span class="bold-txt">Transport: </span><span style="padding-left:15px;">{{ $leg['city']['name'] . ' to ' . $search['itinerary'][$key + 1]['city']['name'] }} (Own Arrangement)</span>
						@endif
					</p>
				</div>
				<div class="col-md-4">

					<?php
					  if(count($leg['city']['image']) != 0){
					$img = $leg['city']['image'][0]['small'] ?  config( 'env.CMS_URL' ) . $leg['city']['image'][0]['small'] : asset('images/no-image.png');
					}else{
		                 $img =  asset('images/no-image.png');
					}
					?>
					<div class="thumb" style="background-image: url( {{ $img }}); width: 100%; height: 200px;background-size: cover;">
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>


	<div class="modal fade" id="emailFriendModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm" role="document" style="width: 450px; margin: 100px auto;">
			<div class="modal-content">
				<div class="modal-header text-center">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<div class="">
						<h4 class="modal-title" id="myModalLabel">Email a Friend</h4>
					</div>
				</div>
				<div class="modal-body">
					<div class="">
						<div class="input-field login-group">

							<p class="success-box" style="display: none; padding-bottom: 5px;">Itinerary sent successfully.</p>
						</div>
							<form id="myForm" class="form-horizontal" method="post">

								<div class="input-field login-group">
									<input id="name" type="text" name="name">
									<label for="name">Full Name</label>
								</div>
								<div class="input-field login-group">
									<input id="emails" type="text" name="emails">
									<label for="emails">Email Address</label>
								</div>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="row m-t-50">
									<div class="col-sm-12 text-center">
										<button type="submit" id="send-email" class="btn btn-primary default-button full-width no-radius default-border send-email">Send</button>
										<button type="button" id="discard" class="btn btn-primary default-button full-width no-radius default-border discard" data-dismiss="modal">Discard</button>
									</div>
								</div>
							</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section( 'custom-js' )
<script>
	$('body').on('click', '.btn-email-friend', function(){
		$('.pax-modal').modal();
	});

	jQuery.validator.addMethod("lettersonly", function(value, element) {
		  return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
		}, "Letters only please."); 
	$("#myForm").validate({
        rules: {
            emails: {
                required: true,
                email:true
            },
            name: {
                required: true,
                lettersonly: true
            }
        },
        messages: {

            email: {
                required: "Please enter Email."
            }

        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
        	emailFriend();
        }
    });

	function emailFriend(){
		if($('#emails').val() && $('#name').val()){
			var email = $('#emails').val();
			var name = $('#name').val();
			
			if (!(isValidEmailAddress(email))) {
				return false;
			}
		
			$.ajax({
				url: 'send/itinerary',
				type: 'GET',
				data: {name:name,emails: email},
				success:function(response){
					console.log(response);

					$( "#name" ).val("");
					$( "#emails" ).val("");
                    $( ".success-box" ).show();
					$( ".send-email" ).text('Send');
					$( "#name" ).focus();
					$(".send-email").prop("disabled",false);
					$( ".discard" ).prop("disabled",false);

				
				}, beforeSend: function(){
			        	
					$( ".send-email" ).text('Sending ');
					$( ".send-email" ).append( '<i class="fa fa-refresh fa-spin center" aria-hidden="true"></i>' );
					$( ".send-email" ).prop("disabled",true);
					$( ".discard" ).prop("disabled",true);
			    },
			    error: function (jqXHR, exception) {
		            $('#error-sending').show();
		        },
			});
		}
	}

	function isValidEmailAddress(email) {
 	  	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			return regex.test(email);
	}		
</script>
@stop



@push('script')
<script type="text/javascript" src="{{ url('js/common.js')}}"></script>
@endpush
