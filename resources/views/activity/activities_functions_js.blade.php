
<script>
    /*
	| Set all required variable & Moved some variables
	*/
    var eroamResponse_length = 0;
    var index, cityId, dateFrom, selectedActivities, destinationId, totalActivityCount = 0;

    var search_session = JSON.parse($('#search-session').val());
    var dates = ['{!! session()->has('dates_available_'.$city_id) ? join("', '", session()->get('dates_available_'.$city_id) ) : '0000/00/00' !!}'];
    var cityName = "{{ $city->name.', '.$city->country_name }}";
    var cityId = {{$city_id}};
    var dateFrom = formatDate("{{$date_from_search}}");
    var dateTo = formatDate("{{$date_from_search}}");
    var postData;
    var destinationId = {{$destinationId}};
    var selected_dates = {{!!join_array_key_value($selected_dates) !!}};
    var selected_date_req = "{{$selected_date_req}}";
    var maxPrice = 0;
    //var travellers = parseFloat(search_session.travellers);
    var travel_pref = [{{join(', ', $activity_pref)}}];
    var available_to_book = {{$available_to_book ? 1 : 0 }};
    var departureDate = "{{ $departure_date }}";
    var cityData = JSON.parse($('.the-city-id').val());
    var leg = {{$leg}};
    var all_viator_category  = <?php echo json_encode($viatorCategory); ?>;   
    var hotel_pickup = '';
    
    $(document).on('change','.location-select', function(){
        var val = $(this).val();
        if(val == 1)
        {
            hotel_pickup = "pickup-"+$(this).data('tourcode');
            $(".choose_location_"+$(this).data('tourcode')).show();
        }
        else
        {
            $(".choose_location_"+$(this).data('tourcode')).hide();
            $("#notListed_"+$(this).data('tourcode')).hide();
            $(".manual-pickup-"+$(this).data('tourcode')).val('');
        }
    });

    $(document).on('change','.pickup-hotel', function(){
        var val = $(this).val();   
        if(val == 'notListed')
        {
            $("#notListed_"+$(this).data('tourcode')).show();            
        }
        else
        {
            $("#notListed_"+$(this).data('tourcode')).hide();
        }
    });    
   

    /*$(document).ajaxStop(function(){
        $(".loader").hide();
    });*/

    $(document).ready(function() {
        selectedActivities = $('#selected-activities').val() != 0 ? JSON.parse($('#selected-activities').val()) : 0;         
        
        buildActivities();
        $('.sort-asc-desc').click(function() {
            $('.sort-asc-desc').find('i').removeClass('white black-background');
            $(this).find('i').toggleClass('white black-background');
            var asc_desc = $(this).find('i').attr('data-sort');
            var asc = asc_desc == 'asc' ? true : false;
            activitySort(asc);
        });

        /**************************** View more info btn function ********************/
        $('body').on('click', '.view-more-btn', showActivityDetails);
        
        $('body').on('click', '.act-btn-less', function() {
            var provider = $(this).data('provider');
            var code = $(this).data('code');
            $('#view-more-details').html('').hide();
            $('#activities-container').show();
            var activity = $('#activities-container').find('.view-more-btn[data-provider="' + provider + '"][data-code="' + code + '"]').offset().top;
            $('.content-body').scrollTop(activity - 250);
        });
        /**************************** END view more info btn function ********************/
        $('.itinerary_page').slimScroll({
            height: '80%',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });
       /* $('.grid-activity-list').slimScroll({
            height: '100%',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });*/

    }); // END OF DOCUMENT READY METHOD
    

    $('#listview-tab').click(function(e) {
        $(this).addClass('active');
        $(".activity-list-selected").show();
        $('#gridview-tab').removeClass('active');
        e.preventDefault();
        $('#listview').show();        
        $('#gridview').hide();
        
        $('#listview').addClass('show');
        $('#listview').addClass('active');

        $('#gridview').removeClass('show');
        $('#gridview').removeClass('active');
        $(".grid-activity-list-selected").hide();
    });

    $('#gridview-tab').click(function(e) {
        $(this).addClass('active');
        $(".activity-list-selected").hide();
        $('#listview-tab').removeClass('active');
        e.preventDefault();
        $('#gridview').show();
        $('#listview').hide();
        $('#gridview').addClass('show');
        $('#gridview').addClass('active');

        $('#listview').removeClass('show');
        $('#listview').removeClass('active');
        $(".grid-activity-list-selected").show();
    });

    /*function heightRemove(id)
    {
        $(id).height('0');
        if(id == "#grid-activity-list")
        {            
            $(id).height('100%');
            $("#gridview").hide();
            $("#listview").show();
        }
        else
        {
            $("#gridview").show();
            $("#listview").hide();
        }
    }*/
    $('input.datetimepick').css({
        'display': 'none'
    });

    function hide_input() {
        //$('input.datetimepick').css({'display':'none'});
    }



    function showActivityDetails(event) {        
        event.preventDefault();
        var provider = $(this).data('provider');
        var code = $(this).data('code');
        var select = $(this).data('select');
        var cityid = $(this).data('cityid');
        var country_city_name = $(this).data('country-city-name');
        var date   = $(this).data('date');
        var act_name   = $(this).data('act-name');
        
        eroam.ajax('get', 'activity/view-more', {
            provider: provider,
            code: code,
            select: select,
            cityid: cityid,
            leg: leg,
            country_city_name: country_city_name,
            date : date,
            activity_name: act_name,
        }, function(response) {
            if(response.error)
            {
                bootbox.alert(response.act_name);            
            }
            else
            {
                $(".all-activity-list").hide();
                
                
                $('#view-more-details').html(response).show();
    			
    			$('#view-more-details').slimScroll({
    				height: '700px',
    				color: '#212121',
    				opacity: '0.7',
    				size: '5px',
    				allowPageScroll: true
    			});
    			
                $(function() {
                    calculateHeight('view');
                    
    				// Setup the carousels. Adjust the options for both carousels here.
    				
    				var jcarousel = $('.jcarousel'); 
    				
    				jcarousel
    	            .on('jcarousel:reload jcarousel:create', function () {
    	                var carousel = $(this),
    	                width = carousel.innerWidth();

    	                if (width >= 1280) {
    	                    width = width / 5;
    	                } else if (width >= 992) {
    	                    width = width / 4;
    	                }
    	                else if (width >= 768) {
    	                    width = width / 4
    	                }
    	                else if (width >= 640) {
    	                    width = width / 4;
    	                } else {
    	                    width = width / 3;
    	                }

    	                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
    	            })
    	            .jcarousel({
    	                wrap: 'circular'
    	            });
    								
    				$('.jcarousel-control-prev').jcarouselControl({
    					target: '-=1'
    				});

    				$('.jcarousel-control-next').jcarouselControl({
    					target: '+=1'
    				});
    				
    				$('.jcarousel-pagination')
    					.on('jcarouselpagination:active', 'a', function() {
    						$(this).addClass('active');
    					})
    					.on('jcarouselpagination:inactive', 'a', function() {
    						$(this).removeClass('active');
    					})
    					.on('click', function(e) {
    						e.preventDefault();
    					})
    					.jcarouselPagination({
    						perPage: 1,
    						item: function(page) {
    							return '<a href="#' + page + '">' + page + '</a>';
    						}
    					});

                });
            }
        }, function() {
            //$('#activities-container').hide();
            $(".loader").show();
            //$('#view-more-loader').show();
        }, function() {
            $(".loader").hide();
            //$('#view-more-loader').hide();
        });
    }

    /**************************** Start Booking and cancel btn function ********************/

    $('body').on('change', '.weight_select', function(e) {
        var value = $(this).val();
        var tourGrade =  $(this).attr('data-tourgrade-title'); 
        
        //$( "body" ).data('tourgrade-title').val(value);
        //$(".weight_select").val(value);
        $(".weight_select").each(function(e) {           
            var dataTourGrade =  $(this).attr('data-tourgrade-title');
            if(dataTourGrade == tourGrade)
            {
                $(this).val(value);
            }
        });
    });

    
    $('body').on('click', '.act-update-btn', function(e) {
        
        e.preventDefault();
        var data = $(this).data();
        var dataId = $(this).attr('data-button-id');
        var book = $(this).text();
        var viewData = $(this).attr('data-view');

        var form = $( "#form-"+data.tourCode );
        var validateForm = form.valid(); 
        var formdata = $(form).serialize();  
        var tourCode = data.tourCode;
        var weightValidate = data.weightValidate;  
        var countAdult = $('.pass-fnm-aAdults-'+tourCode).length;
        var countChild = $('.pass-fnm-aChilds-'+tourCode).length;
        var countInfant = $('.pass-fnm-aInfants-'+tourCode).length;

        var countwAdult = $('.weight-aAdults-'+tourCode).length;
        var countwChild = $('.weight-aChilds-'+tourCode).length;
        var countwInfant = $('.weight-Infants-'+tourCode).length;
        var search_session = JSON.parse( $('#search-session').val() );
        var leg = $(this).attr('data-leg');
        var activity_data = search_session.itinerary[leg].activities[data.akey];
        
        var paxInfo = [];

        var activityBookingQuestion = [];
        var finalArray = [];            
        
        if(validateForm == true)
        {
            if(countAdult > 0)
            {
                var passenger = [];
                var i = 0;
                if(weightValidate == 1)
                {
                    $('.pass-fnm-aAdults-'+tourCode).each(function () { 
                        passenger[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aAdults-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aAdults-'+tourCode+'-'+i).val(),
                           'bookingQuestions' : {
                                'questionID' : 23,
                                'answer' : $('.weight-aAdults-'+tourCode+'-'+i).val()+' '+ $(".weight_select_"+tourCode).val()
                           }
                        };
                        i++;                    
                    });
                }
                else
                {
                    $('.pass-fnm-aAdults-'+tourCode).each(function () { 
                        passenger[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aAdults-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aAdults-'+tourCode+'-'+i).val()
                        };
                        i++;                    
                    });
                }                                   
            }

            if(countChild > 0)
            {
                var passengerc = [];
                var i = 0;
                if(weightValidate == 1)
                {
                    $('.pass-fnm-aChilds-'+tourCode).each(function () { 
                        passengerc[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aChilds-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aChilds-'+tourCode+'-'+i).val(),
                           'bookingQuestions' : {
                                'questionID' : 23,
                                'answer' : $('.weight-aChilds-'+tourCode+'-'+i).val()+' '+ $(".weight_select_"+tourCode).val()
                           }
                        };
                        i++;                    
                    });
                }
                else
                {
                    $('.pass-fnm-aChilds-'+tourCode).each(function () { 
                        passengerc[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aChilds-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aChilds-'+tourCode+'-'+i).val()
                        };
                        i++;                    
                    });
                } 
            }

            if(countInfant > 0)
            {
                var passengeri = [];
                var i = 0;
                if(weightValidate == 1)
                {
                    $('.pass-fnm-aInfants-'+tourCode).each(function () { 
                        passengeri[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aInfants-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aInfants-'+tourCode+'-'+i).val(),
                           'bookingQuestions' : {
                                'questionID' : 23,
                                'answer' : $('.weight-aInfants-'+tourCode+'-'+i).val()+' '+ $(".weight_select_"+tourCode).val()
                           }
                        };
                        i++;                    
                    });
                }
                else
                {
                    $('.pass-fnm-aInfants-'+tourCode).each(function () { 
                        passengeri[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aInfants-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aInfants-'+tourCode+'-'+i).val()
                        };
                        i++;                    
                    });
                }                                   
            }                

            
            paxInfo = {
                'aAdults' : passenger,
                'aChilds' : passengerc,
                'aInfants' : passengeri
            }    
            

            var hotelPickup = [];
            if(data.pickup == "Yes")
            {
                var location_select_val = $("input:radio.location-select-"+tourCode+":checked").val();
                if(location_select_val == 1)
                {
                    if($(".pickup-"+tourCode).val() == 'notListed')
                    {
                        hotelPickup = {
                            'hotelId' : $(".pickup-"+tourCode).val(),
                            'pickupPoint' :$(".manual-pickup-"+tourCode).val()
                        }
                    }
                    else
                    {
                        hotelPickup = {
                            'hotelId' : $(".pickup-"+tourCode).val()
                        }
                    }
                }
                if ( $(".compli-pickup-"+tourCode).length ) 
                {
                    hotelPickup = {
                            'pickupPoint' : $(".compli-pickup-"+tourCode).val()
                        }
                }
            }
            
            var question = [];
            var l = 0;
            $('.booking_'+tourCode).each(function () {                     
                question[l] = {
                   'questionId'  : $(this).data('question'),
                   'answer'   : $(this).val(),
                };
                l++;
            });
            activityBookingQuestion.push(question);
            
            var specialRequirement = $(".special_req_"+tourCode).val();

            finalArray = {
                'paxInfo' : paxInfo,
                'hotelPickup' : hotelPickup,
                'bookingQuestions' : question,
                'specialRequirement' : specialRequirement
            };    
            activity_data.specialRequirement = finalArray.specialRequirement;
            activity_data.paxInfo = finalArray.paxInfo;
            activity_data.bookingQuestionAnswers = finalArray.bookingQuestions;
            activity_data.hotelPickups = finalArray.hotelPickup;
            $(".loader").show();
            bookingSummary.update(JSON.stringify(search_session));  
            
            $(document).ajaxStop(function() {
                var session_search = JSON.stringify( search_session );                
                $('#search-session').val(session_search);
                parent.location.reload();
                $(".loader").hide();                
            });
        }
    });

    $('body').on('click', '.act-select-btn', function(e) {
        e.preventDefault();
        var data = $(this).data();
        var dataId = $(this).attr('data-button-id');
        var book = $(this).text();
        var viewData = $(this).attr('data-view');
        
        if (book == 'Add Activity') {

            if(data.provider == 'viator')
            {
                var form = $( "#form-"+data.tourCode );
                var validateForm = form.valid(); 
                var formdata = $(form).serialize();  
                var tourCode = data.tourCode;
                var weightValidate = data.weightValidate;            

                var countAdult = $('.pass-fnm-aAdults-'+tourCode).length;
                var countChild = $('.pass-fnm-aChilds-'+tourCode).length;
                var countInfant = $('.pass-fnm-aInfants-'+tourCode).length;

                var countwAdult = $('.weight-aAdults-'+tourCode).length;
                var countwChild = $('.weight-aChilds-'+tourCode).length;
                var countwInfant = $('.weight-Infants-'+tourCode).length;
                
                var paxInfo = [];

                var activityBookingQuestion = [];
                var finalArray = [];            
                
                if(validateForm == true)
                {
                    if(countAdult > 0)
                    {
                        var passenger = [];
                        var i = 0;
                        if(weightValidate == 1)
                        {
                            $('.pass-fnm-aAdults-'+tourCode).each(function () { 
                                passenger[i] = {
                                   'firstname'  : $(this).val(),
                                   'lastname'   : $('.pass-lnm-aAdults-'+tourCode+'-'+i).val(),
                                   'age'        : $('.pass-age-aAdults-'+tourCode+'-'+i).val(),
                                   'bookingQuestions' : {
                                        'questionID' : 23,
                                        'answer' : $('.weight-aAdults-'+tourCode+'-'+i).val()+' '+ $(".weight_select_"+tourCode).val()
                                   }
                                };
                                i++;                    
                            });
                        }
                        else
                        {
                            $('.pass-fnm-aAdults-'+tourCode).each(function () { 
                                passenger[i] = {
                                   'firstname'  : $(this).val(),
                                   'lastname'   : $('.pass-lnm-aAdults-'+tourCode+'-'+i).val(),
                                   'age'        : $('.pass-age-aAdults-'+tourCode+'-'+i).val()
                                };
                                i++;                    
                            });
                        }                                   
                    }

                    if(countChild > 0)
                    {
                        var passengerc = [];
                        var i = 0;
                        if(weightValidate == 1)
                        {
                            $('.pass-fnm-aChilds-'+tourCode).each(function () { 
                                passengerc[i] = {
                                   'firstname'  : $(this).val(),
                                   'lastname'   : $('.pass-lnm-aChilds-'+tourCode+'-'+i).val(),
                                   'age'        : $('.pass-age-aChilds-'+tourCode+'-'+i).val(),
                                   'bookingQuestions' : {
                                        'questionID' : 23,
                                        'answer' : $('.weight-aChilds-'+tourCode+'-'+i).val()+' '+ $(".weight_select_"+tourCode).val()
                                   }
                                };
                                i++;                    
                            });
                        }
                        else
                        {
                            $('.pass-fnm-aChilds-'+tourCode).each(function () { 
                                passengerc[i] = {
                                   'firstname'  : $(this).val(),
                                   'lastname'   : $('.pass-lnm-aChilds-'+tourCode+'-'+i).val(),
                                   'age'        : $('.pass-age-aChilds-'+tourCode+'-'+i).val()
                                };
                                i++;                    
                            });
                        } 
                    }

                    if(countInfant > 0)
                    {
                        var passengeri = [];
                        var i = 0;
                        if(weightValidate == 1)
                        {
                            $('.pass-fnm-aInfants-'+tourCode).each(function () { 
                                passengeri[i] = {
                                   'firstname'  : $(this).val(),
                                   'lastname'   : $('.pass-lnm-aInfants-'+tourCode+'-'+i).val(),
                                   'age'        : $('.pass-age-aInfants-'+tourCode+'-'+i).val(),
                                   'bookingQuestions' : {
                                        'questionID' : 23,
                                        'answer' : $('.weight-aInfants-'+tourCode+'-'+i).val()+' '+ $(".weight_select_"+tourCode).val()
                                   }
                                };
                                i++;                    
                            });
                        }
                        else
                        {
                            $('.pass-fnm-aInfants-'+tourCode).each(function () { 
                                passengeri[i] = {
                                   'firstname'  : $(this).val(),
                                   'lastname'   : $('.pass-lnm-aInfants-'+tourCode+'-'+i).val(),
                                   'age'        : $('.pass-age-aInfants-'+tourCode+'-'+i).val()
                                };
                                i++;                    
                            });
                        }                                   
                    }                

                    
                    paxInfo = {
                        'aAdults' : passenger,
                        'aChilds' : passengerc,
                        'aInfants' : passengeri
                    }    
                    

                    var hotelPickup = [];
                    if(data.pickup == "Yes")
                    {
                        var location_select_val = $("input:radio.location-select-"+tourCode+":checked").val();
                        if(location_select_val == 1)
                        {
                            if($(".pickup-"+tourCode).val() == 'notListed')
                            {
                                hotelPickup = {
                                    'hotelId' : $(".pickup-"+tourCode).val(),
                                    'pickupPoint' :$(".manual-pickup-"+tourCode).val()
                                }
                            }
                            else
                            {
                                hotelPickup = {
                                    'hotelId' : $(".pickup-"+tourCode).val()
                                }
                            }
                        }
                        if ( $(".compli-pickup-"+tourCode).length ) 
                        {
                            hotelPickup = {
                                    'pickupPoint' : $(".compli-pickup-"+tourCode).val()
                                }
                        }
                    }
                    
                    var question = [];
                    var l = 0;
                    $('.booking_'+tourCode).each(function () {                     
                        question[l] = {
                           'questionId'  : $(this).data('question'),
                           'answer'   : $(this).val(),
                        };
                        l++;
                    });
                    activityBookingQuestion.push(question);
                    
                    var specialRequirement = $(".special_req_"+tourCode).val();

                    finalArray = {
                        'paxInfo' : paxInfo,
                        'hotelPickup' : hotelPickup,
                        'bookingQuestions' : question,
                        'specialRequirement' : specialRequirement
                    };
                    $(".loader").show();    
                    addNumOfDay(data,finalArray);
                }
            }
            else
            {
                $(".loader").show();    
                addNumOfDay(data);
            }
            
            
            //eroam.confirm(null, 'Would you like to add another day?', function(e) {
                //addNumOfDay(data);
            //}, function(e) {
               // data = null;
            //});
        } else {            
            showDatefield(dataId, data.isSelected, data, viewData);
        }
    });


    <?php /*$('body').on('click', '.add-act', function(e) {
        e.preventDefault();
        var data = $(this).data();
        var dataId = $(this).attr('data-button-id');
        var book = $(this).text();
        var viewData = $(this).attr('data-view');
        
        if (book == 'Book Activity') {
            //eroam.confirm(null, 'Would you like to add another day?', function(e) {
                addNumOfDay(data);
            //}, function(e) {
              //  data = null;
            //});
        } else {
            showDatefield(dataId, data.isSelected, data, viewData);
        }
    }); comment by dhara for two times call for activity remove*/ ?>

    /*
    | function to display date field
    */
    function showDatefield(id, selected, data, view) {
        if (view == 'view') {
            if(data.provider == 'eroam')
            {
                var input = $('#view_datepick-' + id );    
            }
            else
            {
                var input = $('#view_datepick-' + id + '-' + data.tourCode);
            }            
        } else {
            var input = $('#datepick-' + id + '-' + data.tourCode);
        }
        
        if (!selected) {
            $('#date-field-container-' + id).addClass('show');
            $(input).click();
        } else {
            $('#date-field-container-' + id).removeClass('show');

            $(input).nextAll('#act-date').text('');
            deselectThisActivity(data, input);

            $(".loader").show();
            
            if (view == 'view') {
                $(document).ajaxStop(function() {
                    parent.location.reload();
                    $(".loader").hide();    
                });
            } else {
                setTimeout(function() {
                    location.reload();
                }, 2000);
            }


        }

    }

    $('body').on('click', '.datetimepick', function() {
        dates.sort();
        var startDate = (dates.length > 0) ? dates[0] : 0;
        if (startDate == '0000/00/00' || startDate == 0) {
            startDate = dateFrom;
        }

        //$(this).datepicker();

        $(this).datetimepicker({
            //$(this).datepicker({
            onGenerate: function(ct) {
                $(this).css({
                    'background': '#000',
                    'padding': 0,
                });
                $(this).find('.xdsoft_label, .xdsoft_option ').css({
                    'background': '#000',
                    'color': '#fff'
                });
                $(this).find('.xdsoft_calendar').css({
                    'background': '#fff'
                });
                $(this).find('.xdsoft_date').toggleClass('xdsoft_disabled');
                $(this).find('.xdsoft_datepicker').css({
                    'margin-left': 0,
                    'width': '275px'
                });
                $(this).find('th').css({
                    'background': '#000',
                    'color': '#fff',
                    'border-color': '#000'
                });
                $(this).find('.xdsoft_date > div').css({
                    'padding': '7px'
                });
                var datePickerdates = $(this).find('.xdsoft_date');
                datePickerdates.each(function() {
                    if (!$(this).hasClass('xdsoft_disabled')) {
                        //$(this).toggleClass('xdsoft_current');
                        $(this).css({
                            'opacity': '1'
                        }).find('div').css({
                            'font-weight': 'bold',
                            'color': '#fff',
                            'background': '#000',
                            'opacity': '1'
                        });

                    } else {

                        //$(this).css('background', '#999')
                    }
                });
            },
            onSelectDate: function(ct, i) {
                updateOnDateSelected(ct.dateFormat('Y-m-d'), i);
            },
            onClose: function(ct, i) {
                $(i.context).blur()
            },

            closeOnDateSelect: true,
            timepicker: false,
            format: 'Y-m-d',
            disabledDates: dates,
            startDate: startDate.replace(/-/g, '/'),
            showOtherMonths: true,
            selectOtherMonths: false,
            //minDate: -20, maxDate: "+1M +10D" 
        });

        if ($(this).hasClass('focus-triggered')) {
            $(this).removeClass('focus-triggered');
            $(this).blur();
        } else {
            $(this).addClass('focus-triggered');
            $(this).focus();
        }
    });

    /*
    | Function to add number of days if the user want's to stay longer in the selected city
    */
    function addNumOfDay(data,finalArray = null) {
        
        eroam.ajax('get', 'latest-search', {}, function(response) {
            if (response) {
                search_session = JSON.parse(response);

                var session = search_session.itinerary;
                if (session.length > 0) {

                    if (session[leg].city.id == cityId) {
                        var default_nights = search_session.itinerary[leg].city.default_nights;

                        search_session.itinerary[leg].city.days_to_add = 0;
                        search_session.itinerary[leg].city.add_after_date = dateFrom;

                        var act;
                        var selected_date = dateFrom;
						var addDuration = 0; 
                        if (search_session.itinerary[leg].activities != null) {
                            if (search_session.itinerary[leg].activities.length > 0) {
                                act = search_session.itinerary[leg].activities;
                                act = sortByDate(act);
                                //console.log(act[act.length - 1].date_selected);
                                
                                var selected_date = act[act.length - 1].date_selected;
                                
                                if (parseInt(act[act.length - 1].duration) > 1) {
									var string = act[act.length - 1].duration;
									if(typeof string == "string") {
                                        if(string.indexOf('minute') || (string.indexOf('hours') && parseInt(data.duration) < 25))
                                        {
                                            var nNights = 1;
                                        }
                                        else if(string.indexOf('day')){
                                            var nNights = parseInt(data.duration);
                                        }    
                                    } else {
                                        var nNights = parseInt(data.duration);
                                    }
                                    
									
									var addDuration = nNights;
                                }

                            }
                        }

                        var formattedDate = moment(addDays(selected_date, 1)).format('Do, MMMM YYYY');
						//selected_date = addDays(selected_date, addDuration);
                        selected_date = data.bookdate;
                        $('#datepick-' + data.activityId + '-' + data.tourCode).nextAll('#act-date').text(formattedDate);
                        if(data.provider == "viator")
                        {
                            updateOnDateSelected(selected_date, $('#datepick-' + data.activityId + '-' + data.tourCode), true, finalArray);
                        }
                        else
                        {
                            updateOnDateSelected(selected_date, $('#datepick-' + data.activityId ), true);
                        }
                    }
                }
            }
        });


    }

    /*
    | this function will update the selected activities when date is selected
    */
    function updateOnDateSelected(date, i, onAdd = false , finalArray = null) {
        
        if (date) {

            $(i.context).removeData('mousewheelPageHeight');
            $(i.context).removeData('mousewheelLineHeight');
           
            var data = onAdd ? i.data() : $(i.context).data();
            
            data.date_selected = date;
            data.extraNights = 0;

            var dateToStart = formatDate(data.date_selected, '/');
            var startCounting = false;
            var countNumOfDates = 0;

            for (count = 0; dates.length > count; count++) {
                //console.log(dateToStart);
                if ((dateToStart == dates[count]) || startCounting) {
                    startCounting = true;
                    countNumOfDates += 1;
                } else {
                    continue;
                }
            }
            if (data.isSelected) {
                //deselectThisActivity(data, $(i.context));
            }
            else {
                if (parseInt(data.duration) > 1 ) 
				{
                    var string = data.duration;
	                    if(string.toString().indexOf('minute') || (string.toString().indexOf('hours') && parseInt(data.duration) < 25))
	                        var nNights = 1;
	                    else if(string.toString().indexOf('day'))
	                        var nNights = parseInt(data.duration);
                    if (countNumOfDates >= nNights) {
                        selectThisActivity(data,'',finalArray);
                    } else {

                        var extraNights = countNumOfDates - nNights;
                        extraNights = Math.abs(extraNights);
                        data.extraNights = extraNights;
                        //console.log(data);
                        selectThisActivity(data,'',finalArray);
                        <?php /*eroam.confirm(null, 'You need ' + extraNights + ' more night(s) added to your stay for this city if you select this activity on this date. Would you like to continue?', function(e) {

                            selectThisActivity(data);
                        }, function(e) {
                            data = null;
                        }); comment by dhara for now no need to add one more night , user can book multiple activity on same day*/ ?>
                    }

                } else {
                    var add = false;
                    if (countNumOfDates <= 0) {
                        add = true;
                    }
                    selectThisActivity(data, add);
                }

                /*
                | Display date selected
                */
                var formattedDate = moment($(i.context).val()).format('Do, MMMM YYYY');
                //eeeeeeeeeeeeeee
                $(i.context).nextAll('#act-date').text(formattedDate);
            }
        }
    }

    /*
    | Function for adding days to a given date
    */
    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + parseInt(days));

        return formatDate(result, '-');
    }

    function sortByDate(activities) {
        var sorted = activities.sort(function(itemA, itemB) {
            var A = itemA.date_selected;
            var B = itemB.date_selected;
            return A.localeCompare(B);
        });
        return sorted;
    }

    function selectThisActivity(data, add = false, finalArray) {
        if (data) {
            var map_data = [];
            var selected_date = data.date_selected;
            
            switch (data.provider) {

                case 'eroam':
                    var activityDiv = $('.activity[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                    var activityBtn = $('.act-select-btn[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                    activityDiv.toggleClass('selected-activity');
                    activityDiv.toggleClass('listactive');

                    activityDiv.data('is-selected', true);
                    activityBtn.data('is-selected', true);
                    activityBtn.html('REMOVE ACTIVITY');
                    var dataAttr = activityDiv.data();

                    /*
                    | remove the date selected from the dates array
                    */

                    var selectedDates = [];

                    if (parseInt(data.extraNights)) {

                        var aLength = parseInt(data.extraNights) ? parseInt(data.extraNights) + 1 : parseInt(data.duration);
                        var range = _.range(1, aLength);

                        $.each(range, function(key, num) {
                            var sDate = addDays(selected_date, num).replace(/-/g, '/');
                            selectedDates.push(sDate);

                            var dateIndex = dates.indexOf(selected_date.replace(/-/g, '/'));

                            if (dateIndex > -1 && (typeof dates[dateIndex + parseInt(num)] != 'undefined')) {
                                selectedDates.push(dates[dateIndex + parseInt(num)]);
                            }
                        });
                    }

                    selectedDates.push(selected_date.replace(/-/g, '/'));

                    for (index = dates.length - 1; index >= 0; index--) {
                        var eachDate = dates[index];
                        if (typeof eachDate != 'undefined') {

                            if (selectedDates.indexOf(eachDate) > -1) {
                                dates.splice(index, 1);
                            }

                        }
                    }

                    /*
                    | This will update the session for dates available
                    */
                    if (dates.length > 0) {
                        postData = {
                            dates_available: dates,
                            id: cityId
                        };
                    } else {
                        dates.push('0000/00/00');
                        postData = {
                            dates_available: ['0000/00/00'],
                            id: cityId,
                            _token: $('meta[name="csrf-token"]').attr('content')
                        };
                    }
                    var travellers = "{!!session()->get('search')['travellers']!!}";
                    var child = "{!!session()->get('search')['child_total']!!}";
                    var total_pass = parseInt(travellers) + parseInt(child);
                    

                    var eRoamActdata = {
                        "date_from": dateFrom,
                        "date_to": dateTo,
                        'id': dataAttr.activityId,
                        'city_id': dataAttr.cityId,
                        'name': dataAttr.upperName,
                        'city': {
                            'id': dataAttr.cityId
                        },
                        'activity_price': [{
                            'price': parseInt(dataAttr.price) * total_pass,
                            'currency': {
                                'id': globalCurrency_id,
                                'code': dataAttr.currency
                            }
                        }],
                        'price': [{
                            'price': parseInt(dataAttr.price) * total_pass,
                            'currency': {
                                'id': globalCurrency_id,
                                'code': dataAttr.currency
                            }
                        }],
                        'date_selected': selected_date,
                        'currency_id': globalCurrency_id,
                        'currency': dataAttr.currency,
                        'provider': dataAttr.provider,
                        'description': dataAttr.description,
                        'duration': dataAttr.duration,
                    };

                    var extraNights = parseInt(data.extraNights) ? parseInt(data.extraNights) : 0;

                    extraNights = add ? parseInt(data.duration) : extraNights;
                    eRoamActdata.extra_nights = extraNights;

                    updateSession(search_session, eRoamActdata, true, extraNights);

                    break;

                case 'aot':

                    break;

                case 'ae':

                    break;
                case 'viator':
                    var activity_data = data;
                    var activityDiv = $('.activity[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                    var activityBtn = $('.act-select-btn[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"][data-tour-code="' +  data.tourCode + '"]');
                    activityDiv.toggleClass('selected-activity');
                    activityDiv.toggleClass('listactive');

                    activityDiv.data('is-selected', true);
                    activityBtn.data('is-selected', true);
                    activityBtn.html('REMOVE ACTIVITY');                    
                    var ageBands = [];
                    if(activity_data.adult != 0)
                    {
                        ageBands.push({'bandId' : 1 ,'count' : activity_data.adult});
                    }
                    if(activity_data.child != 0)
                    {
                        ageBands.push({'bandId' : 2 ,'count' : activity_data.child});  
                    }
                    if(activity_data.infant != 0)
                    {
                        ageBands.push({'bandId' : 3 ,'count' : activity_data.infant});
                    }
                    data = {
                        "date_from": dateFrom,
                        "date_to": dateTo,
                        'id': activity_data.activityId,
                        'city_id': activity_data.cityId,
                        'name': activity_data.name,
                        'city': {
                            'id': activity_data.cityId
                        },
                        'activity_price': [{
                            'price': activity_data.price,
                            'currency': {
                                'id': 1,
                                'code': activity_data.currency
                            }
                        }],
                        'price': [{
                            'price': activity_data.price,
                            'currency': {
                                'id': 1,
                                'code': activity_data.currency
                            }
                        }],
                        'tourGrades' : {
                            'currencyCode' : activity_data.currency,
                            'retailPrice' : activity_data.price,
                            'merchantNetPrice' : activity_data.merchantnetprice,
                            'gradeCode' : activity_data.tourCode,
                            'gradeTitle' : activity_data.tourgradeTitle,
                            'ageBands' : ageBands,
                        },
                        'date_selected': selected_date,
                        'currency_id': 1,
                        'currency': activity_data.currency,
                        'provider': activity_data.provider,
                        'description': activity_data.description,
                        'images': [activity_data.images],
                        'duration': activity_data.duration,
                        'cancellation_policy': activity_data.cancellationPolicy,
                        'paxInfo': finalArray.paxInfo,
                        'bookingQuestionAnswers' : finalArray.bookingQuestions,
                        'specialRequirement' : finalArray.specialRequirement,
                        'hotelPickups' : finalArray.hotelPickup,                        
                    };
                    

                    var selectedDates = [];
					var string = data.duration;
                    if(string.toString().indexOf('minute') || (string.toString().indexOf('hours') && parseInt(data.duration) < 25))
                        var nNights = 1;
                    else if(string.toString().indexOf('day'))
                        var nNights = parseInt(data.duration);
                    var date = selected_date.replace(/-/g, '/');
                    var available_date =['{!! session()->has('dates_available_'.$city_id) ? join("', '", session()->get('dates_available_'.$city_id) ) : '0000/00/00' !!}'];

                    if($.inArray( date, available_date )!= -1)
                        nNights =nNights -1;
                    
                    var aLength = parseInt(data.extraNights) ? parseInt(data.extraNights) + 1 : nNights;         
                    
                    var range = _.range(1, aLength);

                    $.each(range, function(key, num) {
                        var sDate = selected_date;
                        if(key > 1)
                            var sDate = addDays(selected_date, num).replace(/-/g, '/');
						
                        selectedDates.push(sDate);

                        var dateIndex = dates.indexOf(selected_date.replace(/-/g, '/'));

                        if (dateIndex > -1 && (typeof dates[dateIndex + parseInt(num)] != 'undefined')) {
                            selectedDates.push(dates[dateIndex + parseInt(num)]);
                        }
                    });

                    selectedDates.push(selected_date.replace(/-/g, '/'));
                    for (index = dates.length - 1; index >= 0; index--) {
                        var eachDate = dates[index];
                        if (typeof eachDate != 'undefined') {

                            if (selectedDates.indexOf(eachDate) > -1) {

                                dates.splice(index, 1);
                            }

                        }
                    }

                    /*
                    | This will update the session for dates available
                    */
                    if (dates.length > 0) {
                        postData = {
                            dates_available: dates,
                            id: cityId
                        };
                    } else {
                        dates.push('0000/00/00');
                        postData = {
                            dates_available: ['0000/00/00'],
                            id: cityId,
                            _token: $('meta[name="csrf-token"]').attr('content')
                        };
                    }

                    //var extraNights = parseInt(activity_data.extraNights) ? parseInt(activity_data.extraNights) : 0;
                    //extraNights = add ? parseInt(data.duration) : extraNights;
					
					extraNights = nNights;
                    data.extra_nights = extraNights;
                    //updateSessionViator(search_session, data, true, extraNights); comment by dhara
                    updateSessionViator(search_session, data, true, 0);

                    break;
            }
        }

    }


    function deselectThisActivity(data, input) {
        var map_data = [];
        switch (data.provider) {

            case 'eroam':
                var activityDiv = $('.activity[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                var activityBtn = $('.act-select-btn[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                activityDiv.toggleClass('selected-activity');
                activityDiv.toggleClass('listactive');
                activityDiv.data('is-selected', false);
                activityBtn.data('is-selected', false);
                input.data('is-selected', false);
                activityBtn.html('ADD ACTIVITY');
                map_data.id = data.activityId;
                map_data.city_id = data.cityId;

                updateSession(search_session, map_data, false)
                break;

            case 'aot':

                break;

            case 'ae':

                break;
            case 'viator':
                var activityDiv = $('.activity[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"][data-tour-code="' + data.tourCode + '"]');
                var activityBtn = $('.act-select-btn[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"][data-tour-code="' + data.tourCode + '"]');
                activityDiv.toggleClass('selected-activity');
                activityDiv.toggleClass('listactive');
                activityDiv.data('is-selected', false);
                activityBtn.data('is-selected', false);
                input.data('is-selected', false);
                activityBtn.html('ADD ACTIVITY');   
                $(".act-update-btn").hide();             
                map_data.id = data.activityId;
                map_data.city_id = data.cityId;
                map_data.tour_code = data.tourCode;    
                map_data.booking_date = data.bookdate;               
              
                updateSessionViator(search_session, map_data, false)

                break;

        }
    }

    function updateSessionViator(search_session, new_activity, isSelected, extraNights = 0) {
        var id = parseInt(new_activity.activityId);        
        switch (isSelected) {
            case true:
                $.each(search_session.itinerary, function(key, itinerary) {

                    if (itinerary.city.id == new_activity.city_id /*&& itinerary.activities.length > 0*/ ) {

                        if (itinerary.activities == null) {
                            itinerary.activities = [];
                        }
                        if (extraNights) {

                            itinerary.city.default_nights = parseInt(itinerary.city.default_nights) + parseInt(extraNights);
                        }
                      
                        itinerary.activities.push(new_activity);
                        if (itinerary.city.default_nights == 0 || itinerary.city.default_nights == null || itinerary.city.default_nights == '') {
                            itinerary.city.default_nights = itinerary.activities.length;
                        }
                        var act = itinerary.activities;
                        act = sortByDate(act);
                        itinerary.activities = act;
                    }
                });

                break;
            case false:

                $.each(search_session.itinerary, function(key, itinerary) {

                    var temp = itinerary.activities;

                    if (itinerary.city.id == new_activity.city_id && itinerary.activities.length > 0) {
                        $.each(itinerary.activities, function(k, activity) {
                            if (typeof activity != 'undefined') {
                                
                                if (activity.id == new_activity.id && activity.tourGrades.gradeCode == new_activity.tour_code && activity.date_selected == new_activity.booking_date) {
                                    
                                    <?php /*if (activity.extra_nights) {

                                        search_session.itinerary[key].city.days_to_deduct = activity.extra_nights;
                                        search_session.itinerary[key].city.deduct_after_date = dateFrom;
                                        search_session.itinerary[key].city.default_nights = parseInt(search_session.itinerary[key].city.default_nights) - parseInt(activity.extra_nights);
                                    } comment by dhara for no need to add or remove extra night as per new flow*/ ?>

                                    var aLength = parseInt(activity.duration) - parseInt(activity.extra_nights);
                                    
                                    <?php /*if (aLength) {

                                        if (dates[0] == '0000/00/00') {
                                            dates.splice(0, 1);
                                        }

                                        var range = _.range(1, aLength);
                                        var dateSelected = activity.date_selected.replace(/-/g, '/');
                                        $.each(range, function(key, num) {
                                            dates.push(addDays(dateSelected, num).replace(/-/g, '/'));
                                        });
                                        dates.push(dateSelected);
                                    }

                                    postData = {
                                        dates_available: dates,
                                        id: cityId,
                                        _token: $('meta[name="csrf-token"]').attr('content')
                                    };*/?>
                                    temp.splice(k, 1);
                                }
                            }
                        });
                    }
                });

                break;
        }
        search_session._token = $('meta[name="csrf-token"]').attr('content');
        bookingSummary.update(JSON.stringify(search_session));
        $(document).ajaxStop(function() {
            parent.location.reload();
            $(".loader").hide();    
        });
		calculateHeight('list');
    }

    function updateSession(search_session, new_activity, isSelected, extraNights = 0) {

        var id = parseInt(new_activity.id);

        switch (isSelected) {
            case true:
                $.each(search_session.itinerary, function(key, itinerary) {
                    if (itinerary.city.id == new_activity.city_id /*&& itinerary.activities.length > 0*/ ) {
                        if (itinerary.activities == null) {
                            itinerary.activities = [];
                        }

                        if (extraNights) {
                            itinerary.city.default_nights = parseInt(itinerary.city.default_nights) + parseInt(extraNights);
                        }
                        itinerary.activities.push(new_activity);
                        if (itinerary.city.default_nights == 0 || itinerary.city.default_nights == null || itinerary.city.default_nights == '') {
                            itinerary.city.default_nights = itinerary.activities.length;
                        }
                        var act = itinerary.activities;
                        act = sortByDate(act);
                        itinerary.activities = act;
                    }
                });

                break;
            case false:

                $.each(search_session.itinerary, function(key, itinerary) {

                    var temp = itinerary.activities;
                    if (itinerary.city.id == new_activity.city_id && itinerary.activities.length > 0) {
                        $.each(itinerary.activities, function(k, activity) {
                            if (typeof activity != 'undefined') {
                                if (activity.id == new_activity.id) {
                                    if (activity.extra_nights) {

                                        search_session.itinerary[key].city.days_to_deduct = activity.extra_nights;
                                        search_session.itinerary[key].city.deduct_after_date = dateFrom;
                                        search_session.itinerary[key].city.default_nights = parseInt(search_session.itinerary[key].city.default_nights) - parseInt(activity.extra_nights); //need to check at this statement for add activity
                                    }


                                    var aLength = parseInt(activity.duration) - parseInt(activity.extra_nights);
                                    if (aLength) {

                                        if (dates[0] == '0000/00/00') {
                                            dates.splice(0, 1);
                                        }

                                        //var aLength = parseInt(activity.duration) ;
                                        var range = _.range(1, aLength);
                                        var dateSelected = activity.date_selected.replace(/-/g, '/');
                                        $.each(range, function(key, num) {
                                            dates.push(addDays(dateSelected, num).replace(/-/g, '/'));
                                        });
                                        dates.push(dateSelected);
                                    }



                                    postData = {
                                        dates_available: dates,
                                        id: cityId,
                                        _token: $('meta[name="csrf-token"]').attr('content')
                                    };

                                    temp.splice(k, 1);
                                }
                            }
                        });
                    }
                });

                break;
        }
        search_session._token = $('meta[name="csrf-token"]').attr('content');
        bookingSummary.update(JSON.stringify(search_session));
        $(document).ajaxStop(function() {
            parent.location.reload();
            $(".loader").hide();    
        });
		calculateHeight('list');
    }

    /**************************** End Booking and cancel btn function ********************/

    // function to queue the calls to APIs e.g. eroam, viator
    function buildActivities() {
        var tasks = [            
            buildEroamActivity, // call eroam            
            buildViatorActivity, // call viator next
            showCategory, // call category
            checkActivityCount, // show the available transport types
            hidePageLoader, // hides the page loader when all API calls are done
            @if(count($activity_pref) > 0)
            autoSortActivityByPref,
            hideAutoSortLoader
            @endif
        ];

        $.each(tasks, function(index, value) {
            $(document).queue('tasks', processTask(value));
        });
        // queue
        $(document).queue('tasks');

        $(document).dequeue('tasks');

    }

    function processTask(fn) {
        return function(next) {
            doTask(fn, next);
        }
    }

    function doTask(fn, next) {
        fn(next);
    }

    

    function buildEroamActivity(next) {

        eroamRQ = {
            city_ids: [cityId],
            date_from: dateFrom,
            date_to: dateTo,
            interests: travel_pref
        };
        
        var eroamApiCall = eroam.apiDeferred('city/activity', 'POST', eroamRQ, 'activity', true);
        eroam.apiPromiseHandler(eroamApiCall, function(eroamResponse) {
            
            //if (eroamResponse.length > 0) {
            if (eroamResponse != null) {
                eroamResponse_length = eroamResponse.length;
                eroamResponse.forEach(function(eroamAct, eroamActIndex) {
                    var duration = parseInt(eroamAct.duration);
                    if (duration > 1) {
                        return;
                    }
                    eroamAct.provider = 'eroam';
                    eroamAct.index = eroamActIndex;
                    
                    if (selectedActivities.length > 0) {
                        if (selectedActivities.indexOf('eroam_' + eroamAct.id) > -1) {
                            eroamAct.selected = true;
                            eroamAct.activity_date = selected_dates['eroam_' + eroamAct.id];
                        }
                    }                  

                    try {                        
                        appendActivities(eroamAct);
                        totalActivityCount++;
                    } catch (err) {
                        console.log(err.message);
                    }
                });
            }

            next();
        });
    }

   

    function buildViatorActivity(next) {
        
        var total_viator_activity = Math.abs(20 - eroamResponse_length);
        /*vRQ = {
            destId: destinationId,
            startDate: dateFrom,
            endDate: dateTo,
            currencyCode: 'AUD',
            topX: '1-' + total_viator_activity,
            provider: 'viator'
        };*/

        
        vRQ = {
            destId: destinationId,
            startDate: dateFrom,
            endDate: dateTo,
            currencyCode: 'AUD',
            topX: '1-500',
            provider: 'viator'
        };
        
        var viatorApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', vRQ, 'viator_activity', true);
        eroam.apiPromiseHandler(viatorApiCall, function(viatorResponse) {
            
            calculateHeight('list');
            if (viatorResponse != null) {
                if (viatorResponse.length > 0) {
                    viatorResponse.forEach(function(viatorAct, viatorActIndex) {
                        
                        viatorAct.provider = 'viator';

                        var duration = countViatorActDuration(getViatorDuration(viatorAct.duration));
                        viatorAct.duration = viatorAct.duration;

                        if (selectedActivities.length > 0) {
                            if (selectedActivities.indexOf('viator_' + viatorAct.code) > -1) {
                                viatorAct.selected = true;                                
                                viatorAct.activity_date = selected_dates['viator_' + viatorAct.code];                                
                            }
                        }
                                                
                        var cat_name = getCategoryName(all_viator_category,viatorAct.catIds);
                        viatorAct.category_name = cat_name; 

                        try {                                 
                            //console.log(viatorAct);
                            appendActivities(viatorAct);
                            totalActivityCount++;
                        } catch (err) {
                            console.log(err.message);
                        }

                    });
                }
            }
            next();
        });
    }

    function getCategoryName(all_viator_category,categoryIds)
    {
        var theIndex = '';
        var all_categories = all_viator_category;
        
        $.each(categoryIds, function( index, value ) {
            //console.log( index + ": " + value );
            for (var i = 0; i < all_categories.length; i++) {
                if (all_categories[i].category_from_id == value) {                    
                    theIndex = all_categories[i].category_name + '#' + theIndex;
                    break;
                }
            }
        });        
        return theIndex.substring(0, theIndex.length - 1);
    }


    function countViatorActDuration(duration) {

        var result = 1;
        if (duration) {

            var match = duration.match(/hour|day/);
            match = match ? match[0] : '';
            switch (match) {

                case 'hour':
                    var hour = duration.split(' ');
                    hour = parseInt(hour[0]);
                    day = moment.duration(hour, 'hours').days();
                    result = parseInt(day) == 0 ? 1 : day;
                    break;

                case 'day':
                    var day = duration.split(' ');
                    result = parseInt(day[0]);
                    break;

                default:
                    result = 1;
                    break;

            }
        }
        return result;
    }

    function getViatorDuration(duration) {
        var match = duration.match(/[\d\.]+[\s|-]hour|[\d\.]+[\s|-]minute|[\d\.]+[\s|-]day/i);
        return match ? (match[0].match(/minute/) ? '1 day' : match[0].replace('-', ' ')) : '1 day';
    }

    // function to build the html for activity-list based on api call results 
    function appendActivities(act) {
        var id;
        var name;
        var description;
        var price;
        var currency;
        var imageSrc;
        var dataAttributes;
        var viewMoreBtn; // variable to store "View more" button DOM for
        var index;
        var activityDate;
        var convertedPrice;
        var code;
        var duration;
        var rating;
        var APP_URL =  {!! json_encode(url('/')) !!};
        var rating_html = '';
        var addActivityBtn;

        switch (act.provider) {
            case 'eroam':
                //console.log(act);
                name = act.name;
                selected = (act.selected) ? true : false;
                btn_value = (selected) ? 'REMOVE ACTIVITY' : 'ADD ACTIVITY';
                //class_selected = (selected) ? ' selected-activity listactive' : ' activity ';
                
                activityDate = (selected) ? act.activity_date : '';
                class_selected = (selected && selected_date_req == activityDate) ? ' selected-activity listactive' : ' activity ';

                if(act.description != null)
                {
                    var desc_length = act.description.length;

                    if(desc_length > 225)
                    {
                        description = act.description.substr(0, 225) + '...';
                    }
                    else
                    {
                        description = act.description;
                    }
                }
                else
                {
                    description = '';
                }
                price = parseFloat(act.activity_price[0].price);
                currency = act.activity_price[0].currency.code;

                /*description = act.description;
                price = '5000';
                currency = 'AUD';*/
                
                convertedPrice = eroam.convertCurrency(price, currency);
                if(act.image[0])
                {
                    //imageSrc = "{{config('env.DEV_CMS_URL')}}" + ((act.image[0].medium) ? act.image[0].medium : 'uploads/activities/' + act.image[0].large);
                    imageSrc = $('meta[name="aws_url"]').attr('content') + 'activities/' + act.image[0].original;
                }
                else
                {
                    imageSrc = "/assets/images/no-image-2.jpg";
                }
                index = act.index;
                
                code = act.id;
                duration = act.duration + ' Day';
                rating = act.ranking;
                var costpart = String(rating).split(".");
                for(var x=1 ; x<=rating; x++) {
                    rating_html +=  '<span class="d-inline-block"><i class=" ic-star"></i></span>';
                }
                if (costpart.length > 1) {                    
                    rating_html +=  '<span class="d-inline-block"><i class=" ic-half_star"></i></span>';
                    x++;
                }
                while (x <= 5) {
                    rating_html +=  '<span class="d-inline-block"><i class="ic-star_border"></i></span>';
                    x++;
                }

                var label = [];
                if (act.pivot.length > 0) {
                    if (selected) {
                        label.push(parseInt(travel_pref[0]));
                    } else {
                        $.each(act.pivot, function(index, value) {
                            var labelID = parseInt(value.label_id);
                            label.push(labelID);
                        });
                    }

                }

                //'data-price-id="' + act.activity_price[0].id + '" ',
                /*'data-name="' + act.name.replace(/"/g, '&quot;').toLowerCase() + '"',
                    'data-upper-name="' + act.name.replace(/"/g, '&quot;') + '"',
                    'data-description="' + act.description.replace(/"/g, '&quot;') + '"',*/
                var sortedLabels = sortCatPriority(label);
                dataAttributes = [
                    'data-provider="' + act.provider + '" ',
                    'data-city-id="' + cityId + '" ',
                    'data-index="' + index + '" ',
                    'data-activity-id="' + act.id + '" ',
                    'data-is-selected="' + selected + '" ',
                    'data-price="' + convertedPrice + '" ',
                    'data-currency="' + globalCurrency + '" ',
                    'data-price-id="1" ',
                    'data-name="' + act.name + '"',
                    'data-upper-name="' + act.name + '"',
                    'data-description="' + act.description + '"',
                    'data-duration="' + act.duration + '"',
                    'data-category="' + act.category_name + '"',
                    'data-code="' + act.id + '"',
                    'data-search="1"'
                ];

                if (sortedLabels.length > 0) {
                    var dataLabel = 'data-label="' + sortedLabels[0] + '"';
                    dataAttributes.push(dataLabel);
                }
                dataAttributes = dataAttributes.join('');

                //viewMoreBtn = '<a href="#"  data-select ="' + selected + '" data-cityId ="' + cityId + '" data-provider="' + act.provider + '" data-code="' + act.id + '" data-country-city-name="' + cityName + '" data-date="' + selected_date_req + '" data-act-name="' + name + '" ><button class="btn  btns_input_blue transform d-block w-100 act-view-more">View more</button></a>';
                viewMoreBtn = '<a href="#"  data-select ="' + selected + '" data-cityId ="' + cityId + '"  data-provider="' + act.provider + '" data-code="' + act.code + '" data-country-city-name="' + cityName + '" data-date="' + selected_date_req + '" data-act-name="' + name + '"  class="btn  btns_input_blue transform d-block w-100 act-view-more">View more</a>';

                addActivityBtn = '<div class="col-sm-6 col-xl-6 col-lg-12 mb-2 mb-xl-0"><input type="text" '+dataAttributes+'  class="datetimepick" id="datepick-'+act.id+'" value=""><button class="btn act-btn btn-primary btn-block bold-txt act-select-btn" '+dataAttributes+' style="margin-bottom:0px;" data-button-id="'+act.id+'">'+btn_value+'</button></div>';

                id = act.id;
                
                break;

            case 'viator':
                name = act.title;
                selected = (act.selected) ? true : false;
                btn_value = (selected) ? 'REMOVE ACTIVITY' : 'ADD ACTIVITY';
                
                activityDate = (selected) ? act.activity_date : '';
                class_selected = (selected && selected_date_req == activityDate) ? ' selected-activity listactive' : ' activity ';

                description = stripTags(act.shortDescription.substr(0, 225) + '...');
                price = parseFloat(markUpPrice(act.merchantNetPriceFrom, 30));
                currency = act.currencyCode;
                convertedPrice = eroam.convertCurrency(price, currency);
                
                if(act.thumbnailHiResURL)
                {
                    imageSrc = act.thumbnailHiResURL;                   
                }
                else
                {
                    imageSrc = "/assets/images/no-image-2.jpg";
                }
                index = act.code;
                id = act.code;
                category_name = act.category_name;
                code = act.code;
                duration = act.duration;
                rating = act.rating;
                //rating_html = '';
                //console.log(act.hotelPickup);
                var costpart = String(rating).split(".");

                for(var x=1 ; x<=rating; x++) {
                    rating_html +=  '<span class="d-inline-block"><i class=" ic-star"></i></span>';
                }
                if (costpart.length > 1) {                    
                    rating_html +=  '<span class="d-inline-block"><i class=" ic-half_star"></i></span>';
                    x++;
                }
                while (x <= 5) {
                    rating_html +=  '<span class="d-inline-block"><i class="ic-star_border"></i></span>';
                    x++;
                }
                
                var sortedLabels = [];
                
                @if(session() -> has('search_input') && count(session() -> get('search_input')['interests']) > 0)
					if (selected) {                        
						sortedLabels.push(parseInt(travel_pref[0]));
					} else {
						sortedLabels = sortCatPriority(act.labels);
					}
                @endif
                dataAttributes = [
                    'data-provider="' + act.provider + '" ',
                    'data-city-id="' + cityId + '" ',
                    'data-index="' + index + '" ',
                    'data-activity-id="' + act.code + '" ',
                    'data-is-selected="' + selected + '" ',
                    'data-price="' + convertedPrice + '" ',
                    'data-name="' + act.title.replace(/"/g, '&quot;').toLowerCase() + '"',
                    'data-upper-name="' + act.title.replace(/"/g, '&quot;') + '"',
                    'data-currency="' + globalCurrency + '"',
                    'data-startDate="' + dateFrom + '"',
                    'data-endDate="' + dateTo + '"',
                    'data-description="' + stripTags(act.shortDescription.replace(/"/g, '&quot;')) + '"',
                    'data-images="' + imageSrc + '"',
                    'data-rating="' + act.rating + '" ',
                    'data-duration="' + act.duration + '"',
                    'data-category="' + category_name + '"',
                    'data-search="1"'

                ];
                if (sortedLabels.length > 0) {
                    var dataLabel = 'data-label="' + sortedLabels[0] + '"';
                    dataAttributes.push(dataLabel);
                }
                dataAttributes = dataAttributes.join('');

                viewMoreBtn = '<a href="#"  data-select ="' + selected + '" data-cityId ="' + cityId + '"  data-provider="' + act.provider + '" data-code="' + act.code + '" data-country-city-name="' + cityName + '" data-date="' + selected_date_req + '" data-act-name="' + name + '"  class="btn  btns_input_blue transform d-block w-100 act-view-more">View more</a>';
                id = act.code;

                break;

            case 'ae':

                name = act.ExcursionName;
                selected = (act.selected) ? true : false;
                btn_value = (selected) ? 'REMOVE ACTIVITY' : 'ADD ACTIVITY';
                activityDate = (selected) ? act.activity_date : '';
                //class_selected = (selected) ? ' selected-activity listactive' : ' activity ';
                
              
                class_selected = (selected && selected_date_req == activityDate) ? ' selected-activity listactive' : ' activity ';
                description = stripTags(act.Description.substr(0, 225) + '...');
                price = parseFloat(markUpPrice(parseFloat(act.Prices[0].Price), 30));
                currency = act.Prices[0].Currency;
                convertedPrice = eroam.convertCurrency(price, currency);
                imageSrc = act.Images[0];
                index = act.ExcursionId;
                id = act.ExcursionId;
                
                code = act.code;
                duration = act.duration;
                

                dataAttributes = [
                    'data-provider="' + act.provider + '" ',
                    'data-city-id="' + cityId + '" ',
                    'data-index="' + index + '" ',
                    'data-activity-id="' + id + '" ',
                    'data-is-selected="' + selected + '" ',
                    'data-price="' + convertedPrice + '" ',
                    'data-name="' + name.toLowerCase() + '"',
                    'data-upper-name="' + name + '"',
                    'data-currency="' + globalCurrency + '"',
                    'data-startDate="' + dateFrom + '"',
                    'data-endDate="' + dateTo + '"',
                    'data-search="1"',                    
                    'data-description="' + stripTags(act.Description.substr(0, 225) + '...') + '"'
                ].join('');

                viewMoreBtn = '<a href="#"  data-select ="' + selected + '" data-cityId ="' + cityId + '"  data-provider="' + act.provider + '" data-code="' + act.code + '" data-country-city-name="' + cityName + '" data-date="' + selected_date_req + '" data-act-name="' + name + '"  class="view-more-btn"><button class="btn act-btn btn-primary btn-block  bold-txt act-view-more">View more</button></a>';
                id = act.code;

                break;

            case 'own_arrangement':

                break;

            case 'aot':

                break;

        }
        // var convertedPricePerPerson = Math.ceil( convertedPrice / travellers );
        var priceString = globalCurrency + '  ' + Math.ceil(convertedPrice).toFixed(2);
        roundedUpConvertedPrice = Math.ceil(convertedPrice).toFixed(2);

        maxPrice = parseInt(roundedUpConvertedPrice) > maxPrice ? parseInt(roundedUpConvertedPrice) : maxPrice;

        imageSrcGrid = imageSrc;
        if(imageSrc == "/assets/images/no-image-2.jpg")
        {
            imageSrcGrid = "/assets/images/no-image-grid.jpg";
        }
        
        // add code for title
        var act_title = name;
        var maxLength = 50 // maximum number of characters to extract
        var dot_combine = '';
        if(act_title.length > maxLength) 
        {
            act_title = act_title.substring(0,maxLength);
            var dot_combine = '...';
        }
        //trim the string to the maximum length
        var trimmedString = act_title.substr(0, maxLength);
        //re-trim if we are in the middle of a word
        trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));

        var class_add = '';
        if(class_selected == ' activity ')
        {
            class_add = 'activity-grid';
        }
        ghtml = [            
            '<div class="activities_grid '+ class_add +' col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4 pb-4" ' + dataAttributes + '>',
                '<a href="#"  data-select ="' + selected + '" data-cityId ="' + cityId + '"  data-provider="' + act.provider + '" data-code="' + code + '" data-country-city-name="' + cityName + '" data-date="' + selected_date_req + '" data-act-name="' + name + '" class="view-more-btn">',           
                    '<div class="itinerary_gridview_listing ' + class_selected + '">',
                        '<div class="text-center activity-grid-img" style="height:190px">',
                            '<img src="' + imageSrcGrid+'" alt=""/>',
                        '</div>',
                        '<div class="gridview_dis text-center pt-2 pl-3 pr-3 pb-2 pb-4">',
                            '<h5 class="mt-2 mb-3 hidden-content">' + trimmedString  + dot_combine  + '</h5>',
                            '<h5 style="color:#249dd0">' + priceString + ' Per Person </h5>',
                            '<small class="d-block border-bottom border-secondary pb-2 ">Activity Code: ' + code + ' </small>',
                            '<div class="rating pt-4">' + rating_html + '</div>',
                            '<p class="mb-0 pt-3">' + cityName + '</p>',
                            '<p class="mb-0">' + duration + '</p>',
                        '</div>',
                    '</div>',
                '</a>',
            '</div>',   
        ].join('');

        html = [
            '<span data-select ="' + selected + '" data-cityId ="' + cityId + '"  data-provider="' + act.provider + '" data-code="' + code + '" data-country-city-name="' + cityName + '" style="cursor:pointer" data-date="' + selected_date_req + '"  data-act-name="' + name + '" class="view-more-btn ' + class_selected + '" ' + dataAttributes + '>',
                '<div class="activities_list  pt-4 pb-4 pr-3 pl-3 mb-4 ' + class_selected + '" ' + dataAttributes + '>',
                    '<div class="border-bottom mb-3 activity_category_title">',
                        '<div class="row">',
                            '<div class="col-12 col-sm-7 col-xl-7">',
                                '<h4>' + name + '</h4>',
                                '<p><img src="'+APP_URL+'/images/tour.svg">',
                                ' Activity Code: ' + code + '   |   ',
                                '&nbsp;Location: ' + cityName +
                                '</p></div>',
                                '<div class="col-sm-5 col-xl-5">',
                                '<div class="row align-items-end">',
                                '<div class="col-xl-9 col-sm-8 text-left text-sm-right">',
                                'From <strong>' + priceString + '</strong> Per Person<br>',
                                '</div>',
                                '<div class="col-xl-3 col-sm-4 border-left text-left text-sm-center mt-2 mt-md-0">',
                                '<span class="font-weight-bold mb-0">' + duration + '</span>',
                                '</div>',
                                '</div>',
                                '</div>',
                                '</div>',
                                '</div>',
                                '<div class="clearfix">',
                                '<div class="activity-img">',
                                '<img src="' + imageSrc + '" alt="" style="height:200px"/>',
                                '</div>',
                                '<div class="activity-info pl-0 pl-lg-4 pl-md-3">',
                                '<div class="rating mb-2">' + rating_html + '</div>',
                                '<p>' + description + '</p>',           
                                '<div class="row mt-4">',
                            <?php /*'<div class="col-sm-6 col-xl-6 col-lg-12 mb-2 mb-xl-0">',            
                            //'<input type="hidden" ' + dataAttributes + '  class="datetimepick" id="datepick-' + id + '" value="">',
                            //'<a href="#" class="btn  btns_input_blue transform d-block w-100 act-btn act-select-btn" ' + dataAttributes + ' style="margin-bottom:0px;" data-button-id="' + id + '">' + btn_value + '</a>',
                            //'</div>', */?>
                            '<div class="col-sm-6 col-xl-6 col-lg-12 mb-2 mb-xl-0">' + viewMoreBtn + '</div>',
                            '</div>',                           
                        '</div>',
                    '</div>',
                '</div>',
            '</span>'
        ].join('');

        
        if (class_selected == ' selected-activity listactive') {
            $('.activity-list-selected').prepend(html);
            $('.grid-activity-list-selected').append(ghtml);
        } else {
            $('.activity-list').append(html);
            $('.grid-activity-list').append(ghtml);
        }
		
        hide_input();
    }
        

    function checkActivityCount(next) {
        $('.act-count').html(': Top ' + totalActivityCount + ((totalActivityCount == 1) ? ' Activity' : ' Activities') + ' Found');
        if (totalActivityCount == 0) {
            var html = '<h4 class="text-center blue-txt bold-txt">No Activities Found.</h4>';
            $('.activity-list').append(html);
        }
        next();
    }

    function hidePageLoader(next) {
        $('#activity-loader').fadeOut(300);
        next();
    }

    function showCategory(next){
        var items = {};
        $('div.activities_list').each(function() {
            items[$(this).attr('data-category')] = true;
        });
        var html = '';
        var result = new Array();
        var split_str = '';
        for(var i in items)
        {
            if(i != 'N/A'){
                
                split_str = i.split('#');
                $.each(split_str, function( index, value ) {                    
                    result.push(value);
                });
                //html += '<li><a id="searchCategory" class="dropdown-item">'+i+'</a></li>';
            }            

        }        
        $.each($.unique(result), function(i, value){            
            if(value != 'null')
            {
                html += '<li><a id="searchCategory" class="dropdown-item">'+value+'</a></li>';
            }
        });
        
        $('#category-tab').append( html );
        next();
        
    }

    function hideAutoSortLoader(next) {
        setTimeout(function() {
            $('#auto-sort-loader').fadeOut(300);
        }, 1000);

        next();
    }

    /*
    | Function to sort the activties by preference
    */

    function autoSortActivityByPref(next) {
        $('#auto-sort-loader').fadeIn('slow');
        var activityList = $('.activity-wrapper');

        var sortedActivities = activityList.sort(function(a, b) {
            var A = $(a).find('div.activity').attr('data-label');
            var B = $(b).find('div.activity').attr('data-label');

            var itemA = $.inArray(parseInt(A), travel_pref);
            var itemB = $.inArray(parseInt(B), travel_pref);

            itemA = itemA == -1 ? 100 : itemA;
            itemB = itemB == -1 ? 100 : itemB;

            return itemA - itemB;
        });
        $('#activity-list').html(sortedActivities);
        next();
    }


    /**************************** Start searching function ********************/

    $("#search_val").on("keyup",function search(e) {
        if ($(this).val() == '') {
            var list = $("div.activity-list div.activity");
            $(list).fadeOut("fast");
            var totalCount = 0;
            search_val = '1';
            $("div.activity-list").find("div.activity[data-search*='" + search_val + "']").each(function(i) {
                totalCount++;
                $(this).delay(200).slideDown("fast");
            });

            var grid = $("div.grid-activity-list div.activity-grid");
            $(grid).fadeOut("fast");
            $("div.grid-activity-list").find("div.activity-grid[data-search*='" + search_val + "']").each(function(i) {
                $(this).delay(200).slideDown("fast");
            });

            checkActivityCountForSearch(totalCount);
        }else{
			if(e.keyCode == 13) {
				getActivitySearch();
			}
		}
    });

    function getActivitySearch() {
        var search_val = $('#search_val').val();
        if (search_val) {
            search_val = search_val.toLowerCase();
            var list = $("div.activity-list div.activity");
            $(list).fadeOut("fast");
            var totalCount = 0;
            $("div.activity-list").find("div.activity[data-name*='" + search_val + "']").each(function(i) {
                totalCount++;
                $(this).delay(200).slideDown("fast");
            });

            var grid = $("div.grid-activity-list div.activity-grid");
            $(grid).fadeOut("fast");
            $("div.grid-activity-list").find("div.activity-grid[data-name*='" + search_val + "']").each(function(i) {
                $(this).delay(200).slideDown("fast");
            });

            checkActivityCountForSearch(totalCount);
        } else {
            var list = $("div.activity-list div.activity");
            $(list).fadeOut("fast");
            var totalCount = 0;
            search_val = '1';
            $("div.activity-list").find("div.activity[data-search*='" + search_val + "']").each(function(i) {
                totalCount++;
                $(this).delay(200).slideDown("fast");
            });

            var grid = $("div.grid-activity-list div.activity-grid");
            $(grid).fadeOut("fast");
            $("div.grid-activity-list").find("div.activity-grid[data-search*='" + search_val + "']").each(function(i) {                
                $(this).delay(200).slideDown("fast");
            });
            checkActivityCountForSearch(totalCount);
        }
    }

    function checkActivityCountForSearch(totalActivityCount) {
		$('div.activity-list').find('.noHotelFound').closest('h4').remove();
        $('.act-count').html(': Top ' + totalActivityCount + ((totalActivityCount == 1) ? ' Activity' : ' Activities') + ' Found');
        if (totalActivityCount == 0) {
            var html = '<h4 class="text-center blue-txt bold-txt no_activities">No Activities Found.</h4>';
            $('.activity-list h4.no_activities').fadeIn();
            $('#gridview h4.no_activities_grid').fadeIn();            
        } else {
            $('.activity-list h4.no_activities').fadeOut();
            $('#gridview h4.no_activities_grid').fadeOut();
        }
    }
    /**************************** end searching function ********************/


    /**************************** start sorting function ********************/
    $(document).on('click', '#priceSortAsc', function() { //alert(1);
        sortMeBy('data-price', 'div.activity-list', 'span.activity', 'asc');   
        sortMeBy('data-price', 'div.grid-activity-list', 'div.activity-grid', 'asc');
        $('#priceSortAsc i').removeClass('fa-caret-down').addClass('fa-caret-up');
        $('#priceSortAsc').attr('id', 'priceSortDesc');
    });

    $(document).on('click', '#priceSortDesc', function() {
        $('#priceSortDesc i').removeClass('fa-caret-up').addClass('fa-caret-down');
        sortMeBy('data-price', 'div.activity-list', 'span.activity', 'desc');
        sortMeBy('data-price', 'div.grid-activity-list', 'div.activity-grid', 'desc');
        $('#priceSortDesc').attr('id', 'priceSortAsc');
    });
    $(document).on('click', '#ratingAsc', function() { //alert(1);
        sortMeBy('data-rating', 'div.activity-list', 'span.activity', 'asc');
        sortMeBy('data-rating', 'div.grid-activity-list', 'div.activity-grid', 'asc');
        $('#ratingAsc i').removeClass('fa-caret-down').addClass('fa-caret-up');
        $('#ratingAsc').attr('id', 'ratingDesc');
    });
    $(document).on('click', '#ratingDesc', function() { //alert(2);
        $('#ratingDesc i').removeClass('fa-caret-up').addClass('fa-caret-down');
        sortMeBy('data-rating', 'div.activity-list', 'span.activity', 'desc');
        sortMeBy('data-rating', 'div.grid-activity-list', 'div.activity-grid', 'desc');
        $('#ratingDesc').attr('id', 'ratingAsc');
    });

    $(document).on('click', '#searchCategory', function() {
        //$("#transport-tab").children().removeClass("active");
        var provider = $(this).text(); //alert(provider);
        filterListOperator(provider);
        $("#category-tab").children().removeClass("active");
        $(this).parent().addClass("active");
    });

    function filterListOperator(value) {
        var list = $("div.grid-activity-list div.activity-grid");
        var list1 = $("div.activity-list span.activity");
        $(list).fadeOut("fast");
        $(list1).fadeOut("fast");
        var totalCount = 0;
        if (value == "All") {
            $("div.grid-activity-list").find("div.activity-grid").each(function (i) {
                totalCount ++;
                $(this).delay(200).slideDown("fast");
            });
            $("div.activity-list").find("span.activity").each(function (i) {
       
                $(this).delay(200).slideDown("fast");
            });
        } else { //alert(1);
            //Notice this *=" <- This means that if the data-category contains multiple options, it will find them
            //Ex: data-category="Cat1, Cat2"
            $("div.grid-activity-list").find("div.activity-grid[data-category*='" + value + "']").each(function (i) {
                totalCount ++;                    
                $(this).delay(200).slideDown("fast");
            });

            $("div.activity-list").find("span.activity[data-category*='" + value + "']").each(function (i) {     
                $(this).delay(200).slideDown("fast");
            });
        }
        checkActivityCountForSearch(totalCount);
    }    
    
    function sortMeBy(arg, sel, elem, order) {
        var $selector = $(sel),
            $element = $selector.children(elem);

        $element.sort(function(a, b) {
            var an = parseFloat(a.getAttribute(arg)),
                bn = parseFloat(b.getAttribute(arg));

            if (order == 'asc') {
                if (an > bn)
                    return 1;
                if (an < bn)
                    return -1;
            } else if (order == 'desc') {
                if (an < bn)
                    return 1;
                if (an > bn)
                    return -1;
            }
            return 0;
        });

        $element.detach().appendTo($selector);
    }
    /**************************** end sorting function ********************/


    /*
    | Added separator parameter
    */
    function formatDate(date, separator = '-') {

        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join(separator);
    }
	
	function calculateHeight(type) {
		setTimeout(function(){ timeOutCalculateHeight(type); },3000);
	}
	
	function timeOutCalculateHeight(type) {
		var oheight = $('.booking-summary form').outerHeight();
		var elem = $('.itinerary_right').outerHeight();
		var filter_height = $('.accommodation_top').outerHeight();
		var selected_height = $('.activity-list-selected').outerHeight();
		var elemHeight = oheight - filter_height - selected_height;
		
		$('.booking-summary').outerHeight(oheight);
		$('.itinerary_right').outerHeight(oheight);
		//$('.itinerary_page .tabs-content-container').outerHeight(oheight);
		$(".slimScrollDiv").css({'height':'auto'});
		if(type == 'view'){
			$('#view-more-details').css({'height':(elemHeight)});
		}else{
			$(".activity-list").css({'height':(elemHeight)});
		}
		$("body").css({'padding-bottom': '155px'});
	}
	
    function calculateHeight_old() {
        var winHeight = $(window).height();
        var oheight = $('.page-sidebar').outerHeight();
        var elem = $('.page-content .tabs-container').outerHeight();
        var elemHeight = oheight - elem;
        var winelemHeight = winHeight - elem;
        if (winHeight < oheight) {
            $(".page-content .tabs-content-container").outerHeight(elemHeight);
        } else {
            $(".page-content .tabs-content-container").outerHeight(winelemHeight);
        }
    }

    function sortCatPriority(labelIds) {
        var labelId = labelIds.sort(function(a, b) {
            var itemA = $.inArray(a, travel_pref);
            var itemB = $.inArray(b, travel_pref);
            itemA = itemA == -1 ? 100 : itemA;
            itemB = itemB == -1 ? 100 : itemB;
            if (itemB != 100 && itemA != 100) {
                return (itemA < itemB) ? -1 : (itemA > itemB) ? 1 : 0;
            }
        });
        return labelId;
    }

    function stripTags(str) {
        return str.replace(/<(?:.|\n)*?>/gm, '');
    }   
    
    
</script>
