<?php
	$btnName = ($data['select'] == 'true')?'CANCEL ACTIVITY' : 'BOOK ACTIVITY';
	$btnClass = ($data['select'] == 'true')?'selected-activity' : '';			
			                                             
	$selectDiv = '<div class="form-group col-6 col-sm-4 col-md-3 col-lg-3 col-xl-12">
			  <input type="hidden"  '.$data['attributes'].' class="datetimepick" id="view_datepick-'.$data['code'].'" value="">			  
			  <button type="button" class="btn btn-white transform d-block w-100 act-btn act-select-btn '.$btnClass.'" data-view= "view" data-provider="'.$provider.'" data-city-id="'.$data['cityid'].'" data-index="'.$code.'" data-activity-id="'.$code.'" data-is-selected="'.$data['select'].'" data-price="'.$data['price'].'"  data-price-id=""  data-name="'.$data['activity_name'].'" data-currency="'.session()->get( 'search' )['currency'].'" data-startdate="'.$data['startDate'].'" data-enddate="'.$data['endDate'].'" data-description="'.$data['description'].'" data-images="" data-duration="'.$data['duration'].'" data-label="" data-bookdate = "'.$data['bookingDate'].'" data-button-id="'.$data['code'].'">'.$btnName.'</button>
			</div>';
?>
<div class="outerpage_scroll accommodation_top pb-3">
    <div class="pl-3">
        <div class="accommodation_top pt-4 pb-3 border-bottom">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <a href="javascript:window.location.reload(true)" class="text-dark"><i class="ic-navigate_before"></i> <strong>Back to all Activity</strong></a>
                </div>
                <div class="col-12 col-sm-12 col-md-8 col-lg-8 text-sm-left text-lg-right">
                    <i class="ic-place"></i>{{ $country_city_name }}: {{ $date }} <!--(1 Room)-->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="itinerary_page pt-2">
	<div class="container-fluid">
		<div class="activities">
			<div class="activities_list pt-4 pb-1 pr-3 pl-3 mb-4">
				<!-- <a href="#" class="hotel-close act-btn-less " data-provider="{{ $provider }}" data-code="{{ $code }}"><i class=" ic-clear"></i> </a> -->
				<div class="border-bottom mb-3">
					<div class="row">
						<div class="col-12 col-sm-7 col-xl-8">
							<h4>{{ $data['activity_name'] }}</h4>
							<p> <img src="{{ url('images/tour.svg') }}" alt="" class="mr-2" /> Activity Code: {{ $data['code'] }} |&nbsp;Location: @if($data['address']){{ $data['address'] }}, @endif {{ $data['city_name'] }}
							</p>
						</div>
						<div class="col-sm-5 col-xl-4">
							<div class="row align-items-end">
								<div class="col-xl-9 col-sm-8 text-left text-sm-right">
									From <strong>{{ session()->get( 'search' )['currency'].' '.$data['price'] }}</strong> @lang('home.activity_label9')
									<div class="rating mb-2">
									@php
										for($x=1;$x<=$data['rating'];$x++) {
								        echo '<span class="d-inline-block"><i class=" ic-star"></i></span>';
								    }
								    if (strpos($data['rating'],'.')) {
								        echo '<span class="d-inline-block"><i class=" ic-half_star"></i></span>';
								        $x++;
								    }
								    while ($x<=5) {
								        echo '<span class="d-inline-block"><i class="ic-star_border"></i></span>';
								        $x++;
								    }
									@endphp
									</div>	    
	                            </div>
	                            <div class="col-xl-3 col-sm-4 border-left text-left text-sm-center mt-2 mt-md-0">
	                            	<span class="font-weight-bold mb-0">{{ $data['duration'] }}</span>
	                            </div>
							</div>
						</div>
					</div>
				</div>
				<?php /* ?>
				<div class="row">
					<div class="col-12">
						<div class="owl-carousel owl-theme">
							<?php 
								$i=0;
								foreach($data['images'] as $key => $image){
									$i++
							?>
								<div class="item"><img src="{{ $image }}" alt="Image" /> </div>
							<?php } ?>
						</div>
					</div>
				</div><?php */?>
				
				<div class="jcarousel-wrapper jcarousel-outer">
					<div class="jcarousel">
						<ul>
							<?php 
							$i=0;
							foreach($data['images'] as $key => $image)
							{
								$i++;
								$nimage = ($image != null) ? $image : "/assets/images/no-image-2.jpg";									
								?>
								<li><img src="{{ $nimage }}" alt="Image {{$i}}"></li>
								<?php 	
								
							} ?>
						</ul>
					</div>
					<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
					<a href="#" class="jcarousel-control-next">&rsaquo;</a>
					<p class="jcarousel-pagination"></p>
				</div>
				
				<div class="row pt-5">
					<div class="col-12 accommodation_tabs">
                        <ul class="nav nav-tabs border-0 justify-content-md-center" id="myTab" role="tablist">
                            <li class="nav-item pr-2  text-center">
                                <a class="nav-link border-0 active text-uppercase" id="home-tab" data-toggle="tab" href="#room_rates" role="tab" aria-controls="room_rates" aria-selected="true">Overview</a>
                            </li>
                            @if($provider == 'viator')
                            <li class="nav-item pr-2 pl-2 text-center">
                                <a class="nav-link border-0 text-uppercase" id="profile-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="false">Important Info</a>
                            </li>
                            
                            <li class="nav-item  pl-2 text-center">

                                <a class="nav-link border-0 text-uppercase" id="bookActivity-tab" data-toggle="tab" href="#bookActivity" role="tab" aria-controls="bookActivity" aria-selected="false">Book Activity</a>
                            </li>
                            @else
                            	<li class="nav-item  pl-2 text-center">
                            		<input type="hidden"  {{$data['attributes']}} class="datetimepick" id="view_datepick-{{$data['code']}}" value="">
                            		<?php

                            		$btn_value = ($data['select'] == 'true')?'Remove Activity' : 'Add Activity';
                            		$select_act = ($btn_value != 'Add Activity') ? "true" : "false";
                            		//$select_act = "false";
                            		?>
			                        <input type="hidden"  class="datetimepick" id="datepick-{{ $data['code']}}" data-provider="{{$provider}}" data-city-id="{{$data['cityid']}}" data-index="{{$code}}" data-activity-id="{{$code}}" data-is-selected="{{$select_act}}" data-price="{{$data['price']}}"  data-price-id=""  data-name="{{$data['activity_name']}}" data-currency="{{session()->get( 'search' )['currency']}}" data-startdate="{{$data['startDate']}}" data-enddate="{{$data['endDate']}}" data-description="{{$data['description']}}" data-images="" data-duration="{{$data['duration']}}" data-label="" data-bookdate = "{{$data['bookingDate']}}" value="">

			                        <a type="button" class="btn btn-white transform d-block w-100 act-btn act-select-btn {{$btnClass}}" data-view= "view" data-provider="{{$provider}}" data-city-id="{{$data['cityid']}}" data-index="{{$code}}" data-activity-id="{{$code}}" data-is-selected="{{$select_act}}" data-price="{{$data['price']}}"  data-price-id=""  data-name="{{$data['activity_name']}}" data-currency="{{session()->get( 'search' )['currency']}}" data-startdate="{{$data['startDate']}}" data-enddate="{{$data['endDate']}}" data-description="{{$data['description']}}" data-images="" data-duration="{{$data['duration']}}" data-label="" data-bookdate = "{{$data['bookingDate']}}" data-button-id="{{$data['code']}}" >{{$btn_value}}</a>
	                                <!-- <a class="nav-link border-0 text-uppercase" aria-selected="false">Add Activity</a> -->
	                            </li>
                            @endif
                            
                        </ul>
                    </div>
					<div class="col-12 activity-container">
						<div class="tab-content accommodation_tabs_content " id="myTabContent">
                            <div class="tab-pane fade  show active" id="room_rates" role="tabpanel" aria-labelledby="home-tab">
                                <div class="pt-5 pb-5 ">
                                    <div class="row">
                                    	<?php if($data['departurePoint']){ ?> 
	                                        <div class="col-sm-6">
	                                            <div class="row">                                            	
	                                                <div class="col-sm-5 col-xl-4">
	                                                    <strong>@lang('home.departure_point')</strong>
	                                                </div>
	                                                <div class="col-sm-7 col-xl-8">
														<?php echo $data['departurePoint']; ?>
													</div>
	                                        	</div>
	                                    	</div>
                                    	<?php } ?>
										<?php if($data['hotelPickup']){ ?> 
                                    	<div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-5 col-xl-4">
                                                    <strong>Hotel Pickup Included</strong>
                                                </div>
                                                <div class="col-sm-7 col-xl-8">
                                                    <span><?php echo ($data['hotelPickup'] == "Yes") ? "Yes" : "No"; ?></span>
                                                </div> 
                                            </div>
                                        </div>
                                        <?php }?>
                                    </div>
                                    <div class="row">
                                    	<?php if($data['departureTime']){ ?> 
                                    	<div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-5 col-xl-4">
                                                    <strong>@lang('home.departure_time')</strong>
                                                </div>
                                                <div class="col-sm-7 col-xl-8">
                                                    <span><?php echo $data['departureTime']; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } 
                                        if($data['return_details'])
                                        {?>	
                                        	<div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-5 col-xl-4">
                                                    <strong>@lang('home.return_details')</strong>
                                                </div>
                                                <div class="col-sm-7 col-xl-8">
                                                    <span><?php echo $data['return_details']; ?></span>
                                                </div>
                                            </div>
                                        </div>                                       
                                    <?php }?>                                    	
                                    </div>
                                    <div class="row">
                                    	<?php if($data['highlights']){ ?> 
                                    	<div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-5 col-xl-4">
                                                    <strong>@lang('home.highlights') </strong>
                                                </div>
                                                <div class="col-sm-7 col-xl-8">
													<ul class="m-l-30">
														<?php foreach ($data['highlights'] as $key => $value) { ?>
															<li><?php echo $value?></li>
														<?php } ?>
													</ul>
												</div>
											</div>
										</div>
									<?php } ?>									
                                    </div>
                                    <div class="row">
                                    	
										<?php if($data['reception_email']){ ?>
											<div class="col-sm-6">
	                                            <div class="row">
	                                                <div class="col-sm-5 col-xl-4">
	                                                    <strong>@lang('home.reception_email')</strong>
	                                                </div>
	                                                <div class="col-sm-7 col-xl-8">
	                                                    <span><?php echo $data['reception_email']; ?></span>
	                                                </div>
	                                            </div>
	                                        </div> 	                                		
	                                	<?php } ?>
	                                	<?php if($data['reception_phone']){ ?>
	                                		<div class="col-sm-6">
	                                            <div class="row">
	                                                <div class="col-sm-5 col-xl-4">
	                                                    <strong>@lang('home.reception_phone')</strong>
	                                                </div>
	                                                <div class="col-sm-7 col-xl-8">
	                                                    <span><?php echo $data['reception_phone']; ?></span>
	                                                </div>
	                                            </div>
	                                        </div> 
	                                	<?php } ?>
	                                	

	                                	<?php if($data['has_accomodation'] && $data['accommodation_nights']){ ?>
	                                		<div class="col-sm-6">
	                                            <div class="row">
	                                                <div class="col-sm-5 col-xl-4">
	                                                    <strong>@lang('home.search_accommodation') Nights</strong>
	                                                </div>
	                                                <div class="col-sm-7 col-xl-8">
	                                                    <span><?php echo ($data['accommodation_nights'] > 1) ? $data['accommodation_nights']." Night" : $data['accommodation_nights']." Nights"; ?></span>
	                                                </div>
	                                            </div>
	                                        </div> 
	                                	<?php } ?>

	                                	<?php if($data['has_accomodation'] && $data['accommodation_details']){ ?>
	                                		<div class="col-sm-6">
	                                            <div class="row">
	                                                <div class="col-sm-5 col-xl-4">
	                                                    <strong>@lang('home.search_accommodation') Detail</strong>
	                                                </div>
	                                                <div class="col-sm-7 col-xl-8">
	                                                    <span><?php echo $data['accommodation_details']; ?></span>
	                                                </div>
	                                            </div>
	                                        </div> 
	                                	<?php } ?>

	                                	<?php if($data['has_transport'] && $data['transport_details']){ ?>
	                                		<div class="col-sm-6">
	                                            <div class="row">
	                                                <div class="col-sm-5 col-xl-4">
	                                                    <strong>@lang('home.tour_details_label6')</strong>
	                                                </div>
	                                                <div class="col-sm-7 col-xl-8">
	                                                    <span><?php echo $data['transport_details']; ?></span>
	                                                </div>
	                                            </div>
	                                        </div> 
	                                	<?php } ?>



	                                	<?php if($data['website']){ ?>
	                                		<div class="col-sm-6">
	                                            <div class="row">
	                                                <div class="col-sm-5 col-xl-4">
	                                                    <strong>@lang('home.website')</strong>
	                                                </div>
	                                                <div class="col-sm-7 col-xl-8">
	                                                    <span><?php echo $data['website']; ?></span>
	                                                </div>
	                                            </div>
	                                        </div> 	                                		
	                                	<?php } ?>
	                                	
                                    </div>
                            	</div>
                            	
                            	<div>
                            		{!! $data['activity_description'] !!}
                            		<?php if($data['description']){ ?> 
										<h3 class="font-weight-bold mt-5">@lang('home.what_you_can_expect_text') </h3>
										<p><?php echo $data['description']; ?></p>
									<?php } ?>
									<?php if($data['itinerary']){ ?> 
										<h3 class="font-weight-bold mt-5">@lang('home.itinerary') </h3>
										<p><?php echo $data['itinerary']; ?></p>
									<?php } ?>
                            	</div>
                            </div>
                            <div class="tab-pane fade" id="overview" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="pt-5 pb-5">
                                	<h2>@lang('home.profile_details_text')</h2>

                                	

                                	<?php if($data['inclusions']){ ?>
                                		<p class="mb-0 mt-3"><strong>@lang('home.inclusions')</strong></p>
                                		<ul class="pl-4 pt-2">
                                		<?php foreach ($data['inclusions'] as $key => $value) { ?>
												<li><?php echo $value."<br>";?></li>
											<?php } ?>
										</ul>
                                	<?php } ?>

                                	<?php if($data['exclusions']){ ?> 
										<p class="mb-0 mt-3"><strong>@lang('home.exclusions')</strong></p>
										<ul class="pl-4 pt-2">
											<?php foreach ($data['exclusions'] as $key => $value) { ?>
												<li><?php echo $value?></li>
											<?php } ?>
										</ul>
									<?php } ?>

									<?php if($data['additionalInfo']){ ?> 
										<p class="mb-0 mt-3"><strong>@lang('home.additional_info')</strong></p>
										<ul class="ml-4">
											<?php foreach ($data['additionalInfo'] as $key => $value) { ?>
												<li><?php echo $value?></li>
											<?php } ?>
										</ul>
									<?php } ?>

									<?php if($data['salesPoints']){ ?> 
										<p class="mb-0 mt-3"><strong>@lang('home.salespoints')</strong></p>
										<ul class="ml-4">
										<?php foreach ($data['salesPoints'] as $key => $value) { ?>
											<li><?php echo $value; ?></li>
                                    	<?php }   ?>                                 
                                    	</ul>
									<?php } ?>

									<?php if($data['voucherRequirements']){ ?> 
										<p class="mb-0 mt-3"><strong>@lang('home.voucher_info')</strong></p>
										<p><?php echo $data['voucherRequirements']; ?></p>
									<?php } ?>

									
									<?php if($provider == 'viator') { ?> 
										<p class="m-b-0 m-t-10"><strong>@lang('home.local_operator_info')</strong></p>
										<p>Complete Operator information, including local telephone numbers at your destination, are included on your Confirmation Voucher. Our Product Managers select only the most experienced and reliable operators in each destination, removing the guesswork for you, and ensuring your peace of mind.</p>

										<h2><strong>@lang('home.cancellation_policy')</strong></h2>
										<p>{!!$data['termsAndConditions']!!}</p>

										<p><strong>@lang('home.schedule_and_pricing')</strong></p>
										<p>Click the link below to check pricing & availability on your preferred travel date. Our pricing is constantly updated to ensure you always receive the lowest price possible - we 100% guarantee it. Your currency is set to USD. Click here to change your currency.</p>
									<?php } ?>
								</div>
							</div>
							<div class="tab-pane fade pt-5" id="bookActivity" role="tabpanel" aria-labelledby="bookActivity-tab">
							@php
								if(isset($data['allTourGrades']) && !empty($data['allTourGrades']))
								{
									foreach($data['allTourGrades'] as $tourGrade)
									{
										$btn_value = (in_array($tourGrade['gradeCode'],$data['select_grade_code']) && in_array($data['code'],$data['default_selected_activity'])) ?  'Remove Activity' : "Add Activity";            
	                                	$btnClass = ($btn_value != 'Add Activity') ? 'selected-activity' : '';
	                                	$select_act = ($btn_value != 'Add Activity') ? "true" : "false";
										$class = "";										

										if($tourGrade['available'] == false  || $data['length_of_stay'] == 1)
										{
											$class = "disabled";
										}

										@endphp
		                              	<div class="card panel border-0 p-3 mb-3 {{$class}}">
			                                <div class="row">
			                                    <div class="col-sm-4 col-xl-4 border-right">
			                                        <h5 class="mb-0 {{$class}}">{{$tourGrade['gradeTitle']}}</h5>
			                                        <p class="text-muted">Code: {{$tourGrade['gradeCode']}}</p>
			                                    </div>
			                                    <div class="col-sm-5 col-xl-6 <?php if($class != "disabled"){ ?>  border-right" <?php }?> >
			                                        <p class="depart-text mb-0">Departs {{$tourGrade['gradeDepartureTime']}}</p>
			                                        <p>{{$tourGrade['gradeDescription']}}</p>
			                                    </div>
			                                    @if($class != "disabled")
			                                    <div class="col-sm-3 col-xl-2 text-center">
			                                        AUD<br/>
			                                        <h2 class="font-weight-bold">$ {{ number_format($tourGrade['merchantNetPrice'],2) }}</h2>
			                                    </div>
			                                    @endif
			                                </div>
		                                	<hr>
		                                	<div class="row room_rates_filter_block">
			                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			                                        <div class="row">	
				                                        @php  
				                                        	$adultTotal = 0;
				                                        	$childTotal = 0;
				                                        	$infantTotal = 0;
				                                        @endphp 
			                                        	@if(isset($data['ageBands']) && !empty($data['ageBands']))                                           
			                                        		@foreach($data['ageBands'] as $ageBand)
			                                        			@if($ageBand['bandId'] == 1)
			                                        			@php $adultTotal = $data['adult']; @endphp
					                                            <div class="form-group col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					                                                <div class="fildes_outer">
					                                                    <label>Adults({{$ageBand['ageFrom']}}-{{$ageBand['ageTo']}})</label>
					                                                    <div class="custom-select">
					                                                        <select disabled class="{{$class}}">
					                                                            <option>{{ $data['adult'] }}</option>	      
					                                                        </select>
					                                                    </div>
					                                                </div>
					                                            </div>
					                                            @endif
					                                            @if($ageBand['bandId'] == 2)
					                                            @php $childTotal = $data['child']; @endphp
					                                            <div class="form-group col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					                                                <div class="fildes_outer">
					                                                    <label>Children({{$ageBand['ageFrom']}}-{{$ageBand['ageTo']}})</label>
					                                                    <div class="custom-select">
					                                                        <select disabled class="{{$class}}">
					                                                            <option>{{ $data['child'] }}</option>
					                                                        </select>
					                                                    </div>
					                                                </div>
					                                            </div>
					                                            @endif
					                                            @if($ageBand['bandId'] == 3)
					                                            @php $infantTotal = $data['infant']; @endphp
					                                            <div class="form-group col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					                                                <div class="fildes_outer">
					                                                    <label>Infants({{$ageBand['ageFrom']}}-{{$ageBand['ageTo']}})</label>
					                                                    <div class="custom-select">
					                                                        <select disabled class="{{$class}}">
					                                                            <option>{{ $data['infant'] }}</option>
					                                                        </select>
					                                                    </div>
					                                                </div>
					                                            </div>
					                                            @endif
				                                            @endforeach
			                                            @endif
			                                        </div>
			                                    </div>
			                                    
			                                    @if($class == "disabled" || $data['length_of_stay'] == 1)
			                                    <div class="col-12 col-xl-3">
			                                        <div class="row">
			                                            <div class="form-group col-6 col-sm-4 col-md-3 col-lg-3 col-xl-12">
			                                            	This option is unavailable or sold out on this day.
			                                            </div>                
			                                        </div>
			                                    </div>
			                                    @else
			                                    <div class="col-sm-12" id="collapse-{{$tourGrade['gradeCode']}}">
				                                    <form id="form-<?php echo $tourGrade['gradeCode']; ?>" method="post">

					                                    <?php 
					                                    $validate_weight = 0;
					                                    $countP = 1;
					                                    $paxInfo = $data['paxInfo'];
					                                    foreach ($paxInfo as $key => $pax_info) 
					                                    { 
					                                    	$title = '';
					                                    	
					                                    	if($key == "aAdults")
					                                    	{
					                                    		$title = 'Adults';
					                                    	}
					                                    	elseif($key == "aChilds")
					                                    	{
					                                    		$title = 'Child';
					                                    	}
					                                    	if($key == "aInfants")
					                                    	{
					                                    		$title = 'Infants';
					                                    	}

					                                    	$html = '';
					                                    	$pI = 1;
					                                    	foreach ($pax_info as $passenger) 
					                                    	{ 
					                                    		$fnm   = $tourGrade["gradeCode"]."['".$key."'][]['firstname']";
					                                    		$lnm   = $tourGrade["gradeCode"]."['".$key."'][]['lastname']";
					                                    		$age   = $tourGrade["gradeCode"]."['".$key."'][]['age']";
					                                    		$ic = $pI-1;
					                                    		$html .= '
					                                    		<input type="hidden" name="'.$fnm.'" class="pass-fnm-'.$key.'-'.$tourGrade["gradeCode"].'" value="'.$passenger['firstname'].'">
					                                    		<input type="hidden" name="'.$lnm.'" class="pass-lnm-'.$key.'-'.$tourGrade["gradeCode"].'-'.$ic.'" value="'.@$passenger['lastname'].'">
					                                    		<input type="hidden" name="'.$age.'" class="pass-age-'.$key.'-'.$tourGrade["gradeCode"].'-'.$ic.'"  value="'.@$passenger['age'].'">
					                                    		';
		                                                        $html .= "<tr>
		                                                            <td>".$pI."</td>
		                                                            <td>".$passenger['firstname']."</td>
		                                                            <td>".@$passenger['lastname']."</td>"; // <td>".@$passenger['age']."</td>
		                                                            
		                                                            if(array_search('23', array_column($data['bookingQuestions'], 'questionId')) !== false) 
		                                                            {   
		                                                            	$pass_we = '';
		                                                            	$pass_kg = '';

		                                                            	if(isset($passenger['bookingQuestions']['answer']))
		                                                            	{
			                                                            	$pass = explode(" ",$passenger['bookingQuestions']['answer']);
			                                                            	$pass_we = $pass[0];
			                                                            	$pass_kg = $pass[1];
			                                                            	$option = '<option value="'.$pass_kg.'">'.$pass_kg.'</option>';
			                                                            }
			                                                            else
			                                                            {
			                                                            	$option = '<option value="kg">kg</option><option value="lb">lb</option>';
			                                                            }

		                                                            	$aKey = array_keys(array_column($data['bookingQuestions'], 'questionId'),'23');                         
		                                                            	$data_key = $aKey[0];
		                                                            	$required = '';
		                                                            	if(isset($data['bookingQuestions'][$data_key]['required']) && $data['bookingQuestions'][$data_key]['required'] == true)
		                                                            	{
		                                                            		$validate_weight = 1;
		                                                            		$required = 'required';
		                                                            	}
		                                                            	$weight   = $tourGrade["gradeCode"]."['".$key."'][]['weight']"."[".$countP."]";

		                                                            	$html .= '<td>
		                                                                <div class="row no-gutters">
		                                                                    <div class="col-sm-8">
		                                                                        <div class="fildes_outer">
		                                                                            <label>Weight *</label>
		                                                                            <input name="'.$weight.'" value="'.$pass_we.'" class="form-control weight-'.$key.'-'.$tourGrade['gradeCode'].'-'.$ic.'" data-questionID="23" type="text" '.$required.' placeholder="Weight" min="1" max="500" >
		                                                                        </div>
		                                                                        <label for="'.$weight.'" generated="true" class="error text-left"></label>
		                                                                    </div>
		                                                                    <div class="col-sm-4">
		                                                                        <div class="fildes_outer">
		                                                                            <label></label>
		                                                                            <div class="custom-select">
		                                                                                <select class="weight_select weight_select_'.$tourGrade['gradeCode'].'" data-tourgrade-title='.$tourGrade['gradeCode'].' data-questionID="23">
		                                                                                 '.$option.'  
		                                                                                </select>
		                                                                            </div>
		                                                                        </div>
		                                                                    </div>
		                                                                </div>
		                                                            </td>'; 
		                                                        	}                                                               
		                                                        $html .= '</tr>';	                                                        
		                                                        $pI++;
		                                                        $countP++;
			                                                } 

					                                    	?>

						                                    <h5> <?php echo $title." ".count($paxInfo[$key]); ?></h5>

			                                                <div class="table-responsive activity-table mb-3 mt-2"> 
			                                                    <table class="table table-bordered text-center">
			                                                        <thead class="thead-light">
			                                                            <tr>
			                                                                <th>Passanger No.</th>
			                                                                <th>First Name</th>
			                                                                <th>Last Name</th>
			                                                                <!-- <th>Age</th> -->
			                                                                <?php
			                                                                if(array_search('23', array_column($data['bookingQuestions'], 'questionId')) !== false) {?>
			                                                                <th width="30%">Weight</th>
			                                                                <?php }?>
			                                                                <!-- <th>Action</th> -->
			                                                            </tr>
			                                                        </thead>
			                                                        <tbody>  
			                                                        	{!! $html !!}
			                                                        </tbody>
			                                                    </table>
			                                                </div>

	                                            		<?php
	                                            		}  ?>

	                                            		@if($data['hotelPickup'] == "Yes")

	                                            		<?php 
	                                            		$display = '';                                                                    
	                                                    $hotelId = @$data[$tourGrade['gradeCode']][2]['hotelPickups']['hotelId'];
	                                                                   
	                                            		?>
	                                            		<div class="">
	                                                        <div class=" mt-4 ml-2">
	                                                            <div class="row">
	                                                                <div class="col-12">
	                                                                    <h5 class="title_sub"> Pickup</h5>
	                                                                </div>

	                                                                @if(empty($hotel_list))

	                                                                	<div class="col-12 col-sm-12 col-md-12 mt-3">
	                                                                        <div class="fildes_outer">
	                                                                            <label>Complimentary Pickup *</label>
	                                                                            <input type="text" name="pickup-{{$tourGrade['gradeCode']}}" class="form-control compli-pickup-{{$tourGrade['gradeCode']}}" aria-describedby="emailHelp" placeholder="Complimentary Pickup" required value="{{@$data[$tourGrade['gradeCode']][2]['hotelPickups']['pickupPoint']}}">
	                                                                        </div>
	                                                                        <label for="pickup-{{$tourGrade['gradeCode']}}" generated="true" class="error"></label>
	                                                                    </div>

	                                                                @else
	                                                                    <div class="col-12 col-sm-4 col-md-3 mt-3">
	                                                                        <div class="custom-control custom-radio">
	                                                                            <input name="location-select" id="location2_{{$tourGrade['gradeCode']}}" type="radio" class="custom-control-input location-select location-select-{{$tourGrade['gradeCode']}}" data-tourCode="{{$tourGrade['gradeCode']}}" value="1" <?php echo ((!empty($hotelId)) && $btn_value != 'Add Activity') ? "checked" : "" ?>>
	                                                                            <label class="custom-control-label" for="location2_{{$tourGrade['gradeCode']}}"> Choose Location</label>
	                                                                        </div>
	                                                                    </div>

	                                                                    <?php 
	                                                                    $display = "none";
	                                                                    if(isset($hotelId) && $btn_value != 'Add Activity')
	                                                                    {
	                                                                    	$display = "block";
	                                                                    }

	                                                                    ?>
	                                                                    <div class="col-12 col-sm-3 col-md-9 choose_location_{{$tourGrade['gradeCode']}}" style="display: <?php echo $display ?>">
	                                                                        <div class="fildes_outer">
	                                                                            <label>Locations *</label>
	                                                                        	<div class="custom-select">                          	
	                                                                                <select name="pickup-{{$tourGrade['gradeCode']}}" class="pickup-{{$tourGrade['gradeCode']}} pickup-hotel" data-tourCode="{{$tourGrade['gradeCode']}}" required="">
	                                                                                	<option value="">Select Location</option>
	                                                                                	@foreach($hotel_list as $hotel)
	                                                                                		<?php
	                                                                                		$hotel_name = ($hotel['address'] != null ) ? $hotel['name'] ." , ". $hotel['address'] : $hotel['name'];
	                                                                                		?>
	                                                                                		<option value="{{$hotel['id']}}" <?php if(@$hotelId == $hotel['id'] && $btn_value != 'Add Activity') {?> selected <?php }?>>{{$hotel_name}}</option>
	                                                                                	@endforeach
	                                                                                </select>
	                                                                            </div>
	                                                                        </div>
	                                                                        <label for="pickup-{{$tourGrade['gradeCode']}}" generated="true" class="error"></label>
	                                                                    </div>
	                                                                    <?php 
	                                                                    $displayl = "none";
	                                                                    if(isset($hotelId) && $hotelId == "notListed")
	                                                                    {
	                                                                    	$displayl = "block";
	                                                                    }
	                                                                    ?>
	                                                                    <div class="col-12 col-sm-12 col-md-12 mt-3" id="notListed_{{$tourGrade['gradeCode']}}" style="display: <?php echo $displayl ?>">
	                                                                    	<div class="fildes_outer ">
	                                                                            <label>Enter Pickup Location *</label>
	                                                                            <input type="text" name="manual-pickup-{{$tourGrade['gradeCode']}}" value="<?php echo @$data[$tourGrade['gradeCode']][2]['hotelPickups']['pickupPoint']; ?>" class="form-control manual-pickup-{{$tourGrade['gradeCode']}}" required minlength="20">
	                                                                        </div>
	                                                                        <label for="manual-pickup-{{$tourGrade['gradeCode']}}" generated="true" class="error"></label>
	                                                                    </div>
	                                                                    <div class="col-12 col-sm-12 col-md-12 mt-3">
	                                                                        <div class="custom-control custom-radio">
	                                                                            <input name="location-select" id="location_{{$tourGrade['gradeCode']}}" type="radio" class="custom-control-input location-select location-select-{{$tourGrade['gradeCode']}}" data-tourcode="{{$tourGrade['gradeCode']}}" value="0" <?php echo (!empty($hotelId) && $btn_value != 'Add Activity') ? "" : "checked" ?>>
	                                                                            <label class="custom-control-label" for="location_{{$tourGrade['gradeCode']}}"> I don't need to be picked up</label>
	                                                                        </div>
	                                                                    </div>
	                                                                @endif
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                            		@endif

	                                            		@if($data['bookingQuestions'])
	                                            		<div class=" mt-4 ml-2">
	                                                        <div class="row">
	                                                        	@foreach($data['bookingQuestions'] as $key => $bookingq)
	                                                        		@if($bookingq['questionId'] != 23)
	                                                        			<div class="col-12 col-sm-3 col-md-3">
	                                                                        <div class="fildes_outer">
	                                                                        	<label>{{$bookingq['title']}} *</label>
	                                                                        	<input type="text" class="form-control booking_{{$tourGrade['gradeCode']}}" 
	                                                                        	data-tourCode="{{$tourGrade['gradeCode']}}" 
	                                                                        	name="booking_{{$tourGrade['gradeCode']}}_{{$bookingq['stringQuestionId']}}"
	                                                                        	data-question="{{$bookingq['questionId']}}" value="{{@$data[$tourGrade['gradeCode']]['0']['bookingQuestionAnswers'][$key]['answer']}}" required aria-describedby="emailHelp" >
	                                                                        </div>
	                                                                        <label for="booking_{{$tourGrade['gradeCode']}}_{{$bookingq['stringQuestionId']}}" generated="true" class="error"></label>
	                                                                    </div>
	                                                        		@endif
	                                                        	@endforeach
	                                                        </div>
	                                                    </div>
	                                            		@endif

	                                            		<div class="col-12">
	                                                        <div class="fildes_outer my-3">
	                                                            <label>Special Requirements</label>
	                                                            <textarea name="special_req_{{$tourGrade['gradeCode']}}" rows="4" class="form-control mt-3 special_req_{{$tourGrade['gradeCode']}}" value="Special Requirements" ><?php echo ($btn_value != 'Add Activity') ? @$data[$tourGrade['gradeCode']][1]['specialRequirement'] : "";?></textarea>
	                                                        </div>
	                                                        <label for="special_req_{{$tourGrade['gradeCode']}}" generated="true" class="error"></label>
	                                                    </div>

		                                            	<div class="col-12">	                                            		
					                                            <div class="form-group">
					                                            	<input type="hidden"  {{$data['attributes']}} class="datetimepick" id="view_datepick-{{$data['code']}}-{{$tourGrade['gradeCode']}}" value="">
					                                            	<input type="hidden"  class="datetimepick" id="datepick-{{ $data['code']}}-{{$tourGrade['gradeCode']}}" data-provider="{{$provider}}" data-city-id="{{$data['cityid']}}" data-index="{{$code}}" data-activity-id="{{$code}}" data-is-selected="{{$select_act}}" data-price="{{$tourGrade['merchantNetPrice']}}"  data-price-id=""  data-name="{{$data['activity_name']}}" data-currency="{{session()->get( 'search' )['currency']}}" data-startdate="{{$data['startDate']}}" data-enddate="{{$data['endDate']}}" data-description="{{$data['description']}}" data-images="" data-duration="{{$data['duration']}}" data-label="" data-bookdate = "{{$data['bookingDate']}}" data-merchantNetPrice="{{$tourGrade['merchantNetPrice']}}" data-tour-code="{{$tourGrade['gradeCode']}}" data-tourgrade-title="{{$tourGrade['gradeTitle']}}" data-adult="{{(isset($adultTotal)) ? $adultTotal : 0}}" data-child="{{(isset($childTotal)) ? $childTotal : 0}}" data-infant="{{(isset($infantTotal)) ? $infantTotal : 0}}" data-cancellation-policy="{{$data['termsAndConditions']}}"  value="">

					                                            	<?php if($btn_value == "Remove Activity") {?>
					                                            	<button type="button" class="btn btn-white transform w-25 act-btn act-update-btn mr-2 {{$btnClass}}" data-view= "view" data-provider="{{$provider}}" data-city-id="{{$data['cityid']}}" data-index="{{$code}}" data-activity-id="{{$code}}" data-is-selected="{{$select_act}}" data-price="{{$tourGrade['merchantNetPrice']}}"  data-price-id=""  data-name="{{$data['activity_name']}}" data-currency="{{session()->get( 'search' )['currency']}}" data-startdate="{{$data['startDate']}}" data-enddate="{{$data['endDate']}}" data-description="{{$data['description']}}" data-images="" data-duration="{{$data['duration']}}" data-label="" data-bookdate = "{{$data['bookingDate']}}" data-tour-code="{{$tourGrade['gradeCode']}}" data-button-id="{{$data['code']}}" data-merchantNetPrice="{{$tourGrade['merchantNetPrice']}}" data-tourgrade-title="{{$tourGrade['gradeTitle']}}" data-adult="{{(isset($adultTotal)) ? $adultTotal : 0}}" data-child="{{(isset($childTotal)) ? $childTotal : 0}}" data-infant="{{(isset($infantTotal)) ? $infantTotal : 0}}" data-cancellation-policy="{{$data['termsAndConditions']}}" data-weight-validate="{{$validate_weight}}" data-pickup="{{$data['hotelPickup']}}" data-akey="{{$data['key']}}" data-leg="{{$data['leg']}}">UPDATE ACTIVITY</button>
					                                            <?php }?>
					                                                <button type="button" class="btn btn-white transform w-25 act-btn act-select-btn {{$btnClass}}" data-view= "view" data-provider="{{$provider}}" data-city-id="{{$data['cityid']}}" data-index="{{$code}}" data-activity-id="{{$code}}" data-is-selected="{{$select_act}}" data-price="{{$tourGrade['merchantNetPrice']}}"  data-price-id=""  data-name="{{$data['activity_name']}}" data-currency="{{session()->get( 'search' )['currency']}}" data-startdate="{{$data['startDate']}}" data-enddate="{{$data['endDate']}}" data-description="{{$data['description']}}" data-images="" data-duration="{{$data['duration']}}" data-label="" data-bookdate = "{{$data['bookingDate']}}" data-tour-code="{{$tourGrade['gradeCode']}}" data-button-id="{{$data['code']}}" data-merchantNetPrice="{{$tourGrade['merchantNetPrice']}}" data-tourgrade-title="{{$tourGrade['gradeTitle']}}" data-adult="{{(isset($adultTotal)) ? $adultTotal : 0}}" data-child="{{(isset($childTotal)) ? $childTotal : 0}}" data-infant="{{(isset($infantTotal)) ? $infantTotal : 0}}" data-cancellation-policy="{{$data['termsAndConditions']}}" data-weight-validate="{{$validate_weight}}" data-pickup="{{$data['hotelPickup']}}" >{{$btn_value}}</button>


					                                            </div> 
		                                            	</div>
	                                            	</form>	                                            	
                                            	</div>
                                            	@endif
			                                </div>
		                              	</div> 
	                              	@php
	                              	}
                              	}
                              	else
                              	{
	                              	@endphp
	                              		<div class="card panel border-0 p-3 mb-3">
	                              			<center><h4>No Activity Available</h4></center>
	                              		</div>
	                              	@php	
                              	}
                              	@endphp
                            </div>
                        </div>
                    </div>
                </div>                
			</div>
		</div>
	</div>
</div>	
				<!--- comment -->
				<?php /*
				<div class="border-bottom border-top   pt-1 mt-3">
					<ul class="nav nav-tabs border-0 active_detiles_page" id="myTab" role="tablist">
						<li class="nav-item ">
							<a class="nav-link active border-right-0 border-top-0 border-left-0  " id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="home" aria-selected="true">
							@lang('home.overview_text_heading')
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link border-right-0 border-top-0 border-left-0" id="importantinfo-tab" data-toggle="tab" href="#importantinfo" role="tab" aria-controls="profile" aria-selected="false">@lang('home.important_info') </a>
						</li>

					</ul>
				</div>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
						<div class="pt-4 pr-3 pl-3 pb-3 ">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-9">
									<p><strong>@lang('home.description')</strong> </p>
									<p>{!! $data['activity_description'] !!}</p>
									<?php if($data['highlights']){ ?> 
										<p class="m-b-0"><strong>@lang('home.highlights') </strong></p>
										<ul class="m-l-30">
											<?php foreach ($data['highlights'] as $key => $value) { ?>
												<li><?php echo $value?></li>
											<?php } ?>
										</ul>
									<?php } ?>

									<?php if($data['description']){ ?> 
										<p class="m-b-0"><strong>@lang('home.what_you_can_expect_text') </strong></p>
										<p><?php echo $data['description']; ?></p>
									<?php } ?>

									

									<?php if($data['itinerary']){ ?> 
										<p class="m-b-0"><strong>@lang('home.itinerary') </strong></p>
										<p><?php echo $data['itinerary']; ?></p>
									<?php } ?>
								</div>
								
								<?php echo $selectDiv; ?>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="importantinfo" role="tabpanel" aria-labelledby="importantinfo-tab">
						<div class="pt-4 pr-3 pl-3 pb-3 ">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-9">
									<p><strong> @lang('home.profile_details_text')</strong></p>
									<?php if($data['inclusions']){ ?> 
										<p class="mb-0"><strong>@lang('home.inclusions')</strong></p>
											<?php foreach ($data['inclusions'] as $key => $value) { ?>
												<?php echo $value."<br>";?>
											<?php } ?>
									<?php } ?>
									<br>
									<?php if($data['exclusions']){ ?> 
										<p class="m-b-0"><strong>@lang('home.exclusions')</strong></p>
										<?php foreach ($data['exclusions'] as $key => $value) { ?>
											<li><?php echo $value?></li>
										<?php } ?>
									<?php } ?>
									<br>
									<?php if($data['additionalInfo']){ ?> 
										<p class="mb-0"><strong>@lang('home.additional_info')</strong></p>
										<ul class="ml-4">
											<?php foreach ($data['additionalInfo'] as $key => $value) { ?>
												<li><?php echo $value?></li>
											<?php } ?>
										</ul>
									<?php } ?>
									<br>
									<?php if($data['voucherRequirements']){ ?> 
										<p class="m-b-0"><strong>@lang('home.voucher_info')</strong></p>
										<p><?php echo $data['voucherRequirements']; ?></p>
									<?php } ?>

									
									<?php if($data['departurePoint']){ ?> 
										<p class="m-b-0"><strong>@lang('home.departure_point')</strong></p>
										<p><?php echo $data['departurePoint']; ?></p>
									<?php } ?>

									<?php if($data['departureTime']){ ?> 
										<p class="m-b-0"><strong>@lang('home.departure_time')</strong></p>
										<p><?php echo $data['departureTime'].'<br>'.$data['departureTimeComments']; ?></p>
									<?php } ?>

									<?php if($data['duration']){ ?> 
										<p class="m-b-0"><strong>@lang('home.duration_text')</strong></p>
										<p><?php echo $data['duration']; ?></p>
									<?php } ?>

									<?php if($data['returnDetails']){ ?> 
										<p class="m-b-0"><strong>@lang('home.return_details')</strong></p>
										<p><?php echo $data['returnDetails']; ?></p>
									<?php } ?>
								</div>
								<?php echo $selectDiv; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> */
?>


<script>
	 $(document).ready(function() {
		var owl = $('.owl-carousel');
		owl.owlCarousel({
			margin: 10,
			loop: false,
			dots: true,
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 3
				},
				1000: {
					items: 1
				}
			}
		});		
	});	
</script>