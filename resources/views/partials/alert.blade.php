<!-- START ALERT -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="eroamConfirm" id="eroam-confirm">
    <div class="modal-dialog eroam-confirm-modal-size" role="document" >
        <div class="modal-content">
            <div class="modal-header eroam-confirm-header">
                <h4 class="modal-title eroam-confirm-title" id="gridSystemModalLabel"></h4>
                <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <!-- <div class="row">
                    <div class="col-md-12 eroam-confirm-content-close" style="display:none;">
                        <p>
                            <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
                        </p>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-md-12 eroam-confirm-content">
                    </div>
                </div>
            </div>
            <div class="border-top pt-3 text-center pb-3">
                <button type="button" class="btn btn-white confirm-ok-button">OK</button> &nbsp; 
                <button type="button" class="btn btn-white confirm-cancel-button eroam-confirm-cancel-btn">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->