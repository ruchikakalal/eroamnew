<script type="text/javascript">
	
	var activitySequence = ['{{ join(', ', $interest_ids) }}'];
	
	function allowDrop(ev) {
	    ev.preventDefault();
	}
	function drag(ev) {
	    ev.dataTransfer.setData("text", ev.target.id);
	    var data = ev.target.id;
		if($("#"+data).parent().parent().prop('className') == 'drop-list'){
	      var interest = data.replace('drag_', 'interest-');
	      $(".interestMove").css('opacity','0.5');
	      $("#"+interest+".interestMove").css('opacity','');
	    }
	}

	function drop(ev) { 
	    ev.preventDefault();
	    var data = ev.dataTransfer.getData("text");
	    var name = data.replace('drag', 'name');
	    var inputValue = data.replace('drag', 'inputValue');
	    var interest = data.replace('drag_', 'interest-');
	    var x = document.getElementById(name);
	    var count = 0;
	    var text = '';
		
	    var data_value = document.getElementById(data).getAttribute("data-value");
	    var data_name = document.getElementById(data).getAttribute("data-name");
	    data_value = parseInt(data_value);
	    var parentClass = $("#"+data).parent().parent().prop('className');
	    var ClassName = $("#"+data).parent().prop('className');

	    if(ClassName != "interest-button-active"){
	      if($("."+ev.target.getAttribute("class")).parent().prop('className') == 'drop-list'){
	          if (x.style.display === 'none') {
	              x.style.display = 'block';
	          } else {
	              x.style.display = 'none';
	          }
	          
	          var hidden_interest_field = $("#"+data).parent().find('.input-interest');
	          if( hidden_interest_field.val() ){
	            var interestIndex = activitySequence.indexOf(data_value);
	            activitySequence.splice(interestIndex, 1);
	            hidden_interest_field.val('');
	            hidden_interest_field.attr('name', 'interests[]');
	          } else {
	            activitySequence.push(data_value);
	            var interestIndex = activitySequence.indexOf(data_value);
	            hidden_interest_field.val(data_value);
	            hidden_interest_field.attr('name', 'interests[]');
	          } 

	          ev.target.appendChild(document.getElementById(data));
	          ev.target.appendChild(document.getElementById(inputValue));
	          var parentDiv = $("#"+data).parent();
	          parentDiv.attr('id', interest); 
	          parentDiv.attr('data-name', data_name); 

	          $("#"+data).parent().removeClass('blankInterest').addClass('interest-button-active');
	      }

	    } else {
	      if(ev.target.getAttribute("class") == 'blankInterest' && $("."+ev.target.getAttribute("class")).parent().prop('className') == 'drop-list'){

	          var interestIndex = activitySequence.indexOf(data_value);
	          activitySequence.splice(interestIndex, 1);
	          activitySequence.push(data_value);

	          var hidden_interest_field = $("#"+data).parent().find('.input-interest');
	          hidden_interest_field.attr('name', 'interests[]');  

	          ev.target.appendChild(document.getElementById(data));
	          ev.target.appendChild(document.getElementById(inputValue));
	          $("#"+interest).removeClass('interest-button-active').addClass('blankInterest').removeAttr('data-name');
	          $("#"+interest).removeAttr('id');
	          $("#"+data).parent().attr('id',interest).attr('data-name',data_name);
	          $("#"+data).parent().removeClass('blankInterest').addClass('interest-button-active');
	      } else {
	        if(parseInt(ev.target.getAttribute("data-value")) == data_value){
	          var interestIndex = activitySequence.indexOf(data_value);
	          activitySequence.splice(interestIndex, 1);
				if(x.style.display == 'block' ){
					x.style.display = 'none';
				}else{
					x.style.display = 'block';
				}
	          var hidden_interest_field = $("#"+data).parent().find('.input-interest');
	          hidden_interest_field.val('');
	          hidden_interest_field.attr('name', 'interests[]');  
	          $(".interestMove").css('opacity','');
			
	          ev.target.appendChild(document.getElementById(data));
	          ev.target.appendChild(document.getElementById(inputValue));

	          $("#"+interest).removeClass('interest-button-active').addClass('blankInterest');
	          $("#"+interest).removeAttr('id');
	          x.style.display = 'none';
	        } 
	      }
	    } 
		console.log(activitySequence);
	}	
</script>