<!--City  Modal -->
<div class="modal fade" id="cityModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-city">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Select Departure City</h4>
            </div>
            <div class="modal-body">
                <p class="m-t-10">Select your departure city to see the tour flight inclusive prices. </p>
                <form class="form-horizontal m-t-20">
                    <div class="panel-form-group">
                        <label class="label-control">Select Departure Location</label>
                        <select class="form-control" name="default_selected_city" id="default_selected_city">
                            <?php $default_selected_city = session()->get( 'default_selected_city' );?>
                            <option value="30" <?php if($default_selected_city == '30'){ echo 'selected';}?>>Melbourne, AU</option>
                            <option value="7" <?php if($default_selected_city == '7'){ echo 'selected';}?>>Sydney, AU</option>
                            <option value="15" <?php if($default_selected_city == '15'){ echo 'selected';}?>>Brisbane, AU</option>
                        </select>
                    </div>
                </form>
                <div class="m-t-40 text-right">
                    <a href="javascript://" id="decline_city" data-dismiss="modal" class="m-r-10 modal-link">DECLINE</a>
                    <a href="javascript://" class="modal-link" id="accept_city">ACCEPT</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Close City Modal --> 