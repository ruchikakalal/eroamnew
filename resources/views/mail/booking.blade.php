<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>eroam</title>
    </head>
    <body style="margin: 0px; background: #e3e5e7;">
        <?php 
            $logo = getDomainLogo();
            if(!$logo) {
                $logo = "http://dev.eroam.com/images/email-logo-footer.png";
            }
        ?>
        <table width="100%" style="background: #e3e5e7">
            <tr>
                <td>
                    <table style="margin:0 auto; background: #ffffff;" width="760px" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th style="background-color: #212121; padding-top:20px; padding-bottom: 20px; text-align: center;">
                                <a href="#"><img src="<?php echo $logo; ?>" width="180px" alt="" /> </a>
                            </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="background: #fafafa; border-bottom: 1px solid #f0f0f0; padding-top: 5px; padding-bottom: 5px;">    
                                    <table width="100%">                    
                                        <tr>
                                            <td style="text-align: center">
                                                 <p style="font-family: Arial; font-size: 18px; color: #212121; margin-top: 0px; margin-bottom: 0px;  "><strong> Hi, User!</strong></p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="90%;" style="margin: 0 auto;">
                                        <tbody>
                                            <tr>
                                                <td style="padding: 0;padding-top: 10px; line-height: 1.42857143; vertical-align: top;font-family: Arial; font-size: 14px;"><strong>Tour Booking Information</strong></td>
                                            </tr>
                                            <tr>
                                                <td style="font-family: Arial; font-size: 14px; ">
                                                    <p>We have received your funds and it is being processed by Adventure Travel. One of our friendly staff will contact you within the next 24 hours with your confirmation number.</p>
                                                </td>
                                            </tr>                                            
                                            <tr>
                                                <td>
                                                    <table border="0" width="100%" cellspacing="2" cellpadding="2" style="padding: 15px; position:relative; margin:0 auto; background:#FFFFFF; border:1px solid #efefef;font-size:14px;font-family: Arial;">
                                                        <tr>
                                                            <td colspan="2">
                                                                <h4 style="font-size: 14px; line-height: 22px; font-weight: 500; margin-top: 0px; margin-bottom: 20px;"><strong>Personal Details</strong></h4>
                                                            </td>
                                                        </tr>         
                                                        <tr>
                                                            <td width="50%"><strong>Customer Name :</strong> {{ $name}}</td>
                                                            <td width="50%"><strong>Customer Email :</strong> {{ $email}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%"><strong>Customer Contact :</strong> {{$contact}}</td>
                                                            
                                                        </tr>         
                                                    </table>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    <table border="0" width="100%" cellspacing="2" cellpadding="2" style="padding: 15px; position:relative; margin:0 auto; background:#FFFFFF; border:1px solid #efefef;font-size:14px;font-family: Arial;">
                                                        <tr>
                                                            <td colspan="2">
                                                                <h4 style="font-size: 14px; line-height: 22px; font-weight: 500; margin-top: 0px; margin-bottom: 20px;"><strong>Tour Details</strong></h4>
                                                            </td>
                                                        </tr>         
                                                        <tr>
                                                            <td width="50%"><strong>Tour Name :</strong> {{ $tour_title}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%"><strong>Tour Booking Id :</strong> {{ $booking_id }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%"><strong>Tour Code :</strong> {{$data['code']}}</td>
                                                            <td width="50%"><strong>Travellers :</strong> {{$total_pax}}</td>
                                                        </tr>  
                                                        <tr>
                                                            <td width="50%"><strong>Countries :</strong> @isset($data['countries']){{$data['countries']}}@endisset</td>
                                                            <td width="50%"><strong>Duration :</strong> <?php echo ($data['days'] >1) ? $data['days']." Days" :"1 Day"; ?>
                                                            </td>
                                                        </tr>     
                                                        <tr>
                                                            <td width="50%"><strong>Start :</strong> {{ date("j M Y",strtotime($data['start_date'])) }}</td>
                                                            <td width="50%"><strong>Finish :</strong> {{ date("j M Y",strtotime($data['finishDate'])) }}</td>
                                                        </tr>  
                                                        <tr>
                                                            <td><strong>Total Amount :</strong> AUD {{ number_format($total_amount,2) }}</td>
                                                        </tr>  
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td style="background-color: #394951; padding-top:6px; padding-bottom: 6px; padding-right: 6px; padding-left: 6px; text-align: center;">
                                    <table style="width: 100%;">

                                        <tr>
                                            <td style="font-family: Arial; font-size: 14px;"> <a href="#"><img src="<?php echo $logo; ?>" alt="" /> </a></td>
                                            <td style="color: #fff;font-size: 12px; text-align: right; font-family: Arial; ">Powered by eRoam &copy; Copyright 2018 - 2019. All Rights Reserved. Patent pending AU2016902466</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>