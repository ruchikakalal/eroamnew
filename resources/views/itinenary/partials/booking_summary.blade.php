@php

    if(isset($_SERVER['HTTP_REFERER'])){
        $legkey = explode('?', $_SERVER['HTTP_REFERER']);
        $ex = explode('/',$_SERVER['HTTP_REFERER']);
        $show_blue_view_itinerary = in_array('view-itinerary',$ex) ? true :false;

        if(in_array('payment-summary',$ex) || in_array('review-itinerary',$ex) || in_array('signin-guest-checkout',$ex)){
            $show_blue_booking = true;
        }else{
            $show_blue_booking = false;
        }
    }
    if (isset($legkey) && count($legkey) > 1) {
        $legkey = explode('&', $legkey[1]);
        $legkey = explode('=', $legkey[0]);
        $legkey = $legkey[1];
    }else{
        $legkey = 0;
    }

    $preUrl = url()->previous();
    $isMap = 0;

    if (substr($preUrl, -3) == 'map') {
        $isMap = 1;
    }

    $preUrl = str_replace(url('') . '/', '', $preUrl);
    $countcity = count(session()->get('search') ['itinerary']) - 1;
    $last = count(session()->get('search') ['itinerary']) - 1;
    $temp_data = session()->get('search');

    if (isset($temp_data['travel_date']) && !empty($temp_data['travel_date'])) {
        $departure_date = session()->get('search') ['travel_date'];
    }else {
        $departure_date = date('d-m-Y');
    }

    if (isset($temp_data['itinerary'][$last]['hotel']['checkout'])) {
        $return_date = session()->get('search') ['itinerary'][$last]['hotel']['checkout'];
    }elseif (isset($temp_data['itinerary'][$last]['activities'])) {
        $count_last = count(session()->get('search') ['itinerary'][$last]['activities']) - 1;

        if ($count_last == '-1') {
            $return_date = date('d-m-Y');
        }else{
            $return_date = session()->get('search') ['itinerary'][$last]['activities'][$count_last]['date_selected'];
        }
    }else {
        $return_date = date('d-m-Y');
    }

    $datetime1 = new DateTime($departure_date);
    $datetime2 = new DateTime($return_date);
    $interval = $datetime1->diff($datetime2);

    $day = explode(" ", session()->get('search') ['total_number_of_days']);
    $days = $day[0];
@endphp
<?php
    $getCurrentEndDate = date('d-m-Y');
    $getCurrentEndDate = date('d-m-Y');
    if(!empty(session()->get('search')['itinerary'])){
     $getStartDate        = session()->get('search')['itinerary'];
     $getCurrentStartDate = end($getStartDate)['city']['date_from'];
     $getCurrentEndDate   = end($getStartDate)['city']['date_to'];
    }
?>
<!--Do not remove below code -->
<input type="hidden" value="itsworkingnow">
<!--Do not remove above code -->
<form action="" class="itinerary_left mb-0">
    <div class="section_title mt-2 mb-3"><i class="ic-event_note"></i>&nbsp; @lang('home.booking_summary')</div>
    <div class="itinerary_left_box pb-0">
        <div class="p-2 totlaouter">
            <div class="itinerary_box_sort_title mb-2">@lang('home.price_and_travllers')</div>
            <div class="itinerary_box_main_title mt-1">{{ session()->get( 'search' )['currency'].' '.session()->get( 'search' )['cost_per_day'] }} @lang('home.per_day')<small class="mt-1 mb-1">@lang('home.map_label1') {{ session()->get( 'search' )['currency'].' '.session()->get( 'search' )['cost_per_person'] }} @lang('home.per_person_taxes')</small></div>
            <div class="day_box">{{$days}}<br> <?php echo ($days > 1) ? 'DAYS':'DAY' ?> </div>
        </div>
        <?php
            $travellers = session()->get('search')['travellers'];
            $total_childs = session()->get('search')['child_total'];
            $rooms = session()->get('search')['rooms'];
        ?>
    </div>
    <div class="itinerary_left_box pb-0">
        <div class="fields_sec p-2 box_none">
    
        <div class="itinerary_box_sort_title mb-2 collapse_click collapsed " id="trip_details" data-toggle="collapse" data-target="#details"> <i class="ic-up_down"></i>&nbsp;  Trip Details </div>

        <div class="itinerary_left_box box_none collapse" id="details">
 
            <div class="row">
                <div class="col-12 form-group">
                    <div class="fildes_outer p-2">
                        <label>@lang('home.rooms')</label>
                        <div class="custom-select">
                            <select class="form-control disabled" readonly data-toggle="tooltip" aria-describedby="ui-id-31" data-placement="bottom" data-original-title="@lang('home.create_itenerary_checkbox3_title')">
                                <option value="{{$rooms}}">{{$rooms}} @if($rooms > 1) Rooms @else Room @endif</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 form-group">
                    <div class="fildes_outer p-2">
                        <label>@lang('home.adults')</label>
                        <div class="custom-select">
                            <select class="form-control disabled" readonly data-toggle="tooltip" data-original-title="@lang('home.create_itenerary_checkbox3_title')">
                                <option value="{{$travellers}}" >{{$travellers}} @if ($travellers > 1)
                                        Adults
                                        @else
                                        Adult
                                        @endif</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 form-group">
                    <div class="fildes_outer p-2">
                        <label>@lang('home.childrens')</label>
                        <div class="custom-select">
                        
                            <select class="form-control disabled" readonly data-toggle="tooltip" title="@lang('home.create_itenerary_checkbox3_title')">
                                <option value="{{$total_childs}}">@if($total_childs != 0){{$total_childs}} @endif 
                                        @if($total_childs == 0)
                                        No Child
                                        @elseif($total_childs == 1)
                                        Child
                                        @elseif($total_childs > 1)
                                        Children
                                        @endif</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                    <div class="fildes_outer ">
                        <label>@lang('home.total_trip_duration')  </label> 
                        <input type="text" class="form-control disabled" data-toggle="tooltip" readonly title="@lang('home.create_itenerary_checkbox3_title')" value="{{session()->get('search')['total_number_of_days']}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 form-group">
                    <div class="fildes_outer">
                        <label>@lang('home.departure_date')</label>
                        <input id="departure_dates" class="disabled form-control" readonly type="text" placeholder="31 Jan 2018" class="form-control" value="{{date('d M Y',strtotime($departure_date))}}" data-toggle="tooltip" title="@lang('home.create_itenerary_checkbox3_title')">
                        <span class="arrow_down"><i class="ic-expand_more"></i> </span>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 form-group">
                    <div class="fildes_outer">
                        <label> @lang('home.return_date')</label>
                        <input id="return_dates" type="text" readonly class="disabled form-control" placeholder="11 Feb 2017" class="form-control" value="{{date('d M Y',strtotime($getCurrentEndDate))}}" data-toggle="tooltip" title="@lang('home.create_itenerary_checkbox3_title')">
                        <span class="arrow_down"><i class="ic-expand_more"></i> </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                     <div class="meaages_left">
                        <p class="pt-4 mb-2 w-100">@lang('home.map_paragraph')</p>
                        
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
    <div class="itinerary_left_btns_sec mt-2 mb-2">
        @if (session()->get('map_data')['type'] == 'manual' && count(session()->get('map_data')['cities']) > 2 && session()->get( '_previous' )['url'] == url( 'map' ))
        <a href="#" id="reorder-locations-btn" class="btn btns_input_dark transform d-block w-100 mt-4"><i class="fa fa-reorder m-r-10"></i> @lang('home.reorder_location_txt') </a>
        @endif
    </div>
    <div class="itinerary_left_btns_sec mt-2 mb-2">                    
        <a href="{{ url( 'view-itinerary' ) }}" class="btn @if($show_blue_view_itinerary)btns_input_blue_active @else btns_input_white @endif transform d-block w-100">@lang('home.map_itinerary_detail_link')</a>

        <!-- <a href="javascript:void(0)" class="btn @if($show_blue_view_itinerary)btns_input_blue_active @else btns_input_white @endif transform d-block w-100 disable_item_custom">@lang('home.map_itinerary_detail_link')</a> -->
    </div>
    <div class="itinerary_left_btns_sec mt-2 mb-2">                    
        <a href="{{ url( 'payment-summary' ) }}" class="btn @if($show_blue_booking) btns_input_blue_active @else btns_input_dark @endif transform d-block w-100">@lang('home.map_book_itinerary_link')</a>
    </div>
   
    <div class="clearfix"></div>
    <div class="section_title mt-4 mb-3"><i class="icon icon-itinerary">&nbsp; </i>@lang('home.itinerary_summary')</div>
    <div class="itinerary-container">
        @if (session()->get('search'))
        <div id="accordion">
        @foreach (session()->get('search')['itinerary'] as $key => $leg)
            @php
                $itineraryIndex = $key == $last ? $key : $key + 1;
                $departTimezone = get_timezone_abbreviation($leg['city']['timezone']['name']);
                $arriveTimezone = get_timezone_abbreviation(session()->get('search')['itinerary'][$itineraryIndex]['city']['timezone']['name']);
                $rooms = session()->get('search')['rooms'];
                //echo "<pre>";print_r($rooms);exit;
            @endphp
                <div class="itinerary_left_box pb-0" id="tinerary-panel-leg-{{$key}}">
                    <div class="itinerary_box_sort_title pl-2 pt-2 pr-2 collapse_click mb-2 trip_summary_first collapsed" data-toggle="collapse" data-target="#demo{{$key}}"><i class="ic-up_down"></i>&nbsp; {{ trim(str_limit($leg['city']['name'], 23)) }}, {{ $leg['city']['country']['name'] }} </div>
                    <div class="fields_sec pl-2 pt-2 pr-2">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6 form-group">
                                <div class="fildes_outer" >
                                    <label>@lang('home.check_in_date'):</label>
                                    <input id="start_date_{{ $key }}" type="text" data-leg="{{$key}}" data-cityId="{{ $leg['city']['id'] }}" data-fromDate="{{ $leg['city']['date_from'] }}" data-toDate=" {{ $leg['city']['date_to'] }}" data-nights="{{ $leg['city']['default_nights'] }}" placeholder="{{ date( 'j M Y', strtotime( $leg['city']['date_from'] ) ) }}" class="form-control start_date" value="{{ date( 'j M Y', strtotime( $leg['city']['date_from'] ) ) }}">
                                    <span class="arrow_down"><i class="ic-expand_more"></i> </span>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 form-group">
                                <div class="fildes_outer">
                                    <label>@lang('home.lenght_of_stay'):</label>
                                    <div class="custom-select">
                                        <select class="form-control changeNights" id="changeNights{{$key}}" data-leg="{{$key}}" data-cityId="{{ $leg['city']['id'] }}" data-fromDate="{{ $leg['city']['date_from'] }}" data-toDate=" {{ $leg['city']['date_to'] }}">
                                        @for($i = 1; $i < 11; $i++)
                                        <option value="{{$i}}" {{ ($leg['city']['default_nights'] == $i) ? 'selected' : '' }} > {{ $i }} {{ ($i == 1) ? 'Night': 'Nights' }}</option>
                                        @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="night-data-{{ $key }}" value="{{ $leg['city']['default_nights'] }}">
                        </div>
                    </div>
					<div class="itinerary_left_box collapse box-shadow-0 mb-0" id="demo{{$key}}" data-parent="#accordion">
						@php $current_url = url()->previous(); 
							$arr = explode('/',$current_url);
							$open_city_name = isset($arr)?$arr[3]:'';
							$arr2 =  isset($arr[4])?explode('=',$arr[4]):[];
							$open_task = isset($arr2[0])?explode('?',$arr2[0])[0]:'';
							$open_leg = isset($arr2[1])?$arr2[1]:'';
						@endphp
						<div class="accommodation_outer p-2 mb-0 border-bottom @php echo ((str_replace('%20',' ',$open_city_name) == $leg['city']['name']) && ($key == $open_leg) && ($open_task == 'hotels')) ? 'default_color' : '' @endphp">
							<div class="accommodation" data-type="hotels" data-link="{{ url( $leg['city']['name'].'/hotels?leg='.$key ) }}">
								<div class="accommodation_icon"><i class="ic-local_hotel"></i></div>
								<div class="accommodation_info">
								
									@if( $leg['hotel'] )
									<div class="itinerary_box_sort_title">@lang('home.accommodation_summary')</div>
									<p class="mb-1">{{ $leg['hotel']['name'] }}</p>
									<ul>
										<li>
											<div class="title">
												<strong>@lang('home.check_in'):</strong> 
											</div>
											<div class="info">{{ date('j M Y', strtotime( $leg['city']['date_from'] ) ) }}</div>
										</li>
										<li>
											<div class="title">
												<strong>@lang('home.check_out'):</strong> 
											</div>
											<div class="info">{{ date('j M Y', strtotime( $leg['city']['date_to'] ) ) }}</div>
										</li>
										<li>
											<div class="title">
												<strong>@lang('home.duration')</strong> 
											</div>
											@if ($leg['hotel']['nights'] <= 1)
											<div class="info">{{ $leg['hotel']['nights'] }} @lang('home.night')</div>
											@else
											<div class="info">{{ $leg['hotel']['nights'] }} Nights</div>
											@endif
										</li>
										<li>
											<div class="title">
												<strong>@lang('home.price')</strong> 
											</div>
											<div class="info">AUD 
                                                <?php
                                            if($leg['hotel']['provider'] == 'expedia'){
                                                $RateInfo = json_decode(json_encode($leg['hotel']),true);
                                                    $eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
                                                    if(isset($RateInfo['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'])){
                                                       //echo '<pre>'; print_r($RateInfo); echo '</pre>';
                                                        $singleRate = $RateInfo['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'];
                                                        
                                                        //$singleRate = round(($singleRate * $eroamPercentage) / 100 + $singleRate,2);
                                                        
                                                        $singleRate = round($singleRate,2); 

                                                        $taxes = 0;
                                                        if(isset($RateInfo['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])){
                                                            $taxes = $RateInfo['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
                                                        }
                                                        $total      = $singleRate + $taxes;
                                                        echo number_format($total,2);
                                                    }
                                            }elseif($leg['hotel']['provider'] == 'eroam'){

                                                $roomPrice = 0;
                                                $roomPriceArray = array();
                                                $hotel_nights     = intval( $leg['city']['default_nights'] );
                                                if(!empty( $leg['hotel']['price']) ){
                                                    if( is_array( $leg['hotel']['price'] ) )
                                                    {
                                                        $roomPrice = $leg['hotel']['default_hotel_room']['price'];
                                                        $roomPrice = $roomPrice * $hotel_nights* $rooms;
                                                    }
                                                }
                                                echo number_format($roomPrice,2);
                                            }

                                                
                                                    

                                                    //if(!empty($RateInfo['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'])){
                                                     //echo $RateInfo['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'];
                                                    //}
                                                 
                                                  ?> For {{ $leg['hotel']['nights'] }} Nights</div>
                                      </li>
                                     </ul>
									@else
									<div class="itinerary_box_sort_title">@lang('home.accommodation_summary')</div>
									<br>
									<p>@lang('home.own_arrangement')</p>
									@endif
								</div>
							</div>
						</div>
						@if($leg['transport'] == '' || $leg['transport'] == null)
						<div class="accommodation_outer p-2 mb-0 border-bottom @php echo ((str_replace('%20',' ',$open_city_name) == $leg['city']['name']) && ($key == $open_leg) && ($open_task == 'activities')) ? 'default_color' : '' @endphp">
						@else
							<div class="accommodation_outer p-2 mb-0 border-bottom @php echo ((str_replace('%20',' ',$open_city_name) == $leg['city']['name']) && ($key == $open_leg) && ($open_task == 'activities')) ? 'default_color' : '' @endphp">
							@endif 
								<div class="accommodation" data-type="activities" data-link="{{ url( $leg['city']['name'].'/activities?leg='.$key ) }}">
									<div class="accommodation_icon"><i class="ic-local_activity"></i></div>
									<div class="accommodation_info">
										@if ( $leg['activities'] )
										<div class="itinerary_box_sort_title">@lang('home.activity_summary')</div>
										@foreach ( $leg['activities'] as $activity )
										@php 
                                        if (!isset($activity['duration1'])) {
											$activity['duration1'] = $activity['duration']." Day";
										}
										if (isset($activity['duration1']) && !empty($activity['duration1'])) { 
											$activity['duration1'] = str_replace("hour", "Hour", $activity['duration1']);
											$activity['duration1'] = str_replace("day", "Day", $activity['duration1']);
											$activity['duration1'] = str_replace("minutes", "Minutes", $activity['duration1']);
										}
										if (isset($activity['price']) && isset($activity['price'][0]['price'])) {
											//$activity['price_show'] = $activity['currency'].' '.round($activity['price'][0]['price']).'.00 Per Person  (Per Activity)'; comment by dhara
                                            $activity['price_show'] = $activity['currency'].' '.number_format(round($activity['price'][0]['price']),2);
										}
                                        $date = $activity['date_selected'];
                                        $date_search = strtotime($date);          
										@endphp
                                        <a href="{{url( $leg['city']['name'].'/activities?leg='.$key.'&dt='.$date_search)}}">
										<p class="mb-1">{{ @$activity['name'] }} </p>
										<ul>
											<li>
												<div class="title">
													<strong>@lang('home.date')</strong> 
												</div>
												<div class="info">{{ date('j M Y', strtotime($activity['date_selected'])) }}</div>
											</li>
											@if(isset($activity['duration1']) && !empty($activity['duration1']))
											<li>
												<div class="title">
													<strong>@lang('home.duration')</strong> 
												</div>
												<div class="info">{{$activity['duration1']}}</div>
											</li>
											@endif
											
                                            @if(isset($activity['tourGrades']))
                                            <li>
                                                <div class="title">
                                                    <strong>@lang('home.price')</strong> 
                                                </div>
                                                <div class="info">{{@$activity['tourGrades']['currencyCode']}} {{number_format(@$activity['tourGrades']['merchantNetPrice'],2)}}</div>
                                            </li>
                                            <li>
                                                <div class="title">
                                                    <strong>Option : </strong> 
                                                </div>
                                                <div class="info">{{$activity['tourGrades']['gradeTitle']}}</div>
                                            </li>
                                            <input type="hidden" name="merchantNetPrice" value="$activity['tourGrades']['merchantNetPrice']">
                                            @else
                                            <li>
                                                <div class="title">
                                                    <strong>@lang('home.price')</strong> 
                                                </div>
                                                <div class="info">{{@$activity['price_show']}}</div>
                                            </li>
                                            @endif
										</ul>
										<div class="clearfix"></div>
										@if($loop->iteration !== $loop->count) 
										    <hr>  
                                            </a>  
                                        @else
                                            </a>
										@endif
										@endforeach
										@else
										<div class="itinerary_box_sort_title">@lang('home.activity_summary')</div>
										<br>
										<p>@lang('home.own_arrangement')</p>
										@endif
									</div>
								</div>
							</div>
							@if($leg['transport'])
							@php 
							$duration = $leg['transport']['duration'] ? $leg['transport']['duration'] : '';
							if (!isset($duration)) { 
								if (isset($leg['transport']['provider']) && $leg['transport']['provider'] == 'busbud') {
									$a = (int) $duration;
									$hours = floor($a / 60);
									$minutes = $a % 60;
									$duration = ceil($hours) . ' Hour(s) and ' . ceil($minutes) . ' Minute(s)';
								} else { 
									$duration = str_replace("+", "", $duration);
                                    $duration = str_replace("hour", "Hour", $duration);
                                    $duration = str_replace("minute", "Minute", $duration);
								}
							}  
							if (isset($leg['transport']['price']) && isset($leg['transport']['price'][0]['price'])) {
								$price = $leg['transport']['price'][0]['price'];
							}
							@endphp
							<div class="accommodation_outer p-2 mb-0 @php echo ((str_replace('%20',' ',$open_city_name) == $leg['city']['name']) && ($key == $open_leg) && ($open_task == 'transports')) ? 'default_color' : '' @endphp">
								<div class="accommodation" data-type="transports" data-link="{{ url( $leg['city']['name'].'/transports?leg='.$key ) }}">
									<div class="accommodation_icon"><i class="ic-directions_bus"></i></div>
									<div class="accommodation_info">
									<div class="itinerary_box_sort_title">@lang('home.transport_summary')</div>
										<p class="mb-1">{{ $leg['transport']['transport_name_text'] }}</p>
										<ul>
											<li>
												<div class="title">
													<strong>@lang('home.map_label_11')</strong> 
												</div>
												<div class="info">{!!$leg['transport']['departure_text']!!}</div>
											</li>
											<li>
												<div class="title">
													<strong>@lang('home.map_label_12')</strong> 
												</div>
												<div class="info">{!!$leg['transport']['arrival_text']!!}</div>
											</li>
											<li>
												<div class="title">
													<strong>@lang('home.duration')</strong> 
												</div>
												<div class="info">{{ $duration }}</div>
											</li>
											@if(isset($price))
											<li>
												<div class="title">
													<strong>@lang('home.price')</strong> 
												</div>
												<div class="info"> AUD {{ number_format($price,2) }}</div>
											</li>
											@endif
										</ul>
									</div>
								</div>
							</div>
							@elseif ( last( session()->get('search')['itinerary'] ) != $leg )
							<div class="accommodation_outer p-2 mb-0  @php echo ((str_replace('%20',' ',$open_city_name) == $leg['city']['name']) && ($key == $open_leg) && ($open_task == 'transports')) ? 'default_color' : '' @endphp">
								<div class="accommodation" data-type="transports" data-link="{{ url( $leg['city']['name'].'/transports?leg='.$key ) }}">
									<div class="accommodation_icon"><i class="ic-directions_bus"></i></div>
									<div class="accommodation_info">
									
										<p><strong>@lang('home.transport_summary')</strong> <br/> {{ str_limit( $leg['city']['name'] . ' to ' . session()->get('search')['itinerary'][$key + 1]['city']['name'] , 50 ) }} <br>@lang('home.self')</p>
									</div>
								</div>
							</div>
							@endif
						</div>
					</div>
        @endforeach
        </div>
        @endif
    </div>
    <div id="reorder-locations-container" style="display:none;">
	    <div class="pt-3 pb-3">
            <span class="reorder-btn">@lang('home.drag_txt') <i style="margin: 0 .5rem" class="fa fa-arrows"></i> @lang('home.reorder_txt')</span>
		</div>
        <div class="row">
            <div class="col-sm-6">
                <a href="javascript://" class="btn  btns_input_white transform d-block w-100" id="save-order-btn">@lang('home.save_link_cap')  </a>
            </div>
            <div class="col-sm-6">
                <a href="javascript://" class="btn  btns_input_white transform d-block w-100" id="cancel-reorder-btn">@lang('home.discard_link_cap') </a>
            </div>
        </div>
        <br>
        <ul id="reorder-locations">
            @if (session()->get('search'))
            @foreach (session()->get('search')['itinerary'] as $key => $leg)
            <li class="ui-state-default border-0" style="cursor: move;" data-city="{{ $leg['city']['id'] }}">		
			
                <div class="location_icons"><i class="ic-place"></i></div>
                <div class="locations_info"><span>{{ $leg['city']['name'] }}, {{ $leg['city']['country']['code'] }}</span></div>
            </li>
            @endforeach
            @endif
        </ul>
    </div>
</form>
<style>

	.ui-state-highlight{
		background-color:#249dd0!important;
		border:1px solid #249dd0!important;
	}
</style>
<input type="hidden" id="isMap" value="{{$isMap}}">
<input type="hidden" id="last" value="{{$last}}">
