@if(isset($leg['hotel']) && !empty($leg['hotel']) && $leg['hotel']['provider'] == 'expedia')
    
  @php 
    global $finalTotalCost;
    $leg['hotel'] = json_decode(json_encode($leg['hotel']), true);
    $singleRate = '@nightlyRateTotal';
    $singleRate = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];
    /*$singleRate = ($singleRate * $eroamPercentage) / 100 + $singleRate;*/
    $subTotal = $singleRate;
    $taxes = 0;

    $totalNights = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['NightlyRatesPerRoom']['@size'];

    if (isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])):
      $taxes = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
      $taxesPerDay = $taxes / $totalNights;
    endif;

    $ratePerDay = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@averageBaseRate'];
    $ratePerDay = number_format(($ratePerDay * $eroamPercentage) / 100 + $ratePerDay + $taxesPerDay, 2);

    $selectedRate = number_format($subTotal + $taxes, 2);

    $finalTotalCost = ($subTotal + $taxes) + $finalTotalCost;

    $refundable = ($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['nonRefundable'] == 0) ? 'Refundable':'Non-Refundable';
    $leg['hotel']['cancellation_policy'] = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy'];

    $bedTypeUpdate = (array_key_exists('room', $leg['hotel']) && count($leg['hotel']['room']) > 0)? 1 : 0;
  @endphp

  <tr>
    <td class="border-0">
      <div class="tour_icon cityboxIcon"><i class="ic-local_hotel"></i> </div>
      <div class="tour_info cityboxDetails">
        <strong>{{ $leg['hotel']['name'] }}</strong>
        <p> 
          {{date('d M Y', strtotime($leg['hotel']['checkin']))}} - {{date('d M Y', strtotime($leg['hotel']['checkout']))}}
        
          @isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['rateDescription'])
            <br><strong>{{ $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['rateDescription'] }}</strong>
          @endisset
        </p>
      </div>
    </td>
    <td class="border-0 text-center"> ${{$currency}} {{$selectedRate}}</td>
    <td class="border-0 text-center"> 
      {{$rooms}} {{($rooms > 1) ? 'Rooms':'Room'}}&nbsp;<span data-toggle="tooltip" class="childTooltip" data-placement="top" title="" data-original-title="{{$adults}} Adult{{!empty($children) ? ', '.$children.' Child':''}}"><i class="fa fa-info-circle"></i></span>
    </td>
    <td rowspan="<?php echo (2+$rooms); ?>">
      <div class="text-right">
        <strong> ${{$currency}} {{$selectedRate}}</strong><br>
        <input type="hidden" class="hotelPrice" value="{{$selectedRate}}">
        <a href="javascript:void(0)" class="disable_item_custom">Change Dates</a><br>
        @if(isset($leg['hotel']['cancellation_policy']) && ($leg['hotel']['cancellation_policy'] != ""))
          <?php /*<span class="cancellation_policy1">
            <a href="javascript://">Cancellation Policy</a>
            <div class="cancellation_policybox1"><p>{{$leg['hotel']['cancellation_policy'] }}</p></div>
          </span><br>*/ ?>
          <a href="javascript:void(0)" data-target="#cancellationPolicy{{$cj}}" data-toggle="modal">Cancellation Policy</a><br/>
        @endif 
        <a href="javascript:void(0)" class="disable_item_custom"">Remove From Itinerary</a><br>
        <span class="badge mt-2">{{$refundable}}</span> 
      </div>
    </td>
  </tr>

  @for($i = 0; $i < $rooms; $i++)
    <tr>
      <td class="border-0" colspan="2">
        <div class="tour_icon">&nbsp;</div>
        <strong> Room {{$i+1}}: </strong>{{$paxDetailsArr[$i]}}
        <br><div class="tour_icon">&nbsp;</div>
          <?php 
            if(isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes'])){
              if($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['@size'] == 1){
                echo ucwords($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType']['description']);
              }
            }
          ?>
      </td>
      <td class="border-0">
        <?php
          $roomtype = '';
          if(isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes'])){
            if($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['@size'] > 1){
              //$roomtype .= '<option value="">@lang('home.Bed_Preferences')</option>';
              $j=0;
              $roomtype .= '<div class="fildes_outer">
                              <label>'.__('home.Bed_Preferences').'</label>
                                <div class="custom-select">';
              $roomtype .= '<select name="bedType" class="bedType" data-hotel="'.$leg['hotel']['hotelId'].'" data-leg="'.$key.'" data-room="'.($i+1).'" id="hotelBedType_'.$leg['hotel']['hotelId'].'">';
              foreach($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType'] as $bedType){
                $j++;
                
                if(array_key_exists('room', $leg['hotel']) && $leg['hotel']['room'][$i]['bedTypeId'] == $bedType['@id']){
                    $selected = 'selected="selected"'; 
                } 
                elseif($j == 1){ $selected = 'selected="selected"'; } 
                else { $selected = '';}

                $roomtype .= '<option value="'.$bedType['@id'].'" '.$selected.'>'.ucwords($bedType['description']).'</option>';
              }
              $roomtype .= '</select>';
              $roomtype .= '</div></div>';
            } else {
              //$roomtype .= '<option value="'.$leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType']['@id'].'" selected="selected" >'.ucwords($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType']['description']).'</option>';
              //$roomtype .= ucwords($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType']['description']);
            }
          }
          echo $roomtype;
        ?>
      </td>
    </tr>
  @endfor

  <tr>
    <td class="border-0" colspan="3">
      <div class="pl-5 ml-2 mb-5">
        <div class="fildes_outer">
          <label>Special Instructions</label>
          <textarea class="form-control mt-3 specialInsExpedia" placeholder="Special Instructions" data-hotel="{{$leg['hotel']['hotelId']}}" name="specialInstruction" data-leg="<?php echo $key; ?>" data-room="<?php echo ($i+1); ?>" > <?php echo (array_key_exists('specialInformation', $leg['hotel']) && $leg['hotel']['specialInformation']!= '')? $leg['hotel']['specialInformation'] : '';
          ?></textarea>
        </div>
      </div>
    </td>
  </tr>
@elseif(isset($leg['hotel']) && !empty($leg['hotel']) && $leg['hotel']['provider'] == 'eroam')
  @php 
    global $finalTotalCost;
    $leg['hotel'] = json_decode(json_encode($leg['hotel']), true);
    $nights  = $leg['city']['default_nights'];
    $roomPrice = $leg['hotel']['default_hotel_room']['price'];
    $roomPrice = $roomPrice * $nights;
    $finalTotalCost = (float)$finalTotalCost+(float)$roomPrice;

    $refundable = ($leg['hotel']['refundable'] == 1) ? "Refundable" : "Non-Refundable";
  @endphp

  <tr>
    <td class="border-0">
      <div class="tour_icon cityboxIcon"><i class="ic-local_hotel"></i> </div>
      <div class="tour_info cityboxDetails">
        <strong>{{ $leg['hotel']['name'] }}</strong>
        <p>
          {{date('d M Y', strtotime($leg['hotel']['checkin']))}} - {{date('d M Y', strtotime($leg['hotel']['checkout']))}}
          @isset($leg['hotel']['default_hotel_room']['name'])
            <br><strong>{{ $leg['hotel']['default_hotel_room']['name'] }}</strong>
          @endisset
        </p>

        <?php
          $paxDetails ='';
          for($i = 0; $i < $rooms; $i++){
            $paxDetails .= '<p><strong> Room'.($i+1).' :</strong>';
            $paxDetails .= $paxDetailsArr[$i];
          }
          echo $paxDetails;
        ?>
      </div>
    </td>
    <td class="border-0 text-center"> ${{$currency}} {{ number_format($roomPrice,2)}}</td>
    <td class="border-0 text-center"> 
      {{$rooms}} {{($rooms > 1) ? 'Rooms':'Room'}}&nbsp;<span data-toggle="tooltip" class="childTooltip" data-placement="top" title="" data-original-title="{{$adults}} Adult{{!empty($children) ? ', '.$children.' Child':''}}"><i class="fa fa-info-circle"></i></span>
    </td>
    <td>
      <div class="text-right">
        <strong> ${{$currency}} {{ number_format($roomPrice,2) }}</strong><br>
        <input type="hidden" class="hotelPrice" value="{{ number_format($roomPrice,2) }}">
        <a href="javascript:void(0)" class="disable_item_custom">Change Dates</a><br>
        @if(isset($leg['hotel']['cancellation_policy']) && ($leg['hotel']['cancellation_policy'] != ""))
          <?php /*<span class="cancellation_policy1">
            <a href="javascript://">Cancellation Policy</a>
            <div class="cancellation_policybox1"><p>{{$leg['hotel']['cancellation_policy'] }}</p></div>
          </span><br>*/ ?>
          <a href="javascript:void(0)" data-target="#cancellationPolicy{{$cj}}" data-toggle="modal">Cancellation Policy</a><br/>
        @endif 
        <a href="javascript:void(0)" class="disable_item_custom"">Remove From Itinerary</a><br>
        <span class="badge mt-2">{{$refundable}}</span> 
      </div>
    </td>
  </tr>
@else
  <tr>
    <td colspan="4">
      <div class="tour_icon cityboxIcon"><i class="ic-local_hotel"></i> </div>
      <div class="tour_info cityboxDetails">
        <strong>Own Arrangement</strong>
      </div>
    </td>
  </tr>
@endif

@if(isset($leg['hotel']['cancellation_policy']) && ($leg['hotel']['cancellation_policy'] != ""))
  <div class="modal fade in" id="cancellationPolicy{{$cj}}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header" style="border-bottom: none !important;padding: 15px 15px 0 15px;">
          <h4 class="modal-title" id="gridSystemModalLabel">Cancellation Policy</h4>
        </div>

          <div class="modal-body">
              <div class="roomType-inner m-t-20">
                <div class="m-t-20">
                  <p>{{$leg['hotel']['cancellation_policy']}}</p>
                </div>

                <div class="m-t-30 text-right">
                  <a href="#" data-dismiss="modal" class="modalLink-blue">CLOSE</a>
                </div>
              </div>
            </div>
        </div>
    </div> 
  </div>
@endif