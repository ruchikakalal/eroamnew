<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>:: eRoam ::</title>
    <style type="text/css">
      body{
        font-family: Arial;
        color: #212121;
        font-size: 14px;
        margin: 0px;
        padding: 0px;
      }
      p{
        margin: 5px 0;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
  <?php
  $logo = getDomainLogo();
  if(!$logo) {
      $logo = public_path('/images/logo-big.png');
  }
  ?>
    <table width="100%">
      <tbody>
        <tr>
          <td colspan="2">
            <table width="100%" style="background-color: #212121; color: #fff;">
              <tr>
                <td style="padding: 5px 10px;">
                  <img src="{{ $logo }}" alt="eRoam" style="width: 120px;">
                </td>
                <td style="text-align: right; padding: 3px 15px;">
                  <h2 style="margin-top: 10px;"><img src="{{ public_path('/images/logo/ic_directions_bus_white.png')}}" alt="" style="position: relative; top: 3px;"/> Bus Confirmation Voucher</h2>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th colspan="4">
                    <h2 style="margin: 8px 8px 8px 2px;">Passenger and Ticket Information</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><strong>Passenger Name</strong></td>
                <td>{{$itineryData['passenger_name']}}</td>
                  <td><strong>Booking ID</strong></td>
                  <td>{{$itineryData['invoiceNumber']}}</td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
              <table width="100%" border="1" style="border: solid 3px #fafafa; text-align: center; border-collapse: collapse; margin-top: 5px;" cellspacing="4" cellpadding="10">
              <tr>
                <td>
                  <h3 style="margin: 0;">Booking Date</h3>
                  <p>{{$itineryData['booking_date']}}</p>
                </td>
                <td>
                  <h3 style="margin: 0;">Departure</h3>
                  <p>{{$itineryData['from_city']}}</p>
                </td>
                <td>
                    <h3 style="margin: 0;">Destination</h3> 
                  <p>{{$itineryData['to_city']}}</p>
                </td>
                <td>
                  <h3 style="margin: 0;">Passenger</h3> 
                  <p>{{$itineryData['number_of_passenger']}}</p>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
            <td colspan="2">
              <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px; border-color: #fafafa;" cellspacing="4" cellpadding="6">
                <thead>
                  <tr style="background-color: #fafafa;">
                    <th colspan="7">
                      <h2 style="margin: 8px 8px 8px 2px;">Itinerary &amp; Reservation Details</h2>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="text-align: center; border:solid 1px #fafafa;">
                      <img src="{{ public_path('/images/logo/ic_directions_bus.png')}}" alt="" width="20px"/><br>
                      <p><strong>{{ $itineryData['transportType'] }}</strong></p>
                    </td>
                    <td style="border:solid 1px #fafafa; padding-left: 20px; text-align: center;">
                      <strong>Departure</strong><br>                 
                      <p>{{$itineryData['departure_text']}}</p>
                    </td>
                    <td style="border:solid 1px #fafafa; padding-left: 20px; text-align: center;">
                      <strong>Arrival</strong><br>               
                      <p>{{$itineryData['arrival_text']}}</p>
                    </td>
                    <td style="border:solid 1px #fafafa; text-align: center;">
                      <strong>Class</strong><br>
                      <p>ECONOMY</p>
                    </td>
                    <td style="border:solid 1px #fafafa; text-align: center;">
                      <strong>Duration</strong>
                      <p>{{$itineryData['duration']}}</p>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="7" style="padding: 0px;">
                      <table width="100%" style="border:solid 1px #fafafa; border-collapse: collapse;" cellpadding="6">
                        <thead>
                          <tr>
                            <th style="border:solid 1px #fafafa; padding: 10px 10px 10px 20px;"><strong>Passenger Name</strong></th>
                            <th style="border:solid 1px #fafafa; padding: 10px 10px 10px 20px;">Ticket Number</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if($booking_data!="")
                              @foreach($booking_data['passenger_info'] as $key => $value)
                              <tr>
                              <td style="border:solid 1px #fafafa; padding: 10px 10px 10px 20px;">{{$value['passenger_first_name'].' '.$value['passenger_last_name']}}</td>
                              <td style="border:solid 1px #fafafa; padding: 10px 10px 10px 20px;">{{$itineryData['invoiceNumber']}}</td>
                              </tr>
                              @endforeach
                          @endif
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th colspan="2">
                    <h2 style="margin: 8px 8px 8px 2px;">Payment Information</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                    <td colspan="2">
                      <table width="50%;" cellpadding="4">
                        <tr>
                          <td width="30%">Amount</td>
                          <td style="text-align: right; padding-right: 20px;">{{ $itineryData['currency'] }} {{$itineryData['price']}}</td>
                        </tr>
                        <tr>
                          <td width="30%">Taxes</td>
                          <td style="text-align: right; padding-right: 20px;">{{ $itineryData['currency'] }} 0.00</td>
                        </tr>
                        <tr>
                          <td width="30%"><strong>Total Amount</strong></td>
                          <td style="text-align: right; padding-right: 20px;"><strong>{{ $itineryData['currency'] }} {{$itineryData['price']}}</strong></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <!--<tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th colspan="3">
                    <h2 style="margin: 8px 8px 8px 2px;">Terms - Exchange Policies</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    <ul style="margin-top: 5px;">
                      <li>Lorem Ipsum is simply dummy text of the printing</li>
                      <li>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </li>
                    </ul>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>-->
        <tr>
          <td>
            <p style="margin-top: 40px; font-size: 16px;">We wish you a hassle-free, Happy journey.</p>

          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>