@extends('layouts.common')
@section('content')
    @include('partials.banner')
    @include('partials.search')
    <div class="account-block">
        @include('users.partials.sidebar')
        <div class="account-right p-4 pl-5">
        @include('users.partials.complete_profile')
            <hr class="mt-5">
            <h5 class="mt-4">Saved Trips</h5>
            <div class="mt-5 row">
            @if(count($all_trips) > 0)
                @foreach($all_trips as $trip)
                    @php
                    $trip_id = $trip['id'];
                    $trip = $trip['trip_data'];
                   // pr($trip);die;
                    @endphp
                    <div class="col-xl-4 col-sm-6 mb-4">
                        <a href="{{ url('saved-trip-details/'.$trip_id) }}">
                            <div class="card panel border-0">
                                <div class="trip-container panel py-3">
                                    <div class="tripImage text-center mb-1">
                                        <img src="{{ url('images/trip-image.png')}}" alt="" class="img-fluid">
                                    </div>
                                    @php 
                                     $fromCityname   = '';
                                    $toCityName     = '';
                                    if(!empty($trip['from_city_to_city'])){
                                        $getContriesName = explode(',',$trip['from_city_to_city']);
                                        $fromCityname = current($getContriesName);
                                        $toCityName = end($getContriesName);
                                    }

                                    //For get city airport codes
                                    $from_city_info = get_city_by_city_name($fromCityname);
                                    $to_city_info   = get_city_by_city_name($toCityName);

                                    //For get city names
                                    $from_country   = get_country_by_country_name($from_city_info['country_name']);
                                    $to_country     = get_country_by_country_name($to_city_info['country_name']);
                                    $getContriesName= !empty($trip['from_city_to_city']) ? explode(',',$trip['from_city_to_city']) :'';
                                   
                                    @endphp
                                    <div class="row text-center">
                                        <div class="col-sm-5">
                                        <h3><strong>{{$from_city_info['airport_codes']}}</strong></h3>
                                        <small>{{$fromCityname}}</small>
                                        </div>
                                        <div class="col-sm-2">
                                        <strong>-</strong>
                                        </div>
                                        <div class="col-sm-5">
                                            <?php 
                                            //if airport code not empty 
                                            $finalAirportCode = '';

                                            $finalAirportCode = !empty($to_city_info['airport_codes']) ? explode(',', $to_city_info['airport_codes']) :''; 
                                            $finalAirportCode = !empty($finalAirportCode)  ? end($finalAirportCode):'';

                                            ?>
                                        <h3><strong>
                                        <?php 
                                        // if airport code empty 
                                        if(empty($finalAirportCode)){
                                            if(is_array($to_city_info['iatas']) && !empty(end($to_city_info['iatas']))){
                                                 $finalAirportCode1 = !empty(end($to_city_info['iatas'])) ? end($to_city_info['iatas'])['iata_code']:'';

                                                 $finalAirportCode = !empty($finalAirportCode1) ? $finalAirportCode1:''; 
                                            }
                                        } 
                                        echo $finalAirportCode;

                                         ?>

                                      </strong></h3>
                                        <small>{{$toCityName}}</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="tripBooking p-3 text-center">
                                    <h6 class="text-truncate"><strong>Multi-City Tailormade Auto</strong></h6>
                                    <ul class="list-unstyled mb-0">
                                        <li class="list-inline-item"><a href="#"><i class="ic-local_hotel"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ic-local_activity"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ic-flight"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ic-directions_bus"></i></a></li>
                                    </ul>
                                </div>
                                <hr class="m-0">
                                <div class="traveller-details p-3">
                                    <div class="row mt-1">
                                        <div class="col-sm-5"><strong>Itinerary:</strong></div>
                                        <div class="col-sm-7">
										<p class="itinerary_address">
                                            @php $itinery = str_replace(",",' - ',$trip['from_city_to_city']); @endphp
                                            {{$itinery}}
										</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3 pt-3 px-3 pb-2 travellerPrice-box">
                                    <h5><strong>{{ !empty($trip['currency']) ? $trip['currency']:'N/A ' }}$ {{ !empty($trip['cost_per_day']) ? $trip['cost_per_day']:'N/A' }} Per Day</strong></h5>
                                    <p>Total Cost <strong>${{ !empty($trip['currency']) ? $trip['currency']:'N/A' }} {{ !empty($trip['cost_per_person']) ? $trip['cost_per_person']:'' }}</strong><br/>(Per Person) Inc. Taxes</p>
                                </div>
                                <div class="barcode-box p-3 position-relative text-center">
                                    <p><center>{{ date( 'j M Y', strtotime($trip['from_date']))}} - {{ date( 'j M Y',strtotime($trip['to_date']))}}</center></p>
                                </div>
                            </div>
                        </a>
                    </div>
                 @endforeach 
            @else
                <div class="col-lg-12 col-md-12 col-sm-12 m-t-10">
                    <h3>No Tours Found</h3>
                </div>
            @endif  
            </div>
        </div>
    </div>
@endsection
@include('partials.custom-js')