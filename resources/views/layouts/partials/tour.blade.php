@extends( 'layouts.common' )
@section( 'content' )
<style type="text/css">
.accomodation-grid-img{overflow: hidden; height: 220px;}
</style>
<?php 
    $i=$j=$k=0;
    $allCountryRegion = array();
    $regionList = '';
    $countryList = '';
    $RegionId = array();
    $allCountry = array();
    $countryDropdown = '';
    if(isset($country_id_remove))
    {
        $country_id_remove  = $country_id_remove;
    }
    else
    {
        $country_id_remove  = [];
    }
    
    $cityIds = isset($cityIds) ? $cityIds : []; 

    $availableTourRegions = array();
    if(!empty($tourRegions)){
        foreach ($tourRegions as $key => $value) {
            array_push($availableTourRegions, $value['region_id']);
        }
    }
    $availableTourRegions = array_unique($availableTourRegions);
    $availableTourCounts = array();
    if(!empty($totalTourByRegion)){
        foreach ($totalTourByRegion as $key => $value) {
            $availableTourCounts[$value['id']] = $value['tourCount'];
        }
    }
    
    if($countryIds){
        foreach ($countryIds as $key =>$countryId) { 

            $k++;           
            $countryRegion      = $tourCountries['countryRegions'][$key];
            $countryName        = $tourCountries['countryNames'][$key];
            $countryRegionName  = $tourCountries['countryRegionNames'][$key];
            $label_partial      = 'check_indicator';
            $label_class        = "";
            if($k <= 10){
                $check_checked = 'checked';
                if (in_array($countryId, $country_id_remove))
                {
                    $label_partial = 'check_indicator';
                    $label_class   = "partial_check".$countryId;
                }
                if(isset($country_tour_data[$countryId]['cities']))
                {
                    $open_c = "open";                   
                }
                else
                {
                    $open_c = '';
                }

                if(isset($country_tour_data[$countryId]['cities']))
                {
                    $countryList .= '<p class="regionCountry_'.$countryRegion.' mb-3 clearfix justify-content-between align-items-center">
                                        <span class="custom_check">'.$countryName.'
                                            <input class="countryList '.$label_class.'" data-regionName ="'.$countryRegionName.'" '.$check_checked.' data-region="'.$countryRegion.'" data-name="'.$countryName.'"  type="checkbox" id="checkbox-'.$countryId.'" name="region[]" value="'.$countryId.'" >
                                                <span class="'.$label_partial.'">&nbsp;</span>
                                                ['.$tourCount[$countryName].'] </span>';                       
                    $countryList .='<a href=".morecountry'.$countryId.'" class="moreCountry morecity'.$countryId.' '.$open_c.' text-secondary"  data-id="'.$countryId.'"  data-toggle="collapse" href="#collapseCountry'.$countryId.'" role="button" aria-expanded="true" aria-controls="collapseCountry'.$countryId.'"><i class="fa fa-minus"></i></a></p>';
                
                    $cities         = $country_tour_data[$countryId]['cities'];
                    $countryList .= '<div class="morecountry'.$countryId.' px-3 py-1 collapse show"  id="collapseCountry'.$countryId.'">';

                    foreach ($cities as $city) {
                        $check_checked = '';
                        if (in_array($city['city_id'], $cityIds))
                        {
                            $check_checked = "checked";
                        }

                        $countryList .= '<p class="mb-2"><label class="radio-checkbox label_check custom_check" for="checkbox-'.$city['city_id'].'"><input type="checkbox" class="cityList cityList'.$countryId.'" data-country-id="'.$countryId.'" data-name="'.$countryName.'" data-regionName ="'.$countryRegionName.'" '.$check_checked.' data-region="'.$countryRegion.'" id="checkbox-'.$city['city_id'].'" value="'.$city['city_id'].'" name="city[]"><span class="check_indicator">&nbsp;</span>'.$city['city_name'].' ['.$city['city_count'].']</label></p>';    
                    }
                    $countryList .= '</div>';
                }
                    
            } else {
                $countryList .= '<p class="regionCountry_'.$countryRegion.' mb-2 clearfix justify-content-between align-items-center" style="display:none;">
                    <label class="radio-checkbox label_check custom_check" for="checkbox-'.$countryId.'">
                        <input class="countryList" checked data-regionName ="'.$countryRegionName.'" data-region="'.$countryRegion.'" data-name="'.$countryName.'"  type="checkbox" id="checkbox-'.$countryId.'" name="region[]" value="'.$countryId.'" ><span class="check_indicator">&nbsp;</span>'.$countryName.' 
                    </label>
                    [<span class="count'.$countryName.'" id="count'.$countryName.'">'.$country_tour_data[$countryId]['country_count'].'</span>]<a href="morecountry'.$countryId.'" class="moreCountry morecity'.$countryId.' text-secondary collapsed" data-id="'.$countryId.'" data-toggle="collapse" href="#collapseCountry'.$countryId.'" role="button" aria-expanded="false" aria-controls="collapseCountry'.$countryId.'"><i class="fa fa-minus"></i></a></p>';

                $cities         = $country_tour_data[$countryId]['cities'];
                $countryList .= '<div class="morecountry'.$countryId.' px-3 py-1 collapse " id="collapseCountry'.$countryId.'">';

                foreach ($cities as $city) {
                    $check_checked = '';
                    if (in_array($city['city_id'], $cityIds))
                    {
                        $check_checked = "checked";
                    }
                    $countryList .= '<p class="mb-2"><label class="radio-checkbox label_check custom_check" for="checkbox-'.$city['city_id'].'"><input type="checkbox" class="cityList cityList'.$countryId.'" data-country-id="'.$countryId.'" id="checkbox-'.$city['city_id'].'" city="'.$city['city_id'].'" name="city[]"  data-name="'.$countryName.'"  data-regionName ="'.$countryRegionName.'" '.$check_checked.' data-region="'.$countryRegion.'" value="'.$city['city_id'].'"><span class="check_indicator">&nbsp;</span>'.$city['name'].' ['.$city['city_count'].']</label></p>';   
                }
                
                $countryList .= '</div>';
            }
        }
    }
    $region_count = array();//print_r($countries);exit;
    
    foreach($countries as $country)
    {
        $i++;
        $selectRegionId = 0;
        $totalCount = 0;
        $allCountry = array();
        $total_country_region = 0;
        foreach ($country['countries_showon_eroam'] as $country_data)
        {
            if( count($country_data['city_enabled']) > 0  ){
            // if( count($tourCount[$country_data['name']]) > 0  ){
                $j++;
                $allCountry[$j]['name'] = $country_data['name'];
                $allCountry[$j]['id'] = $country_data['id'];
                $allCountry[$j]['region'] = $country_data['region_id'];
                $allCountry[$j]['regionName'] = $country['name'];

                // $totalCount += $tourCount[$country_data['name']];

                if($countryName == $country_data['name']){
                    $selectRegionId = $country['id'];
                    $RegionId[] = $country['id'];
                }
                if(isset($tourCountries['countryNames']) && !empty($tourCountries['countryNames'])){    
                    if(in_array($country_data['name'], $tourCountries['countryNames'])){
                        $selectRegionId = $country['id'];
                        $RegionId[] = $country['id'];
                    }
                }
            } 
        }

        $selectRegion   = '';
        $disableRegion  = 'disabled';
        if($selectRegionId == $country['id'])
        {
            $selectRegion   = 'checked';              
        }

        if(in_array($country_data['region_id'], $availableTourRegions))
        {
            if(array_key_exists($country_data['region_id'], $availableTourCounts)){
                $total_country_region = $availableTourCounts[$country_data['region_id']];
            }else{
                $total_country_region = 0;
            }
            
            $regionList .= '<p class="mb-2">                                
                                <label class="radio-checkbox label_check custom_check custom_check" for="checkbox-'.$i.'">
                                    <input class="regionList" id="regionList'.$i.'" '.$selectRegion.' type="checkbox" id="checkbox-'.$i.'" value="'.$country['id'].'" '.$disableRegion.' name="country[]" >'.$country['name'].' 
                                <span class="check_indicator">&nbsp;</span></label>
                                [<span>'.$total_country_region.'</span>]</p>';
        }
        usort($allCountry, 'sort_by_name');
        $allCountryRegion[$country['id'].'_'.$country['name']] = $allCountry;
    }
    $RegionId = array_unique($RegionId);
    
    foreach ($allCountryRegion as $name => $allcountry) 
    {
        foreach ($allcountry as $country) 
        { 
            if(!in_array($country['id'], $countryIds))
            {
                if(isset($country_tour_data[$country['id']]))
                {
                    $k++;
                    //if($k <= 10){

                    $label_partial = '';
                    $check_checked = 'checked';
                    $open = '';
                    if (in_array($country['id'], $country_id_remove))
                    {
                        $label_partial = 'partial_check'.$country['id'];
                        $open           = "open";
                    }
                    
                    $cities         = [];
                    
                    if(isset($country_tour_data[$country['id']]))
                    {
                        $cities         = $country_tour_data[$country['id']]['cities'];
                    }
                    

                    $countryList .= '<p style="display:none;" class="mb-2 clearfix justify-content-between align-items-center regionCountry_'.$country['region'].'">
                                <label class="radio-checkbox label_check custom_check" for="checkbox-'.$country['id'].'" id="label-'.$country['id'].'">
                                <input class="countryList '.$label_partial.'" data-regionName ="'.$country['regionName'].'" data-region="'.$country['region'].'" data-name="'.$country['name'].'" type="checkbox" id="checkbox-'.$country['id'].'" name="region[]" value="'.$country['id'].'" ><span class="check_indicator">&nbsp;</span>'.$country['name'].'                                
                        [<span class="count'.$country['name'].'" id="count'.$country['name'].'">'.$tourCount[$country['name']].'</span>]</label>';
                        if(!empty($cities))
                        {
                            $countryList .=' <a href=".morecountry'.$country['id'].'" class="moreCountry morecity'.$country['id'].' '.$open.' text-secondary collapsed" data-id="'.$country['id'].'" data-toggle="collapse" href="#collapseCountry'.$country['id'].''.$country['id'].'" role="button" aria-expanded="false" aria-controls="collapseCountry'.$country['id'].'"><i class="fa fa-minus"></i></a></p>';
                        }

                    $countryList    .= '<div class="morecountry'.$country['id'].' px-3 py-1 collapse"  id="collapseCountry'.$country['id'].'">';
                    
                    if(!empty($cities))
                    {
                        foreach ($cities as $city) 
                        {
                            $check_checked = '';
                            if (in_array($city['city_id'], $cityIds))
                            {
                                $check_checked = "checked";
                            }                               

                            $countryList .= '<p class="mb-2"><label class="radio-checkbox label_check custom_check" for="checkbox-'.$city['city_id'].'"><input type="checkbox" class="cityList cityList'.$country['id'].'" data-country-id="'.$country['id'].'" id="checkbox-'.$city['city_id'].'" city="'.$city['city_id'].'" name="city[]" data-name="'.$country['name'].'"  data-regionName ="'.$country['regionName'].'" '.$check_checked.' data-region="'.$country['region'].'" value="'.$city['city_id'].'"><span class="check_indicator">&nbsp;</span>'.$city['city_name'].' ['.$city['city_count'].']</label></p>';
                        }
                    }                        
                    $countryList .='</div>';
                }
            }
        }
    }

    ?>

    <div class="itinerary_block">
        <form class="form-horizontal itinerary_left mb-0" method="post" action="tours" id="country-model">
                {{ csrf_field() }}
                <div class="tours-sidebar left_sidebar">
                    <div class="media p-3">
                        <div class="tour-icon">
                            <img src="images/search.svg" alt="">
                        </div>
                        <div class="media-body black-checkbox pb-3 mb-0 lh-125">
                            <h5>Search Region</h5>
                            <div class="mt-4">
                                {!! $regionList !!}
                            </div>
                        </div>
                    </div>
                    <hr class="mt-0">
                    <div class="media p-3">
                        <div class="tour-icon"></div>
                        <div class="media-body black-checkbox pb-3 mb-0 lh-125">
                            <h5>Search Country</h5>                        
                            <div class="mt-4">
                                <label for="city[]" generated="true" class="error" style="display: none;position:relative !important;bottom:5px !important;">You must check at least 1 box</label>
                                {!! $countryList !!}
                                <a href="#" class="text-secondary d-block mt-4" data-toggle="modal" data-target="#locationModal">View More Locations</a>
                                <div class="itinerary_left_btns_sec mt-5 mb-2">
                                    <input type="hidden" name="countryNames[]" id="country_names">
                                    <input type="hidden" name="countryRegion[]" id="region_id">
                                    <input type="hidden" name="countryRegionName[]" id="region_name">
                                    <input type="hidden" name="removeCountry[]" id="remove_country" value="">
                                    <input type="hidden" name="from_page"  value="1">
                                    <input type="hidden" name="departure_date" value="{{@$dep_date}}"> 
                                    <input type="hidden" name="tour_type" value="{{@$tour_type}}"> 
                                    <button type="submit" class="btn  btns_input_dark transform d-block w-100">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>    
        </form>   

        @yield('content-tour')
         <div class="modal fade  bd-example-modal-lg" id="locationModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header px-5 border-bottom-0">
                        <h5 class="modal-title" id="myModalLabel">Select a Country</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                  
                    </div>
                    <div class="modal-body px-5 black-checkbox">
                    
                        <form class="form-horizontal" method="post" action="tours" id="country-models">
                            <?php
                                $str = '';
                                $j=0;
                                foreach ($allCountryRegion as $name => $allcountry) {
                                    $idName = explode('_', $name); 

                                    $display = 'style="display:none;"';
                                    //if($idName[0] == $RegionId) { $display = '';}
                                    if(in_array($idName[0], $RegionId)) { $display = '';}

                                    $str .= '<div class="modelRegionCountry" id="modelRegionCountry_'.$idName[0].'" '. $display.'"><p>'.$idName[1].'</p>
                                                <hr/>
                                                <div class="mt-4 px-2 morelocation"><label for="city[]" generated="true" class="error" style="display: none;position:relative !important;bottom:5px !important;">You must check at least 1 checkbox</label>';
                                    $count = round(count($allcountry)/2);
                                    $i=0;

                                    foreach ($allcountry as $country) {
                                        if(isset($country_tour_data[$country['id']]))
                                        {
                                            $modelSelect = '';
                                            $open = 'collapsed';
                                             if(isset($tourCountries['countryNames']) && !empty($tourCountries['countryNames'])){
                                                if(!empty($countryIds) && in_array($country['id'], $countryIds)){
                                                    $modelSelect = 'checked';
                                                    $open = "";        
                                                } else if($countryName == $country['name']){
                                                    $modelSelect = 'checked';
                                                    $open = "";
                                                } else {
                                                    $modelSelect = '';
                                                    $open = 'collapsed';
                                                }
                                            }
                                            $label_partial = 'collapsed';

                                            if (in_array($country['id'], $country_id_remove))
                                            {
                                                $label_partial = 'partial_check'.$country['id'];
                                                $open = "show";
                                            }

                                            
                                            $cities         = $country_tour_data[$country['id']]['cities'];                             

                                            //$cityTourCount = cityTourCount($country['id']);
                                            $str .= '<p class="mb-2 d-flex justify-content-between align-items-center modelCountryList"><span class="custom_check">'.$country['name'].'
                                                    <input class="countryListModel '.$label_partial.'" '.$modelSelect.' data-regionName ="'.$idName[1].'" data-region="'.$country['region'].'" data-name="'.$country['name'].'" type="checkbox" id="checkbox-model-'.$country['id'].'" name="region[]" value="'.$country['id'].'" > 
                                                    <span class="check_indicator">&nbsp;</span>                                              
                                                     [<span class="count'.$country['name'].'">'.$tourCount[$country['name']].'</span>]</span></span>';
                                                
                                                if(!empty($cities))
                                                {
                                                    
                                                    $str .= '<a href=".modelmorecountry'.$country['id'].'" class="text-secondary  modelmoreCountry modelmorecity'.$country['id'].' '.$open.'" data-id="'.$country['id'].'" data-toggle="collapse"><i class="fa fa-minus"></i></a></p>';
                                                }
                                                
                                            $str .= '<div class="moreCountry-block modelmorecountry'.$country['id'].' collapse">';

                                            if(!empty($cities))
                                            {
                                                foreach ($cities as $city) 
                                                {
                                                    if($city['city_count'] != 0)
                                                    {
                                                        $check_checked = '';
                                                        if (in_array($city['city_id'], $cityIds))
                                                        {
                                                            $check_checked = "checked";
                                                        } 
                                                        $str .= '<div class="collapse px-3 py-1 show">';      
                                                        $str .= '<p class="mb-2"><span class="custom_check">'.$city['city_name'].' <input type="checkbox" class="modelcityList modelcityList'.$country['id'].'" data-country-id="'.$country['id'].'" id="model-checkbox-'.$city['city_id'].'" city="'.$city['city_id'].'" name="city[]" data-name="'.$country['name'].'"  data-regionName ="'.$country['regionName'].'" '.$check_checked.' data-region="'.$country['region'].'" value="'.$city['city_id'].'"><span class="check_indicator">&nbsp;</span>['.$city['city_count'].']';
                                                        $str .= '</div>';
                                                        //$str .= '<p><label class="radio-checkbox label_check" for="checkbox-model-city-'.$value['id'].'"><input type="checkbox" class="cityList cityList'.$country['id'].'" data-name="'.$value['name'].'" data-country-id="'.$country['id'].'" data-regionName ="'.$idName[1].'" data-region="'.$country['region'].'" id="checkbox-model-city-'.$value['id'].'" value="'.$value['id'].'" name="city[]">'.$value['name'].'('.$cityTourCount[$value['name']].')</label></p>';      
                                                    }
                                                }
                                            }
                                            $str .= '</div>';
                                            $i++;
                                            $j++;

                                            //if($i%$count == 0) {$str .= '</div>';}
                                        }
                                    }
                                    $str .= '</div></div>';
                                }
                                echo $str;
                            ?>
                            <div class="modal-footer border-top-0">
                                <div class="mt-4 text-right">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="countryNames[]" id="model_country_names">
                                    <input type="hidden" name="countryRegion[]" id="model_region_id">
                                    <input type="hidden" name="countryRegionName[]" id="model_region_name">
                                    <input type="hidden" name="removeCountry[]" id="model_remove_country">
                                    <input type="hidden" name="tour_type" value="{{@$tour_type}}">
                                   
                                    <!-- <a href="#"  class="btn btn-link mr-3" data-dismiss="modal" >DECLINE</a> -->
                                    <!-- <a href="javascript://" id="saveCountry">ACCEPT</a> -->
                                    <input type="submit" value="SUBMIT" class="btn btn-link">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
<script type="text/javascript">
    var base_img_url = "<?php echo config('services.tour.img_url'); ?>";    
</script>
<?php $version = config()->get('services.setting.version'); ?>
@push('scripts')
<script type="text/javascript" src="{{ url( 'js/theme/jquery.slimscroll.js' ) }}"></script>
<script type="text/javascript" src="{{ url('js/tour-list.js?v='.$version.'') }}"></script>
@endpush