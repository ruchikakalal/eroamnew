@extends('layouts.common')
@section('content')
        <div class="body_sec homepage_datepicker">
            <div class="blue modal_outer_man">
                @include('partials.banner')
                @include('partials.search')
            </div>
            <section class="tourlist">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2><span> Top tours </span></h2>
                        </div>
                    </div>
                    <div id="tours-loader">
                        <p class="text-center"><i class="fa fa-circle-o-notch fa-spin"></i> Loading Tours...</p>
                    </div>

                    <div class="row" id="tourList">
                    
                    </div>
                </div>
            </section>
        </div>
        <input type="hidden" id="resetDone" value="{{$resetDone}}">
        <input type="hidden" id="resetSuccess" value="{{$resetSuccess}}">
        @include('home.partials.custom-js')
        @include('partials.custom-js')
@endsection
