<?php
	return [
    'ExpediaEroamCommissionPercentage' => 20,
    'DefaultApiPath'=> env('DEFAULT_API_PATH','dev.cms.eroam.com/api/'),
    'DefaultDomainPath'=>env('DEFAULT_DOMAIN_PATH','dev.cms.eroam.com/'),
    'DefaultMapApiPath'=>env('DEFAULT_MAP_API_PATH','dev.cms.eroam.com/api/map/'),
    'DefaultSchema'=>env('DEFAULT_SCHEMA','https://'),
    'AWSUrl' =>'https://eroam-dev.s3.us-west-2.amazonaws.com/',
];
?>