<?php
namespace App\Helpers;

use App\Http\Controllers\Controller;
use Exception;

use Leafo\ScssPhp\Compiler;
use Illuminate\Support\Facades\Storage;

class FunctionsHelper{
    public static function compileTheme($var_str,$domain_name=NULL){
    	$scss = new Compiler();
        $scss->addImportPath(realpath(app()->path() . '/../resources/assets/sass'));
        $scss->setFormatter('Leafo\ScssPhp\Formatter\Crunched');

        $old_str = file_get_contents(realpath(app()->path() . '/../resources/assets/sass/variables.scss'));
        $file_v=fopen(realpath(app()->path() . '/../resources/assets/sass/variables.scss'),'w');
        fwrite($file_v,$var_str);
        fclose($file_v);

		$output = $scss->compile('@import "app";');
        $css_file_name = 'css/app.css';
        
        if(!empty($domain_name)) {
            $css_file_name = 'css/'.str_replace('.', '-', $domain_name).'.css';
        }
        
        $file_v=fopen(realpath(app()->path() . '/../resources/assets/sass/variables.scss'),'w');
        fwrite($file_v,$old_str);
        fclose($file_v);
        $file=fopen($css_file_name,'w');
		fwrite($file,$output);
		fclose($file);
    }
}

// class FunctionsHelper{
//     public static function compileTheme($var_str){
//         $scss = new Compiler();
//         $scss->addImportPath(realpath(app()->path() . '/../resources/assets/sass'));
//         $scss->setFormatter('Leafo\ScssPhp\Formatter\Crunched');
//         $output = $scss->compile($var_str.'@import "app";');

//         $file=fopen('css/app.css','w');
//         fwrite($file,$output);
//         fclose($file);      
//     }
// }