<?php
/**
 * Custom helper functions
 */
use Carbon\Carbon;	

if ( !function_exists('http') ) {

	function http( $method, $path, $data = [], $headers = []) {
		//$api = "http://localhost/cmseroam/public/api/";
		//$api = "http://cms.eroam.com/eroam/api/v2/";
		//$api = "http://cmseroam.com/api/";

		
		// $api = "http://192.168.1.59:8000/api/";
		$pathHelper = resolve('PathHelper');
		$api = $pathHelper->getApiPath();
		$domain = $pathHelper->getDomain();

		
		try {		
			$client = new \GuzzleHttp\Client(['headers' => ['domain' => $domain]]);
			$response = $client->request($method,$api.$path, [
			    'form_params' => $data
			]);
		} catch(Exception $e) {
			echo $e->getMessage();
			Log::error('An error occured in maps function name(): ' . $e->getMessage());
			die;
		}

		if (isset($response)) {
			$response = json_decode((string) $response->getBody(),true);
		}

		if( isset( $response['status'] ) ){
			if ( $response['status'] == 200 ) { return $response['data']; }
		}elseif( isset( $response['success'] ) ){
			return ( $response['success'] ? $response['data']: [] );
		}else{
			return $response;
		}

		return null;
	}
}

if ( !function_exists('http_local') ) {

	function http_local( $method, $path, $data = [], $headers = []) {
		$api = "http://192.168.1.59:8000/api/";
		
		try {		
			$client = new \GuzzleHttp\Client();
			$response = $client->request($method,$api.$path, [
			    'form_params' => $data
			]);
		} catch(Exception $e) {
			echo $e->getMessage();
			Log::error('An error occured in maps function name(): ' . $e->getMessage());
			die;
		}

		if (isset($response)) {
			$response = json_decode((string) $response->getBody(),true);
		}

		
		if( isset( $response['status'] ) ){
			if ( $response['status'] == 200 ) { return $response['data']; }
		}elseif( isset( $response['success'] ) ){
			return ( $response['success'] ? $response['data']: [] );
		}else{
			return $response;
		}

		return null;
	}
}

if ( !function_exists('test_http') ) {
	function test_http( $method, $url, $data = [] ) {
		$response = json_decode( Guzzle::$method( $url, $data )->getBody(), true );
		//if ( $response['status'] == 200 ) {
			return $response;
		//}
		return null;
	}
}

if ( !function_exists('get_all_countries') ) {
    function get_all_countries() {
        $countries 	= getAllCountries();
       	$allCountry = array();
       	$j=0;
		foreach($countries as $country)
		{
			foreach ($country['countries_showon_eroam'] as $country_data)
			{
				$allCountry[$j]['name'] 		= $country_data['name'];
				$allCountry[$j]['id'] 			= $country_data['id'];
				$allCountry[$j]['region'] 		= $country['id'];
				$allCountry[$j]['regionName'] 	= $country['name'];
				$j++;
			} 
		}
		usort($allCountry, 'sort_by_name');
        return $allCountry;
    }
}

if ( !function_exists('get_city_by_id') ) {
	function get_city_by_id( $id ) {
		$cities = getAllCities();
		$found = array_search( $id, array_column( $cities, 'id' ) );

		if ( $found !== false ) {
			return $cities[array_search( $id, array_column( $cities, 'id' ) )];
		}
	}
}
if ( !function_exists('get_city_by_city_name') ) {
	function get_city_by_city_name( $name ) {
		$cities = Cache::get( 'cities' );
		//pr($cities);die;
		$found = array_search( $name, array_column( $cities, 'name' ) );

		if ( $found !== false ) {
			return $cities[array_search( $name, array_column( $cities, 'name' ) )];
		}
	}
}

if ( !function_exists('get_city_by_name') ) {
	function get_city_by_name( $name ) {
		$cities = getAllCities();
		return $cities[array_search( $name, array_column( $cities, 'name' ) )];
	}
}

if ( !function_exists('get_cities_by_country_id') ) {
	function get_cities_by_country_id( $country_id ) {
		$cities = getAllCities();

		$result = array_where( $cities, function( $value, $key ) use ( $country_id ) {	
			return $value['country_id'] == $country_id;
		});

		return $result;
	}
}

if ( !function_exists('get_cities_by_city_name') ) {
    function get_cities_by_city_name( $name ) {
        $cities = getAllCities();
        $result = array_where( $cities, function( $value, $key ) use ( $name ) {
            return $key['name'] == $name;
        } );
        return $result;
    }
}

if ( !function_exists('get_countries_by_country_name') ) {
    function get_countries_by_country_name( $name ) {
        $countries = getAllCountries();
        foreach($countries as $country){
			if(!empty($country['countries'])){
				$result = array_where($country['countries'], function( $value, $key ) use ( $name ) {
					if($key['name'] == $name){
						return $key['name'] == $name;
						//break;
					}
				});	
			}
        	
	       return $result[1]['id'];
        }
        return 0;
    }
}

if ( !function_exists('get_country_by_country_name') ) {
    function get_country_by_country_name( $name ) {
        $countries = getAllCountries();
        foreach($countries as $country){  
        	if(!empty($country['countries_showon_eroam'])){
        		$result = array_where($country['countries_showon_eroam'], function( $value, $key ) use ( $name ) {
		        	if($value['name'] == $name){
		        		return $value['name'] == $name;
		        		//break;
		        	}
		        });

        	}
	       return $result;
        }

        return 0;
    }
}


if ( !function_exists('convert_date') ) {
	// created by miguel on 2017-01-09;
	// is used to convert date format from "2017-01-30" to "30th January 2017"
	function convert_date( $date, $date_format_name )
	{	
		$the_new_date_string = $date;

		switch( $date_format_name )
		{
			// e.g. 6th December 2016, 23rd October 2018
			case 'new_eroam_format':
				$the_new_date_string = date( 'D j M Y', strtotime($date));
				break;
				
			case 'eroam_format':
			
				$the_new_date_string = date( 'jS, M Y', strtotime($date));
				//$the_new_date_string = $day.' '.$month.' '.$year;
				break;
		}
		return $the_new_date_string;
	} 
}


if ( !function_exists('get_transport_icon') ) {
	function get_transport_icon($transport_type){
		switch ($transport_type) {
			case 'Private boat':
			case 'Ferry':
			case 'Ferry Boat':
			case 'Speed boat':
			case 'Slow boat':
			case 'Boat':
			case 'Jet Boat':
			case 'Cruise':
				$return = 'fa-ship';
				break;
			// case 'Minivan':
			case 'Private Minibus':
			case 'Coach Minivan':
			case 'Coach':
			case 'Bus':
				$return = 'fa-bus';
				break;
			case 'Flight':
				$return = 'fa-plane';
				break;
			case 'Private Car (Landcruiser)':
			case 'Private Car or Minivan':
			case 'Private car':
			case 'Private Car (Deluxe)':
				$return = 'fa-car';
				break;
			case 'Train':
				$return = 'fa-train';
				break;
			case 'Taxi':
				$return = 'fa-taxi';
				break;	
			case 'Coach + Ferry or Boat':
			case 'Train + Ferry or Boat':
			case 'Train + Minivan':
			case 'Train + Coach':
			case 'Minivan + Ferry or Boat':
				$return = 'fa-plus';
				break;
			case 'Transport Pass':
				$return = 'fa-id-card';
			break;
			default:
				$return = 'fa-car';
				break;
		}
		return $return;
	}
}


if ( !function_exists('sing') ) {
	function sing( $number, $string, $with_number = true ) {
		if ( $with_number ) {
			return $number > 1 ? $number . ' '. $string : $number . ' ' . str_singular( $string );
		} else {
			return $number > 1 ? $string : str_singular( $string );
		}

	}
}

if ( !function_exists( 'convert_24hr' ) ) {
	function convert_24hr( $time ) {
		return date( 'H:i', strtotime( $time ) );
	}
}

if ( !function_exists( 'add_str_time' ) ) {
	function add_str_time( $duration, $date, $format ) {

		return date( $format, strtotime( $duration, strtotime( $date ) ) );
	}
}

if ( !function_exists('calculate_transport_duration') ) {
	function calculate_transport_duration( $etd, $eta ) {
		$departure_split = explode(' ', $etd);
		$departure_split_hr_min = explode(':', $departure_split[0]);
	    $departure_hour =(int)$departure_split_hr_min[0];
	    $departure_min = isset($departure_split_hr_min[1]) ? (int)$departure_split_hr_min[1] : 00;
	    $departure_hour_24 = (isset($departure_split[1]) && $departure_split[1] == "PM") ? $departure_hour+12 : $departure_hour;
		  
		//eta = 11:00+2 PM
		$arrival_split = explode('+', $eta);//0=>11:00 1=>2 PM
		$arrival_split_am_pm = isset($arrival_split[1]) ? explode(' ', $arrival_split[1]) : 0 ;//0=>2 1=>PM
		$arrival_split_hour_min = explode( ':', $arrival_split[0] );//0=>11 1=>00
		$arrival_hour = (int)$arrival_split_hour_min[0];
		$arrival_min = isset($arrival_split_hour_min[1]) ? (int)$arrival_split_hour_min[1] : 00;
		$formatted_min = ($arrival_min-$departure_min) / 60;
		$plus_value = isset($arrival_split_am_pm[0]) ? $arrival_split_am_pm[0] : 0;
		//$eta_time = $arrival_split[0].$arrival_split_am_pm[1];
		$arrival_hour_24  = (isset($arrival_split_am_pm[1]) && $arrival_split_am_pm[1] == "PM") ? $arrival_hour+12 : $arrival_hour;

		if($plus_value > 0 ){
		  $temp_hours = (int)(24 - $departure_hour_24); //3 
		  $temp_hours     = (int)$temp_hours + (int)$arrival_hour_24 ;
		  $temp_hours     += $plus_value > 1 ? ($plus_value - 1) * 24 : 0;
		  $total_hours = $temp_hours + $formatted_min;

		}
		else{

		  $departure_time = $departure_hour_24 + ($departure_min / 60);

		  if($departure_time > 12){
		      $arrival_hour_24 = $plus_value == 0 ? $arrival_hour+24 : $arrival_hour;
		    }
		  if($arrival_hour > 12){
		      $arrival_hour_24 =  $arrival_hour;
		    }
		  $arrival_time = $arrival_hour_24 + ($arrival_min / 60);
		  $total_hours = $arrival_time - $departure_time;//64.5
		} 
		//return $total_hours;
		$hours_mins = explode(".",$total_hours);//0=>64 1=>5
		$ho_mins = $hours_mins[1] ?? 0;
		$hours = (int)($hours_mins[0]);
		$mins = round(('0.'.$ho_mins) * 60);
		return $hours.':'.$mins;
	}
}


if ( !function_exists('time_difference') ) {
	function time_difference( $etd, $eta, $additional_days = 0 )
	{
		$milliseconds = (strtotime($eta) - strtotime($etd)) / 3600;
		$time         = $milliseconds / 1000;
		$days         = floor($time / (24*60*60));
		$hours        = floor(($time - ($days*24*60*60)) / (60*60));
		$minutes      = floor(($time - ($days*24*60*60)-($hours*60*60)) / 60);
		return '+'.( $days+$additional_days ).' days '.$hours.' hours '.$minutes.' minutes';
	}
}



if ( !function_exists('create_iata_string') ) {
	function create_iata_string( $iatas )
	{
		$iata_str = '';
		if( count( $iatas ) == 1 )
		{
			$iata_str = $iatas[0]->iata_code;
		}
		if( count( $iatas ) > 1 )
		{
			$i = [];
			foreach( $iatas as $iata )
			{
				array_push( $i, $iata->iata_code );
			}
			$iata_str = implode(',', $i);
		}
		return $iata_str;
	}
}	
if ( !function_exists('get_hour')) {
	function get_hour($time){
		$hour =  date('H', strtotime($time));
		return (int)$hour;
	}
}

if( !function_exists('join_array_key_value') ){
	/*
	| Added by junfel
	| function for joining array keys and value;
	*/
	function join_array_key_value($array){
		return	implode(', ', array_map(function ($value, $key) {
		        	return $key.':"'.$value.'"';
		    	}, 
		    	$array, 
		    	array_keys($array)
			)
		);
	}
	
}
if (!function_exists('get_departure_date')) {
	/*
	| Added by Junfel
	| return exact departure date based on duration and date_to / next city's date_from
	| return format(Y/m/d)
	*/
	function get_departure_date($date_to, $transport_departure, $transport_duration, $hours = false){
		//$exact_arrival_date_time = $date_to.' '.date('H:i',strtotime($transport_departure.' '.$transport_duration));
		$exact_arrival_date_time = date('Y-m-d H:i', strtotime($date_to.' '.$transport_departure.' '.$transport_duration));
		$time_to_deduct = str_replace('+', '-', $transport_duration);
		$number_of_hours = $hours ? ' H' : '';
		$departure_date = date('Y/m/d'.$number_of_hours, strtotime($exact_arrival_date_time.' '.$time_to_deduct));
		return $departure_date;
	}
}
/*
| Added by Junfel
*/
if (!function_exists('get_hours_min')) {
	function get_hours_min($hour_min){
		$hours_mins = explode('.', $hour_min);
		$hours = (int)$hours_mins[0];
		$mins = isset( $hours_mins[1] ) ? (number_format('0.'.$hours_mins[1], 2) * 60) : 0 ;

		return '+'.$hours.' hours +'.ceil($mins).' minutes';
	}
}

if ( !function_exists('sort_by_name') ) {
	/*
	| Added by Junfel
	| Sorting function for array of arrays
	*/
	function sort_by_name($itemA, $itemB){
	    return strcmp($itemA['name'], $itemB['name']);
	}
}

if ( !function_exists('convert_currency') ) {
	// added by miguel on 2017-01-30; description
	function convert_currency( $price, $currency ){

		$converted_price = 0;

		try{

			$currency_layer         = session()->get('currency_layer');
			
			$price_converted_to_aud = floatval( $price ) / floatval( $currency_layer[ $currency ] );
			
			$global_currency        = ( session()->has('currency') ) ? session()->get('currency') : 'AUD';
			
			$converted_price        = floatval( $price_converted_to_aud ) * floatval( $currency_layer[ $global_currency ] );

			// $currency_layer  = session()->get('currency_layer');
			
			// $price_in_aud    = floatval( $price ) * floatval( $currency_layer[ $currency ] );
			
			// $global_currency = ( session()->has('currency') ) ? session()->get('currency') : 'AUD';
			
			// $converted_price = ceil( $price_in_aud ) * floatval( $currency_layer[ $global_currency ] ); 

		}catch( Exception $e ){

			// echo $e->getMessage();

		}
		
		return number_format($converted_price, 2, '.', '');
		
	}
	if (!function_exists('to_one_dim_array_labels')) {

		function to_one_dim_array_labels($index, $array = []){
			if( is_array($array) ){
				$array = array_dot($array);
		     	$array = array_where($array, function ($key, $value) {
					return strpos($key, 'label_id');
				});
				$array = array_flatten($array);
				return $array;
			}
		}
	}

	if (!function_exists('filter_by_labels')) {
		function filter_by_labels($labels){
			$temp = [];
			foreach($labels as $label){
				if ( session()->has( 'search_input' ) && count(session()->get( 'search_input' )['interests']) > 0 ) {
					$interests = session()->get( 'search_input' )['interests'];
					if( in_array( (int)$label, $interests ) ){
						$temp[] = (int)$label;
					}
				}else{
					$temp[] = (int)$label;
				}
			}
			return $temp;
		}
	}

	if (!function_exists('get_pref_by_id')) {
		function get_pref_by_id($prefs, $ids, $index, $arr = true){
			$return = [];
			if( count($ids) > 0 ){
				foreach($prefs as $pref){
					if(in_array($pref['id'], $ids) ){
						$return[] = $pref[$index]; 
					}
				}
			}
			//dd($return);
			return $arr ? $return : implode(', ',$return) ;
		}
	}
	if( !function_exists('count_viator_act_duration') ){
		function count_viator_act_duration( $duration ){
			$result = 1;
			if ( $duration ) {
				$match = preg_match( '/hour|day/', $duration, $matches );
				$match = $match ? $matches[0] : '';
				switch ( $match ) {
					case 'hour':

						$hours = explode(' ', $duration );
						$hours = (int)$hours[0];
						$day = floor( $hours / 24 );
						$result = $day;

						break;
					case 'day':
						$days = explode(' ', $duration );
						$result = (int)$days[0];
						break;
					
					default:
						$result = 1;
						break;
				}
			}
			return $result;
		}
	}

	if ( !function_exists('get_viator_duration') ) {
		function get_viator_duration( $duration ){
			$match = preg_match('/[\d\.]+[\s|-]hour|[\d\.]+[\s|-]minute|[\d\.]+[\s|-]day/i', $duration, $matches);
			return $match ? ( preg_match('/minute/', $matches[0]) ? '1 day' : str_replace('-', ' ', $matches[0]) ): '1 day';
		}
	}

	if ( !function_exists('date_difference') ) {
		function date_difference( $first_date, $second_date ){
			$first_date = new DateTime($first_date);
			$second_date = new DateTime($second_date);

			$interval = $first_date->diff($second_date);
			//return $interval;
			return (int)$interval->format('%r%a');
		}
	}


}

if (!function_exists('api_is_active')) {
	function api_is_active($api) {
		$apis = session()->get('inventory');
		if(in_array($api, $apis)) {
			return true;
		} 
		return false;

		/*$apis = json_decode(Cache::get('apis'));
		$found = [];
		if(isset($apis)){
			foreach ($apis as $key => $value) {
				if (strtolower($value->name) == strtolower($api)) {
					$found = $value;
				}
			}
		}
		return $found && $found->is_active == 'yes' ? true : false;*/
	}
}

if(!function_exists('get_timezone_abbreviation')){
	function get_timezone_abbreviation($timezone_id){
		$default = date_default_timezone_get();
		if($timezone_id){
	
			date_default_timezone_set ( $timezone_id );
			//$abbreviation = date('T O');
			//abbreviation = date('T').' (GMT+'.(date('O')/100).')';
			$abbreviation = date('T').' (GMT+'.(number_format((date('O')/100),2)).')';
			date_default_timezone_set ( $default );
			
			return $abbreviation;
		}
		return false;
	}
}

if ( !function_exists( 'get_hotel_category' )) {
	function get_hotel_category($code, $provider = ''){
		$stars = '';
		switch( $provider ){
			case 'hb':
				switch( $code ){
					case '5EST':
					case 'HS5':
					case 'APTH5':
					case '5LUX':
					case '5LL':
						$stars = get_stars( 5 );
					break;
					case '4EST':
					case 'HS4':
					case 'APTH4':
					case '4LUX':
					case '4LL':
						$stars = get_stars( 4 );
					break;
					case '3EST':
					case 'HS3':
					case 'APTH3':
					case '3LUX':
					case '3LL':
						$stars = get_stars( 3 );
					break;
					case '2EST':
					case 'HS2':
					case 'APTH2':
					case '2LUX':
					case '2LL':
						$stars = get_stars( 2 );
					break;
					case '1EST':
					case 'HS1':
					case 'APTH1':
					case '1LUX':
					case '1LL':
						$stars = get_stars( 1 );
					break;
					case 'H4_5':
						$stars = get_stars( 4, true );
					break;
					case 'H3_5':
						$stars = get_stars( 3, true );
					break;
					case 'H2_5':
						$stars = get_stars( 2, true );
					break;
					case 'H1_5':
						$stars = get_stars( 1, true );
					break;
					default: 
						$stars = get_stars(0);
					break;
				}
			break;
			case 'eroam':
				$stars = get_stars($code);
			break;
			case 'ae':
			break;
			case 'aot':
			break;
		}
		return $stars;
	}
}

if ( !function_exists( 'get_stars')) {
	function get_stars($count, $half = false){
		$stars = '';
		if( (int)$count ){
			for( $star = 1; $star <= $count; $star ++ ){
				$stars .= '<i class="fa fa-star"> </i> ';
			}
			$empty_stars = 5 - $count;
			if($half){
				$stars .= '<i class="fa fa-star-half-o"> </i> ';
				$empty_stars = $empty_stars - 1;
			}
			for( $empty = 1; $empty <= $empty_stars; $empty ++ ){
				$stars .= '<i class="fa fa-star-o"> </i> ';
			}
		}else{
			$stars .= '<i class="fa fa-star-o"> </i> <strong>Unranked</strong> ';

		}
		return $stars;
	}
}

if(!function_exists( 'countryTourCount')){
	function countryTourCount(){
		$key = 'tourCountryCount';
		Cache::forget($key);
		$result = Cache::get($key);
		
		if (!Cache::has($key)){
			$headers = [
				'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
				'Origin' => url('')
			];

			$countries = getAllCountries();
			
			$allCountry = array();
	        foreach($countries as $country){
	      		foreach ($country['countries_showon_eroam'] as $country_data){
					$allCountry[] = $country_data['name'];
				}
			}

			$allCountry = implode('# ', $allCountry);
			$result = http('post', 'tourCountryCount', ['countries' => $allCountry], $headers);

			$expiresAt = now()->addMinutes(241920);
	        // $expiresAt = date('Y-m-d H:i:s', strtotime('+24 week')); //Carbon::now()->addWeeks(24);
	        Cache::put($key, $result, $expiresAt);
		} 

		return $result;
	}
}

if(!function_exists( 'countryToursCount')){
	function countryToursCount($date = '', $tour_type = '' ){
		$key = 'tourCountryCount';
		
		Cache::forget($key);

		$result = Cache::get($key);

		if (!Cache::has($key)){

			$headers = [
				'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
				'Origin' => url('')
			];

			$countries = getAllCountries();

			$allCountry = array();
	        foreach($countries as $country){
	      		foreach ($country['countries_showon_eroam'] as $country_data){
					$allCountry[] = $country_data['name'];
				}
			}

			$allCountry = implode('# ', $allCountry);

			$result = http('post', 'tourCountryCount', ['countries' => $allCountry,'date' => $date, 'tour_type' => $tour_type], $headers);

	        $expiresAt = now()->addMinutes(241920); //Carbon::now()->addWeeks(24);
	        Cache::put($key, $result, $expiresAt);
		} 

		return $result;
	}
}




if(!function_exists( 'cityTourCount')){
	function cityTourCount($country_id){
		$key = 'tourCityCount';		
		$headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];

		$cities = get_cities_by_country_id($country_id);
		$allCity = array();
		$i = 0;
		foreach($cities as $city){
      		$allCity[$i]['id'] = $city['id'];
      		$allCity[$i]['name'] = $city['name'];
      		$i++;
		}
		if(!empty($allCity))
		{
			$result = http('post', 'tourCityCount', ['cities' => $allCity], $headers);
			return $result;
		}
		return [];
	}
}

if(!function_exists( 'tourCounts_23_4')){
	function tourCounts_23_4(){
		$key = 'tourCityCount';		
		$headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];

		$country_tour_count = countryTourCount();
		
		$cities 	= getAllCities();
		$allCity 	= array();
		$i 			= 0;
		$tour_city 	= http('post', 'tourCityCountAll', [], $headers);

		foreach($cities as $city){

      		$search 	= searchForId($city['id'],$tour_city);
      		$country_id = $city['country_id'];
      		//if (empty($allCity[$city['country_id']])) {      		

      		if($search != 0)
      		{
      			if(!array_key_exists($city['country_id'],$allCity))
	      		{
	      			$allCity[$city['country_id']] = array();
	      			$allCity[$city['country_id']]['cities'] = array();
	      		}
      			$allCity[$city['country_id']]['country_count'] = $country_tour_count[$city['country_name']];
      			$allCity[$city['country_id']]['country_name'] = $city['country_name'];
      			$new_arr = array(
      					'city_id' 		=> $city['id'],
      					'city_name' 	=> $city['name'],
      					'city_count'	=> $search,
      					'country_name'	=> $city['country_name'],
      				);
      			array_push($allCity[$city['country_id']]['cities'],$new_arr);
      		}      		
		}

		if(!empty($allCity))
		{
			return $allCity;
		}
		return [];
	}

function searchForId_23_4($id, $array) {
		$count = 0;
	   	foreach ($array as $key => $val) {
	       	if ($val['city_id'] === $id || $val['city_id1'] === $id) {	 
	       		$count =  $val['count_city'] + $count;
	       	}
	   	}
	   	return $count;
	}
}

if(!function_exists( 'tourCounts')){
	function tourCounts($date = '',$tour_type = ''){
		$key = 'tourCityCount';		
		$headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];


		$country_tour_count = countryTourCount();
		
		$cities 	= getAllCities();
		$allCity 	= array();
		$i 			= 0;
		$cData = [];
		

		$cData = array(
                'tour_type' => $tour_type
                );

        if(!empty($date) && $date != '1970-01-01')
        {
            $cData = array(
                'date' => $date,
                'tour_type' => $tour_type
                );
        }

		$tour_city 	= http('post', 'tourCityCountsAll', $cData, $headers);
		//dd($tour_city);
		foreach($cities as $city){
			
      		$search 	= searchForId($city['id'],$tour_city);
      		$country_id = $city['country_id'];
      		//if (empty($allCity[$city['country_id']])) {      		
      		
      		if($search != 0)
      		{
      			if(!array_key_exists($city['country_id'],$allCity))
	      		{
	      			$allCity[$city['country_id']] = array();
	      			$allCity[$city['country_id']]['cities'] = array();
	      		}
      			$allCity[$city['country_id']]['country_count'] = $country_tour_count[$city['country_name']];
      			$allCity[$city['country_id']]['country_name']  = $city['country_name'];
      			$new_arr = array(
      					'city_id' 		=> $city['id'],
      					'city_name' 	=> $city['name'],
      					'city_count'	=> $search,
      					'country_name'	=> $city['country_name'],
      				);
      			
      			array_push($allCity[$city['country_id']]['cities'],$new_arr);
      		}      		
		}
		
		if(!empty($allCity))
		{
			return $allCity;
		}
		return [];
	}

	function searchForId($id, $array) {
		$count = 0;
		
	   	foreach ($array as $key => $val) {
	       	//if ($val['city_id'] === $id || $val['city_id1'] === $id) {	 
	   		if ($val['city_id'] == $id ) {
	       		$count =  $val['count_city'] + $count;
	       	}
	   	}
	   	return $count;
	}
	if ( !function_exists('getExpediaHotelPriceWithEroamMarkup') ) {
		function getExpediaHotelPriceWithEroamMarkup($Rate,$nights,$taxes,$eroamPercentage){
		 $Rate = round(($Rate * $eroamPercentage) / 100 + $Rate,2); 
         $subTotal = $nights * $Rate;
         $Rate = $subTotal + $taxes;
         return $Rate;            
		}
	}
	if ( !function_exists('getExpediaHotelPriceWithoutEroamMarkup') ) {
		function getExpediaHotelPriceWithoutEroamMarkup($Rate,$nights,$taxes){
			 $Rate = round($Rate,2); 
	         $subTotal = $nights * $Rate;
	         $Rate = $subTotal + $taxes;
	         return $Rate; 
		}
	}

	function numberTowords($num){
	    $ones      = array(
	        1 => "one",
	        2 => "two",
	        3 => "three",
	        4 => "four",
	        5 => "five",
	        6 => "six",
	        7 => "seven",
	        8 => "eight",
	        9 => "nine",
	        10 => "ten",
	        11 => "eleven",
	        12 => "twelve",
	        13 => "thirteen",
	        14 => "fourteen",
	        15 => "fifteen",
	        16 => "sixteen",
	        17 => "seventeen",
	        18 => "eighteen",
	        19 => "nineteen"
	    );
	    $tens      = array(
	        1 => "ten",
	        2 => "twenty",
	        3 => "thirty",
	        4 => "forty",
	        5 => "fifty",
	        6 => "sixty",
	        7 => "seventy",
	        8 => "eighty",
	        9 => "ninety"
	    );
	    $hundreds  = array(
	        "hundred",
	        "thousand",
	        "million",
	        "billion",
	        "trillion",
	        "quadrillion"
	    ); //limit t quadrillion 
	    $num       = number_format($num, 2, ".", ",");
	    $num_arr   = explode(".", $num);
	    $wholenum  = $num_arr[0];
	    $decnum    = $num_arr[1];
	    $whole_arr = array_reverse(explode(",", $wholenum));
	    krsort($whole_arr);
	    $rettxt = "";
	    foreach ($whole_arr as $key => $i) {
	        if ($i < 20) {
	            $rettxt .= $ones[$i];
	        } elseif ($i < 100) {
	            $rettxt .= $tens[substr($i, 0, 1)];
	            if (substr($i, 1, 1) != 0) {
		            $rettxt .= " " . $ones[substr($i, 1, 1)];
	            }
	        } else {
	            $rettxt .= $ones[substr($i, 0, 1)] . " " . $hundreds[0];
	            if (substr($i, 1, 1) != 0) {
	            	$rettxt .= " " . $tens[substr($i, 1, 1)];
	            }
	            if (substr($i, 2, 1) != 0) {
		            $rettxt .= " " . $ones[substr($i, 2, 1)];
	            }
	        }
	        if ($key > 0) {
	            $rettxt .= " " . $hundreds[$key] . " ";
	        }
	    }
	    if ($decnum > 0) {
	        $rettxt .= " and ";
	        if ($decnum < 20) {
	            $rettxt .= $ones[$decnum];
	        } elseif ($decnum < 100) {
	            $rettxt .= $tens[substr($decnum, 0, 1)];
	            $rettxt .= " " . $ones[substr($decnum, 1, 1)];
	        }
	    }
	    return $rettxt;
	}
}

/****** Rekha - 01-08-2018 - Remove Catch Function Start ******/
	if ( !function_exists('getAllCities') ) {
	    function getAllCities($type = '') {

	    	if(isset($type) &&  $type == 'forget'){ Cache::forget('cities'); }

	        if (!Cache::has('cities')){
	            $cities = http('get', 'map/city');
	            Cache::forever('cities', $cities);
	        }
	        return Cache::get('cities');
	        //return $cities;
	    }
	}

	if ( !function_exists('getAllCountries') ) {
	    function getAllCountries($type = '') {

	    	if(isset($type) &&  $type == 'forget'){ Cache::forget('countries'); }

	        if (!Cache::has('countries')) {
	            $countries = http('get', 'map/get/all/countries');
	            Cache::forever('countries', $countries);
	        }
	        return Cache::get('countries');
	        //return $countries;
	    }
	}
	//this is countries for select option in review itinery
	if ( !function_exists('getSelectCountries') ) {
	    function getSelectCountries() {

            $countries = http('get', 'all-country-for-select-temp');
	            
	        return $countries;
	        //return $countries;
	    }
	}
	//this is flight meal type for select option in review itinery
	if ( !function_exists('getSelectMealType') ) {
	    function getSelectMealType() {

            $aMealType = http('get', 'flight-meal-select');
	            
	        return $aMealType;
	        //return $countries;
	    }
	}

	if ( !function_exists('getAllHotelCategories') ) {
	    function getAllHotelCategories($type = '') {

	    	if(isset($type) &&  $type == 'forget'){ Cache::forget('hotel_categories'); }

	        if (!Cache::has('hotel_categories')) {
	            $hotelCategories = http('get', 'hotel-categories');
	            Cache::forever('hotel_categories', $hotelCategories);
	        }
	        return Cache::get('hotel_categories');
	        //return $hotelCategories;
	    }
	}

	if ( !function_exists('getAllLabels') ) {
	    function getAllLabels($type = '') {

	    	if(isset($type) &&  $type == 'forget'){ Cache::forget('labels'); }

	        if (!Cache::has('labels')) {
	            $labels = http('get', 'labels');
	            Cache::forever('labels', $labels);
	        }
	        return Cache::get('labels');
	        //return $labels;
	    }
	}

	if ( !function_exists('getTravellerOptions') ) {
	    function getTravellerOptions($type = '') {

	    	if(isset($type) &&  $type == 'forget'){ Cache::forget('travellers'); }

	        if (!Cache::has('travellers')) {
	            $headers = [
	                'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
	                'Origin' => 'http://app.testeroam.com'
	            ];
	            $travellers = http('get', 'traveler-options', [], $headers);
	            Cache::forever('travellers', $travellers);
	        }
	        return Cache::get('travellers');
	        //return $travellers;
	    }
	}

	if ( !function_exists('getAllCountriesBookingspro') ) {
	    function getAllCountriesBookingspro($type = '') {

	    	if(isset($type) &&  $type == 'forget'){ Cache::forget('countriesBookingsPro'); }

	        if (!Cache::has('countriesBookingsPro')) {
	            $countries = http('get', 'map/get/all/countriesBookingPro');
	            Cache::forever('countriesBookingsPro', $countries);
	        }
	        return Cache::get('countriesBookingsPro');
	        //return $countries;
	    }
	}

/****** Rekha - 01-08-2018 - Remove Catch Function End ******/

if ( !function_exists('getAllCountriesBookingspro') ) {
    function getAllCountriesBookingspro() {
        $countries = http('get', 'map/get/all/countriesBookingPro');
        return $countries;
    }
}
/****** Rekha - 01-08-2018 - Remove Catch Function End ******/

if ( !function_exists('checkChildInfantTotal') ) {
	function checkChildInfantTotal($ageBands,$childAge,$adult)
	{
		$adultAge 	= [];
		$childAges 	= [];
		$infantAge 	= [];
		$data 		= [];
		
		foreach($ageBands as $ageBand)
		{
			if($ageBand['bandId'] == 1)
		    {
		    	$ageFromc 	= $ageBand['ageFrom'];
		    	$ageToc 	= $ageBand['ageTo'];
		    	//dd($ageFromc,$ageToc);
		    	for($i = $ageFromc ; $i <= $ageToc ; $i++)
		    	{
		    		array_push($adultAge,$i);
		    	}
		    }

		    if($ageBand['bandId'] == 2)
		    {
		    	$ageFromc 	= $ageBand['ageFrom'];
		    	$ageToc 	= $ageBand['ageTo'];
		    	
		    	for($i = $ageFromc ; $i <= $ageToc ; $i++)
		    	{
		    		array_push($childAges,$i);
		    	}
		    }

		    if($ageBand['bandId'] == 3)
		    {
		    	$ageToc = $ageBand['ageFrom'];
		    	$ageToi = $ageBand['ageTo'];

		    	for($i = $ageToc ; $i <= $ageToi ; $i++)
		    	{
		    		array_push($infantAge,$i);
		    	}
		    }
		}
		
		$adult = $adult;
		$child = 0;
		$infant = 0;
		
		foreach ($childAge as $cage) 
		{
			foreach ($cage as $key => $value) {

				if(in_array($value,$childAges))
				{
					$child++;
				}
				if(in_array($value,$infantAge))
				{
					$infant++;
				}
				if(in_array($value,$adultAge))
				{
					$adult++;
				}
			}			
		}

		$data = [
			'child' 	=> $child,
			'infant' 	=> $infant,
			'adult' 	=> $adult,
		];
		
		if($infant == 0)
		{
			unset($data['infant']);
		}
		if($child == 0)
		{
			unset($data['child']);
		}
		
		return $data;
	}
}

if ( !function_exists('checkAdultChildInfantTotal') ) {
	function checkAdultChildInfantTotal($ageBands,$paxInfo)
	{
		$adultAge 	= [];
		$childAges 	= [];
		$infantAge 	= [];
		$data 		= [];
		
		foreach($ageBands as $ageBand)
		{
			if($ageBand['bandId'] == 1)
		    {
		    	$ageFromc 	= $ageBand['ageFrom'];
		    	$ageToc 	= $ageBand['ageTo'];
		    	//dd($ageFromc,$ageToc);
		    	for($i = $ageFromc ; $i <= $ageToc ; $i++)
		    	{
		    		array_push($adultAge,$i);
		    	}
		    }

		    if($ageBand['bandId'] == 2)
		    {
		    	$ageFromc 	= $ageBand['ageFrom'];
		    	$ageToc 	= $ageBand['ageTo'];
		    	
		    	for($i = $ageFromc ; $i <= $ageToc ; $i++)
		    	{
		    		array_push($childAges,$i);
		    	}
		    }

		    if($ageBand['bandId'] == 3)
		    {
		    	$ageToc = $ageBand['ageFrom'];
		    	$ageToi = $ageBand['ageTo'];

		    	for($i = $ageToc ; $i <= $ageToi ; $i++)
		    	{
		    		array_push($infantAge,$i);
		    	}
		    }
		}
		
		$adult = 0;
		$child = 0;
		$infant = 0;

		$aAdult = [];
		$aChild = [];
		$aInfant = [];

		
		foreach ($paxInfo as $cage) 
		{			
			foreach ($cage as $key => $value) {

				if(in_array($value['age'],$childAges))
				{
					$child++;
					array_push($aChild,$value);
				}
				if(in_array($value['age'],$infantAge))
				{
					$infant++;
					array_push($aInfant,$value);
				}
				if(in_array($value['age'],$adultAge))
				{
					$adult++;
					array_push($aAdult,$value);

				}
			}			
		}

		$data['pax_age'] = [
			'child' 	=> $child,
			'infant' 	=> $infant,
			'adult' 	=> $adult,
		];

		$data['pax_info'] = [
			'aAdults' 	=> $aAdult,
			'aChilds' 	=> $aChild,
			'aInfants' 	=> $aInfant,
		];

		
		if($infant == 0)
		{
			unset($data['pax_age']['infant']);
			unset($data['pax_info']['aInfants']);
		}
		if($child == 0)
		{
			unset($data['pax_age']['child']);
			unset($data['pax_info']['aChilds']);
		}
		//dd($data);
		return $data;
	}
}

if ( !function_exists('checkRefreshCache') ) {
    function checkRefreshCache() {

    	$cData = array('domain' => request()->getHost());
  
    	$reload = http('post', 'map/getRefreshCache',$cData);
    	if(!empty($reload) && $reload['is_reload'] == 1){
    		getAllCities('forget'); 
			getAllCountries('forget');
			getAllHotelCategories('forget');
			getAllLabels('forget');
			getTravellerOptions('forget');
			getAllCountriesBookingspro('forget');
			$update = http('post', 'map/updateRefreshCache', $cData);;
    	}
        return true;
    }
}

/****** Rekha - 01-08-2018 - Remove Catch Function End ******/

/* Octal-21-08-2018 */

if(!function_exists('getDomainLogo')) {
	function getDomainLogo() {
		$pathHelper = resolve('PathHelper');
		$domain = $pathHelper->getDomain();

		$response = http('post','get-domain-logo',['domain'=>$domain]);
		return $response['logo'];
	}
}


if(!function_exists('hex2rgba')) {
	function hex2rgba($color, $opacity = false) {
	 
		$default = 'rgb(0,0,0)';
	 
		//Return default if no color provided
		if(empty($color)) {
	          return $default; 
	 	}
		//Sanitize $color if "#" is provided 
	        if ($color[0] == '#' ) {
	        	$color = substr( $color, 1 );
	        }
	 
	        //Check if color has 6 or 3 characters and get values
	        if (strlen($color) == 6) {
	                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
	        } elseif ( strlen( $color ) == 3 ) {
	                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
	        } else {
	                return $default;
	        }
	 
	        //Convert hexadec to rgb
	        $rgb =  array_map('hexdec', $hex);
	 
	        //Check if opacity is set(rgba or rgb)
	        if($opacity){
	        	if(abs($opacity) > 1) {
	        		$opacity = 1.0;
	        	}
	        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
	        } else {
	        	$output = 'rgb('.implode(",",$rgb).')';
	        }
	 
	        return $output;
	}
}

if(!function_exists('getDomainProduct')) {
	function getDomainProduct() {
		$pathHelper = resolve('PathHelper');
		$domain = $pathHelper->getDomain();
		$response = http('post','get-domain-product',['domain'=>$domain]);
		return $response['products'];
	}
}

if(!function_exists('createPaxInfo')) {
    function createPaxInfo($unset = 0) {
        if(request()->has('firstname') ){
            $firstname = array_values(array_filter(request()->input('firstname')));
            $lastname = array_values(array_filter(request()->input('lastname')));
            $dob = array_values(array_filter(request()->input('dob')));
            $age = array_values(array_filter(request()->input('age')));

            $pax = $num_of_adults = $num_of_children = $childArr =  array();
            $today = date('d-m-Y'); //'19-09-2018';

            foreach ($firstname as $key => $value) {
                $adults = $child = 0;
                foreach ($value as $key1 => $value1) {
                    $pax[$key][$key1 - 1]['firstname'] = $value1;
                    $pax[$key][$key1 - 1]['lastname'] = $lastname[$key][$key1];
                    $pax[$key][$key1 - 1]['dob'] = $dob[$key][$key1];

                    $age = Carbon::parse($dob[$key][$key1])->diff(Carbon::parse($today))->format('%y'); //$age[$key][$key1];

                    if($age == 0){ $age = 1;}
                    $pax[$key][$key1 - 1]['age'] = $age; //$age[$key][$key1];

                    if($age < 18) {
                        $pax[$key][$key1 - 1]['child'] = 1;
                        $childArr[$key][] = $age; //1;
                        $child++;
                    } else {
                        $pax[$key][$key1 - 1]['child'] = 0;
                        $adults++;
                    }
                }
                $num_of_adults[$key] = $adults;
                $num_of_children[$key] = $child;
            }

            /*if($unset == 1){
                request()->offsetUnset('firstname');
                request()->offsetUnset('lastname');
                request()->offsetUnset('dob');
                request()->offsetUnset('age');
                request()->offsetUnset('is_child');
            }*/
            request()->merge( ['child' =>$childArr, 'num_of_adults' => $num_of_adults, 'num_of_children' => $num_of_children,'pax'=>$pax] );

            /*$travellers = array_sum($num_of_adults); // total Adults
			$childrens  = array_sum($num_of_children); // Total Childrens
			request()->merge( ['childrens' => $childrens, 'travellers' => $travellers, 'child' =>$childArr, 'num_of_adults' => $num_of_adults, 'num_of_children' => $num_of_children,'pax'=>$pax] );*/
        }
    }
}

if(!function_exists('getGUID')) {
    function getGUID(){
        if (function_exists('com_create_guid')){
            //return com_create_guid();
            return trim(com_create_guid(), '{}');
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = ''   //chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
            //.chr(125);// "}"
            return $uuid;
        }
    }
}
if(!function_exists('getCityAirportCode')) {
 	function getCityAirportCode(){
        $pathHelper = resolve('PathHelper');
        $api_url = $pathHelper->getApiPath();
        $domain = $pathHelper->getDomain();
        
        $data = array();
        $request = TRUE;
        while( $request ){
                try{
                    $client = new \GuzzleHttp\Client(['headers' => ['domain' => $domain]]);
                    $response = $client->request('post',$api_url . 'get-city-airport-code', [
                        'form_params' => $data
                    ]);
                    $request = FALSE;
                }catch(Exception $e){
                Log::error('An error has occured during a guzzle call on MystiflyController@set for provider "'.$provider.'" '.'with key "'.$key.'" and ERROR MESSAGE:'.$e->getMessage() );
            	}
        }
                
        $result = json_decode( $response->getBody() , true );
        
        return $result['data'];
        
    }
}
if(!function_exists('isAdult')) {
	function isAdult($dob,$travelDate) {
		$travelDate = Carbon::parse($travelDate);
		$dt = Carbon::createFromFormat('d-m-Y', $dob);
		$today = $travelDate->sub(new DateInterval('P12Y'));
		return $dt->lte($today);
	}
}
if(!function_exists('isChild')) {
	function isChild($dob,$travelDate) {

		$childDate = Carbon::parse($travelDate);
		$adultDate = Carbon::parse($travelDate);
		$dt = Carbon::createFromFormat('d-m-Y', $dob);
		$childDate = $childDate->sub(new DateInterval('P2Y'));
		$adultDate = $adultDate->sub(new DateInterval('P12Y'));
		return $dt->lte($childDate) && $dt->gt($adultDate);
	}
}
if(!function_exists('isInfant')) {
	function isInfant($dob,$travelDate) {
		$travelDate = Carbon::parse($travelDate);
		$dt = Carbon::createFromFormat('d-m-Y', $dob);
		$today = $travelDate->sub(new DateInterval('P2Y'));
		return $dt->gt($today);
	}
}

if(!function_exists('findString')) {
	function findString($text,$start,$end) {
		$pattern = sprintf('/%s(.+?)%s/ims', preg_quote($start, '/'), preg_quote($end, '/'));

		if (preg_match($pattern, $text, $matches)) {
		    list(, $match) = $matches;
		    return $match;
		}
		return '';
	}
}


if(!function_exists('getBrowser')){
	function getBrowser() { 
	    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
	    $bname = 'Unknown';
	    $platform = 'Unknown';
	    $version= "";

	    //First get the platform?
	    if (preg_match('/linux/i', $u_agent)) {
	        $platform = 'linux';
	    }
	    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
	        $platform = 'mac';
	    }
	    elseif (preg_match('/windows|win32/i', $u_agent)) {
	        $platform = 'windows';
	    }

	    // Next get the name of the useragent yes seperately and for good reason
	    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
	    { 
	        $bname = 'Internet Explorer'; 
	        $ub = "MSIE"; 
	    } 
	    elseif(preg_match('/Firefox/i',$u_agent)) 
	    { 
	        $bname = 'Mozilla Firefox'; 
	        $ub = "Firefox"; 
	    }
	    elseif(preg_match('/OPR/i',$u_agent)) 
	    { 
	        $bname = 'Opera'; 
	        $ub = "Opera"; 
	    } 
	    elseif(preg_match('/Chrome/i',$u_agent)) 
	    { 
	        $bname = 'Google Chrome'; 
	        $ub = "Chrome"; 
	    } 
	    elseif(preg_match('/Safari/i',$u_agent)) 
	    { 
	        $bname = 'Apple Safari'; 
	        $ub = "Safari"; 
	    } 
	    elseif(preg_match('/Netscape/i',$u_agent)) 
	    { 
	        $bname = 'Netscape'; 
	        $ub = "Netscape"; 
	    } 

	    // finally get the correct version number
	    $known = array('Version', $ub, 'other');
	    $pattern = '#(?<browser>' . join('|', $known) .
	    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	    if (!preg_match_all($pattern, $u_agent, $matches)) {
	        // we have no matching number just continue
	    }

	    // see how many we have
	    $i = count($matches['browser']);
	    if ($i != 1) {
	        //we will have two since we are not using 'other' argument yet
	        //see if version is before or after the name
	        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
	            $version= $matches['version'][0];
	        }
	        else {
	            $version= $matches['version'][1];
	        }
	    }
	    else {
	        $version= $matches['version'][0];
	    }

	    // check if we have a number
	    if ($version==null || $version=="") {$version="?";}

	    return array(
	        'userAgent' => $u_agent,
	        'name'      => $bname,
	        'version'   => $version,
	        'platform'  => $platform,
	        'pattern'    => $pattern
	    );  
	} 
}

if(!function_exists('getBrowserName')){
	function getBrowserName() {
		$ua=getBrowser();
		return $ua['name'] . "/" . $ua['version'];
	}
}