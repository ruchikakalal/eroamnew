<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use StdClass;
use App\Libraries\Filter;
use App\Libraries\EroamSession;
use App\Libraries\ApiCache;
use PDF;
use Mail;
use Cache;
use Validator;
use App\Libraries\Map;
use Carbon\Carbon;
use Config;

use App\Http\Controllers\ExpediaApiController;

class AuthnticationController extends Controller
{

	private $session;
	private $cms_url;
	protected $expediaApi;
	private $headers = [];


	public function __construct() {
		parent::__construct();
		$this->session = new EroamSession;
		$this->map 	   = new Map;
		$this->cms_url = Config::get('env.CMS_URL');
		$this->expediaApi = new ExpediaApiController;
	}

	public function initial(){

		$ip_address = '137.59.252.196';
		$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip_address));
	   
		$countryName = $query['country'];
		
		try
		{
			$ip_address = '137.59.252.196';

			$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip_address));
			
			$countryName = $query['country'];
			$tourCountries = http('post', 'getTourCountriesAvailable', [], $this->headers);
			$tourCities = http('post', 'getTourCitiesAvailable', [], $this->headers);
			$city = ucfirst($query['city']);
			$res = get_cities_by_city_name($city); 
			if ($res) {
				$key = array_keys($res);
				$key = $key[0];
				$from_city_id = $res[$key]['id'];
				$from_country_id = $res[$key]['country_id'];

			} else {
				$from_city_id = '';
				$from_country_id = '';

			}

			$cities = getAllCities();
			$countries = getAllCountries();
			$labels = getAllLabels();
			$travellers = getTravellerOptions();
			$countries1 = getAllCountriesBookingspro();

			if (!session()->has('transport_types')) {

				session()->put('transport_types', $travellers['transport_types']);
			}
			$travel_pref = [];
			$interest_ids = [];
			if (session()->has('travel_preferences')) {
				$travel_pref = session()->get('travel_preferences');
				$travel_pref = reset($travel_pref);
				$interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
			}

			usort($countries, 'sort_by_name');
		} catch (GuzzleHttp\Exception\ClientException $e) {
			$this->home();
		}

		session()->forget('tourCountryData');
		session()->forget('fromTourHome');
		return view(
			'pages.initial-authentication',
			[
				'cities' => $cities,
				'from_city_id' => $from_city_id,
				'from_country_id' => $from_country_id,
				'countries' => $countries,
				'countries1' => $countries1,
				'labels' => $labels,
				'travellers' => $travellers,
				'travel_pref' => $travel_pref,
				'interest_ids' => $interest_ids,
				'countryName' => $countryName,
				'default_currency' => 'AUD',
				'page_will_expire' => 1,
				'tourcountries' => $tourCountries,
				'tourCities' => $tourCities,
			]
		);
	}

	public function validate_auth_credentials(){
		$result = 0;
		$credentials = [
		    ['username' => 'LoftAdmin', 'password' => 'loveloft'],
			['username' => 'ATG', 'password' => 'eRoamPilot'],
			['username' => 'AsianTrails', 'password' => 'eRoamPilot'],
			['username' => 'Bamba', 'password' => 'eRoamPilot'],
			['username' => 'DNATA', 'password' => 'eRoamPilot'],
			['username' => 'Express Travel Group', 'password' => 'eRoamPilot'],
			['username' => 'Fairfax (ijurni)', 'password' => 'eRoamPilot'],
			['username' => 'Mastercard', 'password' => 'eRoamPilot'],
			['username' => 'News', 'password' => 'eRoamPilot'],
			['username' => 'Ornate Travel', 'password' => 'eRoamPilot'],
			['username' => 'OzSale', 'password' => 'eRoamPilot'],
			['username' => 'Paul Hole', 'password' => 'eRoamPilot'],
			['username' => 'Urban Adventures', 'password' => 'eRoamPilot'],
			['username' => 'Virtuoso', 'password' => 'eRoamPilot'],
			['username' => 'MedHolidays', 'password' => 'eRoamPilot'],
			['username' => 'eroamdemo', 'password' => 'eRoamPilot'],
			['username' => 'Grupoviajabien', 'password' => 'eRoamPilot'],
			['username' => 'Preos', 'password' => 'eRoamPilot'],
			['username' => 'RoomerFlex', 'password' => 'eRoamPilot369'],
			['username' => 'Dialibre', 'password' => 'eRoamPilot'],
			['username' => 'eroamAdmin', 'password' => ']Vqv9{SB'],
			['username' => 'LoftAdmin', 'password' => 'loveloft'],
			['username' => 'ExpediaEAN', 'password' => 'Expedia@123'],
			['username' => 'ViatorAPI', 'password' => 'Viator@123'],
            ['username' => 'MystiflyOnePoint', 'password' => 'Mystifly@123'],
            ['username' => 'Travelport', 'password' => 'eRoamPilot'],
			['username' => 'MystiflyDemo', 'password' => 'eRoamPilot'],
		];
		foreach ($credentials as $c) {
			if( request()->input('username') == $c['username'] && request()->input('password') == $c['password'] ) {
				$result = 1;
				$this->session->initial_session();
				$this->session->set_city();
				
				$headers = [
	                'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
	                'Origin' => url('')
            	];
            	
            	$data = array();
            	$data['name'] = $c['username'];
            	$data['ip_address'] = $_SERVER['REMOTE_ADDR'];

            	$response = http( 'post', 'user/user-log', $data, $headers);
			}
		}
		return $result;
	}

}
