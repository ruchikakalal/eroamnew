<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;

class LanguageController extends Controller
{
	public function index($language) {
		Session::put('locale', $language);
		return redirect()->back();
	}
	
}
