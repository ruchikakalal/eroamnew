<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ExpediaApiController;
use App\Libraries\ApiCache;
use App\Libraries\EroamSession;
use App\Libraries\Filter;
use App\Libraries\Map;
use Illuminate\Support\Facades\Input;
use Cache;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Mail;
use PDF;
use Validator;
use \App\Helpers\FunctionsHelper;
use Session;
use App\Service\Mystifly\AirRevalidate;
use App\Service\Mystifly\GetCityAirportCode;

// use Request;

class PagesController extends Controller {

	private $session;
	private $cms_url;
	private $domain;
	protected $expediaApi;
	private $headers = [];


	public function __construct() {
		parent::__construct();
		$this->session = new EroamSession;
		if(!session()->has('default_selected_city')) {
			$this->session->set_city();
		}
		$this->eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
		$this->map = new Map;
		$this->cms_url = Config::get('env.CMS_URL');
		$this->expediaApi = new ExpediaApiController;
		$pathHelper = resolve('PathHelper');
		$this->domain = $pathHelper->getDomain();
	}

	public function sass_to_css(){
		// Set Custome Sass file code into css file //
		$data = ['colors'=>['theme_color'=>'#249DD0','font_color'=>'#212121','header_color'=>'#071B22','footer_color'=>'#071B22','search_color'=>'#071B22'],'fonts'=>['regular'=>'HelveticaNeue','bold'=>'HelveticaNeue-Bold','medium'=>'HelveticaNeueMedium11','light'=>'HelveticaNeueLight08']];
		$sass_var = $this->getVariables($data['colors'],$data['fonts']);
		$temp_str = '';
		foreach($sass_var as $key=>$val){
			$temp_str .= '$'.$key.':'.$val;
		}
		
		FunctionsHelper::compileTheme($temp_str);
		die;
	}
	
	public function getVariables($colors,$fonts){
		$var_array = [
			'border-radius' => '.125rem;',
			'enable-shadows' => 'false;',
			'card-border-radius' => '0;',
			'grid-gutter-width' => '20px;',
			'font-weight-bold' => $fonts['bold'].',sans-serif;',
			'leftColumnwidth' => '350px;',
			'accountColumnwidth' => '350px;',
			
			'font-stack' => $fonts['regular'].',sans-serif;',
			'primary-regular-font' => $fonts['regular'].',sans-serif;',
			'secondary-bold-font' => $fonts['bold'].',sans-serif;',
			'primary-extended-font' => $fonts['bold'].',sans-serif;',
			'primary-lightExt-font' => $fonts['light'].',sans-serif;',
			'primary-boldExt-font' => 'harabara_maisharabaramaisdemo,sans-serif;',
			'primary-color' => '#212121;',
			
			'box-shadow' => '0px 1px 4px 0px rgba(0, 0, 0, 0.24);',
			'header-padding' => '1px 0;',
			'footer-bg-color' => $colors['footer_color'].';',
			'header-bg-color' => hex2rgba($colors['header_color'],0.8).';',
			'search-filter-bg-color' => hex2rgba($colors['search_color'],0.8).';',
			
			'primary' => '#071B22;',
			'secondary' => '#212121;',
			'danger' => '#f53d3d;',
			'light' => '#f4f4f4;',
			'dark' => '#212121;',
			'white' => '#ffffff;',
			'blue' => $colors['theme_color'].';',
			'blacklight' => 'rgba(7, 27, 38, 0.8);',
			'text-align' => 'center;',
			'text-transform' => 'uppercase;',
			'text_decoration_none' => 'none;',


			'btn-border-radius' => '0;',
			'btn-border-width' => '1px;',
			'btn-border-color' =>'transparent;',
			'btn-primary-background-color' => '#ffffff;',
			'btn-lightblue-background-color' => '249DD0;',
			'btn-border-style' => 'solid;',
			'btn-font-size' => '14px;',
			'btns-box-shadow' => '0 2px 5px 0 rgba(149, 149, 149, 0.50);',
			'btns-text-transform' => 'uppercase;',
			'btn-border' => 'none;',
			'btn-radius'=>'0px;',

			'theme-color' => $colors['theme_color'].';',
			'theme-text-color' => $colors['font_color'].';',
			'search-bg-color' => '#071B22;',

			'footer-text-color' => '#fff;',

			'fildes-outer-border' => '1px;',
			'fildes-background-color' => '#ffffff;',
			'fildes-position' => 'relative;',
			'fildes-border-radius' => '0px;',
			'fildes-padding' => '7px 0px 0 0px;',
			'fildes-border-color' => '#F3F3F3;',
			'fildes-border-style' => 'solid;',
			'fildes-title-color' => '#fff;',
		];
		return $var_array;
	}
	
	

	/* public function sass_to_css(){
		// Set Custome Sass file code into css file //
		$sass_var = $this->getVariables();
		$temp_str = '';
		foreach($sass_var as $key=>$val){
			$temp_str .= '$'.$key.':'.$val;
		}
		
		FunctionsHelper::compileTheme($temp_str);
		die;
	}

	public function getVariables(){
		$var_array = [
			'border-radius' => '.125rem;',
			'enable-shadows' => 'false;',
			'card-border-radius' => '0;',
			'grid-gutter-width' => '20px;',
			'font-weight-bold' => 'HelveticaNeue-Bold,sans-serif;',
			'leftColumnwidth' => '350px;',
			'accountColumnwidth' => '350px;',
			
			'font-stack' => 'HelveticaNeue,sans-serif;',
			'primary-regular-font' => 'HelveticaNeue,sans-serif;',
			'secondary-bold-font' => 'HelveticaNeue-Bold,sans-serif;',
			'primary-extended-font' => 'HelveticaNeueMedium11,sans-serif;',
			'primary-lightExt-font' => 'HelveticaNeueLight08,sans-serif;',
			'primary-boldExt-font' => 'harabara_maisharabaramaisdemo,sans-serif;',
			'primary-color' => '#212121;',
			
			'box-shadow' => '0px 4px 4px 0px rgba(0, 0, 0, 0.24);',
			'header-padding' => '1px 0;',
			'footer-bg-color' => '#212121;',
			'header-bg-color' => '#071B22;',
			'search-filter-bg-color' => 'rgba(7, 27, 38, 0.8);',
			
			'primary' => '#071B22;',
			'secondary' => '#212121;',
			'danger' => '#f53d3d;',
			'light' => '#f4f4f4;',
			'dark' => '#212121;',
			'white' => '#ffffff;',
			'blue' => '#249DD0;',
			'blacklight' => 'rgba(7, 27, 38, 0.8);',
			'text-align' => 'center;',
			'text-transform' => 'uppercase;',
			'text_decoration_none' => 'none;',

			'btn-border-radius' => '0;',
			'btn-border-width' => '1px;',
			'btn-border-color' =>'transparent;',
			'btn-primary-background-color' => '#ffffff;',
			'btn-lightblue-background-color' => '249DD0;',
			'btn-border-style' => 'solid;',
			'btn-font-size' => '14px;',
			'btns-box-shadow' => '0 2px 5px 0 rgba(149, 149, 149, 0.50);',
			'btns-text-transform' => 'uppercase;',
			'btn-border' => 'none;',

			'theme-color' => '#249DD0;',
			'theme-text-color' => '#212121;',
			'search-bg-color' => '#071B22;',

			'footer-text-color' => '#fff;',


			'fildes-outer-border' => '1px;',
			'fildes-background-color' => '#ffffff;',
			'fildes-position' => 'relative;',
			'fildes-border-radius' => '0px;',
			'fildes-padding' => '7px 0px 0 0px;',
			'fildes-border-color' => '#F3F3F3;',
			'fildes-border-style' => 'solid;',
			'fildes-title-color' => '#fff;',
		];
		return $var_array;
	}
	*/
	
	public function cache_locations() {
		$apiCache = new ApiCache;

		//$apiCache->get_all_cities();
		//$apiCache->get_all_countries();
		//$apiCache->get_all_hotel_categories();
		//$apiCache->get_all_labels();
		//$apiCache->get_traveller_options();
		getAllCities('forget');
		getAllCountries('forget');
		getAllHotelCategories('forget');
		getAllLabels('forget');
		getTravellerOptions('forget');
		getAllCountriesBookingspro('forget');
		return redirect('/');
	}

	public function home() {

		if (session()->has('reset-success-done')) {
			$resetDone = true;
		}else{
			$resetDone = false;
		}
		
		if (session()->has('reset-success')) {
			$resetSuccess = true;
		}else{
			$resetSuccess = false;
		}
		
		if (!session()->has('initial_authentication')) {
			return redirect('app/login');
		}

		if (starts_with(request()->root(), 'http://'))
		{
		    $domain = $this->domain;
		}
		
		checkRefreshCache();
		
		try{
			$ip_address = '137.59.252.196';
			$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip_address));
			$countryName = $query['country'];
			$tourCountries = http('post', 'getTourCountriesAvailable', [], $this->headers);
			$tourCities = http('post', 'getTourCitiesAvailable', [], $this->headers);

			$city = ucfirst($query['city']);
			$res = get_cities_by_city_name($city); 

			if ($res) {
				$key = array_keys($res);
				$key = $key[0];
				$from_city_id = $res[$key]['id'];
				$from_country_id = $res[$key]['country_id'];

			} else {
				$from_city_id = '';
				$from_country_id = '';

			}

			$cities = Cache::get('cities');
			$countries = Cache::get('countries');
			$labels = Cache::get('labels');
			$travellers = Cache::get('travellers');
			$countries1 = Cache::get('countriesBookingsPro');

			if (!session()->has('transport_types')) {

				session()->put('transport_types', $travellers['transport_types']);
			}

			$travel_pref = [];
			$interest_ids = [];
			if(session()->has('travel_preferences')) {
				$travel_pref = session()->get('travel_preferences');
				// $travel_pref = reset($travel_pref);
				$interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
			}
			
			usort($countries, 'sort_by_name');
		} catch (GuzzleHttp\Exception\ClientException $e) {
			$this->home();
		}
		session()->forget('tourCountryData');
		session()->forget('fromTourHome');
		return view(
			'home.home',
			[
				'cities' => $cities,
				'from_city_id' => $from_city_id,
				'from_country_id' => $from_country_id,
				'countries' => $countries,
				'countries1' => $countries1,
				'labels' => $labels,
				'travellers' => $travellers,
				'travel_pref' => $travel_pref,
				'interest_ids' => $interest_ids,
				'countryName' => $countryName,
				'default_currency' => 'AUD',
				'page_will_expire' => 1,
				'tourcountries' => $tourCountries,
				'tourCities' => $tourCities,
				'resetDone' => $resetDone,
				'resetSuccess' => $resetSuccess,
			]
		);

	}

	public function maphome() {
		if (!session()->has('initial_authentication')) {
			return redirect('app/login');
		}

		try
		{
			$starting_country = request()->input()['country'];
			$starting_city = request()->input()['city'];
			$destination_country = request()->input()['to_country'];
			$destination_city = request()->input()['to_city'];

			if ($destination_city != '') {
				$toName = get_city_by_id($destination_city);
				$mapName = $toName['name'] . ',' . $toName['country_name'];
				$toName = $toName['name'];

				$url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($mapName);
				$json_data = file_get_contents($url);
				$result = json_decode($json_data, TRUE);
				$latitude = $result['results'][0]['geometry']['location']['lat'];
				$longitude = $result['results'][0]['geometry']['location']['lng'];
			} else {
				$toName = '';
				$latitude = '-33.868093';
				$longitude = '151.206427';
			}
			if ($starting_city != '') {
				$startName = get_city_by_id($starting_city);
				$startName = $startName['name'];
			} else {
				$startName = '';
			}

			$countries = Cache::get('countries');
			$labels = Cache::get('labels');
			$travellers = Cache::get('travellers');

			if (!session()->has('transport_types')) {
				session()->put('transport_types', $travellers['transport_types']);
			}
			$travel_pref = [];
			$interest_ids = [];
			if (session()->has('travel_preferences')) {
				$travel_pref = session()->get('travel_preferences');
				$travel_pref = reset($travel_pref);
				$interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
			}

			usort($countries, 'sort_by_name');
		} catch (GuzzleHttp\Exception\ClientException $e) {
			$this->home();
		}

		return view(
			'pages.welcome',
			[
				'countries' => $countries,
				'destination_country' => $destination_country,
				'destination_city' => $destination_city,
				'starting_country' => $starting_country,
				'starting_city' => $starting_city,
				'toName' => $toName,
				'latitude' => $latitude,
				'longitude' => $longitude,
				'startName' => $startName,
				'labels' => $labels,
				'travellers' => $travellers,
				'travel_pref' => $travel_pref,
				'interest_ids' => $interest_ids,
			]
		);

	}

	public function update_booking_summary() {

		$search = request()->input('search');
		// update
        $preferences = session()->get('travel_preferences');
        if ($search) {
			$search = json_decode($search, true);
			$pax_information = [];
			$travel_date = $search['travel_date'];
			$travellers = $search['travellers'];
			$child_total = $search['child_total'];
			$pax_information = $search['pax_information'];
			$rooms = $search['rooms'];

			$next_date = null;
			$start_from = null;
			foreach ($search['itinerary'] as $key => $value) {

				if ($key == 0) {
					$start_from = $travel_date;
					$next_date = $start_from;
				} else {
					$next_date = Carbon::parse($next_date)->addDays($value['city']['default_nights'])->format('Y-m-d');
					$start_from = isset($search['itinerary'][$key - 1]['city']['date_to']) ? $search['itinerary'][$key - 1]['city']['date_to'] : $next_date;

				}

				if (empty($value['hotel'])) {

					$return_hotel = $this->map->set_default_hotel_api(['date_from' => date('Y-m-d', strtotime($start_from)), 'traveller_number' => $travellers], $value['city']);

					$search['itinerary'][$key]['hotel'] = $return_hotel;
				}

				if (empty($value['activities'])) {
					$return_activity = $this->map->set_viator_default_activity(['date_from' => date('Y-m-d', strtotime($start_from)), 'traveller_number' => $travellers], $value['city']);
					$search['itinerary'][$key]['activities'] = $return_activity ? [$return_activity] : null;

				}

				//miguel here
				$last_key = count($search['itinerary']) - 1;
				if ($key != $last_key) {
					if (empty($value['transport'])) {
						$transport = $value['transport'];
						$origin_city = $value['city'];
						$destination_city = $search['itinerary'][$key + 1]['city'];
						$date_from = $start_from;
						$add_days = intval($value['city']['default_nights']);
						$date_to = add_str_time('+' . $add_days . ' days', $start_from, 'Y-m-d');

						$is_flight_filtered = FALSE;
						if (isset($this->default['search_input']['transport_types'])) {
							$transport_type_filters = $this->default['search_input']['transport_types'];
							foreach ($transport_type_filters as $key => $filter) {
								if ($filter == '1') {
									$is_flight_filtered = TRUE;
								}
							}
						}

						$options = [
							'date_from' => $date_from,
							'date_to' => $date_to,
							'origin_city' => $origin_city,
							'destination_city' => $destination_city,
							'traveller_number' => $travellers,
							'leg' => $key,
							'is_flight_filtered' => $is_flight_filtered,
							'search_input'     => session()->get('search_input')
						];
                        $transportType =!empty($preferences[0]['transport'][0]) ? $preferences[0]['transport'][0]:0;
                        if($transportType != '2') {
                            $search['itinerary'][$key]['transport'] = $this->map->get_transport_api_data($transport, $options);
                        }
					}
				} else {

					$search['itinerary'][$key]['transport'] = null;

				}
			}
			$last = count($search['itinerary']) - 1;
			if (!empty($search['itinerary'][$last]['hotel'])) {
				//if (!isset($search['itinerary'][$last]['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])) {
				if (!isset($search['itinerary'][$last]['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo'])) {
					$reqest_data['city_ids'] = $search['itinerary'][$last]['city']['id'];
					$reqest_data['date_from'] = session()->get('search_input')['start_date'];
					$reqest_data['auto_populate'] = session()->get('search_input')['auto_populate'];
					$reqest_data['traveller_number'] = session()->get('search_input')['travellers'];
					$reqest_data['child_number'] = session()->get('search_input')['total_children'];
					$reqest_data['search_input'] = session()->get('search_input');
					$reqest_data['rooms'] = session()->get('search_input')['rooms'];
					$reqest_data['child'] = session()->get('search_input')['child'];
					$reqest_data['pax_information'] = [];
					$response = $this->map->getSelectedHotelRoom($search['itinerary'][$last]['hotel'], $reqest_data);
					if(isset($response)){		
						$search['itinerary'][$last]['hotel']['RoomRateDetailsList']['RoomRateDetails'] = $response;		
					} else {		
						$leg['hotel'] = '';		
					}
				}
			}
			$this->session->set_search($search['itinerary'], ['date_from' => date('Y-m-d', strtotime($travel_date)), 'traveller_number' => $travellers, 'pax_information' => $pax_information, 'child_number' => $child_total, 'rooms' => $rooms]);
		}

		return view('itinenary.partials.booking_summary')->render();
	}

	public function saveBooking() {

		$search = request()->input('search');
		
		// update
		if ($search) {
			$search = json_decode($search, true);
			$pax_information = isset($search['pax_information']) ? $search['pax_information'] : [];
			$travel_date = $search['travel_date'];
			$travellers = $search['travellers'];
			$pax_information = $search['pax_information'];
			$rooms = $search['rooms'];
			$child_total = $search['child_total'];

			$next_date = null;
			$start_from = null;
			foreach ($search['itinerary'] as $key => $value) {
				if ($key == 0) {
					$start_from = $travel_date;
					$next_date = $start_from;
				} else {
					$next_date = Carbon::parse($next_date)->addDays($value['city']['default_nights'])->format('Y-m-d');
					$start_from = isset($search['itinerary'][$key - 1]['city']['date_to']) ? $search['itinerary'][$key - 1]['city']['date_to'] : $next_date;
				}

				$trans_own_arrangement = FALSE;
				$search_preferences = session()->get('search_input');
				if (isset($search_preferences['transport_types'])) {
					if ($search_preferences['transport_types'][0] == 25) {
						$trans_own_arrangement = TRUE;
					}
				}

				$last_key = count($search['itinerary']) - 1;

				if (empty($value['transport'])) {
					if ($key != $last_key) {
						if ($trans_own_arrangement != TRUE) {
							$transport = $value['transport'];
							$origin_city = $value['city'];
							$destination_city = $search['itinerary'][$key + 1]['city'];
							$date_from = $start_from;
							$add_days = intval($value['city']['default_nights']);
							$date_to = add_str_time('+' . $add_days . ' days', $start_from, 'Y-m-d');

							$transport_type_preference = (session()->has('travel_preferences')) ? session()->get('travel_preferences') : [];

							$is_flight_filtered = FALSE;
							if (count($transport_type_preference)) {
								if (isset($transport_type_preference[0]['transport'])) {
									if (count($transport_type_preference[0]['transport']) > 0) {
										$transport_type_filters = $transport_type_preference[0]['transport'];
										foreach ($transport_type_filters as $k => $filter) {
											if ($filter == '1') {
												$is_flight_filtered = TRUE;
											}
										}
									}
								}
							}

							$options = [
								'date_from' => $date_from,
								'date_to' => $date_to,
								'origin_city' => $origin_city,
								'destination_city' => $destination_city,
								'traveller_number' => $travellers,
								'leg' => $key,
								'is_flight_filtered' => $is_flight_filtered,
								'search_input'     => session()->get('search_input')
							];

							$search['itinerary'][$key]['transport'] = $this->map->get_transport_api_data($transport, $options);
						} else {
							$search['itinerary'][$key]['transport'] = NULL;
						}

					}
				}

				if (empty($value['hotel'])) {
					$return_hotel = $this->map->set_default_hotel_api(['date_from' => date('Y-m-d', strtotime($start_from)), 'traveller_number' => $travellers], $value['city']);

					$search['itinerary'][$key]['hotel'] = $return_hotel;
				}
			}

			$this->session->set_search($search['itinerary'], ['date_from' => date('Y-m-d', strtotime($travel_date)), 'traveller_number' => $travellers, 'pax_information' => $pax_information, 'rooms' => $rooms, 'child_number' => $child_total]);
		}

		return view('itinenary.partials.booking_summary')->render();
	}

	public function hotels(Request $request) {
		if (session()->get('search_input') == null) {
			return redirect('/');
		}
		$search_session = json_decode(json_encode(session()->get('search')), FALSE);

		$leg = $request->input('leg');

		if ($leg != NULL) {
			if ($leg >= count($search_session->itinerary)) {
				abort(404);
			}
		} else {
			abort(404);

		}

		$city_name = $request->city_name;
		$request->session()->put('current_city', $city_name); //set session for what city the viewed hotels belong
		$city_data = get_city_by_name($city_name);
		$city_iata_code = $search_session->itinerary[$leg]->city->airport_codes;
		$travellers = session()->get('search_input')['travellers'];
		$hotel_categories = Cache::get('hotel_categories');
		$travel_preferences = (session()->has('travel_preferences')) ? session()->get('travel_preferences') : [];
		$hotel = $search_session->itinerary[$leg];
		$temp_hotel = (array) $hotel;
		//echo "<pre>";print_r($temp_hotel);exit;
		if (empty($temp_hotel['hotel'])) {
			$price_data = 0;
			$city_id = $hotel->city->id;
			$default_hotel_id = 0;
			$default_nights = $hotel->city->default_nights;
			$default_room_price = '0.0';
			$default_room_name = '';
			$default_room_id = '0';
			$default_price_id = 0;
			$date_from = $hotel->city->date_from;
			$date_to = $hotel->city->date_to;
			$default_currency = 'AUD';
			$default_provider = 'eroam';
			$checkin = $hotel->city->date_from;
			$checkout = $hotel->city->date_to;
		} else {

			$city_id = $hotel->city->id;
			if (property_exists($hotel->hotel, 'provider') && $hotel->hotel->provider == 'expedia') {

				$default_hotel_id = $hotel->hotel->hotelId;
				$default_nights = $hotel->hotel->nights;
				$date_from = $hotel->hotel->checkin;
				$date_to = $hotel->hotel->checkout;
				$checkin = $hotel->hotel->checkin;
				$checkout = $hotel->hotel->checkout;

				$total = '@total';
				$currencyCode = '@currencyCode';
				$default_room_price = $hotel->hotel->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->ChargeableRateInfo->$total;
				$rData = json_decode(json_encode($hotel->hotel->RoomRateDetailsList->RoomRateDetails), true);

				if (isset($rData['rateDescription'])) {
					$default_room_name = $rData['rateDescription'];
				} else {
					$default_room_name = $hotel->hotel->RoomRateDetailsList->RoomRateDetails->roomDescription;
				}
				$roomCode = '@roomCode';
				if (isset($rData['RoomType'][$roomCode]) && !empty($rData['RoomType'][$roomCode])) {
					$default_room_id = $rData['RoomType'][$roomCode];
					$default_price_id = $rData['RoomType'][$roomCode];
				} else {
					$default_room_id = $hotel->hotel->RoomRateDetailsList->RoomRateDetails->roomTypeCode;
					$default_price_id = $hotel->hotel->RoomRateDetailsList->RoomRateDetails->roomTypeCode;
				}
				$default_currency = $hotel->hotel->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->ChargeableRateInfo->$currencyCode;
				$default_provider = 'expedia';

			} else {

				$default_hotel_id = $hotel->hotel->id;
				$default_nights = $hotel->hotel->nights;
				$date_from = $hotel->city->date_from;
				$date_to = $hotel->city->date_to;
				$checkin = $hotel->hotel->checkin;
				$checkout = $hotel->hotel->checkout;
				$default_room_price = $hotel->hotel->default_hotel_room->price;
				$default_room_name = !is_array($hotel->hotel->price) ? $hotel->hotel->room_name : $hotel->hotel->price[0]->room_type->name;
				$default_room_id = !is_array($hotel->hotel->price) ? $hotel->hotel->room_id : $hotel->hotel->price[0]->room_type->id;
				$default_price_id = !is_array($hotel->hotel->price) ? $hotel->hotel->price_id : $hotel->hotel->price[0]->id;
				$default_currency = (isset($hotel->hotel->currency->code)) ? $hotel->hotel->currency->code : $hotel->hotel->currency;
				$default_provider = (isset($hotel->hotel->provider)) ? $hotel->hotel->provider : 'eroam';

				if ($hotel->hotel->price) {
					if (is_array($hotel->hotel->price)) {
						$price_data = reset($hotel->hotel->price);
						$default_price_id = $price_data->id;
					} elseif (is_object($hotel->hotel->price)) {
						$price_data = reset($hotel->hotel->price);
						$default_price_id = $price_data->id;
					} elseif (is_string($hotel->hotel->price)) {
						$default_price_id = $hotel->hotel->price_id;
					}

				}
			}

		}

		$suburb = [];
		$suburb = http('get', 'suburb-by-city/' . $city_id);

		$hotel_filter = !empty(session()->get('search_input')['hotel_category_id']) ? session()->get('search_input')['hotel_category_id'] : array();
		$room_type_id = !empty(session()->get('search_input')['room_type_id']) ? session()->get('search_input')['room_type_id'] : array();

		if (empty(session()->get('search_input')['hotel_category_id'])) {
			$hotel_category_id[0] = 1;
			$key = array_search($hotel_categories[0], array_column($hotel_categories, 'id'));
			$hotel_stars = $hotel_categories[$key]['hotel_stars'];
			$hotel_stars = array_map('trim', explode(',', $hotel_stars));
			$default_hotel_category_id = 1;

		} else {
			$key = array_search(session()->get('search_input')['hotel_category_id'][0], array_column($hotel_categories, 'id'));
			$hotel_stars = $hotel_categories[$key]['hotel_stars'];
			$hotel_stars = array_map('trim', explode(',', $hotel_stars));
			$default_hotel_category_id = session()->get('search_input')['hotel_category_id'][0];
		}
		if (empty($travel_preferences)) {
			$hotel_category_id = [];
		} else {
			$hotel_category_id = $travel_preferences[0]['accommodation'];
		}
		$data = [
			'city' => $city_data,
			'city_name' => $city_name,
			'city_id' => $city_id,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'city_ids' => [$city_id],
			'default_provider' => $default_provider,
			'default_hotel_id' => $default_hotel_id,
			'default_nights' => $default_nights,
			'default_room_price' => $default_room_price,
			'default_room_name' => $default_room_name,
			'default_room_id' => $default_room_id,
			'default_price_id' => $default_price_id,
			'default_currency' => $default_currency,
			'city_iata' => $city_iata_code,
			'travellers' => $travellers,
			'leg' => $leg,
			'hotel_filter' => $hotel_filter,
			'room_type_id' => $room_type_id,
			'checkin' => $checkin,
			'checkout' => $checkout,
			'hotel_stars' => $hotel_stars,
			'default_hotel_category_id' => $default_hotel_category_id,
			'suburb' => $suburb,
			'hotel_category_id' => $hotel_category_id,
		];

		return view('accomodation.accomodation', ['data' => $data, 'page_will_expire' => 1]);
	}

	public function transports(Request $request) {
		if (session()->get('search_input') == null) {
			return redirect('/');
		}

		$session_input = session()->get('search_input');


		$transport_pref = isset($session_input['transport_types']) ? $session_input['transport_types'] : [];

		$search_session = json_decode(json_encode(session()->get('search')), FALSE);
		$leg = $request->input('leg');

		if ($leg != NULL) {
			if ($leg >= count($search_session->itinerary)) {
				echo '404 page not found.';die; // check if leg is valid
			}
		} else {
			echo '404 page not found.';die; // check if leg get parameter leg exists
		}
		$city = json_decode(json_encode(get_city_by_name($request->city_name)), FALSE);

		$country_name = $city->country_name;
		$city_name = $request->city_name;
		$selected_transport_id = 0;
		$selected_provider = 'eroam';
		$from_city_iata = NULL;
		$transport_type_id = 0;
		$city_id = 0;
		$to_city_id = 0;
		$num_of_cities = count($search_session->itinerary);
		$transport_departure = false;
		$transport_duration = '';
		$activity_dates = [];

		$itinerary = $search_session->itinerary[$leg];

		if ($leg != ($num_of_cities - 1)) {

			$days_to_add_for_departure_date = intval($itinerary->city->default_nights);
			$next_city_data = $search_session->itinerary[$leg + 1];
			$transport_departure = isset($itinerary->transport->etd) ? $itinerary->transport->etd : '';
			$transport_duration = isset($itinerary->transport->duration) ? $itinerary->transport->duration : '';
			$transport_provider = isset($itinerary->transport->provider) ? $itinerary->transport->provider : 'eroam';
			$city_id = $itinerary->city->id;
			$from_city_iata = ($itinerary->city->iata) ? $itinerary->city->iata->iata_code : NULL;
			$from_city_iatas = $itinerary->city->airport_codes;
			$to_city_id = $next_city_data->city->id; // we need to have this for querying
			$to_city_iata = ($next_city_data->city->iata) ? $next_city_data->city->iata->iata_code : NULL;
			$to_city_iatas = $next_city_data->city->airport_codes;
			$date_from = $itinerary->city->date_from;
			$date_to = $itinerary->city->date_to;
			$to_city_geohash = $next_city_data->city->geohash;
			$from_city_geohash = $itinerary->city->geohash;
			$index = $leg;

			if ($itinerary->transport) {
				$transport_type_id = $itinerary->transport->transport_type_id;
				$selected_transport_id = $itinerary->transport->id;
				if (isset($itinerary->transport->provider)) {
					$selected_provider = $itinerary->transport->provider;
				}
			}
			/*
				| Added by junfel
				| Get all activity dates
			*/
			if ($itinerary->activities) {
				foreach ($itinerary->activities as $activities) {
					$activity_dates[] = $activities->date_selected;
				}
			}

		}

		switch ($transport_provider) {
		case 'mystifly':

			if ($leg == 0) {
				$departure_date = date('Y/m/d', strtotime($date_to));

			} else {
				$transport_duration = get_hours_min($transport_duration);
				$transport_departure = date('H:i A', strtotime($transport_departure));
				$departure_date = get_departure_date($date_to, $transport_departure, $transport_duration);
			}
			break;
		case 'busbud':

			if ($leg == 0) {
				$departure_date = date('Y/m/d', strtotime($date_to));

			} else {
				$transport_duration = get_hours_min($transport_duration);
				$transport_departure = date('H:i A', strtotime($transport_departure));
				$departure_date = get_departure_date($date_to, $transport_departure, $transport_duration);
			}
			break;

		default:
			$departure_date = get_departure_date($date_to, $transport_departure, $transport_duration);

			break;
		}

		$flight_options = [
			'IsRefundable' => FALSE,
			'IsResidentFare' => FALSE,
			'NearByAirports' => FALSE,
		];

		if (session()->has('flight_options')) {
			$fl_opts = session()->get('flight_options');
			if (isset($fl_opts[$leg])) {
				$flight_options = $fl_opts[$leg];
			}
		}

		$data = [
			'country_name' => $country_name,
			'city_name' => $city_name,
			'city_id' => $city_id,
			'from_city_iata' => $from_city_iata,
			'from_city_iatas' => $from_city_iatas,
			'to_city_id' => $to_city_id,
			'to_city_iata' => $to_city_iata,
			'to_city_iatas' => $to_city_iatas,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'key' => $index,
			'flight_options' => $flight_options,
			'selected_transport_id' => $selected_transport_id,
			'selected_provider' => $selected_provider,
			'leg' => $leg,
			'departure_date' => $departure_date,
			'to_city_geohash' => $to_city_geohash,
			'from_city_geohash' => $from_city_geohash,
			'activity_dates' => $activity_dates,
		];

		$transport_type_ids = [$transport_type_id];
		session(['transport_type_ids' => $transport_type_ids]);

		if (session()->get('transport_filter')) {
			session()->forget('transport_type_ids');
			session(['transport_type_ids' => session()->get('transport_filter')]);
		}

		$day = 0;
		if (session()->has('day')) {
			$day = session()->get('day');
		}

		$transport_types = [];
		if (session()->has('transport_types')) {
			$transport_types = session()->get('transport_types');
		}

		$view_data = [
			'transport_type_options' => $transport_types,
			'day' => $day,
			'data' => $data,
			'transport_pref' => $transport_pref,
			'page_will_expire' => 1,
		];
		return view('transport.transport', $view_data);
	}

	public function proposed_itinerary() {
		if (session()->get('search_input') == null) {
			return redirect('/');
		}

		$search = session()->get('search');
		$page_will_expire = 1;
		return view('pages.proposed-itinerary')->with(compact('search', 'page_will_expire'));
	}

	public function propose_itinerary_pdf() {
		
		if (session()->get('search_input') == null) {
			return redirect('/');
		}
		$today = date("F j, Y");
		$year = date('Y');

		$search = session()->get('search');

		$pdf = PDF::loadView('itinenary.proposed-itinerary-pdf', compact('search', 'today', 'year'));
		//return $pdf->stream();
		return $pdf->stream('eRoam Itinerary.pdf');        

	}

	public function activities(Request $request) {
		$filter = new Filter();

		//get the session city by name
		
		$city = json_decode(json_encode(get_city_by_name($request->city_name)), FALSE);
		
		if (session()->get('search_input') == null) {
			return redirect('/');
		}

		//added by jayson for viator api get location by name
		//parameters needed country,destinationName
		$data = [];
		$data['destinationName'] = $city->name;
		$data['country'] = $city->country_name;

		$city_data_viator = http('post', 'service/location', $data);

		$destinationId = ($city_data_viator) ? $city_data_viator['destinationId'] : 0;
		//pr($destinationId);die;
		//end

		$city_id = $city->id;

		//get the session data
		$search_session = json_decode(json_encode(session()->get('search')), FALSE);

		$leg = $request->input('leg');

		if ($leg != NULL) {
			if ($leg >= count($search_session->itinerary)) {
				echo '404 page not found.';die; // check if leg is valid
			}
		} else {
			echo '404 page not found.';die; // check if leg get parameter leg exists
		}
		$selected_dates = [];
		$selected_activities = [];
		$dates = [];
		$dates_available = [];
		$default_nights = 0;
		$date_from = '';
		$date_to = '';
		$own_arrangement = false;
		$transport_own_arrangement = false;
		$transport_departure = false;
		$transport_duration = '';
		$activity_pref = session()->get('search_input')['interests'];
		$available_to_book = false;

		//traverse to itineraries and get city date_from and to
		$value = $search_session->itinerary[$leg];

		$default_nights = $value->city->default_nights;
		$date_from = $value->city->date_from;
		$date_to = $value->city->date_to;
		$date_from_search = strtotime($date_from. ' +1 day');
		$date_from_search= date("Y-m-d",$date_from_search);

		if(isset($request->dt))
		{
			$date_from_search= date("Y-m-d",$request->dt);
		}
		if (!$value->activities) {
			$own_arrangement = true;
		} else {

			foreach ($value->activities as $activity_key => $activity) {
				//dd($date_from_search,$activity->date_selected);
				if($activity->date_selected == $date_from_search)
				{
					$activity_code = isset($activity->provider) ? $activity->provider . '_' . $activity->id : 'eroam_' . $activity->id;
					$date_selected[$activity_code] = isset($activity->date_selected) ? $activity->date_selected : '';
					$selected_activities[] = $activity_code;
					$selected_dates[$activity_code] = date('jS, F Y', strtotime($activity->date_selected));

					$activity_duration = (int) $activity->duration;
					if ($activity_duration > 1) {
						$activity_duration = $activity_duration - 1;
						foreach (range(1, $activity_duration) as $day) {
							$selected_dates[] = date('jS, F Y', strtotime($activity->date_selected . '+' . $day . ' ' . str_plural('day', $day)));
						}
					}
				}
			}
		}
		if (!$value->transport) {
			$transport_own_arrangement = true;
		} else {
			$transport_departure = $value->transport->etd;
			$transport_duration = $value->transport->duration;
		}
		/*
			|
		*/

		$departure_date = $date_to;

		/*
			| Added by junfel
			| Setting available dates for booking an activity
		*/

		if ($default_nights > 1) {
			$time = convert_24hr($transport_departure);

			$time = get_hour($time);
			for ($counter = 1; $counter < $default_nights; $counter++) {
				$date_generated = date('Y/m/d', strtotime($date_from . '+ ' . $counter . ' ' . str_plural('day', $counter)));
				/*
					| Check if there are activities being booked on the dates below, then exclude it to available dates.
				*/
				if (in_array(date('jS, F Y', strtotime($date_generated)), $selected_dates)) {
					continue;
				}
				/*
					| checking if the next date is equals to departure date
					| if not, then add the date.
				*/
				if ($date_generated == $departure_date) {
					/*
						| If departure is evening or own arrangement, then add date
					*/
					if ($time >= 18 || $transport_own_arrangement) {
						$dates_available[] = $date_generated;
						$available_to_book = true;
					}
				} else {
					$dates_available[] = $date_generated;
				}
			}
			/*
				| Just making sure that there will be no duplicated dates
			*/
			$dates_available = array_unique($dates_available);
		}
		/*
			| if dates available is empty, then add 0000/00/00
			| empty array can't be saved in a session.
		*/
		$dates_available = count($dates_available) > 0 ? $dates_available : array('0000/00/00');

		/*
			| Add to session available dates,
			| These dates will be displayed in the datepicker for bookings.
		*/
		session(['dates_available_' . $city_id => $dates_available]);

		/*
			| End setting available dates.
		*/

		$index = 0;
		$city_id = $city->id;
		$selected_activities = count($selected_activities) > 0 ? json_encode($selected_activities) : 0;

		$selected_date_req = '';
		if(isset($date_from_search))
		{
			$selected_date_req = date('jS, F Y',strtotime($date_from_search));
		}
		
		$page_will_expire = 1;

		$viatorCategory = http('get', 'viator/category-list/v', [], $this->headers);
		//dd($viatorCategory);
		//dd(session()->get('search'));
		return view('activity.activity', compact('city', 'date_from', 'date_to', 'index', 'city_id', 'selected_activities', 'destinationId', 'selected_dates', 'leg', 'activity_pref', 'available_to_book', 'departure_date', 'page_will_expire','date_from_search','selected_date_req','viatorCategory'));
	}

	public function view_more_hotel_details(Request $request) {

		$provider = request()->input('provider');
		$code = request()->input('code');
		$data_attr = json_encode(request()->input('dataAttr'));
		$star_rating = request()->input('star_rating');

		// init
		$data = [];
		$data['hotel_name'] = '';
		$data['city_name'] = '';
		$data['hotel_description'] = '';
		$data['primary_photo'] = '';
		$data['images'] = [];
		$data['address'] = '';
		$data['telephone'] = '';
		$data['google_map_location'] = '';
		$data['lat'] = '';
		$data['lng'] = '';
		$data['cancel_policy'] = '';
		$data['defaultKey'] = 'NO';

		switch ($provider) {
		case 'eroam':
			//print_r(session()->get('search_input'));exit;
			$adata = session()->get('search_input');
			$response = http('post', 'eroam/hotel/' . $code,$adata);
			//echo "<pre>";print_r($response);exit;
			if(isset($response['price'][0]))
			{
				$price = $response['price'];
				usort($price,array($this,'sortPrice'));
			}else{
				$price = array();
				$price[0] = $response['price'];
				$response['price'] = $price[0];
			}
  			$response['price'] = $price;
			$search_session = json_decode(json_encode(session()->get('search')), TRUE);
	  		$search_session = $search_session['itinerary'][request()->input('leg')]['hotel'];

	  		$hotel_price_id = '';
	  		if(isset($search_session['default_hotel_room']['hotel_price_id'])){
	  			$hotel_price_id = $search_session['default_hotel_room']['hotel_price_id'];
	  		}
	  		$defaultKey = $this->getDefaultRoomKey($response['price'],$hotel_price_id);
	  		if($defaultKey != 'NO'){
	  			$defaultKey = substr($defaultKey,3);
	  			$data['defaultKey'] = $defaultKey;
	  			$tempRoom[] = $response['price'][$defaultKey];
		  		unset($response['price'][$defaultKey]);
		  		$response['price'] = array_merge($tempRoom,$response['price']);
	  		}
	  		//echo "<pre>";print_r($response);exit;
			$data['provider'] = 'eroam';
			$data['hotelName'] = $response['name'];
			$data['searchCity'] = $response['city']['name'];
			$data['leg'] = request()->input('leg');
			$data['city_id'] = request()->input('city_id');
			$data['country_name'] = request()->input('countryName');
			$data['arrivalDate'] = request()->input('arrivalDate');
			$data['departureDate'] = request()->input('departureDate');
			$data['currencyCode'] = request()->input('currencyCode');
			$data['hotelSeason'] = request()->input('hotelSeason');
			$data['hotel_description'] = $response['description'];
			$data['amenities_description'] = $response['amenities_description'];
			$data['location_description'] = $response['location_description'];
			$data['checkin_instruction'] = $response['checkin_instruction'];
			$data['hotelId'] = request()->input('hotelId');
			$data['address_1'] = request()->input('address_1');
			$data['hotelPrice'] = number_format($response['price'][0]['price'], 2);
			$data['refundable'] = $response['refundable'];
			$data['description'] = $response['description'];
			$data['hotelCity'] = '';
			$data['num_of_rooms'] =  session()->get('search_input')['rooms'];
			$data['latitude'] = (request()->input('latitude') == 'undefined') ? '' : request()->input('latitude');
			$data['longitude'] = (request()->input('longitude') == 'undefined') ? '' : request()->input('longitude');
			if (isset($response['geo_location']) && !empty($response['geo_location'])) {
				$data['google_map_location'] = $response['geo_location'];
			} else {
				$data['google_map_location'] = $response['name'] . ', ' . $response['city']['name'] . ', ' . $response['city']['country']['name'];
			}
			$data['HotelImages']['HotelImage']=array();
			if ($response['image']) {
				foreach ($response['image'] as $key => $value) {

					$data['HotelImages']['HotelImage'][$key]['url'] = trans('home.image_url',['image_title'=>  'hotels/'.$value['original']]) ;
				}
			}
			//print_r($data);exit;
			if ($response['price']) {
				foreach ($response['price'] as $key => $value) {
					$data['rooms'][$key]['hotel_id'] = request()->input('hotelId');
					$data['rooms'][$key]['hotel_price_id'] = $value['id'];
					$data['rooms'][$key]['with_breakfast'] = $value['with_breakfast'];
					$data['rooms'][$key]['minimum_pax'] = $value['minimum_pax'];
					$data['rooms'][$key]['max_pax'] = $value['max_pax'];
					$data['rooms'][$key]['price'] = $value['price'];
					$data['rooms'][$key]['hotel_room_type_id'] = $value['hotel_room_type_id'];
					$data['rooms'][$key]['room_type_id'] = $value['room_type']['id'];
					$data['rooms'][$key]['name'] = $value['room_type']['name'];
					$data['rooms'][$key]['description'] = $value['room_type']['description'];
					$data['rooms'][$key]['pax'] = $value['room_type']['pax'];
					$data['rooms'][$key]['sequence'] = $value['room_type']['sequence'];
					$data['rooms'][$key]['dorm_pax'] = $value['room_type']['dorm_pax'];
					$data['rooms'][$key]['female_only'] = $value['room_type']['female_only'];
					$data['rooms'][$key]['season'] = $value['season']['id'];
					$data['rooms'][$key]['cancellation_policy'] = $value['season']['cancellation_policy'];
				}
			}
			$data = json_decode(json_encode($data));

			break;

		case 'hb':
			
			$response = http('post', 'hb/gethoteldetailsbycodes', ['hotel_code' => array($code)]);

			$data['hotel_name'] = $response[0][$code]['name'];
			$data['city_name'] = session()->get('current_city');
			$data['hotel_description'] = $response[0][$code]['description'];
			$data['address'] = $response[0][$code]['address'];
			$data['telephone'] = $response[0][$code]['phones'] ? json_decode($response[0][$code]['phones'])[0]->phoneNumber : '';
			$data['lat'] = $response[0][$code]['latitude'];
			$data['lng'] = $response[0][$code]['longitude'];

			$data['provider'] = 'hb';
			$data['star_rating'] = $star_rating;
			// IMAGES
			$hb_images = json_decode($response[0][$code]['images']);
			if ($hb_images) {
				$data['primary_photo'] = config('env.HB_IMAGE_PATH') . $hb_images[0]->path;
				foreach ($hb_images as $key => $value) {
					$data['images'][] = config('env.HB_IMAGE_PATH') . $value->path;
				}
			}
			break;
		
		case 'expedia':
			
			$arrivalDate = date('n/j/Y', strtotime(request()->input('arrivalDate')));
			$departureDate = date('n/j/Y', strtotime(request()->input('departureDate')));

			$num_of_rooms = session()->get('search_input')['rooms'];
			$children = session()->get('search_input')['child'];
			$room = '';
			if ($num_of_rooms == 1) {
				$room .= '&room1=' . session()->get('search_input')['num_of_adults'][0];
				if (isset($children[0]) && !empty($children[0])) {
					$c_array = implode(',', $children[0]);
					$c_array = str_replace('0', '1', $c_array);
					$room .= ',' . count($children[0]) . ',' . $c_array;
				}
			} else {
				for ($i = 0; $i < $num_of_rooms; $i++) {
					$n = $i + 1;
					$room .= '&room' . $n . '=' . session()->get('search_input')['num_of_adults'][$i];
					if (isset($children[$i]) && !empty($children[$i])) {
						$c_array = implode(',', $children[$i]);
						$c_array = str_replace('0', '1', $c_array);
						$room .= ',' . count($children[$i]) . ',' . $c_array;
					}
				}
			}
			$roomTypeCode = request()->input('roomTypeCode');
			$includeDetails = request()->input('includeDetails');
			$includeDetails = request()->input('includeDetails');
			$options = request()->input('options');
			$rateKey = request()->input('rateKey');
			$latitude = request()->input('latitude');
			$longitude = request()->input('longitude');
			$customerSessionId = request()->input('customerSessionId');

			$i = 0;
			while($i < 10) {
				$i++;
				$response = $this->expediaApi->getHotelDetails($code, $arrivalDate, $departureDate, $room, $roomTypeCode, $includeDetails, $options, $rateKey, $latitude, $longitude, $customerSessionId);
				if($response != '')
				break;
			}

			//$data = json_encode(value)
			//$response = json_decode($response)->data;
			
			$data = $response;
			$data->leg = request()->input('leg');
			$data->searchCity = request()->input('searchCity');
			$data->provider = 'expedia';
			$city_data = get_city_by_name(request()->input('searchCity'));
			$data->country_name = $city_data['country_name'];
			$data->latitude = request()->input('latitude');
			$data->longitude = request()->input('longitude');
			$data->arrivalDate = request()->input('arrivalDate');
			$data->departureDate = request()->input('departureDate');
			$data->city_id = request()->input('city_id');
			$data->hotelPrice = request()->input('hotelPrice');
			$data->google_map_location = '';
			
			break;

		case 'aot':
			
			$response = http('get', 'aot/hotel/' . $code);
			$data['hotel_name'] = $response['Name'];
			$data['city_name'] = session()->get('current_city');
			$data['hotel_description'] = $response['Description'];
			$data['address'] = $response['Address1'] . ', ' . $response['Address2'];
			$data['lat'] = json_decode($response['MetaData'])->Latitude;
			$data['lng'] = json_decode($response['MetaData'])->Longitude;
			$data['cancel_policy'] = $response['CancellationPolicy'];

			$aot_images = '';
			if (count(json_decode($response['Images'])) > 0) {
				$aot_images = json_decode($response['Images'])->image;
			}
			$data['provider'] = 'aot';
			$data['star_rating'] = $star_rating;

			if ($aot_images) {
				$data['primary_photo'] = $aot_images[0]->ServerPath;
				foreach ($aot_images as $key => $value) {
					$data['images'][] = $value->ServerPath;
				}
			}
			break;
		
		case 'ae':
			
			$response = http('get', 'arabian/explorer/get-hotel-details/' . $code);
			$data['hotel_name'] = $response['Name'];
			$data['city_name'] = session()->get('current_city');

			$data['hotel_description'] = $response['ShortDescription'];
			$data['address'] = $response['Address'];
			$data['lat'] = $response['Latitude'];
			$data['lng'] = $response['Longitude'];
			$data['cancel_policy'] = $response['Policies'];
			$ae_images = json_decode($response['Medias']);
			$data['provider'] = 'ae';
			$data['star_rating'] = $star_rating;

			if ($ae_images->Media) {
				$data['primary_photo'] = config('env.AE_IMAGE_PATH') . $ae_images->Media[0]->Path;
				foreach ($ae_images->Media as $key => $value) {
					$image = config('env.AE_IMAGE_PATH') . $value->Path;
					if (!in_array($image, $data['images'])) {
						$data['images'][] = $image;
					}

				}

			}

			break;
		}
		//echo "next array<pre>";print_r($data);exit;
		$page_will_expire = 1;
		return view('accomodation.accomodation_detail', compact('provider', 'code', 'data', 'data_attr', 'page_will_expire'));
	}

	public function eroam_activity_view_more($id) {
		$activity = http('get', 'eroam/activity/' . $id);

		if (count($activity['image']) > 0) {
			foreach ($activity['image'] as $key => $value) {
				$activity['image'][$key]['small'] = $this->cms_url . $value['small'];
				$activity['image'][$key]['medium'] = $this->cms_url . $value['medium'];
				$activity['image'][$key]['large'] = $this->cms_url . $value['large'];
				$activity['image'][$key]['original'] = $this->cms_url . $value['original'];
				$activity['image'][$key]['thumbnail'] = $this->cms_url . $value['thumbnail'];

				if ($value['is_primary'] == 1) {
					$activity['primary'] = $activity['image'][$key]['small'];
				}
			}
		}
		$page_will_expire = 1;
		return view('pages.view-more-activity', compact('activity', 'page_will_expire'));

	}

	public function viator_activity_view_more($code) {

		$data['code'] = $code;
		$data['currencyCode'] = 'AUD';
		$activity = http('post', 'search/product', $data);
		$page_will_expire = 1;
		return view('pages.view-more-viator-activity', compact('activity', 'page_will_expire'));

	}

	public function view_more_activity_details() {
		$provider = request()->input('provider');
		$code = request()->input('code');
		$select = request()->input('select');
		$cityid = request()->input('cityid');
		$leg = request()->input('leg');
		$startDate = session()->get('search')['itinerary'][$leg]['city']['date_from'];
		$endDate = session()->get('search')['itinerary'][$leg]['city']['date_to'];
		$country_city_name = request()->input('country_city_name');
		$date = request()->input('date');
		$act_name = request()->input('activity_name');

		$data = [];
		$data['activity_name'] = '';
		$data['city_name'] = '';
		$data['activity_description'] = '';
		$data['primary_photo'] = '';
		$data['images'] = [];
		$data['address'] = '';
		$data['telephone'] = '';
		$data['google_map_location'] = '';
		$data['price'] = '';
		$data['code'] = '';
		$data['duration'] = '';

		$data['highlights'] = '';
		$data['itinerary'] = '';
		$data['description'] = '';

		$data['inclusions'] = '';
		$data['exclusions'] = '';
		$data['additionalInfo'] = '';
		$data['voucherRequirements'] = '';
		$data['departurePoint'] = '';
		$data['departureTime'] = '';
		$data['departureTimeComments'] = '';
		$data['returnDetails'] = '';
		$data['provider'] = $provider;
		$data['select_grade_code'] = '';
		$data['return_details'] = '';
		$data['salesPoints'] = '';
		$data['has_accomodation'] = '';
		$data['has_transport'] = '';

		$data['accommodation_nights'] = '';
		$data['accommodation_details'] = '';	
		$data['transport_details'] = '';
		$data['destination_city_id'] = '';

		$data['reception_email'] = '';
		$data['reception_phone'] = '';
		$data['website'] = '';
		$data['pax_type'] = '';	
		$data['bookingQuestions'] = [];
		$data['hotelPickup'] = [];
		$hotel_list = [];
		$data['leg'] = $_REQUEST['leg'];
		$data['key'] = 0;
		$paxInfo = [];
		$error = null;
		switch ($provider) {
		case 'eroam':

			$response = http('get', 'eroam/activity/' . $code);
			
			$data['activity_name'] = $response['name'];
			$data['city_name'] = $response['city']['name'];
			$data['activity_description'] = $response['description'];
			$data['address'] = $response['address_1'];
			$data['telephone'] = $response['reception_phone'];
			$data['google_map_location'] = $response['name'] . ', ' . $response['city']['name'] . ', ' . $response['city']['country']['name'];
			$data['photos']['default'] = [];
			$data['departurePoint'] = $response['pickup'];
			$data['voucherRequirements'] = $response['voucher_comments'];
			$data['return_details'] = $response['dropoff'];
			$data['reception_email'] = $response['reception_email'];
			$data['reception_phone'] = $response['reception_phone'];
			$data['website'] = $response['website'];
			$data['pax_type'] = $response['pax_type'];

			$data['price'] = round($response['activity_price'][0]['price']);
			$data['code'] = $response['id'];
			$data['duration'] = $response['duration'] . ' Day';
			$duration = $response['duration'];
			$data['rating'] = $response['ranking'];
			
			$data['has_accomodation'] = $response['has_accomodation'];
			$data['has_transport'] = $response['accommodation_nights'];

			$data['accommodation_nights'] = $response['accommodation_nights'];
			$data['accommodation_details'] = $response['accommodation_details'];	
			$data['transport_details'] = $response['transport_details'];
			$data['destination_city_id'] = $response['destination_city_id'];
			
			$bdate = str_replace(",", "", $date);
			$timestamp = strtotime($bdate);
			$bookingDate = date('Y-m-d', $timestamp);
			$aws_url = "https://eroam-dev.s3.us-west-2.amazonaws.com/activities/";
			// IMAGES
			if ($response['image']) {
				foreach ($response['image'] as $key => $value) {
					if ($value['is_primary'] == 1) {
						$data['primary_photo'] = $aws_url . $value['original'];
					}
					$data['images'][] = $aws_url . $value['original'];
				}
			}
			
			break;
		case 'viator':
		//$code = "3627PARAPTHTLCDG";

			$response = http('post', 'search/product', ['code' => $code]);	

			if(!empty($response))
			{
				$data['return_details'] = $response['returnDetails'];
				$data['activity_name'] = $response['title'];
				$data['city_name'] = $response['city'];
				$data['activity_description'] = $response['shortDescription'];
				$data['primary_photo'] = $response['thumbnailHiResURL'];
				$data['photos']['default'] = [];
				$data['address'] = $response['location'];
				$data['google_map_location'] = $response['location'];

				$data['price'] = $response['merchantNetPriceFrom'];
				$data['code'] = $response['code'];
				$data['duration'] = $response['duration'];
				$duration = $response['duration'];
				$data['images'][] = $response['thumbnailHiResURL'];


				$data['highlights'] = $response['highlights'];
				$data['description'] = $response['description'];
				$data['itinerary'] = $response['itinerary'];

				$data['inclusions'] = $response['inclusions'];
				$data['exclusions'] = $response['exclusions'];
				$data['additionalInfo'] = $response['additionalInfo'];
				$data['voucherRequirements'] = $response['voucherRequirements'];
				$data['departurePoint'] = $response['departurePoint'];
				$data['departureTime'] = $response['departureTime'];
				$data['departureTimeComments'] = $response['departureTimeComments'];
				$data['returnDetails'] = $response['returnDetails'];
				$data['rating'] = $response['rating'];
				$data['tourGrades'] = $response['tourGrades'];
				$data['ageBands'] = $response['ageBands'];
				$data['salesPoints'] = $response['salesPoints'];
				//$data['termsAndConditions'] = $response['termsAndConditions'];	
				if($response['merchantCancellable'] == true)
				{
					$data['termsAndConditions'] = 'Fully refundable up to 48 hours';
				}
				else
				{
					$data['termsAndConditions'] = 'Fully non-refundable at time of booking';
				}
				$data['bookingQuestions'] = $response['bookingQuestions'];	
				
				$data['hotelPickup'] = ($response['hotelPickup'] == true) ? "Yes" : "No";
				if($response['hotelPickup'] == true)
				{
					
					$hotel_list = http('post', 'service/booking/hotels', ['productCode' => $code]);		
					//$hotel_list = http('post', 'service/booking/hotels', ['productCode' => '2280AAHT']);	
					
				}
				
				$adults 	= session()->get('search_input')['travellers'];
				$childAge 	= session()->get('search_input')['child'];
				$child  	= session()->get('search_input')['total_children'];
				$data['adult'] = $adults;
				$data['child'] = 0;
				$data['infant'] = 0;
				$data['length_of_stay'] = '';

				$sessions = session()->get('search');
				
				$getChildInfantTotals = checkAdultChildInfantTotal($response['ageBands'],session()->get('search_input')['pax']);
				$paxInfo = $getChildInfantTotals['pax_info'];

				$data['paxInfo'] = $paxInfo;
				
				$getChildInfantTotal = $getChildInfantTotals['pax_age'];

				$dataT['ageBands'] = [
					["bandId" => 1,"count" => $getChildInfantTotal['adult']] 
				];
				if(isset($getChildInfantTotal['child']))
				{
					array_push($dataT['ageBands'], ["bandId" => 2,"count" => $getChildInfantTotal['child']]);
					$data['child'] = $getChildInfantTotal['child'];
				}
				if(isset($getChildInfantTotal['infant']))
				{
					array_push($dataT['ageBands'], ["bandId" => 3,"count" => $getChildInfantTotal['infant']]);
					$data['infant'] = $getChildInfantTotal['infant'];
				}
				$data['adult'] = $getChildInfantTotal['adult'];			
				
				$bdate = str_replace(",", "", $date);
				$timestamp = strtotime($bdate);
				$bookingDate = date('Y-m-d', $timestamp);
				$dataT['productCode'] = $code;
				$dataT['bookingDate'] = $bookingDate;
				$dataT['currencyCode'] = 'AUD';

				$data['allTourGrades'] = http('post', 'getTourGrades', $dataT);
				
				if ($response['productPhotos'][0]) {
					foreach ($response['productPhotos'] as $product_photo) {
						$data['photos']['default'][] = $product_photo['photoURL'];
						$data['images'][] = $product_photo['photoURL'];
					}
				}
				if ($response['userPhotos']) {
					foreach ($response['userPhotos'] as $user_photo) {
						$data['images'][] = $user_photo['photoURL'];
					}
				}

				$data['select_grade_code'] = [];
				$data['default_selected_activity'] = [];
				
				
				foreach ($sessions['itinerary'] as $session) {	
					//dd($session['city']['name'], $response['primaryDestinationName']);
					/*if($session['city']['name'] == $response['primaryDestinationName'])
					{*/
						$i = 0;
						if(!empty($session['activities']))
						{
							foreach ($session['activities'] as $key => $activity) {
								if($activity['id'] == $code && $activity['date_selected'] == $bookingDate)
								{	
									$data['key'] = $key;
									$data['paxInfo'] = $activity['paxInfo'];
									array_push($data['select_grade_code'],$activity['tourGrades']['gradeCode']);
									array_push($data['default_selected_activity'],$activity['id']);
									//array_push($data['default_selected_question_answer'],$activity['bookingQuestionAnswers']);
									$data[$activity['tourGrades']['gradeCode']] = [];
									//$data['specialRequirement'][$activity['tourGrades']['gradeCode']] = [];

									if(isset($activity['bookingQuestionAnswers']))
									{
										$bq = ['bookingQuestionAnswers' => $activity['bookingQuestionAnswers']];
										array_push($data[$activity['tourGrades']['gradeCode']],$bq);
									}
									if(isset($activity['specialRequirement']))
									{
										$sr = ['specialRequirement' => $activity['specialRequirement']];
										array_push($data[$activity['tourGrades']['gradeCode']],$sr);
									}
									if(isset($activity['hotelPickups']))
									{
										$hp = ['hotelPickups' => $activity['hotelPickups']];
										array_push($data[$activity['tourGrades']['gradeCode']],$hp);
									}
								}
							}	
						}				
						$data['length_of_stay'] = $session['city']['default_nights'];
					//}

				}
				break;
			}
		}
		
		if(isset($bdate))
		{
			$date = date('j M Y', strtotime($bdate));
		}
	
		if(!empty($response))
		{
			$data['select'] = $select;
			$data['cityid'] = $cityid;
			$data['startDate'] = $startDate;
			$data['endDate'] = $endDate;
			$data['bookingDate'] = $bookingDate;
			
			
			$data['attributes'] = '
			data-provider=' . $provider . '
			data-city-id= ' . $cityid . '
			data-index=' . $data['code'] . '
			data-activity-id=' . $data['code'] . '
			data-is-selected=' . $select . '
			data-price=' . $data['price'] . '
			data-name=' . $data['activity_name'] . '
			data-currency=' . session()->get('search')['currency'] . '
			data-description=' . substr($data['activity_description'], 0, 425) . '
			data-duration=' . $duration . '
			data-label=""
			data-startdate=' . $startDate . '
			data-bookdate=' . $bookingDate . '
			data-enddate=' . $endDate . '';
			
			$page_will_expire = 1;
			
			return view('activity.view-more-activity-details')->with(compact('data', 'provider', 'code', 'page_will_expire','country_city_name','date','paxInfo','hotel_list'))->render();			
		}

		return  response()->json(['error'=>'1','act_name' => "<div class='alert alert-warning' style='margin-bottom:0px !important'>Sorry! There is some problem with <strong>".$act_name."</strong>.</div>"]);;
	}

	public function get_cities_by_country_id() {
		$country_id = request()->input('country_id');

		if ($country_id) {
			return get_cities_by_country_id($country_id);
		}
	}

	public function about_us() {
		return view('pages.about-us');
	}

	public function email_proposed_itinerary() {
		if (session()->get('search_input') == null) {
			return redirect('/');
		}
		$today = date("F j, Y");
		$year = date('Y');
		$email = request()->input('emails');
		$name = request()->input('name');

		$search = session()->get('search');

		$pdf = PDF::loadView('pages.proposed-itinerary-pdf', compact('search', 'today', 'year'));
		$data = [];
		$data['name'] = $name;

		Mail::send('pages.email-itinerary', $data, function ($message) use ($email, $name, $pdf) {
			$message->to($email)->subject('eRoam-Itinerary');
			$message->from('res@eroam.com');
			$message->attachData($pdf->output(), "itinerary.pdf");
		});

		return "success";
	}

	public function getOrderId(){
		$data = http('post','licensee-account',['domain'=>$this->domain]);
		$data = $data['licensee_account']['licensee_account']['order_id_format'];
		return $data;
	}

	public function privacy_policy() {
		$data = http('post','licensee-account',['domain'=>$this->domain]);
		$data = $data['licensee_account']['licensee_account'];
		return view('pages.privacy-policy',compact('data'));
	}
	
	public function affilliates() {
		return view('pages.affilliates');
	}
	
	public function terms() {
		$data = http('post','licensee-account',['domain'=>$this->domain]);
		$data = $data['licensee_account']['licensee_account'];
		return view('pages.terms',compact('data'));
	}
	public function sitemap() {
		return view('pages.sitemap');
	}

	public function contact_us() {
		return view('pages.contact-us');
	}
	
	public function refundPolicy() {
		$domain = substr (request()->root(), 7);
		$data = http('post','licensee-account',['domain'=>$this->domain]);
		$data = $data['licensee_account']['licensee_account'];
		return view('pages.refund-policy',compact('data'));
	}

	public function send_contact_form(Request $request) {

		$name = $request->input('name');
		$email = $request->input('email');
		$message = $request->input('message');

		$validate = Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'required|email',
			'message' => 'required',
		]);

		if ($validate->fails()) {
			return redirect('contact-us')
				->withErrors($validate, 'contact')
				->withInput();
		} else {
			$mail_content = [
				'text' => $message,
				'email' => $email,
				'name' => $name,
			];
			Mail::send(['html' => 'mail.contact-us'], $mail_content, function ($message) {
				$message->to('info@eroam.com')->subject('Contact Us');
				$message->from('res@eroam.com');
			});

			return redirect('contact-us')
				->with('message', 'Message successfully sent.');
		}
	}
	
	public function enquiryForm(Request $request) {
		$first_name = $request->input('first_name');
		$family_name = $request->input('family_name');
		$email = $request->input('email');
		$phone = $request->input('phone');
		$travel_date = $request->input('travel_date');
		$comments = $request->input('comments');
		$product = $request->input('product');
		$postcode = $request->input('postcode');

		$validate = Validator::make($request->all(), [
			'first_name' => 'required',
			'email' => 'required|email',
			'family_name' => 'required',
			'postcode' => 'required',
		]);

		$mail_content = [
			'first_name' => $first_name,
			'family_name' => $family_name,
			'email' => $email,
			'phone' => $phone,
			'travel_date' => $travel_date,
			'comments' => $comments,
			'product' => $product,
			'postcode' => $postcode,
		];
		Mail::send(['html' => 'mail.enquiry'], $mail_content, function ($message) use ($product) {
			$message->to('test02@mailinator.com')->subject('Enquiry For ' . $product . '');
			$message->from('test@octalsoftware.com');
		});
		if (count(Mail::failures()) > 0) {
			echo '2';
		} else {
			echo '1';
		}
		exit;
	}
	
	public function register() {
		if (!session()->has('initial_authentication')) {
			return redirect('app/login');
		}

		$ip_address = '137.59.252.196';

		$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip_address));
		$countryName = $query['country'];
		$cities = Cache::get('cities');
		$countries = Cache::get('countries');
		$labels = Cache::get('labels');
		$travellers = Cache::get('travellers');

		if (!session()->has('transport_types')) {
			session()->put('transport_types', $travellers['transport_types']);
		}
		$travel_pref = [];
		$interest_ids = [];
		if (session()->has('travel_preferences')) {
			$travel_pref = session()->get('travel_preferences');
			$travel_pref = reset($travel_pref);
			$interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
		}

		return view(
			'user.login',
			[
				'travelers' => $travellers,
				'travel_pref' => $travel_pref,
				'interest_ids' => $interest_ids,
				'countryName' => $countryName,
				'default_currency' => 'AUD',
				'is_register' => '1',
			]
		);

	}

	public function send_registration(Request $request) {
		$first_name = $request->input('first_name');
		$last_name = $request->input('last_name');
		$email = $request->input('email');
		$password = $request->input('password');
		$currency = $request->input('currency');
		$nationality = $request->input('nationality');
		$gender = $request->input('gender');
		$age = $request->input('age_group');
		$data = [];
		$data['first_name'] = $first_name;
		$data['last_name'] = $last_name;
		$data['email'] = $email;
		$data['password'] = $password;
		$data['currency'] = $currency;
		$data['pref_nationality_id'] = $nationality;
		$data['pref_gender'] = $gender;
		$data['pref_age_group_id'] = $age;
		$data['url'] = url('');

		$headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url(''),
		];
		$response = http('post', 'user/create-customer', $data, $headers);
	
		if ($response['successful']) {
			return redirect('login')
				->with('register-success', $response['message']);
		} else {
			return redirect('register')
				->with('register_error', $response['message'])
				->withInput();
		}
	}
	
	public function remove_itinerary(Request $request) {
		$itineraryNumber = $request->itineraryNumber;
		$itineraryType = $request->itineraryType;
		$activityNumber = $request->activityNumber;
		$data = session()->get('search');
		if ($itineraryType == 'hotel' || $itineraryType == 'transport'):
			$data['itinerary'][$request->itineraryNumber][$request->itineraryType] = "";
		elseif ($itineraryType == 'activities'):
			unset($data['itinerary'][$request->itineraryNumber][$request->itineraryType][$activityNumber]);
		endif;
		session()->put('search', $data);
		self::recalculate_totle_price(session()->get('search'));
		return redirect('payment-summary/');
	}

	public function recalculate_totle_price($data) {
		if (isset($data['itinerary'])):
			$this->session->set_search($data['itinerary'], ['date_from' => date('Y-m-d', strtotime($data['travel_date'])), 'traveller_number' => $data['travellers'], 'pax_information' => $data['pax_information'], 'child_number' => $data['child_total'], 'rooms' => $data['rooms']]);
		endif;
	}

	public function book_itinerary_signin_guest_checkout() {
		if (session()->get('user_auth')['id']):
			return redirect('/review-itinerary/');
		else:
			$data = session()->get('search');
			if($data['itinerary'][0]['hotel']['provider'] == 'expedia')
				$priceCheck = $this->expediaApi->expediaPriceCheck();
			$totalCost = str_replace(',', '', $data['cost_per_person']);
			$travellers = $data['travellers'];
			$currency = $data['currency'];
			$totalDays = str_replace(' Nights', '', $data['total_number_of_days']);
			$countries = Cache::get('countries');
			$startDate = date('j M Y', strtotime(session()->get('search')['itinerary'][0]['city']['date_from']));
			$endDate = date('j M Y', strtotime(session()->get('search')['itinerary'][count(session()->get('search')['itinerary']) - 1]['city']['date_to']));

			return view('itinenary.partials.guest-checkout')->with(compact('priceCheck', 'itinerary', 'totalCost', 'travellers', 'currency', 'countries', 'startDate', 'endDate', 'totalDays'));
		endif;
	}
	
	public function payment_summary_view(AirRevalidate $AirRevalidate,GetCityAirportCode $GetCityAirportCode) {
		session()->put('page','signin-guest-checkout');
		$data = session()->get('search');
		$child = array_sum(session()->get('search_input')['num_of_children']);
		$isFlightAvailable = 0;
		$isFlightflag = 0;
		//echo "<pre>";print_r($data);exit;
		if($data['itinerary'][0]['hotel']['provider'] == 'expedia')
			$priceCheck = $this->expediaApi->expediaPriceCheck(); //Call to expdia PriceCheck function
		$totalCost = str_replace(',', '', $data['cost_per_person']);
		
		$travellers = $data['travellers'];
		$currency = $data['currency'];
		$totalDays = str_replace(' Nights', '', $data['total_number_of_days']);
		$countries = Cache::get('countries');
		$startDate = date('j M Y', strtotime(session()->get('search')['itinerary'][0]['city']['date_from']));
		$endDate = date('j M Y', strtotime(session()->get('search')['itinerary'][count(session()->get('search')['itinerary']) - 1]['city']['date_to']));

		$domain = $this->domain;
		$data_licensee = http('post','licensee-account',['domain'=>$domain]);
		$licensee_account = $data_licensee['licensee_account']['licensee_account'];

		if (session()->has('search')) {
			$itinerary = json_decode(json_encode(session()->get('search')['itinerary']), false);
			$leg_detail = session()->get('search');

			foreach ($leg_detail['itinerary'] as $key1 => $itinerary) {
				
				foreach ($itinerary as $key => $itinerary_data) {

					if (!empty($itinerary_data)) {
						if ($key == 'transport') {

							if (!empty($itinerary_data)) {
								if (isset($itinerary_data['provider'])) {
									if ($itinerary_data['provider'] == 'busbud') {
										$booking_data = array();
										$booking_data['travellers'] = $travellers;
										$booking_data['child'] = $child;
										$booking_data['childAge']=session()->get('childAge');
										$booking_data['departureId'] = $itinerary_data['id'];
										$booking_data['departuredate'] = $itinerary_data['departuredate'];
										$booking_data['destinationlocationcode'] = $itinerary_data['destinationlocationcode'];
										$booking_data['originlocationcode'] = $itinerary_data['originlocationcode'];
										$headers = [
											'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
											'Origin' => url(''),
										];
										
										$response = http('post', 'createBusBudCart', $booking_data, $headers);
										
										if ($response['result'] == 'Yes') { 
											$leg_detail['itinerary'][$key1]['transport']['cartavailable'] = 'Yes';
											$leg_detail['itinerary'][$key1]['transport']['cartId'] = $response['cartId'];
											$leg_detail['itinerary'][$key1]['transport']['passangerArray'] = $response['passengers'];
											$leg_detail['itinerary'][$key1]['transport']['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
										} else {
											$leg_detail['itinerary'][$key1]['transport']['cartavailable'] = 'No';
											$leg_detail['itinerary'][$key1]['transport']['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
										}

									}elseif(isset($itinerary_data['provider']) && $itinerary_data['provider'] == 'mystifly'){
										
										$returnAirRevalidate = $AirRevalidate->getAirRevalidate($itinerary_data['fare_source_code']);
										
										if(isset($returnAirRevalidate['success']) && $returnAirRevalidate['success'] == 1 && $returnAirRevalidate['data']['IsValid'] == 1){
											if(isset($returnAirRevalidate['data']['PricedItineraries']) && !empty($returnAirRevalidate['data']['PricedItineraries'])){

												$leg_detail['itinerary'][$key1]['transport']['PricedItineraries'] = $returnAirRevalidate['data']['PricedItineraries'];
												if(isset($returnAirRevalidate['data']['ExtraServices1_1']) && !empty($returnAirRevalidate['data']['ExtraServices1_1'])){
													$leg_detail['itinerary'][$key1]['transport']['ExtraServices'] = $returnAirRevalidate['data']['ExtraServices1_1'];
												}else{
													$leg_detail['itinerary'][$key1]['transport']['ExtraServices'] = '';
												}
											}
											else
											{
												$leg_detail['itinerary'][$key1]['transport'] = '';
												if($isFlightflag == 0){
													$isFlightflag = 1;
												}
											}
											 
										}else{

											$isFlightAvailable = 1;
											$leg_detail['itinerary'][$key1]['transport'] = '';
											if($isFlightflag == 0){
												$isFlightflag = 1;
											}
										}
											
									}

								}
							}

						}
						if($key == 'activities')
						{
							if (!empty($itinerary_data)) {								
								foreach ($itinerary_data as $key => $itineraryData) {
									# code...														
									if (isset($itineraryData['provider'])) {

										if ($itineraryData['provider'] == 'viator') {
											$response = http('post', 'search/product', ['code' => $itineraryData['id']]);

											$leg_detail['itinerary'][$key1]['activities'][$key]['bookingQuestions'] = $response['bookingQuestions'];	
					
											$leg_detail['itinerary'][$key1]['activities'][$key]['hotelPickup'] = $response['hotelPickup'];
											if($response['hotelPickup'] == true)
											{
												$leg_detail['itinerary'][$key1]['activities'][$key]['hotel_list'] = http('post', 'service/booking/hotels', ['productCode' => $itineraryData['id']]);	
											}											
											//$leg_detail['itinerary'][$key1]['activities']['hotelPickup'] = ;
										}
									}
								}
							}
						}
					}
				}
			}
			//dd($leg_detail);

			session()->put('search', $leg_detail);
			$getCityAirportCode = $GetCityAirportCode->getCityAirportCode();

			if($isFlightflag == 1){
				$isFlightAvailable = 1;
			}

			return view('itinenary.partials.payment-summary')->with(compact('priceCheck', 'itinerary', 'totalCost', 'travellers', 'currency', 'countries', 'startDate', 'endDate', 'totalDays','licensee_account','getCityAirportCode','isFlightAvailable'));

		} else {
			return redirect('/');
		}
	}
	
	public function review_itinerary_view(AirRevalidate $AirRevalidate) {
		$data = session()->get('search');
		$oCountries = getSelectCountries();
		$aMealType = getSelectMealType();
		$search_input = session()->get('search_input');		
		$child = array_sum(session()->get('search_input')['num_of_children']);
		//echo '<pre>'; print_r($child);
		$data['search_input'] = $search_input;
		$id = session()->get('user_auth')['id'];

		$isFlightAvailable = 0;
		$isFlightflag = 0;

		if($id) {
			$user = http('get', 'user/get-by-id/'.$id, [], $this->headers);
			$user = json_decode(json_encode($user), false);		

			// GET UPDATED PROFILE PIC
			$session = session()->get('user_auth');
			$session['image_url'] = isset( $user->customer->image_url ) ? $user->customer->image_url : null;
			$session['user_id'] = $user->id;
			session()->put('user_auth', $session);
		}

		$UserController = new UserController;
		$user = $UserController->get_user_detail();
		$data['user'] = $user;

		$totalCost = str_replace(',', '', $data['cost_per_person']);
		$finalTotalCost = session()->get('finalTotalCost');
		$gst = session()->get('gst');
		$tax = session()->get('tax');
		$travellers = $data['travellers'];
		$currency = $data['currency'];
		$totalDays = str_replace(' Nights', '', $data['total_number_of_days']);
		$countries = Cache::get('countries');
		$startDate = date('j M Y', strtotime(session()->get('search')['itinerary'][0]['city']['date_from']));
		$endDate = date('j M Y', strtotime(session()->get('search')['itinerary'][count(session()->get('search')['itinerary']) - 1]['city']['date_to']));
		//dd(session()->get('search'));
		if (session()->has('search')) {
		    $itinerary = json_decode(json_encode(session()->get('search')['itinerary']), false);
			$leg_detail = session()->get('search');
			
			foreach ($leg_detail['itinerary'] as $key1 => $itinerary) {
				
				foreach ($itinerary as $key => $itinerary_data) {

					if (!empty($itinerary_data)) {
						if ($key == 'transport') {

							if (!empty($itinerary_data)) {
								if (isset($itinerary_data['provider'])) {
									if(isset($itinerary_data['provider']) && $itinerary_data['provider'] == 'mystifly'){
										
										$returnAirRevalidate = $AirRevalidate->getAirRevalidate($itinerary_data['fare_source_code']);
										
										if(isset($returnAirRevalidate['success']) && $returnAirRevalidate['success'] == 1 && $returnAirRevalidate['data']['IsValid'] == 1){
											if(isset($returnAirRevalidate['data']['PricedItineraries']) && !empty($returnAirRevalidate['data']['PricedItineraries'])){

												$leg_detail['itinerary'][$key1]['transport']['PricedItineraries'] = $returnAirRevalidate['data']['PricedItineraries'];
												if(isset($returnAirRevalidate['data']['ExtraServices1_1']) && !empty($returnAirRevalidate['data']['ExtraServices1_1'])){
													$leg_detail['itinerary'][$key1]['transport']['ExtraServices'] = $returnAirRevalidate['data']['ExtraServices1_1'];
												}else{
													$leg_detail['itinerary'][$key1]['transport']['ExtraServices'] = '';
												}
											}
											else
											{
												$leg_detail['itinerary'][$key1]['transport'] = '';
												if($isFlightflag == 0){
													$isFlightflag = 1;
												}
											}
											 
										}else{

											$isFlightAvailable = 1;
											$leg_detail['itinerary'][$key1]['transport'] = '';
											if($isFlightflag == 0){
												$isFlightflag = 1;
											}
											
										}
									}

								}
							}

						}
						
					}
				}
			}
		    session()->put('search', $leg_detail);

		    if($isFlightflag == 1){
				$isFlightAvailable = 1;
			}

		    return view('itinenary.partials.review-itinerary')->with(compact('data','priceCheck', 'itinerary', 'totalCost', 'travellers', 'currency', 'oCountries', 'startDate', 'endDate', 'totalDays','finalTotalCost','gst','tax','aMealType','isFlightAvailable'));
		} else {
		    return redirect('/');
		}

	}

	public function test() {
		$today = date("F j, Y");
		$year = date('Y');

		$search = session()->get('search');
		return view('itinenary.proposed-itinerary-pdf',compact('today','year','search'));
	}

	public function updatePreferences() {
		$accommodation_options = '';
		$room_type_options = '';
		$transport_type_options = '';
		$nationality = '';
		$gender = '';
		$age_group = '';
		$interestoption = '';

		$labels = Cache::get('labels');
		$travellers = Cache::get('travellers');

		$travel_pref = session()->get('travel_preferences');
		$travel_pref = reset($travel_pref);
		$interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];

		if (count($travellers['categories']) > 0) {
			foreach ($travellers['categories'] as $category) {
				if (empty($category['name'])) {
					continue;
				}
				if (isset($travel_pref['accommodation_name']) && in_array($category['name'], $travel_pref['accommodation_name'])) {
					$accommodation_options .= '<input type="hidden" name="hotel_category_id[]"  value="' . $category['id'] . '" >';
				}
			}
		}

		if (count($travellers['room_types']) > 0) {
			foreach ($travellers['room_types'] as $room_type) {
				if (empty($room_type['name'])) {
					continue;
				}
				if (isset($travel_pref['room_name']) && in_array($room_type['name'], $travel_pref['room_name'])) {
					$room_type_options .= '<input type="hidden" name="room_type_id[]" value="' . $room_type['id'] . '" >';
				}
			}
		}

		if (count($travellers['transport_types']) > 0) {
			foreach ($travellers['transport_types'] as $transport_type) {
				if (empty($transport_type['name'])) {
					continue;
				}
				if (isset($travel_pref['transport_name']) && in_array($transport_type['name'], $travel_pref['transport_name'])) {
					$transport_type_options .= '<input type="hidden" name="transport_types[]" value="' . $transport_type['id'] . '" >';
				}
			}
		}

		if (count($travellers['nationalities']) > 0) {
			foreach ($travellers['nationalities']['featured'] as $featured) {
				if (empty($featured['name'])) {
					continue;
				}
				if (isset($travel_pref['nationality']) && ($featured['name'] == $travel_pref['nationality'])) {
					$nationality .= '<input type="hidden" name="nationality" value="' . $featured['id'] . '" > ';
				}
			}

			foreach ($travellers['nationalities']['not_featured'] as $not_featured) {
				if (empty($not_featured['name'])) {
					continue;
				}
				if (isset($travel_pref['nationality']) && ($not_featured['name'] == $travel_pref['nationality'])) {
					$nationality .= '<input type="hidden" name="nationality" value="' . $not_featured['id'] . '" >';
				}
			}
		}

		$gender_array = ['male', 'female', 'other'];
		foreach ($gender_array as $gen) {
			if (isset($travel_pref['gender']) && ($gen == $travel_pref['gender'])) {
				$gender .= '<input type="hidden" name="gender" value="' . $gen . '">';
			}
		}

		if (count($travellers['age_groups']) > 0) {
			foreach ($travellers['age_groups'] as $age) {
				if (empty($age['name'])) {
					continue;
				}
				if (isset($travel_pref['age_group']) && ($age['name'] == $travel_pref['age_group'])) {
					$age_group .= '<input type="hidden" name="age_group" value="' . $age['id'] . '" >';
				}
			}
		}

		if (count($labels) > 0) {
			foreach ($labels as $i => $label) {
				if (empty($age['name'])) {
					continue;
				}

				$sequence = '';
				if (in_array($label['id'], $interest_ids)) {
					$sequence = array_search($label['id'], $interest_ids);
					$sequence += 1;
					$interestoption .= '<input type="hidden" name="interests[' . $sequence . ']" value="' . $label['id'] . '">';
				}
			}
		}

		return $accommodation_options . $room_type_options . $transport_type_options . $nationality . $gender . $age_group . $interestoption; // . $travel_pref;
	}

	public function convertCurrency($price, $user_currency, $std_currency, $currency) {
		$priceConvertedToAUD = floatval($price) / floatval($currency[$std_currency]);

		$priceConvertedToSelectedPrice = 0.00;
		if (array_key_exists($user_currency, $currency)) {
			$currency_amount = $currency[$user_currency];
			$priceConvertedToSelectedPrice = $priceConvertedToAUD * floatval($currency_amount);
		}
		return number_format($priceConvertedToSelectedPrice, 2);

	}
	
	public function payment(Request $request) {
		//dd(session()->get('search_input')['pax']); exit;
		require base_path() . '/vendor/autoload.php';
		require_once app_path() . '/Libraries/eway-rapid-php-master/include_eway.php';
        //generate random Invoice Number
        $digits_needed = 8;
        $random_number = ''; // set up a blank string
        $count = 0;
        while ($count < $digits_needed) {
            $random_digit = mt_rand(0, 9);

            $random_number .= $random_digit;
            $count++;
        }
        $order_id_format = $this->getOrderId();
        $invoiceNumber = $order_id_format.$random_number;

        //Payment configuration start here
        $apiKey = 'F9802Cyu/LaPEuVH6DayevjOq0xvO2Ppyf/qGM60sSJGOBiTLz2NZvK+D5ZpVV1eaFCxWY';
        $apiPassword = '9NAW7KKQ';
        $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);

        $name = request()->input()['billing_first_name'] . ' ' . request()->input()['billing_last_name'];
        $find = array(",", ".");

        $totalAmount = str_replace($find, '', request()->input()['totalAmount']);

        $transaction = [
            'Customer' => [
                'CardDetails' => [
                    'Name' => $name,
                    'Number' => request()->input()['card_number'],
                    'ExpiryMonth' => request()->input()['month'],
                    'ExpiryYear' => request()->input()['year'],
                    'CVN' => request()->input()['cvv'],
                ]
            ],
            'Payment' => [
                'TotalAmount' => $totalAmount,
            ],
            'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
        ];
       
        $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);
        
        //$response = (object) ['TransactionStatus' => true, 'TransactionID' => '1234567890'];

        if ($response->TransactionStatus) {
        	
            $data = [];
            $leg_detail = [];

            if (session()->get('search')) {
                $leg_detail = session()->get('search');
            } else {
                $leg_detail = '';
            }

            if (!session()->get('user_auth')['id']) {
                $data['user_id'] = "";
            } else {
                $data['user_id'] = session()->get('user_auth')['user_id'];
            }
			
            $data['invoiceNumber'] = $invoiceNumber;
            $data['travel_date'] = $leg_detail['travel_date'];
            $data['travellers'] = $leg_detail['travellers'] + $leg_detail['child_total'];
            $data['adult'] = $leg_detail['travellers'];
            $data['child'] = $leg_detail['child_total'];
            $data['cost_per_day'] = $leg_detail['cost_per_day'];
            $data['from_date'] = $leg_detail['itinerary'][0]['city']['date_from'];
            $data['to_date'] = $leg_detail['itinerary'][count($leg_detail['itinerary']) - 1]['city']['date_to'];
            $data['total_itenary_legs'] = count($leg_detail['itinerary']);
            $data['total_amount'] = $request->input()['totalAmount'];
            $data['total_days'] = $leg_detail['total_number_of_days'];
            $data['total_per_person'] = $leg_detail['cost_per_person'];
            $data['evay_transcation_id'] = $response->TransactionID;
            $data['card_number'] = substr($request->input()['card_number'], -4);
            $data['expiry_month'] = $request->input()['month'];
            $data['currency'] = $request->input()['currency'];
            $data['expiry_year'] = $request->input()['year'];
            $data['cvv'] = $request->input()['cvv'];
            $data['from_city_id'] = $leg_detail['itinerary'][0]['city']['id'];
            $data['to_city_id'] = $leg_detail['itinerary'][count($leg_detail['itinerary']) - 1]['city']['id'];
            $data['from_city_name'] = $leg_detail['itinerary'][0]['city']['name'];
            $data['to_city_name'] = $leg_detail['itinerary'][count($leg_detail['itinerary']) - 1]['city']['name'];
            $data['voucher'] = 'Not Sent';
            $data['status'] = 'Pending';
            $post_data = $request->input();
            $bookingId="";		
			$cityName="";
			$voucherBusbudStatus="";
            $from_city_to_city = '';
            $itineraryId = array();
            $activityId = array();
            $aEroamHotel = array();
            $aMystiflyTransport = array();
			$tansportId = array();
			$tansportBusbudId = array();
			$voucher_data=array();
            foreach (session()->get('search')['itinerary'] as $key => $leg) {
                $from_city_to_city = $from_city_to_city . "," . $leg['city']['name'];
            }
            $data['from_city_to_city'] = ltrim($from_city_to_city, ',');

            $i = 0;

           	//$priceCheck = $this->expediaApi->expediaPriceCheck(); //Call to expdia 
           	//PriceCheck function

            foreach ($leg_detail['itinerary'] as $key => $itinerary) {
                $data['leg'][$i]['from_city_id'] = $itinerary['city']['id'];
                $data['leg'][$i]['to_city_id'] = $itinerary['city']['id'];
                $data['leg'][$i]['from_date'] = $itinerary['city']['date_from'];
                $data['leg'][$i]['to_date'] = $itinerary['city']['date_to'];
                $data['leg'][$i]['country_code'] = $itinerary['city']['country']['code'];
                $data['leg'][$i]['from_city_name'] = $itinerary['city']['name'];
                $data['leg'][$i]['to_city_name'] = $leg_detail['itinerary'][count($leg_detail['itinerary']) - 1]['city']['name'];
                foreach ($itinerary as $key => $itinerary_data) {
                    if (!empty($itinerary_data)) {

                        if ($key == 'hotel' && $itinerary_data['provider'] == 'expedia') {

                            $itinerary_data = json_decode(json_encode($itinerary_data), true);
                            $data['leg'][$i]['hotel'][0]['leg_id'] = '';
                            $data['leg'][$i]['hotel'][0]['leg_type'] = 'hotel';
                            $data['leg'][$i]['hotel'][0]['leg_type_name'] = 'Accommodation';
                            $data['leg'][$i]['hotel'][0]['nights'] = $itinerary_data['nights'];
                            $data['leg'][$i]['hotel'][0]['checkin'] = $itinerary_data['checkin'];
                            $data['leg'][$i]['hotel'][0]['checkout'] = $itinerary_data['checkout'];
                            $data['leg'][$i]['hotel'][0]['hotel_id'] = $itinerary_data['hotelId'];
                            $data['leg'][$i]['hotel'][0]['leg_name'] = $itinerary_data['name'];
                            $data['leg'][$i]['hotel'][0]['provider'] = $itinerary_data['provider'];
                            $data['leg'][$i]['hotel'][0]['address'] =  $itinerary_data['address1'].', '.$itinerary_data['city'];

                            $roomTypeId = '@roomTypeId';
                            $roomCode = '@roomCode';
                            $currencyCode = '@currencyCode';
                            $singleRate = '@nightlyRateTotal';
                            $total = '@total';
                            if(isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['detailPricing'])) {
                            	$data['leg'][$i]['hotel'][0]['detailPricing'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['detailPricing'];
                            }
							if (!isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RoomType'])) {
								$data['leg'][$i]['hotel'][0]['hotel_room_type_id'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['roomTypeCode'];
	                            $data['leg'][$i]['hotel'][0]['hotel_room_type_name'] = '';
	                            $data['leg'][$i]['hotel'][0]['hotel_room_code'] = '';
	                            $data['leg'][$i]['hotel'][0]['hotel_rate_code'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['rateCode'];
							}else{
	                            $data['leg'][$i]['hotel'][0]['hotel_room_type_id'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RoomType'][$roomTypeId];
	                            $data['leg'][$i]['hotel'][0]['hotel_room_type_name'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['rateDescription'];
	                            $data['leg'][$i]['hotel'][0]['hotel_room_code'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RoomType'][$roomCode];
	                            $data['leg'][$i]['hotel'][0]['hotel_rate_code'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['rateCode'];
							}
							

                            $Rate = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];
                            $taxes = 0;
                            if (isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])) {
                                $taxes = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
                            }
                            $data['leg'][$i]['hotel'][0]['hotel_price_per_night'] = $Rate;
                            $data['leg'][$i]['hotel'][0]['hotel_tax'] = $taxes;
                            $data['leg'][$i]['hotel'][0]['hotel_expedia_subtotal'] = $Rate * $itinerary_data['nights'];
                            $data['leg'][$i]['hotel'][0]['hotel_expedia_total'] = $Rate * $itinerary_data['nights'] + $taxes;

                            //$Rate = round(($Rate * $this->eroamPercentage) / 100 + $Rate, 2);
                            $subTotal = $Rate;//$itinerary_data['nights'] * $Rate;

                            $data['leg'][$i]['hotel'][0]['hotel_eroam_subtotal'] = $subTotal;
                            $data['leg'][$i]['hotel'][0]['price'] = number_format($Rate + $taxes, 2);
                            $data['leg'][$i]['hotel'][0]['currency'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$currencyCode];
                            $data['leg'][$i]['hotel'][0]['booking_id'] = '';
                            $data['leg'][$i]['hotel'][0]['provider_booking_status'] = '';
                            $data['leg'][$i]['hotel'][0]['booking_error_code'] = '';
                            
                            if (isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy'])) {
                                $data['leg'][$i]['hotel'][0]['cancellationPolicy'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy'];
                            }

                            if (isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['HotelFees']['HotelFee']['@amount'])) {
                                $data['leg'][$i]['hotel'][0]['HotelFees'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['HotelFees']['HotelFee']['@amount'];
                            }

                            $passengers_info = $request->input();

                            $hotelId = $itinerary_data['hotelId'];
                            $arrivalDate = date('m-d-Y', strtotime($itinerary_data['checkin']));
                            $departureDate = date('m-d-Y', strtotime($itinerary_data['checkout']));

                            if (isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['0']['rateKey'])) {
                                $rateKey = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['0']['rateKey'];
                            } else {
                                $rateKey = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['rateKey'];
                            }

                            if (isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RoomType'])) {
	                            $roomTypeCode = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RoomType'][$roomCode];
                            }else{
	                            $roomTypeCode =  $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['roomTypeCode'];
                            }
                            
                            $rateCode = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['rateCode'];
                            $chargeableRate = $data['leg'][$i]['hotel'][0]['hotel_expedia_total'];
                            $id = '@id';
                            if(isset($itinerary_data['bedTypeId'])){
                            	$bedtypes = $itinerary_data['bedTypeId'];
                            	$data['leg'][$i]['hotel'][0]['BedTypes'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['BedTypes'];
                            } else {
	                            if (isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['BedTypes'])) {
		                            if (isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType'][0])) {
		                                $bedtypes = '';
		                                foreach ($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType'] as $BedType) {
		                                    //$bedtypes .= $BedType[$id] . ',';
		                                    $bedtypes = $BedType[$id];
		                                    break;
		                                }
		                                //$bedtypes = rtrim($bedtypes, ',');
		                            } else {
		                                $bedtypes = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType'][$id];
		                            }

		                            $data['leg'][$i]['hotel'][0]['BedTypes'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['BedTypes'];
	                            }else{
	                            	$bedtypes = '';
	                            }
	                        }
	                        $data['leg'][$i]['hotel'][0]['bedTypeId'] = $bedtypes;

	                        if(isset($itinerary_data['room'])){
		                        $bedtypes = $itinerary_data['room'];
		                        $data['leg'][$i]['hotel'][0]['room'] = $bedtypes;
		                    } else {
		                    	$num_of_rooms = session()->get('search_input') ['rooms'];
		                    	$bedtypes = array();
		                    	for ($j=0; $j < $num_of_rooms; $j++) { 
		                    		$bedtypes[$j] = array('roomNo'=>$j+1, 'bedTypeId' =>$data['leg'][$i]['hotel'][0]['bedTypeId']);
		                    	}
		                        $data['leg'][$i]['hotel'][0]['room'] = $bedtypes;
		                    }

	                        $specialInformation = '';
	                        if(isset($itinerary_data['specialInformation'])){
		                        $specialInformation = $itinerary_data['specialInformation'];
		                        $data['leg'][$i]['hotel'][0]['specialInformation'] = $specialInformation;
		                    }

                            $customerSessionId = $itinerary_data['customerSessionId'];
                            $GUIDCode = getGUID();
                            $data['leg'][$i]['hotel'][0]['GUIDCode'] = $GUIDCode;

                            $rateType = '';
                            if(isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['rateType'])) {
                            	$rateType = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['rateType'];

                            	$data['leg'][$i]['hotel'][0]['rateType'] = $rateType;
                            }

                            $return_data = $this->expediaApi->expediaBooking($hotelId, $arrivalDate, $departureDate, $rateKey, $roomTypeCode, $rateCode, $chargeableRate, $passengers_info, $bedtypes, $customerSessionId,$GUIDCode,$specialInformation,$rateType);

                            $hotelVoucher = $this->expediaApi->hotelVoucher($return_data['itineraryId'],$return_data['numberOfRoomsBooked'],$invoiceNumber,$Rate, $taxes,$request->input()['passenger_email'][0],$data['leg'][$i]['from_city_name'],$data['leg'][$i]['country_code'],$i);

                            $itineraryId[$data['leg'][$i]['from_city_name']] = $return_data['itineraryId'];

                            $data['leg'][$i]['hotel'][0]['booking_id'] = $return_data['itineraryId'];
                            $data['leg'][$i]['hotel'][0]['provider_booking_status'] = isset($return_data['reservationStatusCode']) ? $return_data['reservationStatusCode'] : 'ER';
                            $data['leg'][$i]['hotel'][0]['hotel_total_room'] = isset($return_data['numberOfRoomsBooked']) ? $return_data['numberOfRoomsBooked'] : session()->get('search_input') ['rooms'];
                            $data['leg'][$i]['hotel'][0]['booking_error_code'] = '';
                            $data['leg'][$i]['hotel'][0]['voucher'] = $data['leg'][$i]['from_city_name'] . '_HotelVoucher_' . $return_data['itineraryId'].'php';

                        }elseif($key == 'hotel' && $itinerary_data['provider'] == 'eroam'){
                        	$itinerary_data = json_decode(json_encode($itinerary_data), true);

							$filePath = $itinerary_data['city']['name'].'_HotelVoucher_'.$data['invoiceNumber'].'.pdf';
                        	$aEroamHotel[$i] = $filePath;

                        	
                        	$data['leg'][$i]['hotel'][0]['leg_id'] = '';
                            $data['leg'][$i]['hotel'][0]['leg_type'] = 'hotel';
                            $data['leg'][$i]['hotel'][0]['leg_type_name'] = 'Accommodation';
                            $data['leg'][$i]['hotel'][0]['nights'] = $itinerary_data['nights'];
                            $data['leg'][$i]['hotel'][0]['checkin'] = $itinerary_data['checkin'];
                            $data['leg'][$i]['hotel'][0]['checkout'] = $itinerary_data['checkout'];
                            $data['leg'][$i]['hotel'][0]['hotel_id'] = $itinerary_data['id'];
                            $data['leg'][$i]['hotel'][0]['leg_name'] = $itinerary_data['name'];
                            $data['leg'][$i]['hotel'][0]['provider'] = $itinerary_data['provider'];
                            $data['leg'][$i]['hotel'][0]['address'] =  $itinerary_data['address_1'].', '.$itinerary_data['city']['name'];
                            $data['leg'][$i]['hotel'][0]['hotel_room_type_id'] = $itinerary_data['default_hotel_room']['hotel_room_type_id'];
                            $data['leg'][$i]['hotel'][0]['hotel_room_type_name'] = $itinerary_data['default_hotel_room']['name'];
	                        $data['leg'][$i]['hotel'][0]['hotel_room_code'] = 0;
	                        $data['leg'][$i]['hotel'][0]['hotel_rate_code'] = 0;

	                        $data['leg'][$i]['hotel'][0]['hotel_price_per_night'] = number_format($itinerary_data['default_hotel_room']['price'],2);
	                        $data['leg'][$i]['hotel'][0]['hotel_tax'] = 0;
	                        $roomPrice = $itinerary_data['default_hotel_room']['price'] * $itinerary_data['nights'];
							$roomPrice = number_format($roomPrice,2);
	                        $data['leg'][$i]['hotel'][0]['hotel_expedia_subtotal'] = $roomPrice;
	                        $data['leg'][$i]['hotel'][0]['hotel_eroam_subtotal'] = $roomPrice;
	                        $data['leg'][$i]['hotel'][0]['hotel_expedia_total'] = $roomPrice;
	                        $data['leg'][$i]['hotel'][0]['price'] = $roomPrice;
	                        $data['leg'][$i]['hotel'][0]['currency'] = $itinerary_data['currency']['code'];
	                        $data['leg'][$i]['hotel'][0]['booking_id'] = '';
	                        $data['leg'][$i]['hotel'][0]['provider_booking_status'] = 'CF';
	                        $data['leg'][$i]['hotel'][0]['booking_error_code'] = '';
	                        $data['leg'][$i]['hotel'][0]['cancellationPolicy'] = $itinerary_data['cancellation_policy'];
	                        $data['leg'][$i]['hotel'][0]['checkin_instruction'] = $itinerary_data['checkin_instruction'];
	                        $data['leg'][$i]['hotel'][0]['amenities_description'] = $itinerary_data['amenities_description'];
	                        $data['leg'][$i]['hotel'][0]['taxes'] = 0;
	                        $data['leg'][$i]['hotel'][0]['HotelFees'] = 0;
	                        $data['leg'][$i]['hotel'][0]['BedTypes'] = '';
	                        $data['leg'][$i]['hotel'][0]['bedTypeId'] = '';
	                        $data['leg'][$i]['hotel'][0]['hotel_total_room'] = session()->get('search')['rooms'];
	                        $hotelVoucher = Self::hotelVoucher($data ,$data['leg'][$i],$request->input(),$i);
                        }
                        $booking_data = array();
                        $j = 0;
                        $k = 0;
                        $itemcount = 0;

                        if ($key == 'activities') {

                            foreach ($itinerary_data as $key1 => $activities) {
                            	
                            	$data['leg'][$i]['activities'][$j]['leg_type'] = 'activities';
                                $data['leg'][$i]['activities'][$j]['leg_id'] = '';
                                $data['leg'][$i]['activities'][$j]['leg_name'] = (isset($activities['name']) ? $activities['name'] : '');
                                $data['leg'][$i]['activities'][$j]['from_date'] = (isset($activities['date_selected']) ? $activities['date_selected'] : '');
                                $data['leg'][$i]['activities'][$j]['to_date'] = (isset($activities['date_selected']) ? $activities['date_selected'] : '');
                                $data['leg'][$i]['activities'][$j]['currency'] = (isset($activities['activity_price']['0']['currency']['code']) ? $activities['activity_price']['0']['currency']['code'] : '');
                                $currencyCode = (isset($activities['activity_price']['0']['currency']['code']) ? $activities['activity_price']['0']['currency']['code'] : '');
                                if (empty($data['leg'][$i]['activities'][$j]['currency'])) {
                                    if (isset($activities['currency']) && !empty($activities['currency'])) {
                                        $currencyCode = $activities['currency'];
                                        $data['leg'][$i]['activities'][$j]['currency'] = $currencyCode;
                                    }
                                }
                                $data['leg'][$i]['activities'][$j]['booking_id'] = '';
                                $data['leg'][$i]['activities'][$j]['provider_booking_status'] = '';
                                $data['leg'][$i]['activities'][$j]['booking_error_code'] = '';
                                $data['leg'][$i]['activities'][$j]['leg_type_name'] = '';
                                $data['leg'][$i]['activities'][$j]['voucher_key'] = '';
                                $data['leg'][$i]['activities'][$j]['voucher_url'] = '';
                                if (isset($activities['provider'])) {
                                    $data['leg'][$i]['activities'][$j]['provider'] = $activities['provider'];
                                } else {
                                    $data['leg'][$i]['activities'][$j]['provider'] = '';
                                }
                                if (isset($activities['activity_price']['0']['supplier']['id'])) {
                                    $data['leg'][$i]['activities'][$j]['supplier_id'] = $activities['activity_price']['0']['supplier']['id'];
                                } else {
                                    $data['leg'][$i]['activities'][$j]['supplier_id'] = '';
                                }

                                if (isset($activities['tourGrades'])) {
	                                $data['leg'][$i]['activities'][$j]['price'] = number_format($activities['tourGrades']['retailPrice'],2);
                                }else{
                                	$data['leg'][$i]['activities'][$j]['price'] = $activities['price'][0]['price'];
                                }
                                if (isset($activities['tourGrades']['detailPricing'])) {
	                                $data['leg'][$i]['activities'][$j]['detailPricing'] = $activities['tourGrades']['detailPricing'];
                                }
                                
                                if($activities['provider'] != 'viator')
                                {
                                	$act_data_voucher = array();
                                	$act_data_voucher['passenger_first_name'] = @$post_data['passenger_first_name'];	
	            					$act_data_voucher['child_first_name'] = @$post_data['child_first_name'];
	            					$act_data_voucher['booking_id'] = $invoiceNumber;
	            					$act_data_voucher['billing_first_name'] = $post_data['billing_first_name'];
						            $act_data_voucher['billing_last_name'] = $post_data['billing_last_name'];
						            $act_data_voucher['billing_email'] = $post_data['billing_email'];
	                                $booking_id = (int)self::generateRandomString();

                                	$activityId[$booking_id] = $data['leg'][$i]['from_city_name'];
                                	$activityVoucher = self::activityVoucher('eroam','',$booking_id,$data['leg'][$i]['from_city_name'],$data['leg'][$i]['country_code'],$activities,$act_data_voucher);
                                }

                                /*if (isset($activities['price']['0']['price'])) {
                                    $data['leg'][$i]['activities'][$j]['price'] = number_format((($leg_detail['travellers'] + $leg_detail['child_total'])* $activities['price']['0']['price']), 2);
								} else {
                                    $data['leg'][$i]['activities'][$j]['price'] = (isset($activities['activity_price']['0']['price']) ? number_format($activities['activity_price']['0']['price'], 2) : '');
                                }*/

                                //code for viator booking

	    						if(isset($activities['provider']) && $activities['provider'] == 'viator')
	    						{	    								
    								$tourCode_data = array();

    								$booking_data['demo'] = "false";
    								$booking_data['currencyCode'] = $currencyCode;
    								$booking_data['partnerDetail']['distributorRef'] = 'The eRoam Reference for productCode : '.$activities['id'].$itemcount."_".date("Ymdhis");


    								/*$booking_data['aid'] = 'null';
    								$booking_data['newsletterSignUp'] = 'false'; 
    								$booking_data['promoCode'] = 'null';
    								$booking_data['otherDetail'] = 'null';*/
    								
    								$booking_data['booker']['firstname'] = $post_data['passenger_first_name'][$post_data['location-select']];
    								$booking_data['booker']['surname'] = $post_data['passenger_last_name'][$post_data['location-select']];
    								$booking_data['booker']['title'] = $post_data['passenger_title'][$post_data['location-select']];
    								$booking_data['booker']['email'] = $post_data['passenger_email'][$post_data['location-select']];
    								$booking_data['booker']['homePhone'] = $post_data['passenger_contact_no'][$post_data['location-select']];
    								

    								//$total_passenger = $leg_detail['travellers'] + $leg_detail['child_total'];
    								


    								$booking_data['items'][$itemcount]['partnerItemDetail']['distributorItemRef'] = 'The eRoam Item Reference for productCode : '.$activities['id']."_".date("Ymdhis");

    								if($activities['hotelPickup'] == true)
									{ 
										if(isset($activities['hotelPickups']['hotelId']))
										{
											$booking_data['items'][$itemcount]['hotelId'] = $activities['hotelPickups']['hotelId'];
										}
										else
										{
											$booking_data['items'][$itemcount]['pickupPoint'] = @$activities['hotelPickups']['pickupPoint'];
										}
									}
									else
									{
    									$booking_data['items'][$itemcount]['hotelId'] = "null";
    									$booking_data['items'][$itemcount]['pickupPoint'] = "null";
    								}
    								$booking_data['items'][$itemcount]['travelDate'] = $activities['date_selected'];
    								$booking_data['items'][$itemcount]['productCode'] = $activities['id'];
    								$booking_data['items'][$itemcount]['tourGradeCode'] =  $activities['tourGrades']['gradeCode'];
    								$booking_data['items'][$itemcount]['languageOptionCode'] = 'en/SERVICE_GUIDE';
    								
    								if(isset($activities['bookingQuestionAnswers']))
    								{
    									$booking_data['items'][$itemcount]['bookingQuestionAnswers'] = $activities['bookingQuestionAnswers'];
    								}

    								if(isset($activities['specialRequirement']))
									{
										$booking_data['items'][$itemcount]['specialRequirements'] = $activities['specialRequirement'];
									}

    								$booking_data['items'][$itemcount]['travellers'] = [];
    								$index = 0;
    								$q = 0;
									foreach ($activities['tourGrades']['ageBands'] as $key => $value) 
									{	
										if($value['bandId'] == 1)
										{
											for ($n=0; $n < $value['count']; $n++) 
											{
												if(isset($post_data['passenger_first_name'][$n]))
												{
													$booking_data['items'][$itemcount]['travellers'][$index]['bandId'] = (int)$value['bandId'];
		    										$booking_data['items'][$itemcount]['travellers'][$index]['firstname'] =  $post_data['passenger_first_name'][$n];
		    										$booking_data['items'][$itemcount]['travellers'][$index]['surname'] =  $post_data['passenger_last_name'][$n];
		    										$booking_data['items'][$itemcount]['travellers'][$index]['title'] =  $post_data['passenger_title'][$n];
	    										}
	    										else
	    										{
	    											for($p = $n ; $p < $value['count']; $p++)
	    											{
	    												if(isset($post_data['child_first_name'][$q]))
	    												{
		    												$booking_data['items'][$itemcount]['travellers'][$index]['bandId'] = (int)$value['bandId'];
				    										$booking_data['items'][$itemcount]['travellers'][$index]['firstname'] =  $post_data['child_first_name'][$q];
				    										$booking_data['items'][$itemcount]['travellers'][$index]['surname'] =  $post_data['child_last_name'][$q];
				    										$booking_data['items'][$itemcount]['travellers'][$index]['title'] =  @$post_data['child_title'][$q];
		    												$q++;
		    											}
		    											$index++;
	    											}
	    										}
	    										if($n == $post_data['location-select'])
	    										{
	    											$booking_data['items'][$itemcount]['travellers'][$n]['leadTraveller'] = "true";
	    										}
	    										$index++;
											}  		
										}
										
										if($value['bandId'] == 2)
										{		
											for($r = $q ; $r <= $value['count']; $r++)
											{
												if(isset($post_data['child_first_name'][$r]))
	    										{
													$booking_data['items'][$itemcount]['travellers'][$index]['bandId'] = (int)$value['bandId'];
		    										$booking_data['items'][$itemcount]['travellers'][$index]['firstname'] =  $post_data['child_first_name'][$r];
		    										$booking_data['items'][$itemcount]['travellers'][$index]['surname'] =  $post_data['child_last_name'][$r];
													$q++;
													$index++;
												}
											}
										}

										if($value['bandId'] == 3)
										{		
											for($r = $q ; $r <= $value['count']; $r++)
											{
												if(isset($post_data['child_first_name'][$r]))
	    										{
													$booking_data['items'][$itemcount]['travellers'][$index]['bandId'] = (int)$value['bandId'];
		    										$booking_data['items'][$itemcount]['travellers'][$index]['firstname'] =  $post_data['child_first_name'][$r];
		    										$booking_data['items'][$itemcount]['travellers'][$index]['surname'] =  $post_data['child_last_name'][$r];
													$q++;
													$index++;
												}
											}
										}
										
									}	
									$booking_data['items'][$itemcount]['travellers'] = array_values($booking_data['items'][$itemcount]['travellers']);	

									
									

									//json_encode($booking_data['items'][$itemcount]['travellers'],JSON_NUMERIC_CHECK);						
									//$itemcount++;  
									if(!empty($booking_data))
									{
										$response = http( 'post', 'book-activities', $booking_data);								
										
										if($response['provider_booking_status'] == 'CONFIRMED'){											

											$data['leg'][$i]['activities'][$j]['booking_id'] = $response['booking_id'];
											$data['leg'][$i]['activities'][$j]['provider_booking_status'] = $response['provider_booking_status'];
					    					$data['leg'][$i]['activities'][$j]['booking_error_code'] = '';
					    					$data['leg'][$i]['activities'][$j]['leg_type_name'] = '';
					    					$data['leg'][$i]['activities'][$j]['voucher_key'] = $response['voucherKey'];
											$data['leg'][$i]['activities'][$j]['voucher_url'] = $response['voucherURL'];
											$data['leg'][$i]['activities'][$j]['tour_grade'] = ($response['tourGradeCode'] == null) ? 'DEFAULT' : $response['tourGradeCode'];
											$data['leg'][$i]['activities'][$j]['itineraryId'] = $response['itineraryId'];
											$data['leg'][$i]['activities'][$j]['distributorRef'] = $response['distributorRef'];
											$data['leg'][$i]['activities'][$j]['itemId'] = $response['itemId'];
											$data['leg'][$i]['activities'][$j]['distributorItemRef'] = $response['distributorItemRef'];

											$data['leg'][$i]['activities'][$j]['merchant_net_price'] = $response['merchantNetPrice'];
											//$activityId[$response['booking_id']] = $data['leg'][$i]['from_city_name'];
											//$activityVoucher = self::activityVoucher('viator',$response['voucherURL'],$response['booking_id'],$data['leg'][$i]['from_city_name'],$data['leg'][$i]['country_code']);
											
										}else{

											$data['leg'][$i]['activities'][$j]['booking_id'] = '';
											$data['leg'][$i]['activities'][$j]['provider_booking_status'] = $response['provider_booking_status'];
					    					$data['leg'][$i]['activities'][$j]['booking_error_code'] =  $response['booking_error_code'];;
					    					$data['leg'][$i]['activities'][$j]['leg_type_name'] = '';
					    					$data['leg'][$i]['activities'][$j]['voucher_key'] = '';
											$data['leg'][$i]['activities'][$j]['voucher_url'] = '';
											$data['leg'][$i]['activities'][$j]['tour_grade'] ='';
											$data['leg'][$i]['activities'][$j]['merchant_net_price'] = '';
											$data['leg'][$i]['activities'][$j]['itineraryId'] = '';
											$data['leg'][$i]['activities'][$j]['distributorRef'] = '';
											$data['leg'][$i]['activities'][$j]['itemId'] = '';
											$data['leg'][$i]['activities'][$j]['distributorItemRef'] = '';
											//$activityId[$data['leg'][$i]['from_city_name']] = '';
										}
									}
									$k++;
	    						}
                                $j++;
                            }
                        }
                       
						if (isset($itinerary_data['provider']) && $key == 'transport') {
                            if (!empty($itinerary_data)) {
                                $data['leg'][$i]['transport'][0]['leg_type'] = 'transport';
                                $data['leg'][$i]['transport'][0]['leg_id'] = '';
                                $data['leg'][$i]['transport'][0]['leg_name'] = (isset($itinerary_data['transport_type']['name']) ? $itinerary_data['transport_type']['name'] : '');
                                $data['leg'][$i]['transport'][0]['from_city_id'] = (isset($itinerary_data['from_city']['id']) ? $itinerary_data['from_city']['id'] : '');
                                $data['leg'][$i]['transport'][0]['from_city_name'] = (isset($itinerary_data['from_city']['name']) ? $itinerary_data['from_city']['name'] : '');
                                $data['leg'][$i]['transport'][0]['to_city_id'] = (isset($itinerary_data['to_city']['id']) ? $itinerary_data['to_city']['id'] : '');
                                $data['leg'][$i]['transport'][0]['to_city_name'] = (isset($itinerary_data['to_city']['name']) ? $itinerary_data['to_city']['name'] : '');
                                $data['leg'][$i]['transport'][0]['price'] = (isset($itinerary_data['price']['0']['price']) ? number_format($itinerary_data['price']['0']['price'], 2) : '');
                                $data['leg'][$i]['transport'][0]['currency'] = (isset($itinerary_data['price']['0']['currency']['code']) ? $itinerary_data['price']['0']['currency']['code'] : '');
                                $data['leg'][$i]['transport'][0]['departure_text'] = (isset($itinerary_data['departure_text']) ? $itinerary_data['departure_text'] : '');
                                $data['leg'][$i]['transport'][0]['arrival_text'] = (isset($itinerary_data['arrival_text']) ? $itinerary_data['arrival_text'] : '');
                                $data['leg'][$i]['transport'][0]['booking_summary_text'] = (isset($itinerary_data['booking_summary_text']) ? $itinerary_data['booking_summary_text'] : '');
                                $data['leg'][$i]['transport'][0]['duration'] = (isset($itinerary_data['duration']) ? $itinerary_data['duration'] : '');
                                $data['leg'][$i]['transport'][0]['booking_id'] = '';
                                $data['leg'][$i]['transport'][0]['provider_booking_status'] = '';
                                $data['leg'][$i]['transport'][0]['booking_error_code'] = '';
                                $data['leg'][$i]['transport'][0]['leg_type_name'] = '';
                                if (isset($itinerary_data['detailPricing'])) {
	                                $data['leg'][$i]['transport'][0]['detailPricing'] = $itinerary_data['detailPricing'];
                                }

                                if (isset($itinerary_data['provider'])) {
                                    $data['leg'][$i]['transport'][0]['provider'] = $itinerary_data['provider'];
                                    //code for mystifly booking
                                     if (isset($itinerary_data['provider']) && $itinerary_data['provider'] == 'mystifly') {
                                        $booking_data = array();

                                        $Notes = '';
                                        $FareType = '';
                                        if(isset($itinerary_data['fare_type']) && !empty($itinerary_data['fare_type'])){
                                        	$FareType = $itinerary_data['fare_type'];
                                        }elseif(isset($itinerary_data['PricedItineraries']['AirItineraryPricingInfo'])){
    										$FareType = $itinerary_data['PricedItineraries']['AirItineraryPricingInfo']['FareType'];
  										}elseif(isset($itinerary_data['PricedItineraries']['PricedItinerary']['AirItineraryPricingInfo'])){
								    		$FareType = $itinerary_data['PricedItineraries']['PricedItinerary']['AirItineraryPricingInfo']['FareType'];
								  		}
								  		if(isset($post_data['special_note'][0]) && !empty($post_data['special_note'][0])){
								  			$Notes = $post_data['special_note'][0];
								  		}

								  		
								  		if(isset($itinerary_data['serviceId']) && !empty($itinerary_data['serviceId']))
								  		{	
								  			$booking_data['Services'] = $itinerary_data['serviceId'];
								  		}else{
								  			$booking_data['Services'] = $itinerary_data['serviceId'] = array();
								  		}
								    	
								  		$booking_data['FareSourceCode'] = $itinerary_data['fare_source_code'];
								  		$booking_data['FareType'] = $FareType;
                                        for ($t = 0; $t < count($post_data['passenger_first_name']); $t++) {

                                           $booking_data['passenger_contact_no'][$t] = $post_data['passenger_contact_no'][0];
                                           $booking_data['passenger_email'][$t] = $post_data['passenger_email'][0];

                                            $booking_data['passenger_title'][$t] = strtoupper($post_data['passenger_title'][$t]);
                                            $booking_data['passenger_first_name'][$t] = $post_data['passenger_first_name'][$t];
                                            $booking_data['passenger_last_name'][$t] = $post_data['passenger_last_name'][$t];
                                            $booking_data['passenger_gender'][$t] = strtoupper(substr($post_data['gender'][$t],0,1));
                                            $booking_data['passenger_dob'][$t] = date('Y-m-d', strtotime($post_data['passenger_dob'][$t])) . 'T00:00:00';
                                            $booking_data['passenger_country'][$t] = $this->getCountryCode($post_data['country'][$t]);
                                            $booking_data['passenger_type'][$t] = "ADT";
                                            $booking_data['passenger_meal_preference'][$t] =  $post_data['passenger_meal'][$t];
                                            $booking_data['passenger_area_code'][$t] =  $post_data['area_code'][$t];
                                            $booking_data['passenger_country_code'][$t] =  $post_data['country_code'][$t];


                                            if(isset($post_data['passport_date'][$t]) && !empty($post_data['passport_date'][$t])){
                                            	$booking_data['passenger_passport_expiry_date'][$t] =date('Y-m-d', strtotime($post_data['passport_date'][$t])) . 'T00:00:00';
                                            }else{
                                            	$booking_data['passenger_passport_expiry_date'][$t] = '';
                                            }
                                            if(isset($post_data['passport_no'][$t]) && !empty($post_data['passport_no'][$t])){
                                            	 $booking_data['passenger_passport_number'][$t] = $post_data['passport_no'][$t];
                                            }else{
                                            	$booking_data['passenger_passport_number'][$t] = '';
                                            }
                                            if(isset($post_data['passport_country'][$t]) && !empty($post_data['passport_country'][$t])){
                                            	  $booking_data['passenger_passport_country'][$t] =  $post_data['passport_country'][$t];
                                            }else{
                                            	$booking_data['passenger_passport_country'][$t] =  '';
                                            }
                                            
                                            if(isset($post_data['postcode'][$t]) && !empty($post_data['postcode'][$t])){
                                            	  $booking_data['passenger_post_code'][$t] =  $post_data['postcode'][$t];
                                            }else{
                                            	$booking_data['passenger_post_code'][$t] =  '';
                                            }
                                        }
                                        if(isset($post_data['child_first_name'])){
	                                        for ($m=0; $m < count($post_data['child_first_name']); $m++) {

	                                           $booking_data['passenger_contact_no'][$t] = $post_data['passenger_contact_no'][0];
	                                           $booking_data['passenger_email'][$t] = $post_data['passenger_email'][0];

	                                            $booking_data['passenger_title'][$t] = strtoupper($post_data['child_title'][$m]);
	                                            $booking_data['passenger_first_name'][$t] = $post_data['child_first_name'][$m];
	                                            $booking_data['passenger_last_name'][$t] = $post_data['child_last_name'][$m];
	                                            $booking_data['passenger_gender'][$t] = strtoupper(substr($post_data['child_gender'][$m],0,1));
	                                            $booking_data['passenger_dob'][$t] = date('Y-m-d', strtotime($post_data['child_dob'][$m])) . 'T00:00:00';
	                                            $booking_data['passenger_country'][$t] = $this->getCountryCode($post_data['child_country'][$m]);

	                                            $booking_data['passenger_type'][$t] = $this->getPassengerType($post_data['child_dob'][$m],$data['leg'][$i]['to_date']);
	                                            $booking_data['passenger_meal_preference'][$t] =  $post_data['child_meal'][$m];

	                                            $booking_data['passenger_area_code'][$t] =  $post_data['child_area_code'][$m];
                                            	$booking_data['passenger_country_code'][$t] =  $post_data['child_country_code'][$m];

	                                            if(isset($post_data['child_passport_date'][$m]) && !empty($post_data['child_passport_date'][$m])){

	                                            	$booking_data['passenger_passport_expiry_date'][$t] = date('Y-m-d', strtotime($post_data['child_passport_date'][$m])) . 'T00:00:00';
	                                        	}else{
	                                        			$booking_data['passenger_passport_expiry_date'][$t] = '';
	                                        	}
	                                        	if(isset($post_data['child_passport_no'][$m]) && !empty($post_data['child_passport_no'][$m])){
	                                        		$booking_data['passenger_passport_number'][$t] = $post_data['child_passport_no'][$m];
	                                        	}else{
	                                        		$booking_data['passenger_passport_number'][$t] = '';
	                                        	}
	                                            
	                                            if(isset($post_data['child_passport_country'][$m]) && !empty($post_data['child_passport_country'][$m])){
	                                            	$booking_data['passenger_passport_country'][$t] =  $post_data['child_passport_country'][$m];
	                                            }else{
	                                            	$booking_data['passenger_passport_country'][$t] =  '';
	                                            }
	                                            
		                                        if(isset($post_data['child_postcode'][$m]) && !empty($post_data['child_postcode'][$m])){
		                                            	  $booking_data['passenger_post_code'][$t] =  $post_data['child_postcode'][$m];
		                                          }else{
		                                          	 $booking_data['passenger_post_code'][$t] =  '';
		                                          }

	                                            $t++;
	                                        }
                                        }
                                        $headers = [
                                            'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
                                            'Origin' => url(''),
                                        ];
                                        
                                        
                                        $response = http('post', 'mystifly/flight/book', $booking_data, $headers);

                                        $ExtraServiceId = $itinerary_data['serviceId'];
										$ExtraServices1_1 = array();
										if(!empty($ExtraServiceId))
										{
											for($pa = 0;$pa<count($ExtraServiceId);$pa++)
											{
												$ExtraServices1_1['Services']['ExtraServiceId'][$pa] = $ExtraServiceId[$pa];
											}
										}
                                       
                                        if (isset($response['BookFlightResult']['Status']) && $response['BookFlightResult']['Status'] == 'CONFIRMED' || $response['BookFlightResult']['Status'] == 'PENDING') {

                                            $data['leg'][$i]['transport'][0]['booking_id'] = $response['BookFlightResult']['UniqueID'];
                                            $data['leg'][$i]['transport'][0]['provider_booking_status'] = $response['BookFlightResult']['Status'];
                                            $data['leg'][$i]['transport'][0]['booking_error_code'] = '';
                                            $data['leg'][$i]['transport'][0]['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
                                           
                                            if(isset($response['BookFlightResult']['UniqueID']) && !empty($response['BookFlightResult']['UniqueID'])){
                                            	$response['tripDetails']['FareRules'] =$itinerary_data['FareRules'] ?? '';
                                            	$response['tripDetails']['booking_id'] =$data['invoiceNumber']  ?? '';
                                            	$aMystiflyTransport[] =$this->mystiflyVoucher($itinerary['city']['name'],$data['invoiceNumber'],$response['tripDetails']);
                                            	
                                            }
                                             

                                        } else {
                                            if (isset($response['BookFlightResult']['Errors']['Error'][0]['Message'])) {

                                                $data['leg'][$i]['transport'][0]['provider_booking_status'] = "NOT CONFIRMED";
                                                $data['leg'][$i]['transport'][0]['booking_error_code'] = '';
                                                $data['leg'][$i]['transport'][0]['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');

                                            } elseif (isset($response['BookFlightResult']['Errors']['Error']['Message'])) {

                                                $data['leg'][$i]['transport'][0]['provider_booking_status'] = "NOT CONFIRMED";
                                                $data['leg'][$i]['transport'][0]['booking_error_code'] = '';
                                                $data['leg'][$i]['transport'][0]['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
                                            }
                                        }
                                    }
                                    
									if (isset($itinerary_data['provider']) && $itinerary_data['provider'] == 'busbud') {
																			
										$booking_data = array();										
                                        $booking_data['travellers'] = isset($data['travellers']) ? $data['travellers'] : '';
                                        $booking_data['departureId'] = isset($itinerary_data['id']) ? $itinerary_data['id'] : '';
                                        $booking_data['departuredate'] = isset($itinerary_data['departuredate']) ? $itinerary_data['departuredate'] : '';
                                        $booking_data['destinationlocationcode'] = isset($itinerary_data['destinationlocationcode']) ? $itinerary_data['destinationlocationcode'] : '';
                                        $booking_data['originlocationcode'] = isset($itinerary_data['originlocationcode']) ? $itinerary_data['originlocationcode'] : '';
                                        $booking_data['cartId'] = isset($itinerary_data['cartId']) ? $itinerary_data['cartId'] : '';
										$booking_data['passangerArray'] = isset($itinerary_data['passangerArray']) ? $itinerary_data['passangerArray'] :'';
										//echo '<pre>'; print_r($itinerary_data['passangerArray']);exit;
										 $booking_data['cartData']= array_keys($itinerary_data['passangerArray'][0]['questions']);	

										//echo '<pre>'; print_r(array_keys($itinerary_data['passangerArray'][0]['questions']));exit;	
													
                                        for($t=0;$t<count($post_data['passenger_first_name']);$t++){
                                            if($t==0){
                                                $booking_data['passenger_contact_no'] = isset($post_data['passenger_contact_no'][0]) ? $post_data['passenger_contact_no'][0] :'';
                                                $booking_data['passenger_email'] = isset($post_data['passenger_email'][0]) ? $post_data['passenger_email'][0] :'';
                                            }
													$booking_data['passenger_first_name'][$t] = isset($post_data['passenger_first_name'][$t]) ? $post_data['passenger_first_name'][$t] :'';											
													$booking_data['passenger_last_name'][$t] = isset($post_data['passenger_last_name'][$t]) ? $post_data['passenger_last_name'][$t] :'';
													$booking_data['passenger_dob'][$t] = date('Y-m-d',strtotime($post_data['passenger_dob'][$t]));
													$booking_data['passenger_type'][$t] = "ADT";
													$booking_data['passenger_country'][$t] = $this->getBusCountryCode($post_data['country'][$t]);
													$booking_data['passenger_gender'][$t] = strtolower($post_data['gender'][$t]);
													$booking_data['government_id'][$t] = isset($post_data['government_id'][$t]) ? $post_data['government_id'][$t] :'';
													$booking_data['marital_status'][$t] = isset($post_data['marital_status'][$t]) ? $post_data['marital_status'][$t] :'';
													$booking_data['profession'][$t] = isset($post_data['passenger_contact_no'][$t]) ? $post_data['passenger_contact_no'][$t] :'';
													$booking_data['meal'][$t] = isset($post_data['meal'][$t]) ? $post_data['meal'][$t] :'';	
																																						
										}

										if(isset($post_data['child_first_name'])){
	                                        for ($m=0; $m < count($post_data['child_first_name']); $m++) {                                          
	                                            $booking_data['passenger_first_name'][$t] = isset($post_data['child_first_name'][$m]) ? $post_data['child_first_name'][$m] :'';
												$booking_data['passenger_last_name'][$t] = isset($post_data['child_last_name'][$m]) ? $post_data['child_last_name'][$m] :'';
	                                            $booking_data['passenger_dob'][$t] = date('Y-m-d', strtotime($post_data['child_dob'][$m]));
												$booking_data['passenger_type'][$t] = $this->getBusPassengerType($post_data['child_dob'][$m],$data['leg'][$i]['to_date']);
												$booking_data['passenger_contact_no'][$t] = isset($post_data['child_phone'][$m]) ? $post_data['child_phone'][$m] :'';
												$booking_data['passenger_country'][$t] = $this->getBusCountryCode($post_data['child_country'][$m]);
												$booking_data['passenger_gender'][$t] = strtolower($post_data['child_gender'][$m]);
												$booking_data['government_id'][$t] = isset($post_data['child_government_id'][$m]) ? $post_data['child_government_id'][$m] :'';						
												$booking_data['meal'][$t] = isset($post_data['child_meal'][$m]) ? $post_data['child_meal'][$m] :'';													
												$booking_data['profession'][$t] = isset($post_data['child_phone'][$m]) ? $post_data['child_phone'][$m] :'';
											
												$t++;
	                                        }
										}	

									//echo '<pre>'; print_r($booking_data);exit;

                                        $headers = [
                                            'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
                                            'Origin' => url(''),
                                        ];
										$response = http('post', 'bookBusBud', $booking_data, $headers);
										//echo '<pre>'; print_r($response);exit;

                                        if ($response['result'] == 'Yes') {		
													
											$cityName=$data['leg'][$i]['from_city_name'];
											$booking_id=$response['data']['purchase_id'];											
											$tansportBusbudId[$booking_id]=$cityName;	
											
											$data['leg'][$i]['transport'][0]['booking_id'] = $response['data']['purchase_id'];	

											$voucher_data[$i]['bookingId']=$response['data']['purchase_id'];	
											$voucher_data[$i]['cityName']=$data['leg'][$i]['from_city_name'];

                                            $data['leg'][$i]['transport'][0]['provider_booking_status'] = 'CONFIRMED';
                                            $data['leg'][$i]['transport'][0]['booking_error_code'] = '';
                                            $data['leg'][$i]['transport'][0]['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
                                        } else {									
                                            $data['leg'][$i]['transport'][0]['provider_booking_status'] = 'NOT CONFIRMED';
                                            $data['leg'][$i]['transport'][0]['booking_error_code'] = $response['data']['details'];
                                            $data['leg'][$i]['transport'][0]['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
                                        }
                                    }
									if (isset($itinerary_data['provider']) && $itinerary_data['provider'] == 'eroam') {
							          	$booking_id = (int)self::generateRandomString();
							          	$tansportId[$booking_id] = $data['leg'][$i]['from_city_name'];
							          	$this->transportVoucher($itinerary,$data,$request->input(),$booking_id);
							        }
                               
                                } else {
                                    $data['leg'][$i]['transport'][0]['provider'] = '';
                                    
                                }

                                if (isset($itinerary_data['supplier']['id'])) {
                                    $data['leg'][$i]['transport'][0]['supplier_id'] = $itinerary_data['supplier']['id'];
                                } else {
                                    $data['leg'][$i]['transport'][0]['supplier_id'] = '';
                                }
                            } else {
                                $data['leg'][$i]['transport'] = [];
                            }

                        }

                    }
                }

                $i++;
			}
			
		# Start busbudVoucher
		#Create BusBudTransport Voucher and store at S3 Bucket Aws
		
		if(!empty($voucher_data)){
			for($j=0;$j<count($voucher_data);$j++){
				$bookingId = $voucher_data[$j]['bookingId'];
				$cityName = $voucher_data[$j]['cityName'];
				if($bookingId!="" && $cityName!=""){
				$this->transportBusbudVoucher($cityName,$bookingId);	
				}	
			}
		}											
		#End busbudVoucher

            //$post_data = $request->input();
            $data['totalAmount'] = $post_data['totalAmount'];
            $data['passenger_info'] = [];
            for ($i = 0; $i < count($post_data['passenger_first_name']); $i++) {
                $data['passenger_info'][$i]['passenger_first_name'] = $post_data['passenger_first_name'][$i];
                $data['passenger_info'][$i]['passenger_last_name'] = $post_data['passenger_last_name'][$i];
                $data['passenger_info'][$i]['passenger_dob'] = $post_data['passenger_dob'][$i];
                $data['passenger_info'][$i]['passenger_contact_no'] = $post_data['passenger_contact_no'][$i];
                $data['passenger_info'][$i]['passenger_email'] = $post_data['passenger_email'][$i];
            }
 
            $data['billing_info']['passenger_first_name'] = $post_data['billing_first_name'];
            $data['billing_info']['passenger_last_name'] = $post_data['billing_last_name'];
            $data['billing_info']['passenger_dob'] = $post_data['billing_passenger_dob'];
            $data['billing_info']['passenger_email'] = $post_data['billing_email'];
            $data['billing_info']['passenger_zip'] = $post_data['passenger_zip'];
            $data['billing_info']['passenger_contact_no'] = $post_data['billing_contact'];
            $data['billing_info']['passenger_state'] = $post_data['passenger_state'];
            $data['billing_info']['passenger_suburb'] = $post_data['passenger_suburb'];
            $data['billing_info']['passenger_address_one'] = $post_data['passenger_address_one'];
            $data['billing_info']['passenger_address_two'] = $post_data['passenger_address_two'];
            $data['billing_info']['passenger_company_name'] = $post_data['passenger_company_name'];
            $data['billing_info']['passenger_country'] = $this->getCountryNameByCode($post_data['country'][0]);


            $headers = [
                'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
                'Origin' => url(''),
            ];
            
            //echo '<pre>'; print_r(json_encode($data)); die;
            $response = http('post', 'saveOrderDetail', $data, $headers);
            $data['finalTotalCost'] = session()->get('finalTotalCost');
			$data['gst'] = session()->get('gst');
			$data['tax'] = session()->get('tax');
			$data['totalPaidAmount'] = $data['finalTotalCost'] + $data['gst'] + $data['tax'];           
            $customerEmail = $data['passenger_info'][0]['passenger_email'];           
            session()->put('orderData', $data);
            //echo "<pre>";print_r($data);exit;
            Mail::send(['html' => 'mail.book'], $data, function ($message) use ($customerEmail, $invoiceNumber, $itineraryId, $activityId, $tansportId,$aEroamHotel,$aMystiflyTransport,$tansportBusbudId) {


                $message->to($customerEmail)->subject('Booking summary for Invoice number - ' . $invoiceNumber);
                $message->from('res@eroam.com','eRoam');

                if(isset($activityId))
            	{
            		foreach ($activityId as $key => $value) {
	            		if(isset($value) && $value != ''){
	            			if(file_exists(public_path('uploads').'/activity/'.$value.'_ActivityVoucher_'.$key.'.pdf'))
	            			{
		            			$message->attach(public_path('uploads').'/activity/'.$value.'_ActivityVoucher_'.$key.'.pdf');
		            		}
		            		else
		            		{
		            			$filePath = 'vouchers/activity/'.$value.'_ActivityVoucher_'.$key.'.pdf';
	            				$file = \Storage::disk('s3')->get($filePath); 
	            				if($file)
	            				{
	            					$message->attachData($file, $value.'_ActivityVoucher_'.$key.'.pdf');
	            				}
		            		}
		            	}
	            	}
            	}
                if(isset($tansportId))
                {
                    foreach ($tansportId as $key => $value) {
                        if(isset($value) && $value != ''){
                                $filePath = 'vouchers/transport/'.$value.'_TransportVoucher_'.$key.'.pdf';
                                $file = \Storage::disk('s3')->get($filePath);
                                if($file)
                                {
                                    $message->attachData($file, $value.'_TransportVoucher_'.$key.'.pdf');
                                }
                        }
                    }
				}
				//print_r($tansportBusbudId); exit;
				if(isset($tansportBusbudId)){
					foreach ($tansportBusbudId as $key => $value) {
                        if(isset($value) && $value != ''){
								$filePath = 'vouchers/transport/'.$value.'_BusbudTransportVoucher_'.$key.'.pdf';
								if(isset($filePath)){								
								$file = \Storage::disk('s3')->get($filePath);								
								if($file)
                                {
                                    $message->attachData($file, $value.'_BusbudTransportVoucher_'.$key.'.pdf');
								}
							}
                        }
                    }

				}
                if(isset($aMystiflyTransport))
                {
                    foreach ($aMystiflyTransport as $key => $value) {
                        if(isset($value) && $value != ''){

                                $filePath = 'vouchers/transport/'.$value;
                                $file = \Storage::disk('s3')->get($filePath);
                                if($file)
                                {
                                    $message->attachData($file, $value);
                                }
                        }
                    }
                }

                if(isset($itineraryId) && count($itineraryId)){
	            	foreach ($itineraryId as $key => $value) {
	            		if(isset($value) && $value != ''){

	            			$filePath = 'vouchers/hotels/'.$key.'_HotelVoucher_'.$value.'.pdf';
							$file = \Storage::disk('s3')->get($filePath); 
							if($file)
							{
								$message->attachData($file, $key.'_HotelVoucher_'.$value.'.pdf');
							}
		            		//$message->attach(public_path('uploads').'/'.$key.'_HotelVoucher_'.$value.'.pdf');
		            	}
	            	}
            	}

        		if(isset($aEroamHotel) && count($aEroamHotel)){

            		foreach ($aEroamHotel as $key => $value) {

	            		if(isset($value) && $value != ''){
	            			$filePath = 'vouchers/hotels/'.$value;
							$file = \Storage::disk('s3')->get($filePath); 
							if($file)
							{
								$message->attachData($file, $value);
							}
		            	}
	            	}
            	}

            	
            });
            session()->put('billing_data', $data);
            return redirect('/payment/');
            
        } else {
        	return back()->with('error', 'Your Payment is declined. Please try once again.');
        }
        exit;

	}

	private function getCountryCode($country_id) {
		$countries = Cache::get('countries');
		$return = 'AU';
		if (isset($countries) && !empty($countries)) {
			foreach ($countries as $key => $value) {
				if (isset($value['countries']) && !empty($value['countries'])) {
					foreach ($value['countries'] as $key1 => $value1) {
						if ($value1['id'] == $country_id) {
							$return = $value1['code'];
						}
					}
				}
			}

		}
		return $return;
	}

	private function getBusCountryCode($country_id)
	{
		$countries = Cache::get('countries');
		$return = 'AUS';
		if (isset($countries) && !empty($countries)) {
			foreach ($countries as $key => $value) {
				if (isset($value['countries']) && !empty($value['countries'])) {
					foreach ($value['countries'] as $key1 => $value1) {
						if ($value1['id'] == $country_id) {
							$return = $value1['iso_3_letter_code'];
						}
					}
				}
			}

		}
		return $return;
	}
	
	private function getCountryName($country_id) {
		$countries = Cache::get('countries');
		$return = 'AU';
		if (isset($countries) && !empty($countries)) {
			foreach ($countries as $key => $value) {
				if (isset($value['countries']) && !empty($value['countries'])) {
					foreach ($value['countries'] as $key1 => $value1) {
						if ($value1['id'] == $country_id) {
							$return = $value1['name'];
						}
					}
				}
			}

		}
		return $return;
	}

	private function getCountryNameByCode($country_code) {
		$countries = Cache::get('countries');
		$return = 'AU';
		if (isset($countries) && !empty($countries)) {
			foreach ($countries as $key => $value) {
				if (isset($value['countries']) && !empty($value['countries'])) {
					foreach ($value['countries'] as $key1 => $value1) {
						if ($value1['code'] == $country_code) {
							$return = $value1['name'];
						}
					}
				}
			}

		}
		return $return;
	}

	public function order() {
        $data = session()->get('billing_data');
        return view(
            'itinenary.partials.success', ['return_response' => $data]
        );
    }
	
	public function ordersuccess() {
		return view('pages.success');
	}
	
	public function view_order_pdf(Request $request) {

		$post_data = $request->input();
		$return_response = json_decode($post_data['return_response'], true);
		$pdf = PDF::loadView('pages.order_pdf', compact('return_response'));
		return $pdf->stream();
	}
	
	public function tourDetails($city) {

		if ($city == 'bhutan') {
			return view('tours.bhutan');
		} elseif ($city == 2) {
			return view('tours.sydney');
		} elseif ($city == 3) {
			return view('tours.3');
		} elseif ($city == 4) {
			return view('tours.4');
		} elseif ($city == 5) {
			return view('tours.5');
		} elseif ($city == 6) {
			return view('tours.6');
		} elseif ($city == 7) {
			return view('tours.7');
		} elseif ($city == 8) {
			return view('tours.8');
		}

	}

	public function roomDetails() {
	  $search = request()->input();

	  $response = $this->expediaApi->hotelInfo(request()->input('hotel'));
	  //$response = json_decode($response, true);
	  $thumbnailUrl = '';
	  if (isset($response['data']['HotelInformationResponse']['HotelImages']['HotelImage'][0]['thumbnailUrl'])) {
	   $thumbnailUrl = $response['data']['HotelInformationResponse']['HotelImages']['HotelImage'][0]['thumbnailUrl'];
	  }



	  $search_session = json_decode(json_encode(session()->get('search')), FALSE);
	  //echo '<pre>'; print_r($search_session); 
	  //echo request()->input('leg').'<pre>'; print_r($search_session->itinerary[request()->input('leg')]->hotelInfo); die;
	  $hotelInfo = $search_session->itinerary[request()->input('leg')]->hotelInfo;
	  $search_session = $search_session->itinerary[request()->input('leg')]->hotel;
	  

	  $provider = 'expedia';
	  
	  if($search_session != '' || $hotelInfo != ''){
		  //$provider = isset($search_session->provider)? $search_session->provider : $hotelInfo->provider;
		  $customerSessionId = isset($search_session->customerSessionId)? $search_session->customerSessionId : (isset($hotelInfo->customerSessionId)? $hotelInfo->customerSessionId : '');
		  $cacheKey = isset($search_session->cacheKey)? $search_session->cacheKey : (isset($hotelInfo->cacheKey)? $hotelInfo->cacheKey : '');
		  $cacheLocation = isset($search_session->cacheLocation)? $search_session->cacheLocation : (isset($hotelInfo->cacheLocation)? $hotelInfo->cacheLocation : '');

		  $nights = isset($search_session->nights)? $search_session->nights : $hotelInfo->nights;
		  $checkin = isset($search_session->checkin)? $search_session->checkin : $hotelInfo->checkin;
		  $checkout = isset($search_session->checkout)? $search_session->checkout : $hotelInfo->checkout;
	  } else {
	  		$search_session = json_decode(json_encode(session()->get('search')), true);
	  		$search_session = $search_session['itinerary'][request()->input('leg')];

	  		//$provider = 'expedia';
	  		$customerSessionId = '';
	  		$cacheKey = '';
	  		$cacheLocation = '';
	  		$nights = $search_session['city']['default_nights']; 
	        $checkin =  $search_session['city']['date_from'];
	        $checkout = $search_session['city']['date_to'];
	  }

	  $response['data']['HotelInformationResponse']['HotelSummary']['provider'] = $provider; //$search_session->provider;
	  $response['data']['HotelInformationResponse']['HotelSummary']['customerSessionId'] = $customerSessionId; //$search_session->customerSessionId;
	  $response['data']['HotelInformationResponse']['HotelSummary']['cacheKey'] = $cacheKey; //$search_session->cacheKey;
	  $response['data']['HotelInformationResponse']['HotelSummary']['cacheLocation'] = $cacheLocation; //$search_session->cacheLocation;
	  $response['data']['HotelInformationResponse']['HotelSummary']['nights'] = $nights; //$search_session->nights;
	  $response['data']['HotelInformationResponse']['HotelSummary']['checkin'] = $checkin; //$search_session->checkin;
	  $response['data']['HotelInformationResponse']['HotelSummary']['checkout'] = $checkout; //$search_session->checkout;
	  $response['data']['HotelInformationResponse']['HotelSummary']['thumbNailUrl'] = $thumbnailUrl;
	  $response['data']['HotelInformationResponse']['HotelSummary']['bedTypeId'] = request()->input('bedtype');
	  $response['data']['HotelInformationResponse']['HotelSummary']['RoomRateDetailsList']['RoomRateDetails'] = request()->input('room');
	  $response = $response['data']['HotelInformationResponse']['HotelSummary'];
	  echo json_encode($response);exit;
 	}
 	public function eroamRoomDetails() {
 		//echo '<pre>'; print_r(session()->get('search_input')); die;
 		$data['num_of_adults'] = session()->get('search_input')['num_of_adults'];
 		$data['num_of_children'] = session()->get('search_input')['num_of_children'];
 		$data['rooms'] = session()->get('search_input')['rooms'];

	  $response = http('post', 'eroam/hotel/' . request()->input('hotel'), $data);
	  $roomInfo = request()->input('room');
	  $default_hotel_room = array();
	  $default_hotel_room['hotel_price_id'] = $roomInfo['hotel_price_id']; 
	  $default_hotel_room['hotel_room_type_id'] = $roomInfo['hotel_room_type_id'];
	  $default_hotel_room['room_type_id'] = $roomInfo['room_type_id'];
	  $default_hotel_room['season'] = $roomInfo['season'];
	  $default_hotel_room['name'] = $roomInfo['name'];
	  $default_hotel_room['price'] = $roomInfo['price'];
	  $response['provider'] = 'eroam';
	  $response['default_hotel_room'] = $default_hotel_room;
	  echo json_encode($response);exit;
 	}
	public function view_itinerary() {

        $data = json_decode(json_encode(session()->get('search')), true);
        // echo "<pre>";print_r($data);exit;
        // /pr(current($data['itinerary'])['city']);
        $totalCost = str_replace(',', '', $data['cost_per_person']);
        $travellers = $data['travellers'];
        $currency = $data['currency'];
        $rooms = $data['rooms'];
        $totalDays = str_replace(' Nights', '', $data['total_number_of_days']);
        $countries = Cache::get('countries');
        $startDate = date('j M Y', strtotime(session()->get('search')['itinerary'][0]['city']['date_from']));
        $endDate = date('j M Y', strtotime(session()->get('search')['itinerary'][count(session()->get('search')['itinerary']) - 1]['city']['date_to']));
        $adults = array_sum(session()->get('search_input')['num_of_adults']);
        $children = array_sum(session()->get('search_input')['num_of_children']);
				
        // echo "<pre>";print_r(session()->get('search_input')); exit();
        // $adults = $data['itinerary'][0]['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['numberOfAdults'];
        // $children = $data['itinerary'][0]['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['numberOfChildren'];
        if (session()->has('search')) {
            $itinerary = json_decode(json_encode(session()->get('search')['itinerary']), false);
            $leg_detail = session()->get('search');
            if (session()->get('search_input') == null) {
                return redirect('/');
            }

            //$search = session()->get('search');
            $page_will_expire = 1;
            $alternative_routes = array();
            return view('itinenary.partials.view-itinerary')->with(compact('data','page_will_expire','alternative_routes', 'itinerary', 'totalCost', 'travellers', 'currency', 'countries', 'startDate', 'endDate', 'totalDays','rooms', 'adults', 'children'));
        } else {
            return redirect('/');
        }
    }

    public function book_tour_signin_guest_checkout(Request $request)
	{
		$post_data 		= $request->input();
		$child_price 	= "";
		$infant_price 	= "";

		if(isset($post_data['childPrice']))
		{
			$child_price = $post_data['childPrice'];
		}
		if(isset($post_data['infantPrice']))
		{
			$infant_price= $post_data['infantPrice'];
		}

		
		$end 	= Carbon::parse($post_data['finishDate']);
		$now 	= Carbon::parse($post_data['start_date']);
		$days 	= $end->diffInDays($now);

		$payError = 0; 
		if(Session::has('error'))
		{
		  	$payError = 1;
		}
		$is_DateRadioIdValue = 0;

		if(Session::has('DateRadioIdValue'))
		{ 
		  	$is_DateRadioIdValue = Session::get('DateRadioIdValue');
		}

		$data 	= array(
				'start_date' 	=> $post_data['start_date'],
				'finishDate' 	=> $post_data['finishDate'],
				'singlePrice' 	=> $post_data['singlePrice'],
				'twinPrice' 	=> $post_data['price'],
				'tour_name' 	=> $post_data['tour_name'],
				'tour_id' 		=> $post_data['tour_id'],
				'start_date' 	=> $post_data['start_date'],
				'no_of_days' 	=> $post_data['no_of_days'],
				'durationType' 	=> $post_data['durationType'],
				'code' 			=> $post_data['code'],
				'provider'		=> $post_data['provider'],
				'from'			=> $post_data['from'],
				'childPrice'	=> $post_data['childPrice'],
				'infantPrice'	=> $post_data['infantPrice'],
				'countries'		=> $post_data['countries'],
				'days' 			=> $days,
				'payError' 		=> $payError,
				'is_DateRadioIdValue' => $is_DateRadioIdValue,
				'default_currency'  => 'AUD'
			);
		
		if (isset($post_data['detailPricing'])) {
			$data['detailPricing'] = json_decode($post_data['detailPricing'],true);
		}
		
		$first_name         = "";
		$last_name          = "";
		$email              = "";
		$contact_no         = "";
		$bill_country       = "";
		$bill_address_1     = "";
		$bill_address_2     = "";
		$user_id            = "";

		session()->put('tour_book_user',$data);

		if (session()->get('user_auth')['id']):			
			return redirect('book/tour/');
		else:			
			return view('pages.signin_guest_checkout_tour')->with(compact('data'));
		endif;
	}

	public function tourBookingForm()
	{

		$data = session()->get('tour_book_user');
		$user = [];
		$all_countries = get_all_countries();
		if(session()->get('user_auth')['id'])		
		{		
			$id = session()->get('user_auth')['id'];
			$user = http('get', 'user/get-by-id/'.$id, [], $this->headers);
			$user = json_decode(json_encode($user), false);			
		}

		$days               = $data['days'];
		
		$first_name         = "";
		$last_name          = "";
		$email              = "";
		$contact_no         = "";
		$bill_country       = "";
		$bill_address_1     = "";
		$bill_address_2     = "";
		$user_id            = "";

		if(session()->get('user_auth')['id'])
		{
			$first_name         = $user->customer->first_name;
			$last_name          = $user->customer->last_name;
			$email              = $user->customer->email;
			$contact_no         = $user->customer->contact_no;
			$bill_address_2     = $user->customer->bill_address_1;
			$bill_address_1     = $user->customer->bill_address_2;
			$bill_country       = $user->customer->bill_country;
			$user_id            = $user->id;
		}

		$data_arr 	= [			
				'first_name' 	=> $first_name,
				'last_name' 	=> $last_name,
				'email' 		=> $email,
				'contact_no' 	=> $contact_no,
				'bill_address_1'=> $bill_address_1,
				'bill_address_2'=> $bill_address_2,
				'bill_country' 	=> $bill_country,
				'user_id' 		=> $user_id,
			];		
		$payError = 0; 
		if(Session::has('error'))
		{
		  	$payError = 1;
		}
		$is_DateRadioIdValue = 0;

		if(session()->has('DateRadioIdValue'))
		{ 
		  	$is_DateRadioIdValue = session()->get('DateRadioIdValue');
		}

		return view('pages.tour_booking')->with(compact('data','user','all_countries','data_arr','payError','is_DateRadioIdValue','days'));
	}

	public function sass_to_domain_css($colors,$fonts,$domain_name){
		// Set Custome Sass file code into css file //

		$sass_var = $this->getVariables($colors,$fonts);
		
		$temp_str = '';
		foreach($sass_var as $key=>$val){
			$temp_str .= '$'.$key.':'.$val;
		}
		FunctionsHelper::compileTheme($temp_str,$domain_name);
		return true;
	}

	public function getFrontend() {
		$data=file_get_contents("php://input");
		$data = stripslashes($data);
		$decoded = json_decode($data, true);
		
		$result = $this->sass_to_domain_css($decoded['colors'],$decoded['fonts'],$decoded['domain_name']);
		if($result) {
			return response()->json(['success'=>'true']);
		}
	}

	public function activityVoucher($provider = null,$voucherUrl,$bookingId = null,$city = null, $country_code = null, $actData = null, $pass_info = null)
	{
		if($provider == 'viator')
		{
			$filePath = 'vouchers/activity/'.$city.'_ActivityVoucher_'.$bookingId.'.pdf';

			$file_url = str_replace('service/merchant/voucher.jspa?', 'ticket/download-pdf?merchant=true&', $voucherUrl);
			$pdf = file_get_contents($file_url);
			\Storage::disk('s3')->put($filePath, $pdf, 'public');				
		}
		if($provider == 'eroam')
		{
			$data = array();
			$data['bookingId'] = $bookingId;
			$data['city'] = $city;
			$data['country_code'] = $country_code;

            $filePath = 'vouchers/activity/'.$city.'_ActivityVoucher_'.$bookingId.'.pdf';
			if(!empty($actData))
			{
				$pdf = PDF::loadView('activity.activity_voucher', compact('data','actData','pass_info'));
				\Storage::disk('s3')->put($filePath, $pdf->output(), 'public');
			}
            //$pdf->save($filePath);
		}		
	}

	public function generateRandomString($length = 11) {
	    return substr(str_shuffle(str_repeat($x='0123456789', ceil($length/strlen($x)) )),1,$length);
	}

    private function sortPrice($a,$b) 
  	{
    	return ($a["price"] <= $b["price"]) ? -1 : 1;
  	}
  	private function getDefaultRoomKey($rooms, $room_id) {
	    foreach($rooms as $key => $room) {
	        if($room['id'] == $room_id){
	        	return 'Yes'.$key;
	        } 
	    }
    	return 'NO';
	}

	public function hotelVoucher($aData,$aHotel,$aRequestData,$leg)
	{
		$city = $aHotel['from_city_name'];
		$aHotel = $aHotel['hotel'][0];
		$bookingId = $aData['invoiceNumber'];		
		$filePath = 'vouchers/hotels/'.$city.'_HotelVoucher_'.$bookingId.'.pdf'; 		
		$pdf = PDF::loadView('accomodation.eroam_hotel_voucher', compact('aData','aHotel','aRequestData','leg','city'));
		$file = \Storage::disk('s3')->put($filePath,$pdf->output()); 
	}
	public function mystiflyVoucher($city='mumbai',$bookingId='123',$detailArray='')
	{
		
		$filename = $city.'_TransportVoucher_'.$bookingId.'.pdf';
		//return view('transport.mystifly_voucher')->with(compact('detailArray'));
		$filePath = 'vouchers/transport/'.$filename;
		$pdf = PDF::loadView('transport.mystifly_voucher', compact('detailArray','filename'));
		//return $pdf->stream();
		$file = \Storage::disk('s3')->put($filePath,$pdf->output(),'public');
		return $filename;
	}

	public function transportVoucher($itinerary,$data,$post_data,$booking_id)
    {
        $booking_data['passenger_info'] = [];
        for ($i = 0; $i < count($post_data['passenger_first_name']); $i++) {
            $booking_data['passenger_info'][$i]['passenger_first_name'] = $post_data['passenger_first_name'][$i];
            $booking_data['passenger_info'][$i]['passenger_last_name'] = $post_data['passenger_last_name'][$i];
            $booking_data['passenger_info'][$i]['passenger_dob'] = $post_data['passenger_dob'][$i];
            $booking_data['passenger_info'][$i]['passenger_contact_no'] = $post_data['passenger_contact_no'][$i];
            $booking_data['passenger_info'][$i]['passenger_email'] = $post_data['passenger_email'][$i];
        }

        $arrayData =  $itinerary;
        $arrayData1 = $data;
        $itineryData=[];
        $passenger_name = $post_data['passenger_first_name'][0].' '.$post_data['passenger_last_name'][0];
        $passenger_email = $post_data['passenger_email'][0];

        //$passenger_type="ADT";
        $cityName = $itinerary['transport']['from_city']['name'];
		$itineryData['price']=$itinerary['transport']['price'][0]['price'];
        $itineryData['from_city']=$itinerary['transport']['from_city']['name'];
        $itineryData['to_city']=$itinerary['transport']['to_city']['name'];
        $itineryData['currency']=$itinerary['transport']['currency']['code'];
        $itineryData['transportType']=$itinerary['transport']['transporttype']['name'];
        $itineryData['invoiceNumber']=$arrayData1['invoiceNumber'];
        $itineryData['total_amount']=$arrayData1['total_amount'];
        $itineryData['booking_date']=$arrayData1['travel_date'];
        $itineryData['duration']=str_replace('+','',$arrayData['transport']['duration']);
        $itineryData['passenger_name']=$passenger_name;
        $itineryData['number_of_passenger']=$i;
        //$itineryData['passenger_type']=$passenger_type;
        $itineryData['arrival_text']=str_replace('<br/>',' ',$arrayData['transport']['arrival_text']);
        $itineryData['departure_text']=str_replace('<br/>',' ',$arrayData['transport']['departure_text']);

        $pdf = PDF::loadView('itinenary.transport_voucher',['itineryData'=>$itineryData,'booking_data'=>$booking_data]);
        $pdf->setPaper('A4', 'landscape');
        // $filePath =public_path('transport').'/'.'TransportVoucher_'.$arrayData1['invoiceNumber'].'.pdf';
        // $pdf->save($filePath);
        $filePath = 'vouchers/transport/' .$cityName.'_TransportVoucher_'.$booking_id.'.pdf';
        \Storage::disk('s3')->put($filePath, $pdf->output(),'public');
        //$imageNamePath = \Storage::disk('s3')->url($filePath);
	}
	
	public function transportBusbudVoucher($cityName,$bookingId)
	{		
			$ticket=$this->ticketPollCall($cityName,$bookingId);
			
			if(isset($ticket) && $ticket!=""){
				$filePath='vouchers/transport/'.$cityName.'_BusbudTransportVoucher_'.$bookingId.'.pdf';
				\Storage::disk('s3')->put($filePath, file_get_contents($ticket),'public');	
				return 'success';			
			}
			
	}

	public function ticketPollCall($cityName,$bookingId){
		//echo $cityName .''. $bookingId; exit;
		$baseUrl  = 'https://napi-preview.busbud.com/';
		$header = array('X-Busbud-Token: PARTNER_GjUXjuHSRjitXRG7icgj-w','version:2');
        $url = $baseUrl.'purchases/'.$bookingId;		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_URL, $url); 
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($curl);
		curl_close($curl);
		$data = json_decode($result);
		foreach($data->tickets as $key => $value){
				if(isset($key)){
				$ticket=$data->tickets->$key->pdf_url;
				if(!isset($ticket) && $ticket==""){
					return $this->ticketPollCall($cityName,$bookingId);
					}
					if(isset($ticket) && $ticket!=""){
						return $ticket;
					}		
			} 						
		}
	}

    private function getPassengerType($date,$travelDate)
    {
    	//calc
     	$dob = date('d-m-Y',strtotime($date));
     	if(isAdult($dob,$travelDate))
        {
            return 'ADT';
        }
    	if(isChild($dob,$travelDate))
    	{
        	return 'CHD';
    	}
	    if(isInfant($dob,$travelDate))
	    {
	        return 'INF';
	    }	
	}
	private function getBusPassengerType($date,$travelDate)
    {
    	//calc
     	$dob = date('d-m-Y',strtotime($date));
     	if(isAdult($dob,$travelDate))
        {
            return 'adult';
        }
    	if(isChild($dob,$travelDate))
    	{
        	return 'child';
    	}
	    if(isInfant($dob,$travelDate))
	    {
	        return 'child';
	    }	
	}

}


								  			