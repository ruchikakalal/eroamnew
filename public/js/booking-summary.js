
/**
 * Booking summary updater script
 */

var bookingSummary = ( function(  ) {

	function update( searchSession, initialLoad = false, callback ) {

		var url = ( initialLoad ) ? 'update-booking-summary' : 'save-booking';
		
		eroam.ajax( 'post',url, { search: searchSession }, function( response ) {
			setTimeout( function() {
				var searchResult = response.search("itsworkingnow");
				if (searchResult > 0) {
					$( '.booking-summary' ).html( response );
					
				}else{
					var url = $('#site-url').val();
					// window.location.href = url;
				}
				customReady();
				var page = $('#itinerary-page').val();

				if (page == 'hotels' || page == 'activities' || page == 'transports' || page == 'hotel_detail') {
					var leg = $('#itinerary-leg').val();
					$("#tinerary-panel-leg-"+leg).children('.collapse_click').click();
				}

				if (callback) {
					callback();
				}
					// $('#trip_details').trigger('click');
					// $('#trip_details').find('.ic-up_down').addClass('ic-down_up').removeClass('ic-up_down');
					// $('.trip_summary_first').eq(0).find('.ic-up_down').addClass('ic-down_up').removeClass('ic-up_down');
			}, 200 );
		}, function() {
			$( '.booking-summary' ).html( '<span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> Updating Data...</span>' );	
		} );
	}

	function replaceOwnArrangementsWithApis( searchSession ){
		
	}

	function showMiniLoader(element) {
		element.html(
			'<div class="la-ball-pulse" style="color: #2AA9DF">' +
				'<div></div>' +
				'<div></div>' +
				'<div></div>' +
			'</div>'
		);
		element.closest('.itinerary-leg-link').attr('data-loading', 'yes').css('cursor', 'wait');
	}

	function hideMiniLoader(element) {
		// console.log( "hide element", element);
		element.closest('.itinerary-leg-link').attr('data-loading', '').css('cursor', 'pointer');
	}

	function tempUpdate( searchSession, key , position ) {
		var leg = key;
		var pos = position;
		var url = 'save-booking';

		eroam.ajax( 'post',url, { search: searchSession }, function( response ) {

			setTimeout( function() {
				var searchResult = response.search("itsworkingnow");

				if (searchResult > 0) {
					$( '.booking-summary' ).html( response );
				}else{
					var url = $('#site-url').val();
					window.location.href = url;
				}
				
				var page = $('#itinerary-page').val();
				if(!page){
					page = 'map';
				}
			

				$('.itinerary-leg-container .panel-heading a').addClass('collapsed');
				$('.itinerary-leg-container .panel-heading#heading-'+leg+' a').removeClass('collapsed');

				// OPEN BODY
				$('.itinerary-leg-container .panel-collapse').removeClass('in');
				$('.itinerary-leg-container #collapse-'+ leg).addClass('in');

				// SCROLL ON SELECTED
			
				$('.itinerary-container').scrollTop(pos);

				$('.itinerary-leg-container[data-index="'+leg+'"] .itinerary-leg-link[data-type="'+page+'"]').addClass('active');
				
			}, 200 );

		} );
	}

	return {
		tempUpdate : tempUpdate,
		update : update,
		showMiniLoader : showMiniLoader,
		hideMiniLoader : hideMiniLoader,
	};

} )( jQuery );

$( function() {
	bookingSummary.update();
} );


