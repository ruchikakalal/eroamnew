
var HBController = new function() {



	function checkAvailability( hotel, callbackSuccess, callbackFailure )
	{
		var hbRQ = {
			code : hotel.code
		};
		var hbApiCall = eroam.apiDeferred('hb/checkavaialability', 'GET', hbRQ, 'rawResponse', true );
		eroam.apiPromiseHandler( hbApiCall, function( rs ){
			( rs.success ) ? callbackSuccess( rs.data ) : callbackFailure( rs.error );
		});
	}


	function checkPrice( hotel, callbackSuccess, callbackFailure )
	{
		
	}


	return {
		checkAvailability : checkAvailability,
		checkPrice : checkPrice
	};
	
}

