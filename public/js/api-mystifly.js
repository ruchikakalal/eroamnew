
var MystiflyController = new function() {



	function revalidate( faresourceCode, callbackSuccess, callbackFailure )
	{
		var mystiflyRQ = { FareSourceCode : faresourceCode };
		var mystiflyApiCall = eroam.apiDeferred('mystifly/revalidate', 'GET', mystiflyRQ, 'rawResponse', true );
		eroam.apiPromiseHandler( mystiflyApiCall, function( rs ){
			( rs.success ) ? callbackSuccess( rs.data ) : callbackFailure( rs.error );
		});
	}


	function flightRules( faresourceCode, callbackSuccess, callbackFailure )
	{
		var mystiflyRQ = { FareSourceCode : faresourceCode };
		var mystiflyApiCall = eroam.apiDeferred('mystifly/fare-rules', 'GET', mystiflyRQ, 'rawResponse', true );
		eroam.apiPromiseHandler( mystiflyApiCall, function( rs ){
			( rs.success ) ? callbackSuccess( rs.data ) : callbackFailure( rs.error );
		});
	}


	function book( data, callbackSuccess, callbackFailure )
	{
		var fareType = data.flightData.fareType;

		var mystiflyRQ = {
			FareSourceCode : data.faresourceCode,
			TravelerInfo : {
				AirTravelers : {
					AirTraveler : []
				}
			}
		};

		data.pax_information.forEach(function( pax ){
			var passport = {};
			var traveler = {
				PassengerType : '',
				PassengerNationality : '',
				DateOfBirth : '',
				Gender : '',
				PassengerName : {
					PassengerTitle : '',
					PassengerFirstName : '',
					PassengerLastName: ''
				},
				Passport : passport,
				FrequentFlyerNumber : null,
				SpecialServiceRequest : {
					SeatPreference : 'Any',
					MealPreference : 'Any',
				},
				ExtraServices1_1 : {
					Services : {
						ExtraServiceId : []
					}
				}
			};
			mystiflyRQ.TravelerInfo.AirTravelers.AirTraveler.push();
		});

		var mystiflyApiCall = eroam.apiDeferred('mystifly/flight/book', 'POST', mystiflyRQ, 'rawResponse', true );
		eroam.apiPromiseHandler( mystiflyApiCall, function( rs ){
			if( rs.success )
			{
				if( data.fareType == 'WebFare' )
				{
					callbackSuccess( rs.data )
				}
				else
				{
					if( rs.data.Status == 'PENDING' )
					{
						orderTicket( rs.data.UniqueID, callbackSuccess, callbackFailure );
					}
					else
					{
						callbackSuccess( rs.data );
					}
				}
			}
			else
			{
				callbackFailure( rs.error )
			}
		});
	}


	function orderTicket( uniqueId, callbackSuccess, callbackFailure )
	{
		var mystiflyRQ = { UniqueID : uniqueId };
		var mystiflyApiCall = eroam.apiDeferred('mystifly/ticket/order', 'POST', mystiflyRQ, 'rawResponse', true );
		eroam.apiPromiseHandler( mystiflyApiCall, function( rs ){
			( rs.success ) ? callbackSuccess( rs.data ) : callbackFailure( rs.error );
		});
	}


	function tripDetails( uniqueId, callbackSuccess, callbackFailure ){
		var mystiflyRQ = { UniqueID : uniqueId };
		var mystiflyApiCall = eroam.apiDeferred('mystifly/trip-details', 'GET', mystiflyRQ, 'rawResponse', true );
		eroam.apiPromiseHandler( mystiflyApiCall, function( rs ){
			( rs.success ) ? callbackSuccess( rs.data ) : callbackFailure( rs.error );
		});
	}



	return {
		revalidate : revalidate,
		flightRules : flightRules,
		book : book,
		tripDetails : tripDetails
	};
	
}

