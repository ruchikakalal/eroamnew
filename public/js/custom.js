 $(document).ready(function () {
    $('#sign_in_button').click(function(){
       $('#signupmodal').modal('hide');
       $('#forgotModal').modal('hide');
       $("#loginmodal").modal('show');        
   });

   $("#sign_up_button").click(function(){ 
       $("#loginmodal").modal('hide');
       $('#forgotModal').modal('hide');
       $('#signupmodal').modal('show');        
   });

   $("#forgot_password_button").click(function(){
       $("#loginmodal").modal('hide');
       $('#signupmodal').modal('hide');
       $('#forgotModal').modal('show');
   });

     $("#auto").prop('checked',true);
     // $("#profile-tab").addClass('active');
     // $("#createitinerary").addClass('show active');
     
    $('#manual').click(function(e) {
		if($(this).prop('checked') == true) {
			$('#start_location').val("");
			$('#end_location').val("");
			$('#auto').prop('checked',false);
			$("#departing").addClass('d-none');

            $("#destination .lable_title").html('<i class="ic-place"></i>'+LocationLabel);
            $("#destination").removeClass('col-sm-6 col-md-6 col-lg-6 col-xl-6').addClass('col-sm-12 col-md-12 col-lg-12 col-xl-12');

		}else{
			$('#start_location').val("");
			$('#end_location').val("");
			$('#manual').prop('checked',true);
		} 
    });
	$('#auto').click(function(e) {
		if($(this).prop('checked') == true) {
			$('#manual').prop('checked',false);
			$('#start_location').val("");
			$('#end_location').val("");
			$("#departing").removeClass('d-none');

            $("#destination .lable_title").html('&nbsp;');
            $("#destination").removeClass('col-sm-12 col-md-12 col-lg-12 col-xl-12').addClass('col-sm-6 col-md-6 col-lg-6 col-xl-6');

		} else {
			$('#start_location').val("");
			$('#end_location').val("");
			$('#auto').prop('checked',true);
		}
	});

	// search tab change
	 $('#profile-tab').click(function(){
		$("#search-form2 label.error").hide();
		//$('#search-form2')[0].reset();
	 });

	 $('#home-tab').click(function(){
		$("#search-form label.error").hide();
		//$('#search-form')[0].reset();
		$('#start_location').val('');
		$('#end_location').val('');
	 });
     
     // Modal popup
     $("body").on("click", "#modalpopup", function () {
         $("#myModal").modal("show");
         // $(".blue").addClass("after_modal_appended");
         //appending modal background inside the blue div
         // $('.modal-backdrop').appendTo('.blue');
     });

     $("body").on("click", "#profile_popup", function () {
         $("#profileModal").modal("show");
         // $(".blue").addClass("after_modal_appended");
         //appending modal background inside the blue div
         // $('.modal-backdrop').appendTo('.blue');
     });

     $("body").on("click", "#multiday-tour", function () {
         $("#multidaytour").modal("show");
         // $(".blue").addClass("after_modal_appended");
         //appending modal background inside the blue div
         // $('.modal-backdrop').appendTo('.blue');
     });

     $("body").on("click", "#login-modal", function () {
         $("#loginmodal").modal("show");
         // $(".blue").addClass("after_modal_appended");
         //appending modal background inside the blue div
         // $('.modal-backdrop').appendTo('.blue');
     });

     $("body").on("click", "#signup-modal", function () {
         $("#signupmodal").modal("show");
         // $(".blue").addClass("after_modal_appended");
         //appending modal background inside the blue div
         // $('.modal-backdrop').appendTo('.blue');
     });

     if ($(window).width() < 580) {
         $(".regular").slick({
             dots: false,
             prevArrow: false,
             nextArrow: false,
             infinite: false,
             slidesToShow: 1,
             slidesToScroll: 1,
             centerMode: true,
             variableWidth: true
         });

         $(".tabtour a").click(function () {
             $('li a').not(this).removeClass('active show');
             $(this).addClass('active show');
             var tabshow = $(this).attr("href");
             $('div').not(tabshow).removeClass('active show');
             $(tabshow).addClass('active show');
         })
     }
     
     // Map left sec hide show
     $('.click_map_hide').click(function(){
         $(this).toggleClass('active');
         $('.itinerary_left').toggleClass('active');
         $('.itinerary_right').toggleClass('active');
     });
     
     // Collapse click
     $('.collapse_click').click(function(){
         if($(this).parent('.itinerary_left_box').hasClass('itinerary_left_active')){
             $(this).parent('.itinerary_left_box').removeClass('itinerary_left_active');
             $(this).find('.ic-down_up').addClass('ic-up_down').removeClass('ic-down_up');
         }else{
             $(this).parent('.itinerary_left_box').addClass('itinerary_left_active');
             $(this).find('.ic-up_down').addClass('ic-down_up').removeClass('ic-up_down');
         }
         
     });
     
     // Itinerary filter padding
 });

 // Index page javascript
 callStyle();

 function callStyle() {
     //$('.bottom_chat').hide();
     $('body').css('min-height', $(window).outerHeight());
     $('body').css('padding-top', $('header').outerHeight());
     $('body').css('padding-bottom', $('footer').outerHeight());
     $('.body_sec').css('min-height', $(window).outerHeight() - ($('header').outerHeight() + $('footer').outerHeight()));

     // Map height
     $('.itinerary_block').css('min-height', $(window).outerHeight() - ($('header').outerHeight() + $('footer').outerHeight()));

     if ($(window).width() > 991) {
         // Itinerary height
         var windowhight = $(window).outerHeight();
         var headerhight = parseInt($('header').outerHeight());
         var footerhight = parseInt($('footer').outerHeight());
         var filterhight = parseInt($('.itinerary_filter').outerHeight());
         var leftsidebarHeight = parseInt($('.left_sidebar').outerHeight());
         $('.itinerary_page').css('height', leftsidebarHeight - (headerhight + filterhight));
     } else {
         $('.itinerary_page').css('height', 'auto');
     }

     // itinerary_right padding
     $('.itinerary_right').css('padding-left', $('.right_form_click_block').outerWidth() + 0);
     $('.account-block').css('min-height', $(window).outerHeight() - ($('header').outerHeight() + $('footer').outerHeight() + $('.blue.modal_outer_man').outerHeight()));
 }

 $('.active_detiles_page a').click(function () {
     setTimeout(function () {
         $(".itinerary_page").getNiceScroll().resize();
     }, 200);
 });

 // Resize function
 $(window).resize(function () {
     callStyle();
 });

 $(".collapse").on('shown.bs.collapse', function (e) {
     callStyle();
 });
 $(".collapse").on('hidden.bs.collapse', function (e) {
     callStyle();
 });

